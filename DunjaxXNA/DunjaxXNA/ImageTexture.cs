﻿using System.Collections.Generic;
using Dunjax.Attributes;
using Dunjax.Geometry;
using Dunjax.Images.Concrete;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DunjaxXNA
{
    /**
     * A Dunjax image realized as an XNA texture.
     */
    class ImageTexture : ISystemImage
    {
        /**
         * The name of the image, excluding any path info.  These names must
         * be unique across all images, even those in different paths. 
         */
        public string ImageName { get; private set; }

        /**
         * The rectangle this texture occupies within the sprite sheet.
         */
        public Rectangle SourceRectangle { get; private set; }

        [Constructor]
        public ImageTexture(string ImageName, Rectangle SourceRectangle)
        {
            this.ImageName = ImageName;
            this.SourceRectangle = SourceRectangle;
            size.Set((short)SourceRectangle.Width, (short)SourceRectangle.Height);
        }
        
        [Implementation]
        public ISize Size { get { return size; } }
        private readonly ISize size = GeometryFactory.CreateSize();
    }

    /**
     * Plugs itself in as the creator of system-images 
     * for the game program.
     */
    class ImageTextureCreator : ISystemImageCreator
    {
        /**
         * The sprite sheet loaded by this object, which contains 
         * all the images used in this game program.
         */
        public static Texture2D SpriteSheet;

        /**
         * A mapping of each image's name (minus path info) to its
         * rectangle within the sprite sheet.
         */
        private readonly Dictionary<string, Rectangle> 
            imageNameToSpriteSheetRectangleMap;

        [Constructor]
        public ImageTextureCreator(ContentManager content)
        {
            Image.systemImageCreator = this;

            // load the sprite sheet and associated map through the content manager
            SpriteSheet = content.Load<Texture2D>("images/sprite sheet");
            imageNameToSpriteSheetRectangleMap =
                content.Load<Dictionary<string, Rectangle>>("images/sprite sheet map");       
        }

        [Implementation]
        public ISystemImage CreateSystemImage(string imageFilePath)
        {
            var imageName = GetImageNameFromPath(imageFilePath);
            return new ImageTexture(imageName,
                imageNameToSpriteSheetRectangleMap[imageName]);
        }

        [Implementation]
        public bool FileExists(string path)
        {
            return imageNameToSpriteSheetRectangleMap.ContainsKey(
                GetImageNameFromPath(path));
        }

        /**
         * Returns the image name found at the end of the given path.
         */
        private static string GetImageNameFromPath(string path)
        {
            var slashIndex = path.LastIndexOf('/');
            return (slashIndex >= 0) ? path.Substring(slashIndex + 1) : path;
        }
    }
}
