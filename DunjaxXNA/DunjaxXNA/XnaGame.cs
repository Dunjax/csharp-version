using System;
using Dunjax;
using Dunjax.Attributes;
using DunjaxXNA.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Nuclex.Input;

namespace DunjaxXNA
{
    public class XnaGame : Game
    {
        /**
         * Is the display for the game window.
         */
        protected readonly GraphicsDeviceManager graphics;
        
        /**
         * Performs the drawing for the game display.
         */
        protected SpriteBatch spriteBatch;

        /**
         * The font used to draw text within the map-view.
         */
        protected SpriteFont font;

        /**
         * The gamepad(s) (if any) used by the player to play the game.
         * On the Xbox, this will report input from any gamepad.
         */
        protected GamePad gamePad;
        
        /**
         * The Nuclex library object which allows for 
         * managing older, DirectInput gamepads.
         */
        protected InputManager input;

        /**
         * The various screens displayed in the game.
         */
        protected TitleScreen titleScreen;
        protected StoryScreen storyScreen;
        protected ActionScreen actionScreen;
        protected PauseScreen pauseScreen;

        /**
         * Whether the player has made it to the end of the first level
         * of this game during this running of the program.  If it has,
         * after the player dies it should start at the beginning of the
         * second level, rather than go back to the first.
         */
        private bool firstLevelCompleted;

        /**
         * Whether the current game being played has been completed (i.e. won).
         */
        private bool gameCompleted;

        [Constructor]
        public XnaGame()
        {
            Window.Title = "Dunjax";

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set the size of all screens in the game to a resolution which 
            // is one of the standard modes for the Xbox
            graphics.PreferredBackBufferWidth = 640; // 1280;
            graphics.PreferredBackBufferHeight = 480; // 720;

            // create the Nuclex object for managing gamepads
            input = new InputManager(Services, Window.Handle);
            Components.Add(input);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // creation of these objects connects the logical Dunjax code
            // to the system-dependent code in this project, so this  
            // creation has to occur early, before any images or sounds 
            // are loaded
            new ImageTextureCreator(Content);
            new SystemSoundCreator(Content);

            // create the game pad object
            gamePad = new GamePad(input);

            base.Initialize();

            // create the user input object, now that the player class has been
            // referenced, as the former will plug itself into the latter
            new UserInput(gamePad);

            new DebugSettings();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Font1");

            // create the title screen
            titleScreen = new TitleScreen(this, graphics, spriteBatch, font, gamePad);
            titleScreen.Exited += TitleScreenExited;

            // create the story screen
            storyScreen = new StoryScreen(this, graphics, spriteBatch, font, gamePad);
            storyScreen.Exited += StoryScreenExited;

            // create the pause screen
            pauseScreen = new PauseScreen(this, graphics, spriteBatch, font, gamePad);
            pauseScreen.Exited += PauseScreenExited;
            pauseScreen.GameQuit += GameQuit;

            CreateGame();

            base.LoadContent();
        }

        /**
         * Overridden to display the first screen to the player.
         */
        protected override void BeginRun()
        {
#if FINAL
            titleScreen.Show();
#else
            actionScreen.Show();
#endif
        }

        [Handler]
        protected void ActionScreenPaused(object sender, EventArgs args)
        {
            actionScreen.Hide();
            actionScreen.DunjaxGame.Pause();
            pauseScreen.Show();
        }

        [Handler]
        protected void PauseScreenExited(object sender, EventArgs args)
        {
            pauseScreen.Hide();
            actionScreen.Show();
            actionScreen.DunjaxGame.Resume();
        }

        [Handler]
        protected void GameQuit(object sender, EventArgs args)
        {
            pauseScreen.Hide();
            RestartGame();
        }

        [Handler]
        protected void TitleScreenExited(object sender, EventArgs args)
        {
            titleScreen.Hide();
            ShowLevelStory();
        }

        [Handler]
        protected void StoryScreenExited(object sender, EventArgs args)
        {
            storyScreen.Hide();

            // determine what to show next
            if (storyScreen.StoryPart == StoryPart.Registration) ShowLevelStory();
            else if (!gameCompleted) actionScreen.Show();
            else RestartGame();
        }

        [Handler]
        private void LevelCompleted(object sender, EventArgs args)
        {
            firstLevelCompleted = true;

#if FINAL
            // stop the current-level song's playing
            MediaPlayer.Stop();
#endif
        }

        [Handler]
        private void NextLevelReady(object sender, EventArgs args)
        {
#if XBOX
            actionScreen.Hide();
            ShowLevelStory();
#elif FINAL
            actionScreen.Hide();

            // if the program is registered, move on to the story 
            // for the next level
            if (Registration.IsRegistered()) ShowLevelStory();

            // otherwise
            else {
                // show the registration screen
                storyScreen.StoryPart = StoryPart.Registration;
                storyScreen.Show();
            }
#endif
        }

        /**
         * Displays the story for the current game's current level.
         */
        private void ShowLevelStory()
        {
            // show the story screen
            var currentLevelNumber = actionScreen.DunjaxGame.CurrentLevelNumber;
            storyScreen.StoryPart =
                (currentLevelNumber == 2) ? 
                    StoryPart.SecondLevel : StoryPart.FirstLevel;
            storyScreen.Show();

#if FINAL
            // load and play the level's song
            var song = Content.Load<Song>(
                "songs/dunjax" + currentLevelNumber);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.4f;
            MediaPlayer.Play(song);
#endif
        }

        [Handler]
        protected void GameOver(object sender, EventArgs args)
        {
#if FINAL
            // stop the current-level song's playing
            MediaPlayer.Stop();
#endif

            actionScreen.Hide();

            RestartGame();
        }

        [Handler]
        protected void GameCompleted(object sender, EventArgs args)
        {
#if FINAL
            // stop the current-level song's playing
            MediaPlayer.Stop();
#endif

            actionScreen.Hide();

            gameCompleted = true;

            // show the ending story
            storyScreen.StoryPart = StoryPart.Ending;
            storyScreen.Show();
        }

        /**
         * Creates a new action screen which contains a new game instance
         * to play.
         */
        private void CreateGame()
        {
            // create the action screen
            actionScreen = 
                new ActionScreen(this, firstLevelCompleted ? 2 : 1, 
                    graphics, spriteBatch, font, gamePad);
            actionScreen.Paused += ActionScreenPaused;
            actionScreen.DunjaxGame.GameOver += GameOver;
            actionScreen.DunjaxGame.LevelCompleted += LevelCompleted;
            actionScreen.DunjaxGame.NextLevelReady += NextLevelReady;
            actionScreen.DunjaxGame.GameCompleted += GameCompleted;
        }

        /**
         * Resets this programs per-game state, and creates and starts a new game to play.
         */
        private void RestartGame()
        {
            gameCompleted = false;

            CreateGame();

#if FINAL
            titleScreen.Show();
#else 
            actionScreen.Show();
#endif
        }
    }
}
