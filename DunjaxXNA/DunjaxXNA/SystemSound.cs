﻿using System.Collections.Generic;
using Dunjax.Attributes;
using Dunjax.Sound.Concrete;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace DunjaxXNA
{
    class SystemSound : ISystemSound
    {
        /**
         * The game-side sound object this object realizes.
         */
        private readonly Sound sound;

        /**
         * The sound effect object from which we may create instances to play.
         */
        private readonly SoundEffect soundEffect;

        /**
         * If only one of this sound may play at a time, this is that lone instance.
         */
        private readonly SoundEffectInstance soundEffectInstance;

        /**
         * If more than one of this sound may play at a time, this holds
         * the instances we've created to play as many of this sound at one time 
         * as has been demanded so far.
         */
        private readonly List<SoundEffectInstance> soundEffectInstances;

        /**
         * The volume level currently used for all sounds.
         */
        private const float DefaultVolume = 0.7f;

        [Constructor]
        public SystemSound(Sound sound, SoundEffect soundEffect)
        {
            this.sound = sound;
            this.soundEffect = soundEffect;

            // if only one of this sound should play at a time
            if (sound.onlyOneClipAtATime) {
                // create and initialize the lone instance
                soundEffectInstance = CreateSoundEffectInstance();
            }

            // otherwise
            else {
                // create a list to hold our instances of this sound
                soundEffectInstances = new List<SoundEffectInstance>();
            }
        }

        /**
         * Creates and returns a new sound-effect instance for this 
         * sound to play.
         */
        private SoundEffectInstance CreateSoundEffectInstance()
        {
            var instance = soundEffect.CreateInstance();
            instance.IsLooped = sound.loop;
            instance.Volume = DefaultVolume;
            return instance;
        }

        [Implementation]
        public void Play()
        {
            // if only one of this sound should play at a time
            if (sound.onlyOneClipAtATime) {
                // if our lone instance isn't already playing, play it
                if (soundEffectInstance.State != SoundState.Playing) 
                    soundEffectInstance.Play();
            }

            // otherwise
            else {
                // for each sound effect instance of this sound
                SoundEffectInstance playInstance = null;
                for (int i = 0; i < soundEffectInstances.Count; i++) {
                    var instance = soundEffectInstances[i];

                    // if this instance isn't already playing
                    if (instance.State != SoundState.Playing) {
                        // use this instance below
                        playInstance = instance;
                        break;
                    }
                }

                // if no such instance was found above
                if (playInstance == null) {
                    // create a new one and add it to our list
                    playInstance = CreateSoundEffectInstance();
                    soundEffectInstances.Add(playInstance);
                } 

                // play the instance found or created above
                playInstance.Play();
            }
        }

        [Implementation]
        public void Stop()
        {
            soundEffectInstance.Stop();
        }

        [Implementation]
        public bool Playing
        {
            get { return soundEffectInstance.State == SoundState.Playing; }
        }
    }

    class SystemSoundCreator : ISystemSoundCreator
    {
        private readonly ContentManager content;

        public SystemSoundCreator(ContentManager content)
        {
            this.content = content;
            Sound.SystemSoundCreator = this;
        }

        public ISystemSound CreateSystemSound(Sound sound)
        {
            var soundEffect = 
                content.Load<SoundEffect>("sounds/" + sound.fileName);
            return new SystemSound(sound, soundEffect);
        }
    }
}
