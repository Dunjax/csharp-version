using Dunjax;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Player;
using Microsoft.Xna.Framework.Input;

namespace DunjaxXNA
{
    public class UserInput : IUserInput
    {
        /**
         * The default keyboard keys used to signal commands, in the order of 
         * the commands' specification in the Commands enumeration.
         */
        private static readonly Keys[] commandKeys = {
            Keys.Left, Keys.Right, Keys.Up, Keys.Down,
            Keys.D, Keys.Space, Keys.X, Keys.C
        };

        /**
         * Properties which expose the configurable elements of the 
         * above commandKeys array to external configuration.
         */
        public static Keys JumpKey
        {
            get { return commandKeys[(int)Command.Jump]; }
            set { commandKeys[(int)Command.Jump] = value; }
        }
        public static Keys FireKey
        {
            get { return commandKeys[(int)Command.Fire]; }
            set { commandKeys[(int)Command.Fire] = value; }
        }
        public static Keys SwingKey
        {
            get { return commandKeys[(int)Command.Swing]; }
            set { commandKeys[(int)Command.Swing] = value; }
        }
        public static Keys SwitchGunsKey
        {
            get { return commandKeys[(int)Command.SwitchGuns]; }
            set { commandKeys[(int)Command.SwitchGuns] = value; }
        }

        /**
         * The gamepad employed by the user to control their actions
         * in this game program.
         */
        private readonly IGamePad gamePad;

        [Constructor]
        public UserInput(IGamePad gamePad)
        {
            this.gamePad = gamePad;

            // set this user-input as that used by all players
            PlayerFactory.UserInput = this;
        }

        [Implementation]
        public bool IsSignaled(Command command) 
        {
            return Keyboard.GetState().IsKeyDown(commandKeys[(int)command])
                || gamePad.IsSignaled(command);
        }
    }
}