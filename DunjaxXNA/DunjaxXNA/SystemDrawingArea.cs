﻿using System;
using System.Collections.Generic;
using Dunjax.Attributes;
using Dunjax.Gui;
using DunjaxColor = Dunjax.Gui.Color;
using Dunjax.Images;
using Dunjax.Images.Concrete;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Color = Microsoft.Xna.Framework.Color;

namespace DunjaxXNA
{
    class SystemDrawingArea : IDrawingArea
    {
        /**
         * Performs the physical drawing on the graphics device.
         */
        protected SpriteBatch spriteBatch;

        /**
         * The output display for the drawing.
         */
        protected GraphicsDevice graphicsDevice;

        /**
         * Monochrome textures used to draw fills as in FillRect().
         */
        protected Dictionary<Color, Texture2D> solidColorTextures = 
            new Dictionary<Color, Texture2D>();

        /**
         * The font used by this drawing area when displaying text.
         */
        protected SpriteFont font;

        [Constructor]
        public SystemDrawingArea(SpriteBatch spriteBatch, 
            GraphicsDevice graphicsDevice, SpriteFont font)
        {
            this.spriteBatch = spriteBatch;
            this.graphicsDevice = graphicsDevice;
            this.font = font;
        }

        [Implementation]
        public void DrawImage(IImage image, int x, int y)
        {
            // draw the image from its rectangle in the sprite sheet
            spriteBatch.Draw(
                ImageTextureCreator.SpriteSheet,
                new Vector2(x, y), 
                ((ImageTexture)((Image)image).image).SourceRectangle, 
                Color.White, 0, Vector2.Zero, 1,
                ((Image)image).DrawFlipped ? 
                    SpriteEffects.FlipHorizontally : SpriteEffects.None, 0);
        }

        [Implementation]
        public void FillRect(
            int x, int y, int width, int height, DunjaxColor color_)
        {
            var color = ConvertColor(color_);

            // if we don't already have a fill-texture for the given color
            if (!solidColorTextures.ContainsKey(color)) {
                // create one and add it to our collection
                var texture = new Texture2D(graphicsDevice, 1, 1);
                texture.SetData(new[] { color });
                solidColorTextures.Add(color, texture);
            }

            // fill the rectangle using our fill-texture for the given color
            spriteBatch.Draw(solidColorTextures[color], 
                new Rectangle(x, y, width, height), Color.White); 
        }

        [Implementation]
        public void DrawText(string text, int x, int y, DunjaxColor color)
        {
            spriteBatch.DrawString(
                font, text, new Vector2(x, y), ConvertColor(color));
        }

        /**
         * Returns the system color corresponding to the given logical color.
         */
        private static Color ConvertColor(DunjaxColor color)
        {
            switch (color) {
                case DunjaxColor.Black: return Color.Black;
                case DunjaxColor.White: return Color.White;
                case DunjaxColor.Yellow: return Color.Yellow;
            }

            throw new Exception("Invalid color specified");
        }
    }
}
