﻿using System;
#if !DEBUG
using System.IO;
#endif
using Dunjax;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Player.Concrete;
using Dunjax.Game;
using Dunjax.Geometry;
using DunjaxColor = Dunjax.Gui.Color;
using Dunjax.Images;
using Dunjax.Map;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DunjaxXNA.Screens
{
    /**
     * The screen which displays the game's action as it is being played.
     */
    public class ActionScreen : Screen
    {
        /**
         * The Dunjax-domain game object.
         */
        public IGame DunjaxGame { get; private set; }

        /**
         * Renders the player's current surroundings. 
         */
        protected IMapView mapView;

        /**
         * The icons for the stats displayed in the stats line.
         */
        protected IImage keyStatImage;
        protected IImage armorStatImage;
        protected IImage pulseShotStatImage;
        protected IImage grapeShotStatImage;

        /**
         * Whether the FinalRoomTriggerReached event has been previously
         * handled by this screen, meaning it no longer needs to be handled.
         */
        protected bool finalRoomTriggerReached;

        /**
         * Whether the action on this screen is currently paused.
         */
        private bool paused;

        /**
         * The y-ordinate which marks the start of where the player's
         * stats line should display.
         */ 
        private readonly int statsY;

        [Constructor]
        public ActionScreen(Game game, int startingLevelNumber,
            GraphicsDeviceManager graphics,
            SpriteBatch spriteBatch, SpriteFont font, ISystemGamePad gamePad)
            : base(game, graphics, spriteBatch, font, gamePad) 
        {
            DunjaxGame = GameFactory.CreateGame(startingLevelNumber);
            DunjaxGame.NextLevelReady += NextLevelReady;
            DunjaxGame.Map.Player.ReachedFinalRoomTrigger += ReachedFinalRoomTrigger;

            // create the map view to display the game's map
            mapView = MapFactory.CreateMapView();
            mapView.Size = GeometryFactory.CreateSize(
                (short)graphics.PreferredBackBufferWidth,
                (short)graphics.PreferredBackBufferHeight);
            mapView.Map = DunjaxGame.Map;

            // determine how far up from the bottom of the screen the player's
            // stats should display; it must be higher up on the Xbox to 
            // be within its title-safe area
            statsY = graphics.PreferredBackBufferHeight - font.LineSpacing
#if XBOX
                * 2;
#else
                ;
#endif

            // load the stats images
            keyStatImage = ImageFactory.CreateImage("entities/items/key1");
            armorStatImage = ImageFactory.CreateImage("armorStat");
            pulseShotStatImage = ImageFactory.CreateImage("entities/items/pulseAmmoBig1");
            grapeShotStatImage = ImageFactory.CreateImage("entities/items/grapeshotGun3");
        }

        /**
         * Is called by XNA for this screen to execute a turn in the game.
         */
        public override void Update(GameTime gameTime)
        {
            ReadInputState();

            if (IsPauseSignaled()) {
#if FINAL
                MediaPlayer.Pause();
#endif
                paused = true;

                Paused(this, null);
            }

            SetOldInputState();

            try {
                DunjaxGame.DoTurn();
            } catch (Exception e) {
#if DEBUG
                throw e;
#elif !XBOX
                // if we aren't debugging, we don't want any exception
                // thrown during the processing of the current game 
                // turn to cause the game to suddenly end, so just log it,
                // instead (except on the Xbox, where we can't write to the disk)
                Log.Write(e.ToString());
#endif
            }

            CheckForLabelEntitiesCommand();

            base.Update(gameTime);
        }

        /**
         * Is fired when the user signals that the game should be paused.
         */
        public event EventHandler Paused;

        /**
         * Is called by XNA to draw the current view of the game's action, 
         * as well as the player's onscreen stats.
         */
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // don't do the draw if the game or map or player doesn't exist yet
            if (DunjaxGame == null) return;
            var map = DunjaxGame.Map;
            if (map == null) return;
            var player = map.Player;
            if (player == null) return;

            // draw the map with a clip rectangle which excludes the stats line
            spriteBatch.Begin(SpriteSortMode.Deferred, null);
            mapView.Draw(drawingArea);
            spriteBatch.End();

            // draw the player's stats
            spriteBatch.Begin();
            DrawStats();
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /**
         * Checks whether the command is being given to label/unlabel entities 
         * within the map-view with their IDs, and if so, carries out that command.
         */
        protected void CheckForLabelEntitiesCommand()
        {
            var state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.RightControl) && state.IsKeyDown(Keys.RightAlt)
                 && state.IsKeyDown(Keys.L)) {
                mapView.LabelEntities = true;
            }
            if (state.IsKeyDown(Keys.RightControl) && state.IsKeyDown(Keys.RightAlt)
                 && state.IsKeyDown(Keys.OemSemicolon)) {
                mapView.LabelEntities = false;
            }
        }

        /**
         * Draws stats about the player and its items, beneath the map-view display.
         */
        protected void DrawStats()
        {
            var player = DunjaxGame.Map.Player;
            var windowWidth = graphics.PreferredBackBufferWidth;

            // draw the armor stat
#if XBOX
            var armorX = 10;
#else
            var armorX = 0;
#endif
            drawingArea.DrawImage(armorStatImage,
                armorX, statsY + font.LineSpacing / 2 - armorStatImage.Size.Height / 2);
            drawingArea.DrawText(player.ArmorLeft.ToString(),
                armorX + armorStatImage.Size.Width + 1, statsY, DunjaxColor.White);

            // draw the number of keys stat
            var keyX = windowWidth / 4 + armorX;
            drawingArea.DrawImage(keyStatImage,
                keyX, statsY + font.LineSpacing / 2 - keyStatImage.Size.Height / 2);
            drawingArea.DrawText(player.KeysHeld.ToString(),
                keyX + keyStatImage.Size.Width + 1, statsY, DunjaxColor.White);

            // draw the ammo stat
            var ammoX = windowWidth / 2 + armorX;
            var ammo = player.AmmoInUse;
            var ammoStatImage = (ammo is Ammo.Pulse) ?
                pulseShotStatImage : grapeShotStatImage;
            drawingArea.DrawImage(ammoStatImage,
                ammoX, statsY + font.LineSpacing / 2 - ammoStatImage.Size.Height / 2);
            drawingArea.DrawText(ammo.Amount.ToString(),
                ammoX + ammoStatImage.Size.Width + 1, statsY, DunjaxColor.White);

#if WINDOWS
            // draw the pause instruction
            var pauseMessage = "Enter=help";
            spriteBatch.DrawString(
                font, pauseMessage,
                new Vector2(
                    graphics.PreferredBackBufferWidth 
                      - (int)font.MeasureString(pauseMessage).X - 5, 
                    statsY), 
                Color.DimGray);
#endif
        }

        [Handler]
        private void NextLevelReady(object sender, EventArgs args)
        {
            // have the map-view switch to drawing the next level's map
            mapView.Map = DunjaxGame.Map;
        }

        [Handler]
        private void ReachedFinalRoomTrigger(object sender, EventArgs args)
        {
            // if we've handled this event before, there is nothing 
            // to do this time
            if (finalRoomTriggerReached) return;

#if FINAL
            // stop the current-level song's playing
            MediaPlayer.Stop();

            // play the song which accompanies the master
            var song = Game.Content.Load<Song>("songs/dunjax3");
            MediaPlayer.Play(song);
#endif

            // remember that we've now handled this event once
            finalRoomTriggerReached = true;
        }

        [HookOverride]
        protected override void Shown()
        {
            // if we are returning from the pause screen
            if (paused) {
                // we are no longer paused
                paused = false;
#if FINAL
                // resume playing the current level's song
                MediaPlayer.Resume();
#endif
            }
        }
    }
}
