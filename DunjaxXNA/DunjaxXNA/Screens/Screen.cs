﻿using Dunjax.Attributes;
using Dunjax.Gui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
using Nuclex.Input.Devices;

namespace DunjaxXNA.Screens
{
    /**
     * The base class for the various screens shown in the game.
     */
    public class Screen : DrawableGameComponent
    {
        /**
         * Provides info about this screen, such as its size.
         */
        protected GraphicsDeviceManager graphics;
    
        /**
         * The object used to perform the physical drawing operations on this screen.
         */
        protected SpriteBatch spriteBatch;

        /**
         * The font used to display text on this screen.
         */
        protected SpriteFont font;

        /**
         * The gamepad(s) (if any) used by the player to play the game.
         * On the Xbox, this will report input from any gamepad.
         */
        protected ISystemGamePad gamePad;

        /**
         * The current state of which keyboard keys are down, along with 
         * the previous such state, to be compared against each other to 
         * determine which keys have been pressed.
         */
        protected KeyboardState keyboardState;
        protected KeyboardState oldKeyboardState;
        protected bool oldKeyboardStateSet;

        /**
         * The current state of which XBOX/PC gamepad buttons are pressed, along with 
         * the previous such state, to be compared against each other to 
         * determine which buttons have been pressed and then released.
         */
        protected GamePadState gamePadState;
        protected GamePadState oldGamePadState;
        protected bool oldGamePadStateSet;

        /**
         * The current state of which PC gamepad buttons are pressed, along with 
         * the previous such state, to be compared against each other to 
         * determine which buttons have been pressed and then released.  
         * We need this state in addition to the regular gamepad state above,
         * because the PC gamepad buttons aren't mapped sensibly by Nuclex 
         * to the XBOX button values.
         */
        protected ExtendedGamePadState extendedGamePadState;
        protected ExtendedGamePadState oldExtendedGamePadState;
        protected bool oldExtendedGamePadStateSet;

        /**
         * Abstraction-wraps the graphics and spriteBatch objects
         * to be a graphics interface usable by the Dunjax domain.
         */
        protected IDrawingArea drawingArea;

        [Constructor]
        public Screen(Game game, GraphicsDeviceManager graphics,
            SpriteBatch spriteBatch, SpriteFont font, ISystemGamePad gamePad)
            : base(game)
        {
            this.graphics = graphics;
            this.spriteBatch = spriteBatch;
            this.font = font;
            this.gamePad = gamePad;

            Initialize();

            drawingArea = new SystemDrawingArea(spriteBatch, GraphicsDevice, font);

            Hide();

            game.Components.Add(this);
        }

        /**
         * Makes this screen visible and accepting of input.
         */
        public void Show()
        {
            // ignore any state saved from the last time this screen was shown
            oldKeyboardStateSet = false;
            oldGamePadStateSet = false;
            oldExtendedGamePadStateSet = false;

            Enabled = true;
            Visible = true;

            Shown();
        }

        [Hook]
        protected virtual void Shown() {}

        /**
         * Hides this screen and makes it no longer listen for input.
         */
        public void Hide()
        {
            Enabled = false;
            Visible = false;
        }

        /**
         * Reads and stores the current state of all relevant input devices.
         */
        protected void ReadInputState()
        {
            keyboardState = Keyboard.GetState();

            gamePadState = gamePad.GamePadState;
#if !XBOX
            extendedGamePadState = gamePad.ExtendedGamePadState;
#endif
        }

        /**
         * Brings our old input state variables current, so that the current
         * values will be the old ones the next time the input devices are polled.
         */
        protected void SetOldInputState()
        {
            oldKeyboardState = keyboardState;
            oldKeyboardStateSet = true;

            oldGamePadState = gamePadState;
            oldGamePadStateSet = true;

            oldExtendedGamePadState = extendedGamePadState;
            oldExtendedGamePadStateSet = true;
        }

        /**
         * Checks the input-devices state to see if a pause command was issued.
         */
        protected bool IsPauseSignaled()
        {
            return WasKeyPressed(Keys.Enter) || WasStartButtonPressed();
        }

        /**
         * Returns whether the start button was pressed on either 
         * the XBOX or PC gamepad.
         */
        protected bool WasStartButtonPressed()
        {
            return 
#if XBOX
                WasButtonPressed(Buttons.Start);
#else
                WasExtendedButtonPressed(9);
#endif
        }

        /**
         * Returns whether the exit button was pressed on either 
         * the XBOX or PC gamepad.
         */
        protected bool WasExitButtonPressed()
        {
            return
#if XBOX
                WasButtonPressed(Buttons.B);
#else
                // there is no exit button for the Windows gamepad
                false;
#endif
        }

        /**
         * Returns whether the given XBOX or PC-directional gamepad button 
         * was pressed and released just now.
         */
        protected bool WasButtonPressed(Buttons button)
        {
            return gamePadState.IsButtonDown(button)
                && oldGamePadStateSet && oldGamePadState.IsButtonUp(button);
        }

        /**
         * Returns whether the given PC gamepad button was pressed and released 
         * just now.
         */
        protected bool WasExtendedButtonPressed(int button)
        {
            return extendedGamePadState.IsButtonDown(button)
                && oldExtendedGamePadStateSet 
                && oldExtendedGamePadState.IsButtonUp(button);
        }

#if XBOX
        /**
         * The bitfield values for the A,B,X,Y buttons on the Xbox controller.
         */   
        private static readonly int[] commandButtons = new[] {
            (int)Buttons.A, (int)Buttons.B, (int)Buttons.X, (int)Buttons.Y
        };
#endif

        /**
         * Returns which of the four main XBOX (A,B,X,Y) or PC (1,2,3,4) 
         * non-directional buttons was pressed and released just now, or 
         * null if none were.
         */
        protected int? GetCommandButtonPressed()
        {
            for (int i = 0; i < 4; i++) {
#if XBOX
                if (WasButtonPressed((Buttons)commandButtons[i])) 
#else
                if (WasExtendedButtonPressed(i))
#endif
                    return i;
            }

            return null;
        }

        /**
         * Returns which keyboard key was pressed and released just now, or
         * null if none were.
         */
        protected Keys? GetCommandKeyPressed()
        {
            var keys = Keyboard.GetState().GetPressedKeys();
            if (keys.Length == 0 
                || !oldKeyboardStateSet 
                || oldKeyboardState.IsKeyDown(keys[0])) return null;
            return keys[0];
        }

        /**
         * Returns whether the given keyboard key was pressed and released just now.
         */
        protected bool WasKeyPressed(Keys key)
        {
            return keyboardState.IsKeyUp(key)  
                && oldKeyboardStateSet && oldKeyboardState.IsKeyDown(key);
        }

        /**
         * Returns whether an up-command was just signaled on any input device.
         */
        protected bool IsUpSignaled()
        {
            return WasKeyPressed(Keys.Up)
                || WasButtonPressed(Buttons.DPadUp)
                || WasButtonPressed(Buttons.LeftThumbstickUp);
        }

        /**
         * Returns whether a down-command was just signaled on any input device.
         */
        protected bool IsDownSignaled()
        {
            return WasKeyPressed(Keys.Down)
                || WasButtonPressed(Buttons.DPadDown)
                || WasButtonPressed(Buttons.LeftThumbstickDown);
        }
    }
}
