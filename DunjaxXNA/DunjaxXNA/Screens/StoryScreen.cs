﻿using System;
using Dunjax.Attributes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DunjaxXNA.Screens
{
    public enum StoryPart
    {
        FirstLevel, SecondLevel, Registration, Ending
    }

    public class StoryScreen : Screen
    {
        /**
         * Which part of the overall story this screen is currently displaying.
         */
        public StoryPart StoryPart {get; set;}

        private readonly string[] firstLevelStory = 
        {
	        "Level 1:  After the Thieves",
	        "",
	        "Your spaceship having crash-landed on an unexplored",
	        "planet, you awake from unconsciousness to find your",
	        "vessel ransacked by beings who have since left the",
	        "area. Missing is a critical element of the ship's",
	        "power core without which you cannot achieve lift-off.",
	        "Donning your power armor, you follow a large group",
	        "of unidentifiable footprints to a cave entrance set",
	        "into the base of a nearby mountainside.",
	        "",
	        "With limited supplies you cannot survive on this",
	        "planet for long. You must reclaim your power core",
	        "at any cost.  Into the cave you go!",
        };

        private readonly string[] secondLevelStory = 
        {
	        "Level 2:  What Dark Thing Lurks Here?",
	        "",
	        "It would appear the task before you is much more",
	        "difficult than you had first guessed. Who or what",
	        "built this massive network of caves?  Who created",
	        "these horrid animations that seek your life at",
	        "every turn? And more importantly, will your armor",
	        "and weapons hold out against them until you can",
	        "find your power core and escape?",
	        "",
	        "The exit from the first cave leads you down a long",
	        "passageway which opens into yet another vast",
	        "underground expanse...",
        };

        private readonly string[] endingStory = 
        {
	        "Their master defeated, the remaining monsters",
            "surrendered your ship's missing part to you,",
            "intent on you leaving their caves.",
	        "",
            "With your ship repaired, you were able to lift",
            "off from the planet and resume your journey.",
	        "",
            "Congratulations!",
	        "",
	        "We hope you enjoyed this game.",
	        "",
	        "Thanks for playing.",
        };

        private readonly string[] registrationText = 
        {
	        "You have made it to the halfway point in",
            "this game!",
	        "",
	        "To continue, you must register your copy",
	        "of Dunjax at http://dunjax.com.",
	        "Registration costs only $3 US.",
            "Please help support independent game development.",
        };

#if !XBOX        
        /**
         * The reason presented to the user why they may not 
         * currently progress from the registration screen.
         */
        private string whyNotRegisteredMessage;
#endif

        [Constructor]
        public StoryScreen(Game game, GraphicsDeviceManager graphics,
            SpriteBatch spriteBatch, SpriteFont font, ISystemGamePad gamePad)
            : base(game, graphics, spriteBatch, font, gamePad)
        {
        }

        /**
         * Is fired when the user exits this screen.
         */
        public event EventHandler Exited;

        /**
         * Checks whether the user wants to proceed from this screen.
         */
        public override void Update(GameTime gameTime)
        {
            ReadInputState();

            // if an exit from this screen is signaled 
            if (WasKeyPressed(Keys.Space) || WasStartButtonPressed()) {
#if !XBOX
                // if this is the registration screen
                if (StoryPart == StoryPart.Registration) {
                    // only allow the exit if the program has been registered
                    if (Registration.IsRegistered(out whyNotRegisteredMessage)) 
                        Exited(this, null);
                }

                // otherwise
                else {
                    // exit this screen
                    Exited(this, null);
                }
#else
                // exit this screen
                Exited(this, null);
#endif
            }

            SetOldInputState();

            base.Update(gameTime);
        }

        /**
         * Is called by XNA to draw this screen's display.
         */
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // detm which story text to display
            string[] storyLines = null;
            switch (StoryPart) {
                case StoryPart.FirstLevel:
                    storyLines = firstLevelStory;
                    break;
                case StoryPart.SecondLevel:
                    storyLines = secondLevelStory;
                    break;
                case StoryPart.Registration:
                    storyLines = registrationText;
                    break;
                case StoryPart.Ending:
                    storyLines = endingStory;
                    break;
            }

            // for each line in the story text
            var y = 30;
            foreach (var line in storyLines) {
                // draw this line
                spriteBatch.DrawString(font, line,
                    new Vector2(30, y),
                    Color.Gray);
                y += font.LineSpacing;
            }

            // draw the prompt string
            string promptEnd = "to begin";
            switch (StoryPart) {
                case StoryPart.Registration : promptEnd = "when registered"; break;
                case StoryPart.Ending: promptEnd = "to return"; break;
            }
            spriteBatch.DrawString(font,
#if XBOX
                "Press Start " 
#else
                "Press Start or Space " 
#endif
                + promptEnd,
                new Vector2(30, graphics.PreferredBackBufferHeight - 50),
                Color.White);

#if !XBOX
            // if this is the registration screen
            if (StoryPart == StoryPart.Registration) {
                // draw the product ID display
                spriteBatch.DrawString(font,
                    "Your product ID number is " + Registration.ProductId,
                    new Vector2(30, graphics.PreferredBackBufferHeight / 2f),
                    Color.LightGoldenrodYellow);

                // if there is a why-not-registered message
                if (whyNotRegisteredMessage != null) {
                    // draw it
                    spriteBatch.DrawString(font,
                        whyNotRegisteredMessage,
                        new Vector2(30, graphics.PreferredBackBufferHeight - 80),
                        Color.Red);
                }
            }
#endif 

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
