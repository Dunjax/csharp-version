﻿using System;
using Dunjax.Attributes;
using Dunjax.Images;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DunjaxXNA.Screens
{
    public class TitleScreen : Screen
    {
        private readonly IImage logo;

        private readonly string[] notices = 
        {
            "Version 2.0.0",
            "",
            "Copyright 1991-2011 Jeff Mather",
#if !XBOX
            "Visit http://dunjax.com for updates"
#endif
        };

        private readonly string[] creditsRoles = 
        {
	        "Programming, Game Concept",
            "Game Design, Level Design",
            "Graphics",
            "Music",
            "Additional Level Design",
            "Logo",
        };

        private readonly string[] creditsNames = 
        {
	        "Jeff Mather",
            "David Niecikowski",
            "Gavin Corneveaux",
            "Ed Niecikowski",
            "George Corneveaux",
            "Howard Kistler",
        };

        [Constructor]
        public TitleScreen(Game game, GraphicsDeviceManager graphics,
            SpriteBatch spriteBatch, SpriteFont font, ISystemGamePad gamePad)
            : base(game, graphics, spriteBatch, font, gamePad)
        {
            logo = ImageFactory.CreateImage("logo");
        }

        /**
         * Is fired when the user exits this screen.
         */
        public event EventHandler Exited;

        /**
         * Checks whether the user wants to proceed from this screen.
         */
        public override void Update(GameTime gameTime)
        {
            ReadInputState();

            // if the user has signaled they want to play
            if (WasKeyPressed(Keys.Space) || WasStartButtonPressed()) {
                // exit this screen
                Exited(this, null);
            }

            // if the user has signaled they want to exit, do so
            if (WasKeyPressed(Keys.Escape) || WasExitButtonPressed())
                Game.Exit();

            SetOldInputState();

            base.Update(gameTime);
        }

        /**
         * Is called by XNA to draw this screen's display.
         */
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            var centerX = graphics.PreferredBackBufferWidth / 2;

            spriteBatch.Begin();

            // draw the logo
            drawingArea.DrawImage(logo, centerX - logo.Size.Width / 2, 40);

            // for each line in the notices text
            var y = 160;
            foreach (var line in notices) {
                // draw this line
                spriteBatch.DrawString(font, line,
                    new Vector2(centerX - font.MeasureString(line).X / 2, y),
                    Color.Gray);
                y += font.LineSpacing;
            }

            // for each line in the credits-roles text
            var creditsCenterX = centerX + 40;
            const int creditsStartY = 270;
            y = creditsStartY;
            foreach (var line in creditsRoles) {
                // draw this line
                spriteBatch.DrawString(font, line,
                    new Vector2(creditsCenterX - 20 - font.MeasureString(line).X, y),
                    Color.Gray);
                y += font.LineSpacing;
            }

            // for each line in the credits-names text
            y = creditsStartY;
            foreach (var line in creditsNames) {
                // draw this line
                spriteBatch.DrawString(font, line,
                    new Vector2(creditsCenterX + 20, y),
                    Color.WhiteSmoke);
                y += font.LineSpacing;
            }

            // draw the prompt to begin
#if XBOX
            var promptString = "Press Start to play, B to exit";
#else
            var promptString = "Press Start or Space to play, Esc to exit";
#endif 
            spriteBatch.DrawString(font,
                promptString,
                new Vector2(
                    centerX - font.MeasureString(promptString).X / 2, 
                    graphics.PreferredBackBufferHeight - 50),
                Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
