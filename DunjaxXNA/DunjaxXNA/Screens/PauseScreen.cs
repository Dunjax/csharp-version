﻿using System;
using System.IO;
using System.Xml.Serialization;
using Dunjax.Attributes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DunjaxXNA.Screens
{
    /**
     * The screen displayed when the user pauses the game. 
     * Includes a menu with options to change the command keys and buttons.
     */
    public class PauseScreen : Screen
    {
        /**
         * Whether this screen is in the mode of letting the user alter
         * the gamepad buttons used to signal commands.
         */
        private bool changingButtons;

        /**
         * Whether this screen is in the mode of letting the user alter
         * the keyboard keys used to signal commands.
         */
        private bool changingKeys;

        /**
         * The choices displayed in this screen's menu.
         */
        private readonly string[] choices = 
            new [] {
#if !XBOX
                "- - -", 
                "Change command buttons",
                "Change command keys",
#endif
                "- - -", 
                "Quit game"
            };

        /**
         * The index of the currently selected choice in this screen's menu.
         */
        private int selectedChoice;

        /**
         * The names of the commands for which buttons and keys can be 
         * altered within this screen.
         */
        private readonly string[] commandsToChange = 
            new [] {"Jump", "Fire", "Swing sword", "Switch guns"};

        /**
         * The index of the command within the above commands for which
         * the user is currently modifying the button or key used to signal it.
         */
        private int currentCommandToChange;

        /**
         * The name the file used to hold the settings for which buttons
         * and keys signal the configurable commands.
         */
        private static readonly string settingsFilename = "settings.xml";

        [Constructor]
        static PauseScreen()
        {
            // load the settings from the settings file
            try { LoadSettings(); } 
            catch (IOException) {Console.WriteLine("Could not load settings file.");}
        }

        [Constructor]
        public PauseScreen(Game game, GraphicsDeviceManager graphics,
            SpriteBatch spriteBatch, SpriteFont font, ISystemGamePad gamePad)
            : base(game, graphics, spriteBatch, font, gamePad)
        {
        }

        /**
         * Is called by XNA to have this screen update what it is going to display
         * to the user.
         */
        public override void Update(GameTime gameTime)
        {
            ReadInputState();

            // update this screen according the current mode it is in
            if (changingButtons) UpdateWhenChangingButtons();
            else if (changingKeys) UpdateWhenChangingKeys();
            else UpdateMainScreen();

            SetOldInputState();

            base.Update(gameTime);
        }

        /**
         * Updates the user's navigation and selection within this screen's
         * menu, and also checks whether the user wants to exit from this screen.
         */
        private void UpdateMainScreen()
        {
            // if an exit from this screen is signaled 
            if (WasKeyPressed(Keys.Space) || WasStartButtonPressed()) {
                // exit this screen
                Exited(this, null);
                return;
            }

            // if an up-command is signaled
            if (IsUpSignaled()) {
                // move one choice up the menu
                selectedChoice--;
                if (selectedChoice < 0) selectedChoice = choices.Length - 1;
            }

            // else, if a down-command is signaled
            else if (IsDownSignaled()) {
                // move one choice down the menu
                selectedChoice++;
                if (selectedChoice >= choices.Length) selectedChoice = 0;
            }

            // if a menu-choice-selection command was issued
            var commandButtonPressed = GetCommandButtonPressed();
            if (commandButtonPressed != null || WasKeyPressed(Keys.Enter)) {
                // if the change-buttons choice is selected
                if (choices[selectedChoice].Contains("buttons")) {
                    // put this screen in the mode of changing command buttons
                    currentCommandToChange = 0;
                    changingButtons = true;
                }

                // if the change-key choice is selected
                if (choices[selectedChoice].Contains("keys")) {
                    // put this screen in the mode of changing command keys
                    currentCommandToChange = 0;
                    changingKeys = true;
                }

                // if the quit-game choice is selected
                if (choices[selectedChoice].Contains("Quit")) {
                    // signal this to listeners
                    GameQuit(this, null);
                }
            }
        }

        /**
         * Updates this screen when the user is changing command buttons.
         */
        private void UpdateWhenChangingButtons()
        {
            // if a button was pressed for the command being changed
            var commandButtonPressed = GetCommandButtonPressed();
            if (commandButtonPressed != null) {
                // set the new button for that command,
                // and move on to the next command
                var toChange = currentCommandToChange++;
#if XBOX
                var button = (Buttons)commandButtonPressed;
#else
                var button = (int)commandButtonPressed;
#endif
                if (toChange == 0) GamePad.JumpButton = button;
                if (toChange == 1) GamePad.FireButton = button;
                if (toChange == 2) GamePad.SwingButton = button;
                if (toChange == 3) GamePad.SwitchGunsButton = button;
                
                // if this was the last command to be changed
                if (currentCommandToChange >= commandsToChange.Length) {
                    // save the new settings, and remove this screen from the mode
                    // of changing command buttons
                    SaveSettings();
                    changingButtons = false;
                }
            }
        }

        /**
         * Updates this screen when the user is changing command keys.
         */
        private void UpdateWhenChangingKeys()
        {
            // if a key was pressed for the command being changed
            var commandKeyPressed = GetCommandKeyPressed();
            if (commandKeyPressed != null) {
                // set the new key for that command,
                // and move on to the next command
                var toChange = currentCommandToChange++;
                var key = (Keys)commandKeyPressed;
                if (toChange == 0) UserInput.JumpKey = key;
                if (toChange == 1) UserInput.FireKey = key;
                if (toChange == 2) UserInput.SwingKey = key;
                if (toChange == 3) UserInput.SwitchGunsKey = key;
                
                // if this was the last command to be changed
                if (currentCommandToChange >= commandsToChange.Length) {
                    // save the new settings, and remove this screen from the mode
                    // of changing command keys
                    SaveSettings();
                    changingKeys = false;
                }
            }
        }

        /**
         * Is fired when the user exits this screen.
         */
        public event EventHandler Exited;

        /**
         * Is fired when the user quits the current game from this screen.
         */
        public event EventHandler GameQuit;

        /**
         * Is called by XNA to draw this screen's display.
         */
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // draw according to what mode this screen is in
            if (changingButtons || changingKeys) DrawWhenChangingCommands();
            else DrawMainScreen();

            base.Draw(gameTime);
        }

        /**
         * Draws this screen's menu, and a help-list of the current command
         * buttons and keys.
         */
        private void DrawMainScreen()
        {
            // setup for drawing
            spriteBatch.Begin();
            var centerX = graphics.PreferredBackBufferWidth / 2;
            var startY = 50;

            // draw a "PAUSED" message
            var pausedString = "PAUSED";
            spriteBatch.DrawString(font, pausedString,
                new Vector2(
                    centerX - font.MeasureString(pausedString).X / 2, 
                    startY),
                Color.White);

            // draw a help-list of the current command buttons and keys
            var commandsString = string.Format(
                "Command:      Gamepad button:\n" +
                "Jump                 {0}\n" +
                "Fire                 {1}\n" +
                "Swing sword          {2}\n" +
                "Switch guns          {3}\n",
#if XBOX
                GamePad.JumpButton,
                GamePad.FireButton,
                GamePad.SwingButton,
                GamePad.SwitchGunsButton
#else
                GamePad.JumpButton + 1,
                GamePad.FireButton + 1,
                GamePad.SwingButton + 1,
                GamePad.SwitchGunsButton + 1
#endif
                );
            var commandsStringWidth = font.MeasureString(commandsString).X;
#if !XBOX
            var keyNames =
                new[] {
                    Enum.GetName(typeof(Keys), UserInput.JumpKey),
                    Enum.GetName(typeof(Keys), UserInput.FireKey),
                    Enum.GetName(typeof(Keys), UserInput.SwingKey),
                    Enum.GetName(typeof(Keys), UserInput.SwitchGunsKey)                
                };
            var keysString = string.Format(
                "Keyboard:\n" +
                "    {0}\n" +
                "    {1}\n" +
                "    {2}\n" +
                "    {3}\n",
                keyNames[0],
                keyNames[1],
                keyNames[2],
                keyNames[3]);
            var keysStringWidth = font.MeasureString(keysString).X;
            int keysStringPadding = 30;
#endif
            spriteBatch.DrawString(font, commandsString,
                new Vector2(
                    centerX - commandsStringWidth / 2 
#if !XBOX
                        - keysStringWidth / 2 - keysStringPadding / 2
#endif
                    , startY + 60),
                Color.Gray);
#if !XBOX
            spriteBatch.DrawString(font, keysString,
                new Vector2(
                    centerX + commandsStringWidth / 2 + 
                        keysStringPadding / 2 - keysStringWidth / 2, 
                    startY + 60), 
                Color.Gray);
#endif

            // for each menu choice
            var i = 0;
            foreach (var choice in choices) {
                // draw this menu choice, highlighted if selected
                spriteBatch.DrawString(font, choice,
                    new Vector2(
                        centerX - font.MeasureString(choice).X / 2,
                        startY + 200 + i * font.LineSpacing),
                        i == selectedChoice ? Color.Yellow : Color.White);
                i++;
            }

            // draw a message instructing how to exit this screen
#if XBOX
            var resumeString = "Press Start to resume";
#else
            var resumeString = "Press Start or Space to resume";
#endif
            spriteBatch.DrawString(font, resumeString,
                new Vector2(
                    centerX - font.MeasureString(resumeString).X / 2, 
                    startY + 340),
                Color.White);

            spriteBatch.End();
        }

        /**
         * Draws an instruction message for the current command being configured.
         */
        private void DrawWhenChangingCommands()
        {
            spriteBatch.Begin();
            var centerX = graphics.PreferredBackBufferWidth / 2;

            var pressString =
                "Press " + (changingButtons ? "button" : "key") 
                    + " for command: " + commandsToChange[currentCommandToChange];
            spriteBatch.DrawString(font, pressString,
                new Vector2(centerX - font.MeasureString(pressString).X / 2, 200),
                Color.White);

            spriteBatch.End();
        }

        [HookOverride]
        protected override void Shown()
        {
            // home the menu on its first choice
            selectedChoice = 0;
        }

        /**
         * Saves the command-buttons-and-keys settings configured within this 
         * screen to an XML file.
         */
        private static void SaveSettings()
        {
#if !XBOX
            var stream = File.OpenWrite(settingsFilename);
            var serializer = new XmlSerializer(typeof(Settings));
            serializer.Serialize(stream,
                new Settings {
                     JumpKey = UserInput.JumpKey,
                     FireKey = UserInput.FireKey,
                     SwingKey = UserInput.SwingKey,
                     SwitchGunsKey = UserInput.SwitchGunsKey,
                     JumpButton = GamePad.JumpButton,
                     FireButton = GamePad.FireButton,
                     SwingButton = GamePad.SwingButton,
                     SwitchGunsButton = GamePad.SwitchGunsButton,   
                });
            stream.Close();
#endif
        }

        /**
         * An intermediary object used to hold the game settings being saved to or
         * loading from the game's XML settings file.
         */
        public class Settings
        {
            public Keys JumpKey, FireKey, SwingKey, SwitchGunsKey;
            public int JumpButton, FireButton, SwingButton, SwitchGunsButton;
        }

        /**
         * Loads the command-buttons-and-keys settings configured within this 
         * screen from an XML file.
         */
        private static void LoadSettings()
        {
            var stream = File.OpenRead(settingsFilename);
            var serializer = new XmlSerializer(typeof(Settings));
            var settings = (Settings)serializer.Deserialize(stream);
            UserInput.JumpKey = settings.JumpKey;
            UserInput.FireKey = settings.FireKey;
            UserInput.SwingKey = settings.SwingKey;
            UserInput.SwitchGunsKey = settings.SwitchGunsKey;
#if XBOX
            GamePad.JumpButton = (Buttons)settings.JumpButton;
            GamePad.FireButton = (Buttons)settings.FireButton;
            GamePad.SwingButton = (Buttons)settings.SwingButton;
            GamePad.SwitchGunsButton = (Buttons)settings.SwitchGunsButton;
#else
            GamePad.JumpButton = settings.JumpButton;
            GamePad.FireButton = settings.FireButton;
            GamePad.SwingButton = settings.SwingButton;
            GamePad.SwitchGunsButton = settings.SwitchGunsButton;
#endif
        }
    }
}
