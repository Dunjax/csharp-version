﻿using Microsoft.Xna.Framework.Input;
using Nuclex.Input.Devices;

namespace DunjaxXNA
{
    /**
     * A gamepad used by the player to play the game.
     * On the Xbox, this will report input from any gamepad.
     */
    public interface ISystemGamePad
    {
        GamePadState GamePadState { get; }

#if !XBOX        
        ExtendedGamePadState ExtendedGamePadState { get; }
#endif
    }
}
