﻿using System;
using Dunjax;
using Dunjax.Attributes;
using Microsoft.Xna.Framework;
using IGamePad_ = Dunjax.IGamePad;
using Microsoft.Xna.Framework.Input;
using Nuclex.Input;
#if !XBOX
using Nuclex.Input.Devices;
#endif

namespace DunjaxXNA
{
    public class GamePad : Dunjax.IGamePad, ISystemGamePad
    {
        /**
         * The Nuclex Input library object which retrieves
         * the gamepad's current state.
         */
        private readonly InputManager input;

#if XBOX
        /**
         * The current controller to return the state of, when none have
         * recently changed state.
         */
        private PlayerIndex currentPlayerIndex = PlayerIndex.One;
#endif

        /**
         * The packet numbers most recently read for all the controllers.
         * By comparing the current packet numbers to those stored here, we
         * may tell which controller (if any) has just changed state.
         */
        private readonly int[] previousPacketNumbers 
            = new int[(int)PlayerIndex.Four + 1];

        [Constructor]
        public GamePad(InputManager input)
        {
            this.input = input;

            // for each controller
            for (var i = PlayerIndex.One; i <= PlayerIndex.Four; i++) {
                // store this controller's initial packet-number, so 
                // we may detect changes, later
                previousPacketNumbers[(int)i] = 
                    input.GetGamePad(i).GetState().PacketNumber;
            }
        }

        /**
         * For the XBox, returns the state of the first controller polled whose
         * state has changed since the last access of this property (such that 
         * any controller may be used at any time to play the game).  If none have
         * changed state, returns the state of the current controller.
         * 
         * For the PC, returns the state of the first gamepad.
         */
        public GamePadState GamePadState
        {
            get { 
#if XBOX
                // for each controller
                for (var i = PlayerIndex.One; i <= PlayerIndex.Four; i++) {
                    // if this controller isn't connected, skip it
                    var gamePad = input.GetGamePad(i);
                    if (!gamePad.IsAttached) continue;

                    // if this controller's state has changed since last time 
                    var state = gamePad.GetState();
                    if (state.PacketNumber != previousPacketNumbers[(int)i]) {
                        // return this controller's state
                        currentPlayerIndex = i;
                        previousPacketNumbers[(int)i] = state.PacketNumber;
                        return state;
                    }
                }

                // if we make it here, no controllers have changed state, 
                // so return the state of the current controller
                return input.GetGamePad(currentPlayerIndex).GetState();
#else
                // return the state of the first gamepad
                return input.GetGamePad(ExtendedPlayerIndex.Five).GetState(); 
#endif
            }
        }

#if !XBOX
        /**
         * Returns the current extended-state of the first PC gamepad.
         */
        public ExtendedGamePadState ExtendedGamePadState
        {
            get { 
                return input.GetGamePad(ExtendedPlayerIndex.Five)
                    .GetExtendedState(); 
            }
        }
#endif

        /**
         * The gamepad buttons used to issue non-directional commands.
         * These are public so they may be set by an external configuation
         * system.
         */
#if XBOX
        public static Buttons JumpButton = Buttons.A;
        public static Buttons FireButton = Buttons.X;
        public static Buttons SwingButton = Buttons.Y;
        public static Buttons SwitchGunsButton = Buttons.B;
#else
        public static int JumpButton = 2;
        public static int FireButton = 3;
        public static int SwingButton;
        public static int SwitchGunsButton = 1;
#endif

        /**
         * Returns whether the given command is being signaled by the given
         * (regular or extended) gamepad state.
         */
        public bool IsSignaled(Command command)
        {
            var state = GamePadState;
#if XBOX
            var buttonsState = state;
#else
            var buttonsState = ExtendedGamePadState;
#endif

            switch (command) {
                case Command.Left:
                    return state.IsButtonDown(Buttons.DPadLeft)
                        || state.IsButtonDown(Buttons.LeftThumbstickLeft);
                case Command.Right:
                    return state.IsButtonDown(Buttons.DPadRight)
                        || state.IsButtonDown(Buttons.LeftThumbstickRight);
                case Command.Up:
                    return state.IsButtonDown(Buttons.DPadUp)
                        || state.IsButtonDown(Buttons.LeftThumbstickUp);
                case Command.Down:
                    return state.IsButtonDown(Buttons.DPadDown)
                        || state.IsButtonDown(Buttons.LeftThumbstickDown);
                case Command.Swing:
                    return buttonsState.IsButtonDown(SwingButton);
                case Command.SwitchGuns:
                    return buttonsState.IsButtonDown(SwitchGunsButton);
                case Command.Fire:
                    return buttonsState.IsButtonDown(FireButton);
                case Command.Jump:
                    return buttonsState.IsButtonDown(JumpButton);
                default:
                    throw new Exception("Invalid command.");
            }
        }
    }
}
