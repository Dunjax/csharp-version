﻿using System;
using System.IO;
using Dunjax.Attributes;

namespace DunjaxXNA
{
    /**
     * Contains methods pertaining to the user's registration of the game program.
     */
    public class Registration
    {
        /**
         * Returns the product ID of this game program when run on the 
         * user's computer.
         */
        public static int ProductId
        {
            get {
                return (Environment.MachineName + "_dunjax").GetHashCode() % 10000;
            }
        }

        /**
         * Returns the registration code which corresponds to the given product ID.
         */
        public static int GetRegistrationCode(int productId)
        {
            return (productId * 71) / 47;
        }

        /**
         * The name of the file in which the registration code should be contained.
         */
        public static string RegistrationCodeFileName = "registrationCode.txt";

        [Shorthand]
        public static bool IsRegistered()
        {
            string ignored;
            return IsRegistered(out ignored); 
        }

        /**
         * Returns whether the correct registration code is found within the 
         * expected registration code file.  If it's not, the reason is given
         * in the output parameter.
         */
        public static bool IsRegistered(out string whyNot)
        {
            // try to read the registration code from the 
            // registration code file
            string code;
            try {
                code = File.ReadAllText(RegistrationCodeFileName);
            }
            catch (FileNotFoundException) {
                whyNot = "Could not find registration-code file in game folder";
                return false;
            }
            catch (Exception) {
                whyNot = "Could not read registration code from file";
                return false;
            }

            // if the code is not correct
            if (code != GetRegistrationCode(ProductId).ToString()) {
                // the game is not registered
                whyNot = "Registration code is incorrect";
                return false;
            }

            // the game is registered
            whyNot = null;
            return true;
        }
    }
}
