﻿using System;
using System.IO;
using DunjaxXNA;

namespace RegistrationCodeGenerator
{
    /**
     * Takes a [Dunjax game program] product ID as input, and outputs
     * the corresponding registration code file which the game program
     * checks to see if it has been registered.
     */
    class Program
    {
        public static void Main()
        {
            // read in the product ID
            Console.Write("Enter product ID: ");
            var productId = int.Parse(Console.ReadLine());

            // generate the registration code file for that product ID
            var code = Registration.GetRegistrationCode(productId);
            File.WriteAllText(Registration.RegistrationCodeFileName, code.ToString());
        }
    }
}
