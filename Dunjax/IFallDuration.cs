﻿namespace Dunjax
{
    /**
     * A duration geared specifically to help time a falling motion influenced
     * by gravity.  That is, the duration starts out at a certain maximum value,
     * and then with each restart decreases over time to simulate gravity's
     * acceleration.
     */
    public interface IFallDuration : IDuration
    {
        /**
         * Is called so this object may note the time when the current falling
         * motion started, so it may adjust the speed of the fall according to 
         * how long it has been occurring, to account for gravity's acceleration.
         */
        void FallStarted();

        /**
         * By what factor to slow down the fall (i.e. increase the duration 
         * between falls).  The default is 1.
         */
        float SlowdownFactor { set; }
    }
}
