﻿namespace Dunjax
{
    /**
     * A gamepad employed by the user to control their actions
     * in this game program.
     */
    public interface IGamePad
    {
        /**
         * Returns whether this gamepad is currently signaling the issuing 
         * of the given command by the user.
         */
        bool IsSignaled(Command command);
    }
}
