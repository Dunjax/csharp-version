﻿using System;
using Dunjax.Attributes;

namespace Dunjax.Geometry.Concrete
{
    public class Point : IPoint
    {
        public short X { get; set; }
        public short Y { get; set; }

        [Constructor]
        public Point() {}

        [Constructor]
        public Point(short x, short y) : this()
        {
            X = x;
            Y = y;
        }

        [Implementation]
        public IPoint SetX(short x)
        {
            X = x;
            return this;
        }

        [Implementation]
        public IPoint SetY(short y)
        {
            Y = y;
            return this;
        }

        [Implementation]
        public IPoint Set(short x, short y)
        {
            X = x;
            Y = y;
            return this;
        }

        [Implementation]
        public IPoint Set(IPoint location)
        {
            X = location.X;
            Y = location.Y;
            return this;
        }

        [Implementation]
        public IPoint Add(short dx, short dy)
        {
            X += dx;
            Y += dy;
            return this;
        }

        /** 
        * Overridden to compare point objects by their coordinates.
        */
        public override bool Equals(object obj)
        {
            if (!(obj is Point)) return false;
            var other = (Point) obj;
            return X ==  other.X && Y == other.Y;
        }

        /**
         * Is overridden because Equals is overridden.
         */ 
        public override int GetHashCode()
        {
            unchecked {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        [Implementation]
        public float GetSlope(IPoint to)
        {
            if (Equals(to)) return 0;
            if (X == to.X) return to.Y > Y ? 1000 : -1000;
            return (to.Y - Y) / (float)(to.X - X);
        }

        [Implementation]
        public float GetXSlope(IPoint to)
        {
            if (Equals(to)) return 0;
            if (Y == to.Y) return to.X > X ? 1000 : -1000;
            return (to.X - X) / (float)(to.Y - Y);
        }

        [Implementation]
        public float GetDistance(IPoint to)
        {
            int a = to.Y - Y, b = to.X - X;
            return (float)Math.Sqrt(a * a + b * b);
        }

        [Implementation]
        public bool IsDistanceAtMost(IPoint to, short distance)
        {
            int a = to.X - X, b = to.Y - Y;
            return a * a + b * b < distance * distance;
        }

        [Implementation]
        public IPoint GetDifference(IPoint to)
        {
            return GeometryFactory.CreatePoint(
                (short)(to.X - X), (short)(to.Y - Y));
        }

        [Optimized]
        [Implementation]
        public IFloatPoint GetUnitVector(IPoint to)
        {
            // handle the case where the two points are coincident
            if (Equals(to)) return new FloatPoint {X = 0, Y = 0};

            float distance = GetDistance(to);
            return new FloatPoint {
                X = (to.X - X) / distance,
                Y = (to.Y - Y) / distance
            };
        }

        [Implementation]
        public bool IsLocationInDirection(IPoint to, IFloatPoint direction)
        {
            const double range = Math.PI / 3;
            return direction.GetAngleBetween(GetUnitVector(to)) <= range;
        }

        [Implementation]
        public float GetAngleBetween(IPoint other)
        {
            // if either given vector has a zero magnitude, return a zero result
            float magnitude1 = GetMagnitude(),
                magnitude2 = other.GetMagnitude();
            if (magnitude1 == 0 || magnitude2 == 0) return 0;

            // use the formula for the dot product between the two vectors
            // to detm the angle between them
            float cosine = GetDotProduct(other) / (magnitude1 * magnitude2);
            if (cosine > 1) cosine = 1;
            if (cosine < -1) cosine = -1;
            return (float)Math.Acos(cosine);
        }

        [Implementation]
        public float GetMagnitude()
        {
            return (float) Math.Sqrt(X * X + Y * Y);
        }

        [Implementation]
        public float GetDotProduct(IPoint other)
        {
            return X * other.X + Y * other.Y;
        }
    }
}
