using Dunjax.Attributes;

namespace Dunjax.Geometry.Concrete
{
    public class Rectangle : IRectangle
    {
        /**
         * The top-left hand corner of this rectangle.
         */
        private readonly IPoint location = GeometryFactory.CreatePoint();

        [Destructor]
        ~Rectangle()
        {
            GeometryFactory.ReclaimPoints(
                location, center,
                topleft, topRight, bottomLeft, bottomRight, 
                topCenter, bottomCenter, leftCenter, rightCenter);
            GeometryFactory.ReclaimSize(size);
        }
            
        /**
         * The dimensions of this rectangle.
         */
        private readonly ISize size = GeometryFactory.CreateSize();
        public ISize Size
        {
            get { return size; }
            set { size.Set(value); }
        }

        /**
         * A property for accessing just this rectangle's width.
         */
        public short Width
        {
            get { return Size.Width; }
            set { size.Width = value; }
        }

        /**
         * A property for accessing just this rectangle's height.
         */
        public short Height
        {
            get { return Size.Height; }
            set { size.Height = value; }
        }

        /**
         * A property for accessing just this rectangle's left-x value.
         */
        public short LeftX
        {
            get {return location.X;}
            set {location.X = value;}
        }
        
        /**
         * A property for accessing just this rectangle's right-x value.
         */
        public short RightX
        {
            get {return (short)(location.X + Size.Width - 1);}
            set {location.X = (short)(value - Size.Width + 1);}
        }
        /**
         * A property for accessing just this rectangle's top-y value.
         */
        public short TopY
        {
            get {return location.Y;}
            set {location.Y = value;}
        }
        
        /**
         * A property for accessing just this rectangle's bottom-y value.
         */
        public short BottomY
        {
            get {return (short)(location.Y + Size.Height - 1);}
            set {location.Y = (short)(value - Size.Height + 1);}
        }

        /**
         * This rectangle's top-left corner location.
         */
        public IPoint TopLeft
        {
            get { return topleft.Set(location); }
            set {location.Set(value);}
        }
        private readonly IPoint topleft = GeometryFactory.CreatePoint();
    	
        /**
         * This rectangle's top-right corner location.
         */
        public IPoint TopRight
        {
            get { 
                return topRight.Set(
                    (short)(location.X + Size.Width - 1), location.Y); 
            }
            set {location.Set((short)(value.X - Size.Width + 1), value.Y);}
        }
        private readonly IPoint topRight = GeometryFactory.CreatePoint();

        /**
         * This rectangle's bottom-left corner location.
         */
        public IPoint BottomLeft
        {
            get {
                return bottomLeft.Set(
                    location.X, (short)(location.Y + Size.Height - 1));
            }
            set {location.Set(value.X, (short)(value.Y - Size.Height + 1));}
        }
        private readonly IPoint bottomLeft = GeometryFactory.CreatePoint();

        /**
         * This rectangle's bottom-right corner location.
         */
        public IPoint BottomRight
        {
            get { 
                return bottomRight.Set(
                    (short)(location.X + Size.Width - 1), 
                    (short)(location.Y + Size.Height - 1)); 
            }
            set {
                location.Set(
                    (short)(value.X - Size.Width + 1), 
                    (short)(value.Y - Size.Height + 1));
            }
        }
        private readonly IPoint bottomRight = GeometryFactory.CreatePoint();

        /**
         * This rectangle's center location.
         */
        public IPoint Center
        {
            get { 
                return center.Set(
                    (short)(location.X + Size.Width / 2), 
                    (short)(location.Y + Size.Height / 2)); 
            }
            set {
                location.Set(
                    (short)(value.X - Size.Width / 2), 
                    (short)(value.Y - Size.Height / 2));
            }
        }
        private readonly IPoint center = GeometryFactory.CreatePoint();

        /**
         * This rectangle's top-center location.
         */
        public IPoint TopCenter
        {
            get {
                return topCenter.Set(
                    (short)(location.X + Size.Width / 2), location.Y);
            }
            set {location.Set((short)(value.X - Size.Width / 2), value.Y);}
        }
        private readonly IPoint topCenter = GeometryFactory.CreatePoint();

        /**
         * This rectangle's bottom-center location.
         */
        public IPoint BottomCenter
        {
            get {
                return bottomCenter.Set(
                    (short)(location.X + Size.Width / 2), 
                    (short)(location.Y + Size.Height - 1));
            }
            set {
                location.Set(
                    (short)(value.X - Size.Width / 2), 
                    (short)(value.Y - Size.Height + 1));
            }
        }
        private readonly IPoint bottomCenter = GeometryFactory.CreatePoint();

        /**
         * This rectangle's left-center location.
         */
        public IPoint LeftCenter
        {
            get {
                return leftCenter.Set(
                    location.X, (short)(location.Y + Size.Height / 2));
            }
            set {location.Set(value.X, (short)(value.Y - Size.Height / 2));}
        }
        private readonly IPoint leftCenter = GeometryFactory.CreatePoint();

        /**
         * This rectangle's right-center location.
         */
        public IPoint RightCenter
        {
            get {
                return rightCenter.Set(
                    (short)(location.X + Size.Width - 1), 
                    (short)(location.Y + Size.Height / 2));
            }
            set {
                location.Set(
                    (short)(value.X - Size.Width + 1), 
                    (short)(value.Y - Size.Height / 2));
            }
        }
        private readonly IPoint rightCenter = GeometryFactory.CreatePoint();

        [Constructor]
        public Rectangle() {} 

        [Constructor]
        public Rectangle(short x, short y, short width, short height) 
        {
            location.Set(x, y);
            size.Set(width, height);
        }

        public IRectangle Set(IRectangle other)
        {
            location.Set(other.TopLeft);
            size.Set(other.Size);
            return this;
        }

        public IRectangle AddToLocation(short dx, short dy)
        {
            location.Add(dx, dy);
            return this;
        }

        public bool Contains(IPoint queryLocation)
        {
            return queryLocation.X >= location.X
                && queryLocation.Y >= location.Y
                && queryLocation.X <= location.X + Size.Width - 1
                && queryLocation.Y <= location.Y + Size.Height - 1;
        }

        public bool Intersects(IRectangle other)
        {
            // code from Java platform
            int tw = Width;
            int th = Height;
            int rw = other.Width;
            int rh = other.Height;
            if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) return false;
            int tx = LeftX;
            int ty = TopY;
            int rx = other.LeftX;
            int ry = other.TopY;
            rw += rx;
            rh += ry;
            tw += tx;
            th += ty;
            // overflow || intersect
            return ((rw < rx || rw > tx) &&
                (rh < ry || rh > ty) &&
                (tw < tx || tw > rx) &&
                (th < ty || th > ry));
        }
    }
}
