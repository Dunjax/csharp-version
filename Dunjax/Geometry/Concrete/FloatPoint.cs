﻿using System;
using Dunjax.Attributes;

namespace Dunjax.Geometry.Concrete
{
    public class FloatPoint : IFloatPoint
    {
        public float X { get; set; }
        public float Y { get; set; }

        [Constructor]
        public FloatPoint() { }

        public IFloatPoint Set(float x, float y)
        {
            X = x;
            Y = y;
            return this;
        }

        public IFloatPoint Set(IFloatPoint location)
        {
            X = location.X;
            Y = location.Y;
            return this;
        }

        public IFloatPoint Add(float dx, float dy)
        {
            X += dx;
            Y += dy;
            return this;
        }

        /** 
        * Overridden to compare point objects by their coordinates.
        */
        public override bool Equals(object obj)
        {
            if (!(obj is Point)) return false;
            var other = (Point) obj;
            return X ==  other.X && Y == other.Y;
        }

        // this is overridden because Equals is overridden
        public override int GetHashCode()
        {
            unchecked {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public float GetSlope(IFloatPoint to)
        {
            if (Equals(to)) return 0;
            if (X == to.X) return to.Y > Y ? 1000 : -1000;
            return (to.Y - Y) / (to.X - X);
        }

        public float GetXSlope(IFloatPoint to)
        {
            if (Equals(to)) return 0;
            if (Y == to.Y) return to.X > X ? 1000 : -1000;
            return (to.X - X) / (to.Y - Y);
        }

        public float GetDistance(IFloatPoint to)
        {
            float a = to.Y - Y, b = to.X - X;
            return (float)Math.Sqrt(a * a + b * b);
        }

        public bool IsDistanceAtMost(IFloatPoint to, float distance)
        {
            float a = to.X - Y, b = to.Y - X;
            return a * a + b * b < distance * distance;
        }

        public IFloatPoint GetDifference(IFloatPoint to)
        {
            return new FloatPoint {X = to.X - X, Y = to.Y - Y};
        }

        [Optimized]
        public IFloatPoint GetUnitVector(IFloatPoint to)
        {
            // handle the case where the two points are coincident
            if (Equals(to)) return new FloatPoint {X = 0, Y = 0};

            float distance = GetDistance(to);
            return new FloatPoint {
                X = (to.X - X) / distance,
                Y = (to.Y - Y) / distance
            };
        }

        public bool IsLocationInDirection(IFloatPoint to, IFloatPoint direction)
        {
            const double range = Math.PI / 3;
            return direction.GetAngleBetween(GetUnitVector(to)) <= range;
        }

        public float GetAngleBetween(IFloatPoint other)
        {
            // if either given vector has a zero magnitude, return a zero result
            float magnitude1 = GetMagnitude(),
                magnitude2 = other.GetMagnitude();
            if (magnitude1 == 0 || magnitude2 == 0) return 0;

            // use the formula for the dot product between the two vectors
            // to detm the angle between them
            float cosine = GetDotProduct(other) / (magnitude1 * magnitude2);
            if (cosine > 1) cosine = 1;
            if (cosine < -1) cosine = -1;
            return (float)Math.Acos(cosine);
        }

        public float GetMagnitude()
        {
            return (float) Math.Sqrt(X * X + Y * Y);
        }

        public float GetDotProduct(IFloatPoint other)
        {
            return X * other.X + Y * other.Y;
        }

        public bool Equals(FloatPoint other)
        {
            return other.X == X && other.Y == Y;
        }
    }
}
