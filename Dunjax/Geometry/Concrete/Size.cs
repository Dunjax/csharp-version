﻿using Dunjax.Attributes;

namespace Dunjax.Geometry.Concrete
{
    public class Size : ISize
    {
        public short Width { get; set; }

        public short Height { get; set; }

        [Constructor]
        public Size() {}

        public ISize Set(short width, short height)
        {
            Width = width; 
            Height = height;
            return this;
        }

        public ISize Set(ISize size)
        {
            Width = size.Width;
            Height = size.Height;
            return this;
        }
    }
}
