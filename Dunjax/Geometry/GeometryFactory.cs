using Dunjax.Attributes;
using Dunjax.Geometry.Concrete;

namespace Dunjax.Geometry
{
    public class GeometryFactory
    {
        [ObjectPool]
        private static readonly IObjectPool<IPoint> PointPool =
            DunjaxFactory.CreateObjectPool<IPoint, Point>();

        [ObjectPool]
        private static readonly IObjectPool<IFloatPoint> FloatPointPool =
            DunjaxFactory.CreateObjectPool<IFloatPoint, FloatPoint>();

        [ObjectPool]
        private static readonly IObjectPool<ISize> SizePool =
            DunjaxFactory.CreateObjectPool<ISize, Size>();

        [Factory]
        public static IPoint CreatePoint()
        {
            return PointPool.Retrieve().Set(0, 0);
        }

        [Factory]
        public static IPoint CreatePoint(short x, short y)
        {
            return CreatePoint().Set(x, y);
        }

        [Reclaimer]
        public static void ReclaimPoint(IPoint point) { PointPool.Reclaim(point); }

        [Reclaimer]
        public static void ReclaimPoints(params IPoint[] points)
        {
            for (int i = 0; i < points.Length; i++) PointPool.Reclaim(points[i]);
        }

        [Factory]
        public static IFloatPoint CreateFloatPoint()
        {
            return FloatPointPool.Retrieve().Set(0, 0);
        }

        [Factory]
        public static IFloatPoint CreateFloatPoint(float x, float y)
        {
            return CreateFloatPoint().Set(x, y);
        }

        [Reclaimer]
        public static void ReclaimFloatPoint(IFloatPoint point)
        {
            FloatPointPool.Reclaim(point);
        }

        [Factory]
        public static ISize CreateSize()
        {
            return SizePool.Retrieve().Set(0, 0);
        }

        [Factory]
        public static ISize CreateSize(short width, short height)
        {
            return CreateSize().Set(width, height);
        }

        [Reclaimer]
        public static void ReclaimSize(ISize size)
        {
            SizePool.Reclaim(size);
        }

        [Factory]
        public static IRectangle CreateRectangle() { return new Rectangle(); }
    }
}