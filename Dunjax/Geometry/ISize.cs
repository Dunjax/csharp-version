﻿namespace Dunjax.Geometry
{
    public interface ISize
    {
        short Width { get; set; }
        short Height { get; set; }

        // these setters return this point
        ISize Set(short width, short height);
        ISize Set(ISize size);
    }
}
