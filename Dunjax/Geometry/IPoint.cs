﻿namespace Dunjax.Geometry
{
    public interface IPoint
    {
        short X { get; set; }
        short Y { get; set; }

        IPoint SetX(short x);
        IPoint SetY(short y);
        IPoint Set(short x, short y);
        IPoint Set(IPoint location);
        IPoint Add(short dx, short dy);

        /**
         * Returns the slope of the line between this and the given point.
         */
        float GetSlope(IPoint to);

        /**
         * Returns the x-slope (i.e. run / rise) of the line between this and the given point.
         */
        float GetXSlope(IPoint to);

        /**
         * Returns the distance between this and the given point.
         */
        float GetDistance(IPoint to);

        /**
         * Returns whether the distance between this and the given point is at most
         * the given amount.  Is meant to be much quicker for this purpose than 
         * calling GetDistance().
         */
        bool IsDistanceAtMost(IPoint to, short distance);

        /**
         * Returns the difference of the given point from this point.
         */
        IPoint GetDifference(IPoint to);

        /** 
         * Returns the unit vector between this and the given point.
         */
        IFloatPoint GetUnitVector(IPoint to);

        /**
	     * Returns whether the given point is in the general given direction
	     * from this one.
	     */
        bool IsLocationInDirection(IPoint to, IFloatPoint direction);

        /**
         * Returns the angle (in radians) formed between the vectors formed by
         * this and the given point in relation to the origin.
         */
        float GetAngleBetween(IPoint other);

        /**
         * Returns the magnitude of the vector this point forms from the origin.
         */
        float GetMagnitude();

        /**
         * Returns the dot product of the vectors formed by
         * this and the given point in relation to the origin.
         */
        float GetDotProduct(IPoint other);
    }
}
