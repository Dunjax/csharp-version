﻿namespace Dunjax.Geometry
{
    public interface IRectangle
    {
        short LeftX { get; set; }
        short RightX { get; set; }
        short TopY { get; set; }
        short BottomY { get; set; }

        IPoint Center { get; set; }
        IPoint TopLeft { get; set; }
        IPoint TopRight { get; set; }
        IPoint BottomLeft { get; set; }
        IPoint BottomRight { get; set; }
        IPoint TopCenter { get; set; }
        IPoint BottomCenter { get; set; }
        IPoint LeftCenter { get; set; }
        IPoint RightCenter { get; set; }

        ISize Size { get; set; }
        short Width { get; set; }
        short Height { get; set; }

        IRectangle Set(IRectangle other);

        IRectangle AddToLocation(short dx, short dy);

        /**
         * Returns whether this rectangle's bounds contains (i.e. covers) the given location.
         */
        bool Contains(IPoint location);

        /**
         * Returns whether this rectangle's bounds intersects with those of the 
         * given other rectangle.
         */
        bool Intersects(IRectangle other);
    }
}
