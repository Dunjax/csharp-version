﻿namespace Dunjax.Geometry
{
    public interface IFloatPoint
    {
        float X { get; set; }
        float Y { get; set; }

        IFloatPoint Set(float x, float y);
        IFloatPoint Set(IFloatPoint location);
        IFloatPoint Add(float dx, float dy);

        /**
         * Returns the slope of the line between this and the given point.
         */
        float GetSlope(IFloatPoint to);

        /**
         * Returns the x-slope (i.e. run / rise) of the line between this and the given point.
         */
        float GetXSlope(IFloatPoint to);

        /**
         * Returns the distance between this and the given point.
         */
        float GetDistance(IFloatPoint to);

        /**
         * Returns whether the distance between this and the given point is at most
         * the given amount.  Is meant to be much quicker for this purpose than 
         * calling GetDistance().
         */
        bool IsDistanceAtMost(IFloatPoint to, float distance);

        /**
         * Returns the difference of the given point from this point.
         */
        IFloatPoint GetDifference(IFloatPoint to);

        /** 
         * Returns the unit vector between this and the given point.
         */
        IFloatPoint GetUnitVector(IFloatPoint to);

        /**
	     * Returns whether the given point is in the general given direction
	     * from this one.
	     */
        bool IsLocationInDirection(IFloatPoint to, IFloatPoint direction);

        /**
         * Returns the angle (in radians) formed between the vectors formed by
         * this and the given point in relation to the origin.
         */
        float GetAngleBetween(IFloatPoint other);

        /**
         * Returns the magnitude of the vector this point forms from the origin.
         */
        float GetMagnitude();

        /**
         * Returns the dot product of the vectors formed by
         * this and the given point in relation to the origin.
         */
        float GetDotProduct(IFloatPoint other);
    }
}
