using Game_ = Dunjax.Game.Concrete.Game;

namespace Dunjax.Game
{
    public class GameFactory
    {
        public static IGame CreateGame(int startingLevelNumber)
        {
            return new Game_(startingLevelNumber);
        }
    }
}