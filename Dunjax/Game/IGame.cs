using System;
using Dunjax.Map;

namespace Dunjax.Game
{
    /** 
     * A playing of the game program, where the player tries to progress from one
     * map to the next until either he is killed, or wins.
     */
    public interface IGame
    {
        /**
         * The current map being played in this game.
         */
        IMap Map { get; }

        /**
         * The one-based index of the current level being played in this game.
         */
        int CurrentLevelNumber { get; }

        /**
         * Has this game process its next turn, if it feels it's time to do so.
         */
        void DoTurn();

        /**
         * Fires when the game is over for the player.
         */ 
        event EventHandler GameOver;

        /**
         * Fires when the player has reached the end of the current level 
         * in this game.
         */
        event EventHandler LevelCompleted;

        /**
         * Fires when the next level the player is about to explore is 
         * fully loaded and ready to play.
         */
        event EventHandler NextLevelReady;

        /**
         * Fires when the player has completed this game.
         */
        event EventHandler GameCompleted;

        /**
         * Pauses this game's action.
         */
        void Pause();

        /**
         * Resumes this game's action after a call to Pause().
         */
        void Resume();
    }
}