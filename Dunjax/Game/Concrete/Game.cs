using System;
using System.Threading;
using Dunjax.Attributes;
using Dunjax.Entities;
using Dunjax.Entities.Beings.Monsters;
using Dunjax.Entities.Beings.Player;
using Dunjax.Map;
using Dunjax.Sound;

namespace Dunjax.Game.Concrete
{
    class Game : IGame
    {
        /**
         * Keeps track of the time in ms that has elapsed since the start of this game.
         */
        protected readonly IUpdateableClock clock = DunjaxFactory.CreateClock();
        
        /**
         * Whether this game it is over (meaning, it has finished before the player 
         * was able to reach the ending).
         */
        protected bool over;
        
        /**
         * How long this game should continue running after it's over. 
         */
	    protected readonly IDuration postGameOverRunTime;

        /**
         * The current map being explored by the player.  This should only be set
         * by calling setMap().
         */
        public IMap Map { get { return map; } }
	    protected IMap map;
        
        /**
         * The latest game time for which scheduled entity actions have been processed
         * by this game's ProcessEntityActions() method.
         */
        protected long lastTimeEntityActionsProcessed = -1;
        
        /**
         * Whether it's time for this game to move on to the next map.
         */
        protected bool shouldAdvanceToNextMap;

        /**
         * Whether this game's master monster has been killed, to help us determine
         * why this game is ending when it does.
         */
        protected bool masterDead;

        [Implementation]
        public int CurrentLevelNumber { get { return currentLevelNumber; } }
        private int currentLevelNumber;

        [Constructor]
        public Game(int startingLevelNumber)
        {
            currentLevelNumber = startingLevelNumber;

            postGameOverRunTime = DunjaxFactory.CreateDuration(clock, 4000, false);

            clock.Start();

            // create the player
            var player = PlayerFactory.CreatePlayer(clock);

            // listen for the player's events
            player.Died += PlayerDead;
            player.ReachedExit += PlayerReachedExit;
            
            LoadMap("maps/" + currentLevelNumber + ".wmp");
            
            PutPlayerAtStartCave(player);

            // temp code for testing various areas of the first map
            if (startingLevelNumber == 1) {
                //player.CenterOn(
                //    map.GetLocationOfTerrainFeature(TerrainFeature.ExitCave).Add(-1 * MapConstants.UnitsPerTenFeet, 0));
                //player.CenterOn(player.Center.Set(
                    //688, 1264)); // slide sequence
                    //997, 2032)); // slide sequence
                    //1511, 2416)); // multi-slide sequence
                    //1455, 697)); // on ladder near door
                    //384,3144)); // small slides
                    //2639, 1728)); // spiders
                    //2650, 3168)); // slimes
                    //2894, 544)); // master
                    //439, 1840)); // stalagmites
                    //1152, 1920)); // grape
                    //2484, 890)); // whirlwinds
                    //662, 1248)); // yellow gargoyles
                    //1168, 1072)); // yellow gargoyles on ladders
                    //1179, 2128)); // blue gargoyles
            }
        }
        
        /**
         * Adds the player to the new current map at the location of its start cave.
         */
        protected void PutPlayerAtStartCave(IPlayer player)
        {
            map.AddEntity(player);
            player.CenterOn(
                map.GetLocationOfTerrainFeature(TerrainFeature.StartCave).
                    Add(-MapConstants.UnitsPerTenFeet / 2, 0));
            player.AlignWithSurroundings();
        }
        
        /**
         * Returns whether this game is done running.
         */
        protected bool IsGameDone()
        {
            return over && postGameOverRunTime.DoneOnly;
        }
        
        /**
         * Processes the next game turn.
         */
        public void DoTurn()
        {
            if (IsGameDone()) {
                ExitGame();
                return;
            }

            clock.Update();

            CheckToMoveAllActionsToCurrentTime();

            ProcessEntityActions();
            
            map.Act();
            
            if (shouldAdvanceToNextMap) AdvanceToNextMap();
        }

        /**
         * Fires events which will cause this game to be discarded.
         */
        private void ExitGame()
        {
            if (map.Player.Dead) GameOver(this, null);
                
            else if (masterDead) {
                GameCompleted(this, null);
                    
                // stop any repeating sounds which might be playing
                Sounds.PlayerThrust.Stop();
            }
        }
        
        /**
         * If entity actions have yet to be processed for the first 
         * time by this game, reschedules all actions to the 
         * current clock time.  This way, there won't be a huge 
         * block of actions to process before the map is even 
         * displayed, due to entities scheduling successive actions 
         * from time zero to the current time (as the current time
         * includes the duration it took to load and initialize the 
         * level).  
         */
        private void CheckToMoveAllActionsToCurrentTime()
        {
            // if this game has processed actions at least once already,
            // there is nothing to do
            if (lastTimeEntityActionsProcessed >= 0) return;

            // move all existing actions to the current clock time
            map.EntityActionSchedule.MoveAllActionsToTime((long)clock.Time);
            
            // move the last-time-actions-processed marker to just 
            // before the current clock time
            lastTimeEntityActionsProcessed = (long)clock.Time - 10;
        }

        /**
         * Executes all entity actions scheduled in the range from the last game time 
         * for which this was called, up to the current game time.   
         */
        protected void ProcessEntityActions()
        {
            // for each millisecond of game time since the last one processed by this
            // method, to the current game time
            var currentTimeFloored = (long)Math.Floor(clock.Time);
            for (var i = lastTimeEntityActionsProcessed + 1; i <= currentTimeFloored; i++) {
                // if there are no actions scheduled for this millisecond, skip it
                var actions =
                    map.EntityActionSchedule.RemoveActionsScheduledForTime(i);
                if (actions == null) continue;
                
                // for each action scheduled for this millisecond
                for (var j = 0; j < actions.Count; j++) {
                    var action = actions[j];
                    
                    // if some other entity in this loop caused this entity to be 
                    // removed from the map, skip this entity
                    var entity = action.Entity;
                    if (entity.Map == null) continue;
                    
                    // if the entity is permanently inactive, don't have it act
                    if (entity.PermanentlyInactive) continue;
        
                    // have this action occur, fudging the current game time to be that
                    // scheduled for the action, so that durations for the entity that should 
                    // be up, will be up, and those that should be reset are set to the
                    // correct starting time
                    var oldCurrentTime = clock.Time;
                    clock.Time = action.Time;
                    entity.Act();
                    clock.Time = oldCurrentTime;
                }
                
                EntitiesFactory.ReclaimEntityActionList(actions);
            }

            // remember the last millisecond interval of game time processed by this call
            lastTimeEntityActionsProcessed = currentTimeFloored;
        }
        
        /**
         * Has this game switch from running within its current map to running within
         * the next one.
         */
        protected void AdvanceToNextMap()
        {
            shouldAdvanceToNextMap = false;
            
            var player = map.Player;
            map.RemoveEntity(player);
            
            LoadMap("maps/" + ++currentLevelNumber + ".wmp");

            PutPlayerAtStartCave(player);

            NextLevelReady(this, null);
        }

        [Handler]
        public void PlayerDead(object sender, EventArgs args) 
        {
            // this game is now over
            over = true;
            
            // start the final duration during which this game will run, now
            // that it is over
            postGameOverRunTime.Start();
        }

        [Handler]
        public void PlayerReachedExit(object sender, EventArgs args) 
        {
            // if this exit-reach has already been signaled, ignore this re-signaling
            if (shouldAdvanceToNextMap) return;

            LevelCompleted(this, null);

            // play the level-end-reached sound and wait till it's finished
            Sounds.LevelEndReached.Play();
            while (Sounds.LevelEndReached.Playing) Thread.Sleep(100);

            shouldAdvanceToNextMap = true;
        }
        
        /**
         * Loads the map of the given filename, and makes it this game's current map.
         */
        protected void LoadMap(String flieName)
        {
            // add our listener for the master-created event to the map-loader
            var loader = MapFactory.CreateMapLoader(clock);
            loader.MasterCreated += MasterCreated;

            // load the map
            map = loader.Load(flieName);

            // mark that the initial actions for the level will need to be moved 
            // to the level's starting time
            lastTimeEntityActionsProcessed = -1;
        }

        [Implementation]
        public event EventHandler LevelCompleted;

        [Implementation]
        public event EventHandler NextLevelReady;

        [Implementation]
        public void Pause()
        {
            clock.Stop();
        }

        [Implementation]
        public void Resume()
        {
            clock.Resume();
        }

        [Handler]
        private void MasterCreated(object sender, EventArgs args)
        {
            var master = sender as IMaster;
            master.Killed += MasterKilled;
        }

        [Handler]
        private void MasterKilled(object sender, EventArgs args)
        {
            masterDead = true;

            // this game is now over
            over = true;

            // start the final duration during which this game will run, now
            // that it is over
            postGameOverRunTime.Start();
        }

        [Implementation]
        public event EventHandler GameOver;

        [Implementation]
        public event EventHandler GameCompleted;
    }
}