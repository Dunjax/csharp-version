﻿using System;

namespace Dunjax.Attributes
{
    /**
     * Signifies a property of a factory class used to 
     * externally inject a concrete class's dependency on 
     * some platform-dependent object, without having to expose
     * the concrete class to the outside world.
     */
    [AttributeUsage(AttributeTargets.Property)]
    class PlatformDependencySetter : Attribute
    {
    }
}
