﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class HookOverride : Attribute
    {
    }
}

