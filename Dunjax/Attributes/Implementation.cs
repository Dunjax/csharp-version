﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
    public class Implementation : Attribute
    {
    }
}

