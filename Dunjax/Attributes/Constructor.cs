﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Constructor)]
    public class Constructor : Attribute
    {
    }
}

