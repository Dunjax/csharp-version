﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(System.AttributeTargets.Field)]
    class Singleton : System.Attribute
    {
    }
}

