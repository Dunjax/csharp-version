﻿using System;

namespace Dunjax.Attributes
{
    /**
     * Means changes have been made to make the method run faster which may
     * sometimes violate software engineering principles, leaving the method
     * harder to maintain.  It is presumed that the method is called often enough
     * to warrant such negative measures.
     */
    [AttributeUsage(System.AttributeTargets.Method)]
    class Optimized : System.Attribute
    {
    }
}

