﻿using System;

namespace Dunjax.Attributes
{
    /**
     * Note that we can't call this EventHandler as that causes name 
     * clashes with the System class of that name.
     */
    [AttributeUsage(AttributeTargets.Method)]
    public class Handler : Attribute
    {
    }
}

