﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
    class OverriddenValue : Attribute
    {
    }
}

