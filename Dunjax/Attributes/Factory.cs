﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    class Factory : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    class Reclaimer : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Field)]
    class ObjectPool : Attribute
    {
    }
}

