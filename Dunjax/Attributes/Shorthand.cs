﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Shorthand : Attribute
    {
    }
}

