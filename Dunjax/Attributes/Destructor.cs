﻿using System;

namespace Dunjax.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Destructor : Attribute
    {
    }
}

