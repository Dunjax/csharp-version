﻿using System;
using System.IO;

namespace Dunjax
{
    /**
     * Allows writing of diagnostic messages to a log file.
     */
    public class Log
    {
        /**
         * Appends the given message to the log file, whether it 
         * exists already, or not.
         */
        public static void Write(string message)
        {
            var file = File.AppendText("log.txt");
            file.WriteLine("\n[" + DateTime.Now + "]\n" + message);
            file.Close();
        }
    }
}
