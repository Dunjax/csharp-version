using Dunjax.Attributes;
using Dunjax.Concrete;

namespace Dunjax
{
    class DunjaxFactory
    {
        [Factory]
        public static IDuration CreateDuration(
            IClock clock, float length = 0, bool representsPeriod = true)
        {
            return new Duration(clock, length, representsPeriod);
        }

        [Factory]
        public static IFallDuration CreateFallDuration(IClock clock)
        {
            return new FallDuration(clock);
        }

        [Factory]
        public static IUpdateableClock CreateClock()
        {
            return new Clock();
        }

        [Factory]
        public static IObjectPool<T> CreateObjectPool<T, U>() 
            where U : T, new()
        {
            return new ObjectPool<T, U>();
        }
    }
}