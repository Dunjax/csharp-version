using System;
using Dunjax.Attributes;
using Dunjax.Geometry;

namespace Dunjax.Entities.Beings 
{
    public interface IBeing : IMapEntity
    {
        /**
         * Returns whether this being is currently in the given being-state.
         */
        bool InState(BeingState state);

        /**
         * Whether this being is currently facing left (vs. right).
         */
        bool FacingLeft { get; }
        
        /**
         * Returns whether the given location corresponds to a strikable spot on 
         * this being.
         */
        bool IsStrikableAt(IPoint location);
        
        /**
         * Informs this being that it has been struck at the given location,
         * by something that would normally cause the given amount of damage.
         */
        void Struck(IPoint location, int damage);

        /**
         * Whether this being is not at its maximum health level.
         */
        bool Damaged { get; }

        /**
         * Whether this being has no health left (i.e. is dead).
         */
	    bool Dead { get; }
    }
        
    /**
     * Any of the activities or states-of-being a being may be performing 
     * or may be in.  A being may be in more than one state at a time.
     * 
     * This class is publicly visible because a being's state is a publicly visible
     * property of that being, upon which other program entities or tests might want 
     * to base their decisions.  For instance, a shot might want to ignore hitting a 
     * monster which is already dead, or a monster might not want to attack a player
     * which is currently in the hit state, or a monster might want to run from a player 
     * who is firing.  
     */
    public class BeingState
    {
        /**
         * For identifying states in the debugger.
         */
        private readonly string name;
        public override string ToString() { return "State: " + name; }

        [Constructor]
        protected BeingState(string name) { this.name = name;}

        [Flyweights]
        public static readonly BeingState
            Still = new BeingState("Still"),
            Moving = new BeingState("Moving"),
            Turning = new BeingState("Turning"),
            Hit = new BeingState("Hit"),
            Dying = new BeingState("Dying"),
            Dead = new BeingState("Dead");
    }
}