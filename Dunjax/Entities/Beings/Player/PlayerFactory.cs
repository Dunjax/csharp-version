using Dunjax.Attributes;
using Player_ = Dunjax.Entities.Beings.Player.Concrete.Player;

namespace Dunjax.Entities.Beings.Player
{
    public class PlayerFactory
    {
        public static IPlayer CreatePlayer(IClock clock) { return new Player_(clock); }

        [PlatformDependencySetter]
        public static IUserInput UserInput { set { Player_.userInput = value; } }
    }
}