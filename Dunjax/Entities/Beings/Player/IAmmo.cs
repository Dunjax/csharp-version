using Dunjax.Geometry;
using Dunjax.Map;

namespace Dunjax.Entities.Beings.Player
{
    /**
     * Represents the player's held amount of a particular type of ammunition.
     */
    public interface IAmmo
    {
        /**
         * Returns how many units of ammo this object currently represents.
         */
        int Amount {get;}

        /**
         * Adds the given amount to that represented by this ammo object.
         */
        void Add(int amount);

        /**
         * Informs this ammo that it has been fired from the given location on the 
         * given map towards the given horizontal and vertical sides of this
         * ammo's possessor.  This object is then responsible for creating one or
         * more shots on the map. 
         */
        void Fired(IMap map, IPoint firingLocation, 
            Side toSide, VerticalSide toVerticalSide);
    }
}