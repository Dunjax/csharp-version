using System;
using Dunjax.Attributes;

namespace Dunjax.Entities.Beings.Player
{
    public interface IPlayer : IBeing
    {
        /**
         * Informs this player that it has just been webbed.
         */
        void Webbed();

        /**
	     * Informs this player that it has just been tossed by a whirlwind.
	     */
        void Whirlwinded(Side toSide);

        /**
         * Is fired when this player reaches the exit to its current map.
         */
        event EventHandler ReachedExit;

        /**
         * Is fired when this player dies.
         */
        event EventHandler Died;

        /**
         * How many keys this player is currently holding.
         */
        int KeysHeld { get; }

        /**
         * How much armor this player has left to protect it.
         */
        int ArmorLeft { get; }

        /**
         * The ammo this player is currently firing.
         */
        IAmmo AmmoInUse { get; }

        /**
         * Is fired when this player touches a final-room-trigger
         * entity.
         */
        event EventHandler ReachedFinalRoomTrigger;
    }

    /**
     * See parent class.  Specifies player-specific states.
     */
    class PlayerState : BeingState
    {
        [Constructor]
        private PlayerState(string name) : base(name) {}

        [Flyweights]
        public static PlayerState
            Falling = new PlayerState("Falling"),
            Jumping = new PlayerState("Jumping"),
            Sliding = new PlayerState("Sliding"),
            Climbing = new PlayerState("Climbing"),
            Landing = new PlayerState("Landing"),
            Swinging = new PlayerState("Swinging"),
            Whirlwinded = new PlayerState("Whirlwinded"),
            Webbed = new PlayerState("Webbed");
    }
}
