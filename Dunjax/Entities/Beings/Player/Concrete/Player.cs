using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Concrete;
using Dunjax.Entities.Items;
using Dunjax.Entities.Shots;
using Dunjax.Geometry;
using Dunjax.Map;
using AllSounds = Dunjax.Sound.Sounds;

namespace Dunjax.Entities.Beings.Player.Concrete
{
    class Player : Being, IPlayer
    {
        /**
         * Reports what the user would like this player to do.  
         * Should be set by external, platform dependent code.
         */
        public static IUserInput userInput;

	    /**
	     * To which side this player is currently in the midst of a toss by a whirlwind.
	     */
	    protected Side whirlwindedSide = Side.NeitherSide;
    	
        /**
         * Whether this player's vertical movement due to a current instance
         * of being whirlwinded has ended, due to the player hitting something
         * impassable.
         */
        protected bool whirlwindedVerticalMoveEnded;
        
        /**
         * How much damage this player takes when impacting a solid surface
         * due to being whirlwinded.
         */
        private const int whirlwindImpactDamage = 6;

        /**
         * The ammo this player is currently using.
         */
        [Implementation]
        public IAmmo AmmoInUse { get { return ammoInUse; }}
        protected IAmmo ammoInUse;

        /**
         * This player's current supply of pulse ammo.
         */
        protected readonly IAmmo pulseAmmo;

        /**
         * This player's current supply of grapeshot ammo.
         */
        protected readonly IAmmo grapeshotAmmo;

        /**
         * The various durations affecting this player.
         */
        protected readonly Durations durations;
        
        /**
         * How long (in ms) a player will stay webbed.
         */
        protected const int WebbedDurationLength = 3000;
        
        // flyweight player-specific durations
        protected class Durations
	    {
            [Flyweights]
		    public IDuration 
			    BetweenMoves,
                BetweenFires,
                BetweenSwings,
                BetweenClimbs,
                BetweenGunSwitches,
			    BetweenStalagtiteHits,
			    BetweenSlimePoolHits,
			    BetweenSpikesHits,
			    BetweenJumpMoves,
                BetweenJumpMomentumMoves,
                BetweenWhirlwindedMoves,
                BetweenWhirlwindedVerticalMoves,
			    WhirlwindEffect;

            [Flyweights]
		    public IFallDuration 
			    BetweenFallMoves,
                BetweenSlides;

            private readonly IClock clock;

            public Durations(IClock clock)
            {
                this.clock = clock;
			    BetweenMoves = CreateDuration(4);
                BetweenFires = CreateDuration(140);
                BetweenSwings = CreateDuration(250);
                BetweenSlides = DunjaxFactory.CreateFallDuration(clock);
                BetweenSlides.SlowdownFactor = 2;
                BetweenClimbs = CreateDuration(6);
                BetweenGunSwitches = CreateDuration(350);
                BetweenStalagtiteHits = CreateDuration(300);
                BetweenSlimePoolHits = CreateDuration(700);
                BetweenSpikesHits = CreateDuration(1000);
                BetweenFallMoves = DunjaxFactory.CreateFallDuration(clock);
                BetweenJumpMoves = CreateDuration(6);
                BetweenJumpMomentumMoves = CreateDuration((int)BetweenMoves.Length);
                BetweenWhirlwindedMoves = CreateDuration(3);
                BetweenWhirlwindedVerticalMoves = CreateDuration(2);
                WhirlwindEffect = CreateDuration(1500);
            }

            [Shorthand]
            private IDuration CreateDuration(int length)
            {
                return DunjaxFactory.CreateDuration(clock, length);
            }
        }

        /**
         * The maximum height a jump by this player can reach.
         */
        protected readonly int MaxJumpHeight = 6 * UnitsPerTenFeet;

        /**
         * The moves (or, attempts to move) executed so far during this player's 
         * current jump.
         */
        protected int jumpMovesSoFar;

        /**
	     * To which side (if any) this player currently possesses jump momentum, 
	     * which will horizontally propel his airborne movement.
	     */
        protected Side jumpMomentumSide = Side.NeitherSide;

        [Implementation]
        public int KeysHeld { get { return keysHeld; } }
        protected int keysHeld;

        /**
         * Whether this player possesses a grapeshot-gun, and therefore may 
         * employ graphshot-ammo.
         */
        protected bool hasGrapeshotGun;
        
        /**
         * See parent class.  Specifies player-specific frame-sequences.
         * 
         * This is public as there are FrameSequence tests that scrutinize some of the 
         * more interesting members of this class.
         */
        public class PlayerFrameSequencesGroup : BeingFrameSequencesGroup
        {
            public IFrameSequence 
                Jump, MidJump, Climb, Slide, Land, Swing, 
        	    Fire, FireUp, FireDown, FireOnLadder, 
        	    FireUpOnLadder, FireDownOnLadder, FireOnSlide,
        	    FireUpOnSlide, Whirlwinded, Webbed;

            [Singleton]
            public static PlayerFrameSequencesGroup Singleton = 
                new PlayerFrameSequencesGroup();

	        [Constructor]
            private PlayerFrameSequencesGroup()
	        {
		        Move = CreateFrameSequence("move");
		        Still = CreateFrameSequence("still", images:new [] {Move[0].Image});
		        Turn = CreateFrameSequence("turn");
		        Death = CreateFrameSequence("death", priority:FramePriority.BeingDeath);
                Hit = CreateFrameSequence("hit", priority:FramePriority.BeingHit);
                Carcass = CreateFrameSequence("carcass", true, FramePriority.BeingDeath);
                Jump = CreateFrameSequence("jump");
                MidJump = CreateFrameSequence("midJump", images:new[] { Jump[2].Image });
                Climb = CreateFrameSequence(
                    "climb", false, repeatedFrames:new [] {-1, -1, -1, 1});
                Slide = CreateFrameSequence("slide");
                Land = CreateFrameSequence("land");
                Swing = CreateFrameSequence("swing", priority:FramePriority.BeingHit);
                Swing.SetDurationOfAllFrames(40);
                Fire = CreateFrameSequence("fire");
                FireUp = CreateFrameSequence("fireUp");
                FireDown = CreateFrameSequence("fireDown");
                FireUp[0].Duration = FireUp.Mirror[0].Duration =
                    FireDown[0].Duration = FireDown.Mirror[0].Duration = 500;
                FireOnLadder = CreateFrameSequence(
                    "fireOnLadder", repeatedFrames:new [] { -1, -1, -1, 1 });
                FireUpOnLadder = CreateFrameSequence(
                    "fireUpOnLadder", false, repeatedFrames:new [] { -1, -1, -1, 1 });
                FireDownOnLadder = CreateFrameSequence(
                    "fireDownOnLadder", false, repeatedFrames:new [] { -1, -1, -1, 1 });
                FireOnSlide = Fire;
                FireUpOnSlide = CreateFrameSequence("fireUpOnSlide");
                Whirlwinded = CreateFrameSequence(
                    "whirlwinded", images:new [] { Move[0].Image });

                Webbed = CreateFrameSequence("webbed", priority:FramePriority.BeingWebbed);
                Webbed[Webbed.NumFrames - 1].Duration = 
                    Webbed.Mirror[Webbed.NumFrames - 1].Duration = 3000;
            }

            [OverriddenValue]
            protected override string ImagesPath { get {return "entities/player/";} }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "player";} }
        }

        [OverriddenValue]
        public PlayerFrameSequencesGroup PlayerFrameSequences 
            { get { return PlayerFrameSequencesGroup.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return PlayerFrameSequences; } }

        [OverriddenValue]
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly BeingSoundsGroup Sounds = 
            new BeingSoundsGroup {
                Death = AllSounds.PlayerDeath,
                Hit = AllSounds.PlayerHit
            };

        [Constructor]
        public Player(IClock clock) : base(clock)
        {
            FacingLeft = false;

            durations = new Durations(clock);

            pulseAmmo = new Ammo.Pulse(clock);
            pulseAmmo.Add(300);
            grapeshotAmmo = new Ammo.Grapeshot(clock);
            ammoInUse = pulseAmmo;
        }

        [OverriddenValue]
        protected override int StartingHealth { get{ return 100; } }

        [OverriddenValue]
        protected override float IntervalUntilNextAction
        {
            // return the shortest duration this player uses
            get { return durations.BetweenWhirlwindedVerticalMoves.Length; }
        }

        /**
         * Returns whether this player can currently move along the ground in 
         * the given direction.
         */
        protected bool CanMove(bool toLeft)
        {
            // this player can't move while sliding, webbed, whirlwinded, or swinging
            // its melee weapon
            if (InState(PlayerState.Sliding)
                || InState(PlayerState.Webbed)
                || Whirlwindeding
                || InState(PlayerState.Swinging)) return false;

            // if this player has jump-momentum to the given direction, we don't
            // want him moving to that direction here, also
            if (jumpMomentumSide == (toLeft ? Side.Left : Side.Right)) return false;

            // if it hasn't been long enough since the last move, don't 
            // move this time
            if (!durations.BetweenMoves.DoneOnly) return false;
            
            return true;
        }
            
        /**
         * Tries to move this player one increment in the given direction.
         */
        protected void TryToMove(bool toLeft)
        {
            if (IsPassibleToSide(toLeft ? Side.Left : Side.Right, 
                ImpassableEntitiesSet.BeingsSet)) Move(toLeft);
                
            if (!IsTurningToSide(toLeft)) DecideStateAfterMoveAttempt(toLeft);

            FacingLeft = toLeft;

            CheckToOpenDoor();
            
            durations.BetweenMoves.Start();
        }
        
        /**
         * Returns whether this player is currently turning to the given direction.
         */
        protected bool IsTurningToSide(bool toLeft)
        {
            return InState(BeingState.Turning) && toLeft == FacingLeft;
        }
        
        /**
         * Assigns this player its state after (presumedly) having just attempted 
         * to move in the given direction.
         */
        protected void DecideStateAfterMoveAttempt(bool toLeft)
        {
            // if this player is jumping
            if (InState(PlayerState.Jumping)) {
                // if the move will change his facing to opposite that of the 
                // jump animation, switch to the mirror of that animation
                if (toLeft != FacingLeft) 
                    AddAnimation(PlayerFrameSequences.Jump.ThisOrMirror(toLeft));
            }

            // else, if this player isn't facing the direction just moved,
            // this player is now turning
            else if (toLeft != FacingLeft) AddState(BeingState.Turning);

            // otherwise, this player is now moving
            else AddState(BeingState.Moving);
        }
        
	    /**
         * Moves this player one increment in the given direction.
         */
        protected void Move(bool toLeft)
        {
            // move this player one increment in the given direction
            AddToLocation((short) (toLeft ? -1 : 1), 0);

            // if there's solid ground under the player's new location,
            // play this player's foot-movement sound
            if (!Map.IsFallThroughable(BottomCenter.Add(0, 1))) 
                AllSounds.PlayerMove.Play();
            
            // if this player has jump momentum opposed to the given direction,
            // end that momentum
            if ((!toLeft && jumpMomentumSide == Side.Left) 
                || (toLeft && jumpMomentumSide == Side.Right))
                jumpMomentumSide = Side.NeitherSide;
        }
        
        /**
	     * Checks to see if there is a closed door to the side of this player
	     * that he is facing, and if so, tries to open it.
	     */
        protected void CheckToOpenDoor()
        {
            var location = Center.SetX((short) (FacingLeft ? LeftX - 1 : RightX + 1));
            if (IsOpenableDoorAt(location)) OpenDoorAt(location);
        }
        
        /**
         * Returns whether there is a door at the given location that this player
         * can open.
         */
        protected bool IsOpenableDoorAt(IPoint location)
        {
            var feature = Map.GetTerrainFeatureAt(location); 
            return feature == TerrainFeature.ClosedDoor ||
                (feature == TerrainFeature.LockedDoor && keysHeld > 0);
        }
        
        /**
         * Opens the (presumed) door at the given location.
         */
        protected void OpenDoorAt(IPoint location)
        {
            // if the door is locked, use one of this player's keys
            if (Map.GetTerrainFeatureAt(location) == TerrainFeature.LockedDoor) keysHeld--;

            // change the closed door to an open door
            Map.SetTerrainFeatureAt(location, TerrainFeature.OpenDoor);

            AllSounds.Door.Play();
        }

        /**
         * Returns whether this player can currently start a jump towards the 
         * given side, or straight up if neither side is specified.
         */
        protected bool CanJump()
        {
            // this player may not jump when it's falling, landing, already in a jump,
            // webbed, or on a ladder
            if (InState(PlayerState.Falling) 
                || InState(PlayerState.Landing) || InState(PlayerState.Jumping)
                || InState(PlayerState.Webbed) || IsOnLadder(false))
                return false;

            // this player may not jump when it's sliding with something 
            // impassible above it (so it cannot easily jump back up tight slides)
            if (InState(PlayerState.Sliding) 
                && (!Map.IsPassible(TopLeft.Add(0, (short)-(Height + 4)))
                    || !Map.IsPassible(TopRight.Add(0, (short)-(Height + 4)))))
                return false;

            // return whether there's solid terrain under this player at either 
            // its left or right sides
            return !Map.IsFallThroughable(BottomLeft.Add(0, 1))
        	    || !Map.IsFallThroughable(BottomRight.Add(0, 1));
        }
        
        /**
         * Starts this player on a jump to the given side, or straight up if 
         * neither side is specified.
         */
        protected void Jump(Side toSide)
        {
            // start this player on the jump
            AddState(PlayerState.Jumping);
            jumpMovesSoFar = 0;
            jumpMomentumSide = toSide;

            AllSounds.PlayerJump.Play();
        }

        /**
         * Returns whether this player can climb or descend (according to the 
         * given value) the ladder it's presumed to be on.
         */
        protected bool CanClimbOrDescend(bool climb)
        {
            if (InState(PlayerState.Webbed)) return false;

            if (!durations.BetweenClimbs.Done) return false;

            // return whether this player is on a ladder, and above (for a climb) 
            // or below (for a descend) him is passible on both his sides
            var test = GeometryFactory.CreatePoint(
                0, (short) (climb ? TopY - 1: BottomY + 1));
            var beingsSet = ImpassableEntitiesSet.BeingsSet;
            var result = 
                IsOnLadder(!climb) 
                && Map.IsPassible(test.SetX((short) (LeftX + 4)), beingsSet)
                && Map.IsPassible(test.SetX((short) (RightX - 4)), beingsSet);
            GeometryFactory.ReclaimPoint(test);
            return result;
        }

        /**
         * Has this player climb or descend (according to the given value) one 
         * increment on the ladder it's presumed to be on.
         */
        protected void ClimbOrDescend(bool climb)
        {
            // move this player upwards (for a climb) or downwards 
    	    // (for a descend) one increment
            AddToLocation(0, (short) (climb ? -1 : 1));

            AddState(PlayerState.Climbing);

            AllSounds.PlayerClimb.Play();
        }

        /**
         * Returns whether any part of the middle vertical line through this 
         * player is on a ladder.
         *
         * @param orAbove       Whether a ladder just beneath this player counts.
         */
        protected bool IsOnLadder(bool orAbove)
        {
            // return whether the terrain feature at this player's top-center,
    	    // center, or bottom-center (or just below it, if orAbove is specified) 
            // is a ladder
            const TerrainFeature ladder = TerrainFeature.Ladder;
            return Map.GetTerrainFeatureAt(TopCenter) == ladder
    		    || Map.GetTerrainFeatureAt(Center) == ladder
    		    || Map.GetTerrainFeatureAt(BottomCenter.Add(0, (short) (orAbove ? 1 : 0))) 
    		        == ladder;
        }

        /**
         * Returns whether this player can fire its weapon (using its current ammo). 
         */
        protected bool CanFire()
        {
            // this player may not fire while swinging its melee weapon, or while webbed
            if (InState(PlayerState.Swinging) || InState(PlayerState.Webbed)) return false;
        	
    	    if (!durations.BetweenFires.Done) return false;

            // if this player is out of ammo, it cannot fire
            if (ammoInUse.Amount <= 0) return false;
            
            return true;
        }
        
        /**
         * Has this player fire its weapon (using its current) ammo to the 
         * given vertical side, or horizontally if neither side is specified.
         */
        protected void Fire(VerticalSide toSide)
        {
            // signal this player's current ammo that it's been fired, so it may 
            // create the appropriate shot(s) at the firing spot
            ammoInUse.Fired(Map, GetFireSpot(toSide), 
                FacingLeft ? Side.Left : Side.Right, toSide);
            
            DecideOnPostFireFrameSequence(toSide);
        }

        /**
         * Returns the location on this player from whence shots currently will be 
         * fired toward the given vertical side (or horizontally, if neither side 
         * is specified).
         */
        protected IPoint GetFireSpot(VerticalSide toSide)
        {
            // if this player is firing upwards or downwards, the shot needs to 
            // start out just above or below the player's gun-tip
            var fireSpot = Center;
            if (toSide == VerticalSide.Top || toSide == VerticalSide.Bottom) 
                fireSpot.Set(
                    (short) (Center.X + ((FacingLeft && !IsOnLadder(false)) ? -2 : 4)),
                    (short) ((toSide == VerticalSide.Top) ? TopY - 1 : BottomY + 1));

            // otherwise, the shot needs to start out just to the side of the 
            // player's gun-tip
            else 
                fireSpot.Set(
                    (short) (FacingLeft ? LeftX - 1 : RightX + 1),
                    (short)(Center.Y - (InState(PlayerState.Jumping) ? 8 : 3)));
            
            return fireSpot;
        }
        
        /**
         * Chooses an animation for this player's firing of a shot, 
         * based on its current state.
         */
        protected void DecideOnPostFireFrameSequence(VerticalSide fireSide)
        {
            // if this player is jumping and not firing up or down,
            // its current animation will do
            bool firingUp = (fireSide == VerticalSide.Top), 
                firingDown = (fireSide == VerticalSide.Bottom);
            IFrameSequence sequence = null;
            var sequences = PlayerFrameSequencesGroup.Singleton;
            if (InState(PlayerState.Jumping) && fireSide == VerticalSide.NeitherVerticalSide) {}

            // else, if this player is sliding
            else if (InState(PlayerState.Sliding)) {
                // use the appropriate fire-while-sliding animation
                sequence = firingUp ? 
            	    sequences.FireUpOnSlide.ThisOrMirror(FacingLeft) :
            	    sequences.FireOnSlide.ThisOrMirror(FacingLeft);
            }

            // else, if this player is on a ladder
            else if (IsOnLadder(false)) {
                // use the appropriate fire-while-on-a-ladder animation
                if (firingUp) sequence = sequences.FireUpOnLadder;
                else if (firingDown) sequence = sequences.FireDownOnLadder;
                else sequence = sequences.FireOnLadder.ThisOrMirror(FacingLeft); 
            }

            // otherwise
            else {
                // use the appropriate normal fire animation
                if (firingUp) sequence = sequences.FireUp.ThisOrMirror(FacingLeft);
                else if (firingDown) sequence =	sequences.FireDown.ThisOrMirror(FacingLeft);
                else sequence = sequences.Fire.ThisOrMirror(FacingLeft);
            }

            // if an appropriate animation was detm'd above, use it
            if (sequence != null) AddAnimation(sequence, true);
        }

        /**
         * Checks to see if a fall by this player is currently possible, and if 
         * it is, has this player fall one increment downwards.
         * If the fall movement is blocked, then if the player was falling,
         * it is made to start landing.
         */
        protected void CheckForFall()
        {
            if (CanFall()) Fall();

            else if (InState(PlayerState.Falling) 
                && cantFallDueToBlocked && !Dead && !Whirlwindeding) Land();
        }

        /**
         * Whether the last call to CanFall() returned false because the terrain
         * under the player wasn't fall-throughable.
         */
        protected bool cantFallDueToBlocked;
        
        /**
         * Returns whether this player is currently in a state and location to
         * fall.
         */
        protected bool CanFall()
        {
            cantFallDueToBlocked = false;
            
            // this player cannot fall when in certain states
            if (IsOnLadder(false) || InState(PlayerState.Jumping) 
                || InState(PlayerState.Sliding)
                || Whirlwindeding) return false;

            // if the area just beneath this player is not fall-throughable 
            var test = BottomCenter.Add(0, 1);
            if (!Map.IsFallThroughable(test.SetX(LeftX))
                || !Map.IsFallThroughable(test.SetX(RightX))) {
                // no fall is allowed, for that specific reason
                cantFallDueToBlocked = true;
                return false;
            }

            // if the time since this player's last fall hasn't been long enough,
            // and this player has previously been falling, it can't fall right now;
            // if this player hasn't been falling, then we want the check for a 
            // fall to happen for sure, in case the player is moving over a false floor
            if (!durations.BetweenFallMoves.Done && InState(PlayerState.Falling))
                return false;

            return true;
        }

        /**
         * Has this player fall one increment downward.
         */
        protected void Fall()
        {
            // move this player downward one increment
            AddToLocation(0, 1);
            
            // if this player is not already considered to be falling
            if (!InState(PlayerState.Falling)) {
                // it is falling, now
                AddState(PlayerState.Falling);
                durations.BetweenFallMoves.FallStarted();
            }
        }
        
        /**
         * Has this player start to land after having fallen.
         */
        protected void Land()
        {
            // start this player landing
            AddState(PlayerState.Landing);
            jumpMomentumSide = Side.NeitherSide;
            AllSounds.PlayerLand.Play();
        }
        
        /**
         * Checks to see if this player can (and should) do a jump-momentum move, 
         * and if so, attempts to do so.
         */
        protected void CheckForJumpMomentumMove()
        {
            // if this player has no jump-momentum due, then do nothing
            if (!HasJumpMomentumDue()) return;
            
            // if it's passible just to the side of this player in the 
            // direction of its jump-momentum, do the move
            if (IsPassibleToSide(jumpMomentumSide, ImpassableEntitiesSet.BeingsSet)) {
                AddToLocation((short) (jumpMomentumSide == Side.Left ? -1 : 1), 0);
            }
            
            // otherwise, the jump-momentum is over
            else jumpMomentumSide = Side.NeitherSide;
        }
        
        /**
         * Returns whether this player has jump momentum whose next move is ready
         * to take place.
         */
        protected bool HasJumpMomentumDue()
        {
            return jumpMomentumSide != Side.NeitherSide 
                && durations.BetweenJumpMomentumMoves.Done;
        }
        
        /**
         * Checks to see if this player can (and should) do a whirlwinded-move,
         * and if so, attempts to do so.
         */
        protected void CheckForWhirlwindedMove()
        {
            // if this player has no whirlwinded-move due, then do nothing
            if (!HasWhirlwindedMoveDue()) return;
            
            // if it's passible just to the side of this player in the 
            // direction of its whirlwinded-ness
            if (IsPassibleToSide(whirlwindedSide, ImpassableEntitiesSet.NoneSet)) {
                // do the move to that side
                AddToLocation((short) (whirlwindedSide == Side.Left ? -1 : 1), 0);
            }
            
            // otherwise
            else {
                // the whirlwinded-ness is over, and the player takes damage
                // from the impact
                EndWhirlwinded();
                AllSounds.ShotImpact.Play();
                Struck(Center, whirlwindImpactDamage);
            }
        }
        
        /**
         * Returns whether this player has a whirlwinded move ready to take place.
         */
        protected bool HasWhirlwindedMoveDue()
        {
            return Whirlwindeding && durations.BetweenWhirlwindedMoves.Done;
        }

        /**
         * Checks to see if this player can (and should) do a whirlwinded 
         * vertical-move, and if so, attempts to do so.
         */
        protected void CheckForWhirlwindedVerticalMove()
        {
            // if this player has no whirlwinded vertical-move due, then do nothing
            if (!HasWhirlwindedVerticalMoveDue()) return;

            // if it's passible just above this player
            if (IsPassibleToVerticalSide(VerticalSide.Top, ImpassableEntitiesSet.NoneSet)) {
                // move this player up one increment
                AddToLocation(0, -1);
            }

            // otherwise
            else {
                // the player's vertical movement is done for this 
                // instance of being whirlwinded
                whirlwindedVerticalMoveEnded = true;

                // the player takes damage from the impact
                AllSounds.ShotImpact.Play();
                Struck(Center, whirlwindImpactDamage);
            }
        }

        /**
         * Returns whether this player has a whirlwinded vertical-move 
         * ready to take place.
         */
        protected bool HasWhirlwindedVerticalMoveDue()
        {
            return Whirlwindeding 
                && !whirlwindedVerticalMoveEnded 
                && durations.BetweenWhirlwindedVerticalMoves.Done;
        }

        /**
         * Checks to see if this player's current situation indicates it should 
         * slide, and if so, slides this player.
         */
        protected void CheckForSlide()
        {
            if (!CanSlide()) return;

            if (CanSlide(Side.Left)) Slide(Side.Left);
            
            else if (CanSlide(Side.Right)) Slide(Side.Right);

            else if (InState(PlayerState.Sliding)) EndSlide();

            // if this player isn't sliding, make sure the sliding sound isn't playing
            if (!InState(PlayerState.Sliding)) AllSounds.PlayerSlide.Stop();
        }
        
        /**
         * Returns whether this player is currently in a state to slide, not
         * considering what direction to slide in.
         */
        protected bool CanSlide()
        {
            // this player cannot slide while jumping
            if (InState(PlayerState.Jumping)) return false;

            return durations.BetweenSlides.Done;
        }
        
        /**
         * Returns whether this player is currently in a state to slide in the 
         * given direction.
         */
        protected bool CanSlide(Side toSide)
        {
            // return whether there is a slide below this player on the side of the 
            // given slide direction, along with whether the area just below this
            // player is passible (where 6 seems to be the magical number to add,
            // as 4 gets the player stuck on the end of slides before a dropoff)
            return IsSlideUnderneath(toSide == Side.Left)
                && Map.IsPassible(BottomLeft.Add(5, 1))
                && Map.IsPassible(BottomRight.Add(-5, 1));
        }
        
        /**
         * Returns whether there is a slide of the given direction underneath this
         * player.
         */
        protected bool IsSlideUnderneath(bool toLeft)
        {
            var slide = toLeft ? TerrainFeature.SlideLeft : TerrainFeature.SlideRight;
            var location = toLeft ? BottomRight.Add(0, 1) : BottomLeft.Add(0, 1);
            return Map.GetTerrainFeatureAt(location) == slide;
        }

        /**
         * Has this player slide one increment in the given direction.
         */
        protected void Slide(Side toSide)
        {
            // move this player one increment in the direction that the slide goes
            bool toLeft = (toSide == Side.Left);
            AddToLocation((short) (toLeft ? -1 : 1), 1);

            // this player's facing is matched to the direction of the slide
            FacingLeft = toLeft;

            // this player is now sliding
            if (!InState(PlayerState.Sliding)) 
                durations.BetweenSlides.FallStarted();
            AddState(PlayerState.Sliding);
            AllSounds.PlayerSlide.Play();
        }
        
        /**
         * Ends this player's presumed current slide.
         */
        protected void EndSlide()
        {
            RemoveState(PlayerState.Sliding);
            durations.BetweenSlides.MakeDone();
        }

        /**
         * Tries to have this player perform a jump-move (that is, an upwards
         * movement as part of a presumed jump).
         *
         * @param jumpForceStillBeingAdded  
         *      If this isn't true, the jump will continue only to a certain 
         *      minimum jump height.  If it is true, which means the user is
         *      still pressing the jump button, the jump will continue while
         *      unimpeded up to the maximum jump height.
         */
        protected void TryToJumpMove(bool jumpForceStillBeingAdded)
        {
            if (ShouldShowMidJumpSequence()) 
                AddAnimation(PlayerFrameSequences.MidJump.ThisOrMirror(FacingLeft), true);

            if (!IsJumpMoveBlocked()) AddToLocation(0, -1);

            else JumpMoveBlocked(jumpForceStillBeingAdded);
            
            // record that this jump has lasted one more increment
            jumpMovesSoFar++;

            // if this player is still jumping, play its thrust sound
            if (InState(PlayerState.Jumping)) AllSounds.PlayerThrust.Play();
        }
        
        /**
         * Returns whether this player is currently in a state to execute a jump-move.
         */
        protected bool CanJumpMove()
        {
            return InState(PlayerState.Jumping) && durations.BetweenJumpMoves.Done;
        }
        
        /**
         * Returns whether this player is currently jumping, but also on a ladder.
         */
        protected bool IsOnLadderDuringJump()
        {
            return InState(PlayerState.Jumping) && IsOnLadder(false);
        }
        
        /**
         * Ends this player's current jump to have it climb the ladder it's presumed
         * to be on.
         */
        protected void EndJumpToClimbOnLadder()
        {
            EndJump();
            AddState(PlayerState.Climbing);
       }
        
        /**
         * Returns whether this player's current presumed jump has reached its
         * maximum height.  
         * 
         * @param jumpForceStillBeingAdded      See TryToJumpMove().
         */
        protected bool HasMaxJumpHeightBeenReached(bool jumpForceStillBeingAdded)
        {
            // return whether this player is jumping, and has reached the max 
            // height for this jump, or the user is no longer pressing the jump key
            return InState(PlayerState.Jumping) &&
                (jumpMovesSoFar >= MaxJumpHeight || !jumpForceStillBeingAdded);
        }
        
        /**
         * Returns whether, as part of this player's presumed current jump-move, 
         * the player should display its mid-jump frame sequence.  It should do so
         * when it has a non-jump animation which has just finished, so it may 
         * look like it is still jumping.
         */
        protected bool ShouldShowMidJumpSequence()
        {
            if (currentAnimation == null) return false;

            var sequence = currentAnimation.FrameSequence;
            return !sequence.IsSameOrMirror(PlayerFrameSequences.Jump)
                && !sequence.IsSameOrMirror(PlayerFrameSequences.MidJump)
                && currentAnimation.Done;
        }
        
        /**
         * Returns whether this player's current presumed jump-move is blocked
         * by something impassible in the map.
         */
        protected bool IsJumpMoveBlocked()
        {
            var beingSet = ImpassableEntitiesSet.BeingsSet;
            const int jumpFudgeMargin = 1;
            return !Map.IsPassible(TopLeft.Add(jumpFudgeMargin, -1), beingSet)
                || !Map.IsPassible(TopRight.Add(-jumpFudgeMargin, -1), beingSet);
        }

        /**
         * Informs this player that it's current presumed jump-move is blocked
         * by something impassible in the map.
         */
        protected void JumpMoveBlocked(bool jumpForceStillBeingAdded)
        {
            // if jump force is no longer being added (since, if it was, the 
            // jump-thrust would keep him aloft), end this jump
            if (!jumpForceStillBeingAdded) EndJump();

            // hitting something ends this player's jump momentum
            jumpMomentumSide = Side.NeitherSide;
        }
        
        /**
         * Ends this player's current jump.
         */
        protected void EndJump() 
        {
            // this player is no longer jumping
            RemoveState(PlayerState.Jumping);

            // switch the player to a still animation, because the potentially 
            // ensuing fall state carries no animation, and we don't want this
            // player to look like it's still jumping
            AddAnimation(PlayerFrameSequences.Still.ThisOrMirror(FacingLeft), true);
        }

        /**
         * Retewens whether this player is in a state where it can start the 
         * swinging of its melee weapon.
         */
        protected bool CanSwingMeleeWeapon()
        {
            if (InState(PlayerState.Swinging) || InState(PlayerState.Webbed)) 
                return false;

            return durations.BetweenSwings.Done;
        }
        
        /**
         * Has this player begin to start the swinging of its melee weapon.
         */
        protected void SwingMeleeWeapon()
        {
            // this player is now swinging
            AddState(PlayerState.Swinging);
        }

	    /**
	     * Returns whether this player is in a state to switch to using the next 
	     * gun in its internal sequence of guns.
	     */
	    protected bool CanSwitchGuns()
	    {
            return durations.BetweenGunSwitches.Done;
	    }
    	
        /**
         * Has this player to switch to using the next gun (if there is one) in its 
         * internal sequence of guns.
         */
        protected void SwitchGuns()
        {
            // if the current ammo being used is the pulse-ammo, and this player
            // has a graphshot-gun, use the graphshot-ammo
            IAmmo oldAmmo = ammoInUse;
            if (ammoInUse == pulseAmmo && hasGrapeshotGun) ammoInUse = grapeshotAmmo;
            
            // otherwise, use the pulse-ammo
            else ammoInUse = pulseAmmo;
            
            // if what ammo is in use changed above, play a switch sound
            if (ammoInUse != oldAmmo) AllSounds.PlayerSwitchGuns.Play();
	    }

        [Implementation]
	    public void Webbed()
	    {
		    // if this player is already webbed, do nothing
		    if (InState(PlayerState.Webbed)) return;

		    // start this player being webbed
		    AddState(PlayerState.Webbed);
		    AllSounds.PlayerWebbed.Play();
	    }
    	
        [Implementation]
        public event EventHandler ReachedExit;

        /**
	     * Checks to see what terrain feature this player is currently on, 
	     * and administers its effects.
	     */
	    protected void CheckForTerrainFeature()
	    {
		    // if this player is jumping into a stalagmite, and it's been long
		    // enough since the last stalagmite hit he's taken
		    var feature = Map.GetTerrainFeatureAt(Center);
		    if (InState(PlayerState.Jumping)
			    && feature == TerrainFeature.Stalagmite
			    && durations.BetweenStalagtiteHits.Done) {
			    // the stalagmite causes damage
			    Struck(Center, 4);
		    }

		    // if this player has reached the exit cave
		    if (feature == TerrainFeature.ExitCave) {
			    // fire an event, so that the game knows to change maps
		        ReachedExit(this, null);
			    return;
		    }

		    // if this player is in a slime pool, and it's been long
		    // enough since the last slime damage he's taken
		    if (feature == TerrainFeature.SlimePool
			    && durations.BetweenSlimePoolHits.Done) {
			    // the slime causes damage
			    Struck(Center, 16);
		    }

		    // if this player is in spikes, and it's been long
		    // enough since the last spikes damage he's taken
		    if (feature == TerrainFeature.Spikes
			    && durations.BetweenSpikesHits.Done) {
			    // the spikes cause damage
		        Struck(Center, 12);
		    }
	    }
    	
	    [HookOverride]
        protected override void BeforeSurvivedStrike()
        {
	        if (InState(PlayerState.Jumping)) EndJump();
        }

        /**
	     * Checks to see if this player is on an item, and if so, takes the item
	     * and confers its benefits on this player.
	     */
	    protected void CheckForItem()
	    {
            // if there is no item at the player's location, then there 
            // is nothing to do
            var item = Map.GetItemInBounds(Bounds);
            if (item == null) return;

            AcceptItemEffects(item);
            
            item.Consumed();
	    }
    	
	    /**
	     * Confers the effects of the given item upon this player.
	     */
	    protected void AcceptItemEffects(IItem item)
	    {
            var type = item.ItemType; 
            if (type == ItemType.PulseAmmo) pulseAmmo.Add(30);
            else if (type == ItemType.PulseAmmoBig) {
                pulseAmmo.Add(100);
                while (ammoInUse != pulseAmmo) SwitchGuns();
            }
            else if (type == ItemType.GrapeshotAmmo) grapeshotAmmo.Add(30);
            else if (type == ItemType.GrapeshotGun) {
                grapeshotAmmo.Add(100);
                hasGrapeshotGun = true;
                while (ammoInUse != grapeshotAmmo) SwitchGuns();
            }
            else if (type == ItemType.ShieldPowerUp) health += 50;
            else if (type == ItemType.ShieldPowerPack) health += 200;
            else if (type == ItemType.Key) keysHeld++;
	    }

        [HookOverride]
        protected override void ActAfterAnimationChecked_AsBeingSubtype()
	    {
            ActAsPlayer();
        }

        /**
         * Executes this player's actions for the current game turn.
         */
        protected void ActAsPlayer()
        {
             if (Dead) {
                CheckForSlide();
                return;
            }
            
            // if moving left or right is requested, try to do that
            bool leftSignaled = userInput.IsSignaled(Command.Left),
                rightSignaled = userInput.IsSignaled(Command.Right);
            if (leftSignaled || rightSignaled) {
                if (CanMove(leftSignaled)) TryToMove(leftSignaled);
            }
            
            // if climbing or descending is requested, try to do that
            bool upSignalled = userInput.IsSignaled(Command.Up),
                downSignalled = userInput.IsSignaled(Command.Down);
            if (upSignalled || downSignalled) {
                if (CanClimbOrDescend(upSignalled)) ClimbOrDescend(upSignalled);
            }
            
            // if switching of guns is requested, try to do that
            if (userInput.IsSignaled(Command.SwitchGuns)) { 
                if (CanSwitchGuns()) SwitchGuns();
            }

            // if swinging of the melee weapon is requested, try to do that
            if (userInput.IsSignaled(Command.Swing)) {
                if (CanSwingMeleeWeapon()) SwingMeleeWeapon();
            }

            // a slide should come before a fall, because a sliding player can't fall,
            // while a falling player can slide
            CheckForSlide();
            CheckForFall();

            // if a jump is requested, try to jump
            bool jumpSignaled = userInput.IsSignaled(Command.Jump);
            if (jumpSignaled) {
                Side side = leftSignaled ? Side.Left :
                    (rightSignaled ? Side.Right : Side.NeitherSide);
                if (CanJump()) Jump(side);
            }

            // if a firing is requested, try to fire
            if (userInput.IsSignaled(Command.Fire)) 
                if (CanFire()) Fire(upSignalled ? VerticalSide.Top : 
                    (downSignalled ? VerticalSide.Bottom : VerticalSide.NeitherVerticalSide));
                else CheckForOutOfAmmoSound();
            
            if (IsOnLadderDuringJump()) EndJumpToClimbOnLadder();
            if (HasMaxJumpHeightBeenReached(jumpSignaled)) EndJump();
            if (CanJumpMove()) TryToJumpMove(jumpSignaled);
            CheckForJumpMomentumMove();
            CheckForWhirlwindedMove();
            CheckForWhirlwindedVerticalMove();
            if (ShouldNoLongerBeWhirlwinded()) EndWhirlwinded();
            
            CheckForItem();
            CheckForTerrainFeature();
            CheckForSpecialEntity();
        }

        [Implementation]
        public void Whirlwinded(Side toSide)
	    {
            // if this player is already whirlwinded, do nothing
            if (Whirlwindeding) return;

		    // this player is now whirlwinded
		    whirlwindedSide = toSide;
            AddState(PlayerState.Whirlwinded);
            whirlwindedVerticalMoveEnded = false; 
		    durations.WhirlwindEffect.Start();

		    AllSounds.PlayerWhirlwinded.Play();
	    }
    	
        /**
         * Returns whether this player should no longer be whirlwinded, including
         * taking into account whether the player is whirlwinded to begin with.
         */
        protected bool ShouldNoLongerBeWhirlwinded()
        {
            return Whirlwindeding && durations.WhirlwindEffect.Done;
        }

        /**
         * A shorthand way to determine if this player is current under the effects
         * of being whirlwinded.
         */
        [Property]
        private bool Whirlwindeding { get { return whirlwindedSide != Side.NeitherSide; } }

        /**
         * Ends this player's presumed current instance of being whirlwinded.
         */ 
        private void EndWhirlwinded()
        {
            whirlwindedSide = Side.NeitherSide;
            RemoveState(PlayerState.Whirlwinded);
        }

        [Implementation]
        public new event EventHandler Died;

        [HookOverride]
        protected override void BeforeAnimationDoneAsBeing(IFrameSequence sequence)
	    {
            var sequences = PlayerFrameSequences;

            if (sequence.IsSameOrMirror(sequences.Swing)) MeleeWeaponSwung();
            
            else if (sequence.IsSameOrMirror(sequences.Land)) RemoveState(PlayerState.Landing);

            else if (sequence.IsSameOrMirror(sequences.Webbed)) {
                RemoveState(PlayerState.Webbed);

                // this player is now moving, to make it look like
                // it was working hard to escape the web
                AddState(BeingState.Moving);
            }

            // if this player just finished dying, report that fact
            if (sequence.IsSameOrMirror(sequences.Death)) Died(this, null);
	    }
        
        /**
         * Informs this player that it has just finished swinging its melee weapon.
         */
        protected void MeleeWeaponSwung()
        {
            // start a halberd shot to the side of this player that's he facing
            var shot = ShotFactory.CreateShot(
                ShotType.HalberdShot, GetHalberdShotDirection(), clock);
            shot.MoveTo(GetHalberdShotLocation(shot));
            Map.AddEntity(shot);
            
            AllSounds.HalberdFire.Play();

            // this player is no longer swinging
            RemoveState(PlayerState.Swinging);

            // add a temporary still animation for the player so that the end
            // of the swing animation isn't still displayed for it
            AddAnimation(PlayerFrameSequences.Still.ThisOrMirror(FacingLeft), true);
        }
        
        /**
         * Returns the direction that a halberd shot should have when issued from
         * this player's current orientation.
         */
        protected IPoint GetHalberdShotDirection()
        {
            return GeometryFactory.CreatePoint((short) (FacingLeft ? -1 : 1), 0);
        }
        
        /**
         * Returns the location that a halberd shot should have when issued from
         * this player's current location and orientation.
         */
        protected IPoint GetHalberdShotLocation(IShot shot)
        {
            return GeometryFactory.CreatePoint(
                (short) (FacingLeft ? LeftX - shot.Width / 2 : RightX - shot.Width / 2),
                (short) (Center.Y - shot.Height / 2));        
        }

        [HookOverride]
        protected override IFrameSequence 
            GetFrameSequenceForStateAsBeingSubtype(BeingState state)
        {
            IFrameSequence sequence = null;
            var sequences = PlayerFrameSequences;
            if (state == PlayerState.Jumping) sequence = sequences.Jump;
            else if (state == PlayerState.Landing) sequence = sequences.Land;
            else if (state == PlayerState.Sliding) sequence = sequences.Slide;
            else if (state == PlayerState.Climbing) sequence = sequences.Climb;
            else if (state == PlayerState.Swinging) sequence = sequences.Swing;
            else if (state == PlayerState.Whirlwinded) sequence = sequences.Whirlwinded;
            else if (state == PlayerState.Webbed) sequence = sequences.Webbed;

            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }

        [HookOverride]
        protected override void BeforeDying()
        {
            // stop any sounds that may have been eminating from this player
            // while it was alive 
            AllSounds.PlayerThrust.Stop();
            AllSounds.PlayerSlide.Stop();
        }

        /**
         * If this player is out of ammo, play a sound to inform the user.
         */
        protected void CheckForOutOfAmmoSound()
        {
            if (ammoInUse.Amount <= 0) AllSounds.OutOfAmmo.Play();
        }

        [Implementation]
        public int ArmorLeft { get { return health; } }

        [HookOverride]
        protected override void BeforeRemoveStatesForStateAddition(
            BeingState state)
        {
            if (state == BeingState.Still) {
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Landing);
            }

            else if (state == BeingState.Moving) {
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Sliding);
            }

            else if (state == BeingState.Dying) {
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Landing);
                RemoveState(PlayerState.Webbed);
            }

            else if (state == PlayerState.Falling) {
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Landing);
            }

            else if (state == PlayerState.Jumping) {
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Landing);
            }

            else if (state == PlayerState.Sliding) {
                RemoveState(BeingState.Moving);
                RemoveState(BeingState.Turning);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Climbing);
                RemoveState(PlayerState.Landing);
            }

            else if (state == PlayerState.Climbing) {
                RemoveState(BeingState.Turning);
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Landing);
            }

            else if (state == PlayerState.Landing) {
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Climbing);
            }

            else if (state == PlayerState.Whirlwinded) {
                RemoveState(BeingState.Turning);
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Falling);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Landing);
                RemoveState(PlayerState.Climbing);
            }

            else if (state == PlayerState.Webbed) {
                RemoveState(BeingState.Turning);
                RemoveState(BeingState.Moving);
                RemoveState(PlayerState.Jumping);
                RemoveState(PlayerState.Sliding);
                RemoveState(PlayerState.Landing);
            }
        }

        [HookOverride]
        protected override void AfterRemoveState(BeingState state)
        {
            if (state == PlayerState.Jumping) AllSounds.PlayerThrust.Stop();
        }

        /**
         * Checks whether this player is contacting an entity which doesn't 
         * fall into the usual categories (e.g. beings or items), and if so,
         * takes the appropriate action for that entity. 
         */
        protected void CheckForSpecialEntity()
        {
            // if there is no special entity at the player's location, 
            // then there is nothing to do
            var entity = Map.GetAnimationEntityInBounds(Bounds);
            if (entity == null) return;

            // if the special entity is a final-room-trigger
            if (entity.AnimationEntityType 
                == AnimationEntityType.FinalRoomTrigger) {
                // signal that fact
                ReachedFinalRoomTrigger(this, null);

                // remove the trigger
                Map.RemoveEntity(entity);
            }
        }

        [Implementation]
        public event EventHandler ReachedFinalRoomTrigger;
    }
}
