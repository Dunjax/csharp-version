using Dunjax.Attributes;
using Dunjax.Entities.Shots;
using Dunjax.Geometry;
using Dunjax.Map;
using Dunjax.Sound;

namespace Dunjax.Entities.Beings.Player.Concrete
{
    public abstract class Ammo : IAmmo
    {
        /**
         * How many units of ammo this object currently represents.
         */
        protected int amount;

        /**
         * Is passed along to any shots this ammo creates.
         */
        protected IClock clock;

        [Constructor]
        public Ammo(IClock clock)
        {
            this.clock = clock;
        }

        [Implementation]
        public void Fired(IMap map, IPoint firingLocation,
    	    Side toSide, VerticalSide toVerticalSide)
        {
            amount--;

            CreateShots(map, firingLocation, toSide, toVerticalSide);
        }

        /**
         * Has this ammo create shots at the given location, of the type associated
         * with this ammo's type.
         *
         * See this.fired() for parameter descriptions.
         */
        [Hook]
        protected abstract void CreateShots(IMap map, IPoint firingLocation,
    	    Side toSide, VerticalSide toVerticalSide);

        [Implementation]
        public int Amount {get {return amount;}}

        [Implementation]
        public void Add(int amountToAdd)
        {
            amount += amountToAdd;
        }

        /**
         * Produces the player's normal shot.
         */
        public class Pulse : Ammo
        {
            [Constructor]
            public Pulse(IClock clock) : base(clock) {}

            [HookOverride]
            protected override void CreateShots(IMap map, IPoint firingLocation,
        	    Side toSide, VerticalSide toVerticalSide)
            {
                // detm the direction of the shot based on the given parameters
                var direction = GeometryFactory.CreatePoint();
                if (toVerticalSide == VerticalSide.Top) direction.Y = -1;
                else if (toVerticalSide == VerticalSide.Bottom) direction.Y = 1;
                else if (toSide == Side.Left) direction.X = -1;
                else direction.X = 1;

                // create the shot with the direction detm'd above
                var shot = ShotFactory.CreateShot(ShotType.PulseShot, direction, clock);
                
                // center the shot on the given location
                shot.CenterOn(firingLocation);
                map.AddEntity(shot);

                Sounds.PulseFire.Play();
            }
        }
        
        /**
         * Produces a three-way kind of shot.
         */
        public class Grapeshot : Ammo
        {
            [Constructor]
            public Grapeshot(IClock clock) : base(clock) { }

            [HookOverride]
            protected override void CreateShots(IMap map, IPoint firingLocation,
        	    Side toSide, VerticalSide toVerticalSide)
            {
                // for each of the three shots that will be created
                for (int i = 0; i < 3; i++) {
                    // detm the direction of this shot based on the given parameters
                    var direction = GeometryFactory.CreatePoint();
                    if (toVerticalSide == VerticalSide.Top) {
                        direction.Y = -5;
                        if (i == 1) direction.X = -1;
                        if (i == 2) direction.X = 1;
                    }
                    else if (toVerticalSide == VerticalSide.Bottom) {
                        direction.Y = 5;
                        if (i == 1) direction.X = -1;
                        if (i == 2) direction.X = 1;
                    }
                    else if (toSide == Side.Left) {
                        direction.X = -5;
                        if (i == 1) direction.Y = -1;
                        if (i == 2) direction.Y = 1;
                    }
                    else {
                        direction.X = 5;
                        if (i == 1) direction.Y = -1;
                        if (i == 2) direction.Y = 1;
                    }

                    // create the shot with the direction detm'd above
                    var shot = ShotFactory.CreateShot(
                        ShotType.GrapeshotShot, direction, clock);
                    shot.CenterOn(firingLocation);

                    // add the shot to map at the given location
                    map.AddEntity(shot);
                }

                Sounds.GrapeFire.Play();
            }
        }
    }
}