using System;
using System.Collections.Generic;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Concrete;
using Dunjax.Geometry;
using Dunjax.Sound;

namespace Dunjax.Entities.Beings.Concrete 
{
    abstract class Being : MapEntity, IBeing
    {
        /**
         * An abstraction of this being's well-being.  When this value goes below 
         * one, this being dies.
         */
	    protected int health;
         
	    /**
	     * Whether this being is currently facing left (vs. right).
	     */
        public bool FacingLeft { get; protected set; }

        /**
	     * The current being-states this being is in.
         * 
         * Note that we can't use a HashSet here, as that class is not available
         * on the Xbox.
	     */
        private readonly Dictionary<BeingState, BeingState> beingStates
            = new Dictionary<BeingState, BeingState>();

        /**
         * Returns the sounds emitted by this being. Each subclass of this class
         * will declare a BeingSoundsGroup (or derivative) instance to specify its
         * sounds, and that instance must be returned through an override of this
         * method.
         */
	    [OverridableValue]
        protected abstract BeingSoundsGroup BeingSounds { get; }

        /**
         * The different sounds issued by beings of a particular class.  Each
         * class of being must declare an instance of this type and return it 
         * through an override of BeingSounds.  
         */
        public class BeingSoundsGroup
        {
    	    public ISound Death, Hit;
        }
        
        /**
	     * The frame-sequences used to depict this being in its various
	     * states. Each subclass of this class will declare a BeingFrameSequencesGroup
	     * (or derivative) instance to specify its frame-sequences, and that
	     * instance must be returned through an override of this property.
	     */
        [OverridableValue]
        protected abstract BeingFrameSequencesGroup BeingFrameSequences { get; }

        /**
	     * The different frame-sequences used to depict the various activities of
	     * beings of a particular class. Each class of being must declare an
	     * instance of this type and return it through an override of the
	     * BeingFrameSequences property. 
	     */
        public abstract class BeingFrameSequencesGroup : FrameSequences
        {
            /**
             * The different frame-sequences a being may display.  Subtypes are free
             * to populate whichever of these fields they want.
             */
    	    public IFrameSequence Still, Move, Turn, CeilingStill, CeilingMove,
                CeilingTurn, Death, CeilingDeath, Spatter, Hit, Carcass;
        }

        /**
         * Times this being's fall as a carcass, once it dies.
         */
        private IFallDuration betweenFallsAsCarcassDuration;
        
        [Constructor]
        protected Being(IClock clock) : base(clock)
        {
            AddState(BeingState.Still);
        	
    	    health = StartingHealth;
        }
        
        [OverridableValue]
        protected abstract int StartingHealth { get; }

        /** 
        * Overridden to specify that beings are impassible.
        */
        public override bool Passible { get { return false; } }

        /**
	     * Informs this being that it has been damaged for the given amount.
	     */
        protected void OnDamaged(int amount)
        {
            health -= amount;
        	health = Math.Max(health, 0);

            if (Dead) Die();
        }

        [Property]
        public bool Dead { get {return health <= 0;} }

        [Property]
        public bool Damaged { get { return health < StartingHealth;} }

        [Implementation]
        public void Struck(IPoint location, int damage)
        {
            if (IsVulnerableAt(location)) StruckWhereVulnerable(location, damage);
        }
        
        /**
         * Informs this being that it has been struck at the given vulnerable 
         * location for the given amount of damage.
         */
        protected void StruckWhereVulnerable(IPoint location, int damage)
        {
            OnDamaged(damage);

            if (!Dead) SurvivedStrike(location);
        }
        
        [Hook]
        protected virtual void BeforeSurvivedStrike() {}
        
        /**
         * Informs this being that it has survived a damaging strike at the given 
         * location.
         */
        protected void SurvivedStrike(IPoint location) 
        {
            BeforeSurvivedStrike();
            
            if (HasSpatterSequence()) ShowSpatterSequence(location);
            
            if (MayEnterHitState()) AddState(BeingState.Hit);
            
            PlayIfExists(BeingSounds.Hit);
        }
        
        /**
         * Returns whether this being may enter the 'hit' state.  Some beings
         * may not, because they have no frame-sequence for that state.  
         */
        protected bool MayEnterHitState()
        {
            return BeingFrameSequences.Hit != null;
        }
        	
        /**
         * Returns whether a spatter sequence is specified for this being.
         */
        protected bool HasSpatterSequence()
        {
            return BeingFrameSequences.Spatter != null;
        }
        
        /**
         * Show this being's spatter sequence on this side of this being
         * which is implied by the given location.
         */
        protected void ShowSpatterSequence(IPoint location)
        {
            // create an animation-entity that will display the spatter sequence
            bool onLeftSide = location.X < Center.X; 
            var entity = 
                EntitiesFactory.CreateSpatter( 
                    BeingFrameSequences.Spatter.ThisOrMirror(onLeftSide), clock);

            // add the spatter entity to the map on the proper side of this being
            entity.MoveTo(
                (short)(onLeftSide ? LeftX - entity.Width : RightX + 1),
                (short)(Center.Y - entity.Height / 2));
            Map.AddEntity(entity);
        }

        /**
         * Has this being perform its death protocol.
         */
        protected void Die()
        {
            BeforeDying();
            
            // this being is now dying
            AddState(BeingState.Dying);
            
            // play this being's death sound
            PlayIfExists(BeingSounds.Death);

            // create this being's duration to time its fall as a caracss
            betweenFallsAsCarcassDuration = 
                DunjaxFactory.CreateFallDuration(clock);
            betweenFallsAsCarcassDuration.FallStarted();
        }
        
        [Hook]
        protected virtual void BeforeDying() {}

        [Implementation]
        public bool IsStrikableAt(IPoint location)
        {
            if (Dead || !Contains(location)) return false;

            return IsStrikableAtContainedLocation(location);
        }
        
        [OverridableValue]
        protected virtual bool IsStrikableAtContainedLocation(IPoint location) {return true;}

        /**
         * Returns whether the given location corresponds to a vulnerable spot 
         * on this being.
         */
        protected bool IsVulnerableAt(IPoint location)
        {
            if (!IsStrikableAt(location)) return false;
            
            return IsVulnerableAtStrikableLocation(location);
        }

        [OverridableValue]
        protected virtual bool IsVulnerableAtStrikableLocation(IPoint location) {return true;}

        /** 
        * Overridden to specify what should happen when a being's animation is done.
        */
        protected override void AnimationDone(IFrameSequence sequence)
        {
            BeforeAnimationDoneAsBeing(sequence);
            
            // if this being was turning
            if (sequence.IsSameOrMirror(BeingFrameSequences.Turn)
                || sequence.IsSameOrMirror(BeingFrameSequences.CeilingTurn)) {
                // it no longer is
                RemoveState(BeingState.Turning);

                // this being's new facing is that of the sequence just finished
                FacingLeft = sequence.Mirror == null;
            }

            // if this being was hit
            if (sequence.IsSameOrMirror(BeingFrameSequences.Hit)) {
                // it no longer is
                RemoveState(BeingState.Hit);
            }
        	
    	    // if this being was dying, it has died
		    if (sequence.IsSameOrMirror(BeingFrameSequences.Death)
                || sequence.IsSameOrMirror(BeingFrameSequences.CeilingDeath)) Died();
        }
        
        [Hook]
        protected virtual void BeforeAnimationDoneAsBeing(IFrameSequence sequence) { }
        
        /**
         * Informs this being that it has finished dying.
         */
        protected void Died()
        {
            // this being is now dead
            RemoveState(BeingState.Dying);
            AddState(BeingState.Dead);
            
            // if this being has no carcass sequence, remove it from its map
            if (!HasCarcassSequence()) RemoveFromPlay();
        }

        /**
         * Returns whether this has a frame sequence to display itself as a carcass.
         */
        protected bool HasCarcassSequence() 
        {
            return GetFrameSequenceForState(BeingState.Dead) != null;
        }
        
        /**
         * Returns the frame sequence this being uses to represent itself while
         * in the given state, given its current facing.
         */
        protected IFrameSequence GetFrameSequenceForState(BeingState state)
        {
            nullSequenceSpecified = false;
            IFrameSequence sequence = 
                GetFrameSequenceForStateAsBeingSubtype(state);
            if (sequence != null || nullSequenceSpecified) return sequence;
            
            BeingFrameSequencesGroup sequences = BeingFrameSequences;
            if (state == BeingState.Still) sequence = sequences.Still;
            else if (state == BeingState.Moving) sequence = sequences.Move;
            else if (state == BeingState.Hit) sequence = sequences.Hit;
            else if (state == BeingState.Dead) sequence = sequences.Carcass;
            else if (state == BeingState.Dying) sequence = sequences.Death;

            // for turning, the sequence is the one facing the opposite of 
            // the being's current facing
            else if (state == BeingState.Turning)
                return sequences.Turn.ThisOrMirror(!FacingLeft);

            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }
        
        /**
         * Setting this to true lets overrides of 
         * GetFrameSequenceForStateAsBeingSubtype() indicate to 
         * GetFrameSequenceForState() that a null value is the correct sequence
         * result to return for a given state.
         */
        protected bool nullSequenceSpecified;
        
        /**
         * Should be overridden to return results for BeingState subtype instances.
         */
        protected abstract IFrameSequence 
            GetFrameSequenceForStateAsBeingSubtype(BeingState state);

        [Implementation]
        public bool InState(BeingState state)
        {
            return beingStates.ContainsKey(state);
        }

        /**
         * Adds the given being-state to those this being is currently in.
         */
        public void AddState(BeingState state)
        {
            // if this being is already in the given state
            if (InState(state)) {
                // just restart its animation if its done
                RestartCurrentAnimationIfDone(GetFrameSequenceForState(state));

                // don't add the state
                return;
            }

            RemoveStatesForStateAddition(state);

            // if there's a frame sequence associated with the given state
            // for this being
            var sequence = GetFrameSequenceForState(state);
            if (sequence != null) {
                // add a new animation of that frame sequence to this being
                AddAnimation(sequence);
            }

            beingStates.Add(state, state);
        }

        /**
         * Removes the given being-state from those this being is currently in.
         */
        public void RemoveState(BeingState state)
        {
            // if this being isn't in the given state, there is nothing to do
            if (!InState(state)) return;

            // remove the state
            beingStates.Remove(state);

            // if there is an associated animation, remove it from those 
            // currently depicting this entity
            var sequence = GetFrameSequenceForState(state);
            if (sequence != null) RemoveAnimation(sequence);
            
            AfterRemoveState(state);
        }

        [Hook]
        protected virtual void AfterRemoveState(BeingState state) {}

        /**
         * Removes states from this being that aren't compatible with the given
         * one this being is presumedly entering.
         */
        private void RemoveStatesForStateAddition(BeingState state)
        {
            BeforeRemoveStatesForStateAddition(state);

            if (state == BeingState.Still) {
                RemoveState(BeingState.Moving);
                RemoveState(BeingState.Turning);
            }

            else if (state == BeingState.Moving) {
                RemoveState(BeingState.Still);
                RemoveState(BeingState.Turning);
            }

            else if (state == BeingState.Turning) {
                RemoveState(BeingState.Still);
                RemoveState(BeingState.Moving);
            }

            else if (state == BeingState.Dying) {
                RemoveState(BeingState.Still);
                RemoveState(BeingState.Moving);
                RemoveState(BeingState.Turning);
                RemoveState(BeingState.Hit);
            }
        }

        [Hook]
        protected virtual void BeforeRemoveStatesForStateAddition(
            BeingState state) { }

        [HookOverride]
        protected override void ActAfterAnimationChecked()
        {
            ActAfterAnimationChecked_AsBeingSubtype();

            if (Dead) {
                // if this being's carcass can fall, have it fall
                bool fallBlocked;
                if (CanFallAsCarcass(out fallBlocked)) FallAsCarcass();

                // otherwise, if the fall was blocked by an impediment,
                // and this being has reached the carcass state, 
                // it never needs to act, again
                else if (fallBlocked && InState(BeingState.Dead)) 
                    PermanentlyInactive = true;
            }
        }

        [Hook]
        protected virtual void ActAfterAnimationChecked_AsBeingSubtype() {}

        /**
         * Returns whether this being can fall this turn as a presumed carcass.
         * 
         * fallBlocked: when the being can't fall, if it's because its fall
         * was blocked by an impediment
         */
        private bool CanFallAsCarcass(out bool fallBlocked)
        {
            // if the time since this being's last fall hasn't been long enough,
            // it cannot fall at this moment
            fallBlocked = false;
            if (!betweenFallsAsCarcassDuration.Done) return false;

            // if the area just beneath this being's bottom-center
            // is not passible (note we don't use fall-throughable-ness 
            // as we want carcasses to fall through terrain like ladders)
            var test = BottomCenter.Add(0, 1);
            if (!Map.IsPassible(test.Add(-4, 0))
                || !Map.IsPassible(test.Add(4, 0))) {
                // no fall is allowed
                fallBlocked = true;
                return false;
            }

            return true;
        }

        /**
         * Has this being fall as a presumed carcass.
         */
        private void FallAsCarcass()
        {
            // move this being downward one increment
            AddToLocation(0, 1);
        }

        [OverriddenValue]
        protected override float IntervalUntilNextAction
        {
            get {
                // falls are faster than most other being actions
                return Dead ? 
                    betweenFallsAsCarcassDuration.LengthLeft : 100;
            }
        }
    }
}
