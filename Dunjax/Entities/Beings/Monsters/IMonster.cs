using System;
using Dunjax.Attributes;

namespace Dunjax.Entities.Beings.Monsters
{
    public interface IMonster : IBeing
    {
        /**
         * This monster's monster-type.
         */
        MonsterTypeEnum MonsterType {get;}
    }

    /**
     * The different types of monsters that exist in the game.
     */

    public enum MonsterTypeEnum
    {
        BlueGargoyle, GreenGargoyle, Master, Slime, Spider,
        Stalagmite, Whirlwind, YellowGargoyle
    };

    /**
     * Specifies monster-specific states.
     * 
     * See the parent class for why this class is globally visible.
     */
    class MonsterState : BeingState
    {
        [Constructor]
        protected MonsterState(string name) : base(name) { }

        [Flyweights]
        public static MonsterState
            Attacking = new MonsterState("Attacking"),
            Sleeping = new MonsterState("Sleeping"),
            Waking = new MonsterState("Waking");
    }

    /**
     * Specifies flying-monster-specific states.
     */
    class FlyingMonsterState : MonsterState
    {
        [Constructor]
        private FlyingMonsterState(string name) : base(name) { }

        [Flyweights]
        public static FlyingMonsterState
            Flying = new FlyingMonsterState("Flying"),
            FlyTurning = new FlyingMonsterState("FlyTurning"),
            Landing = new FlyingMonsterState("Landing");
    }

    /**
     * Specifies slime-specific states.
     */
    class SlimeState : MonsterState
    {
        [Constructor]
        private SlimeState(string name) : base(name) { }

        [Flyweights]
        public static SlimeState
            Falling = new SlimeState("Falling"),
            FallingUp = new SlimeState("FallingUp");
    }

    /**
     * Specifies stalagmite-specific states.
     */
    class StalagmiteState : MonsterState
    {
        [Constructor]
        private StalagmiteState(string name) : base(name) { }

        [Flyweights]
        public static StalagmiteState
            Falling = new StalagmiteState("Falling"),
            HittingGround = new StalagmiteState("HittingGround");
    }

    /**
     * Specifies master-specific states.
     */
    class MasterState : MonsterState
    {
        [Constructor]
        private MasterState(string name) : base(name) { }

        [Flyweights]
        public static MasterState
            Firing = new MasterState("Firing"),
            Sliming = new MasterState("Sliming"),
            PostFiring = new MasterState("PostFiring"),
            PostSliming = new MasterState("PostSliming");
    }

    /**
     * The monster at the end of the game.
     */
    interface IMaster : IMonster
    {
        /**
         * Is fired when this master is killed, which signals the completion of the game.
         */
        event EventHandler Killed;
    }
}