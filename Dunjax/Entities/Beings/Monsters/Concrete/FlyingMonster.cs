using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement;
using Dunjax.Sound;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    abstract class FlyingMonster : Monster, IFlyMonsterMovementMethodView
    {
	    /**
         * The movement method employed when this monster is moving along the ground.
         */
        protected IMonsterMovementMethod groundMovementMethod;

        /**
         * The movement method employed when this monster is flying.
         */
        protected IMonsterMovementMethod flyMovementMethod;

        /**
	     * Returns the frame-sequences used to depict this flying monster in its various
	     * states. Each subclass of this class will declare a FlyingMonsterFrameSequencesGroup
	     * (or derivative) instance to specify its frame-sequences, and that
	     * instance must be returned through an override of this method.
	     */
        [OverridableValue]
        protected abstract FlyingMonsterFrameSequencesGroup 
    	    FlyingMonsterFrameSequences {get;}
        
        /**
         * See parent class.  Specifies flying-monster-specific frame-sequences.
         */
        public abstract class FlyingMonsterFrameSequencesGroup 
    	    : MonsterFrameSequencesGroup
        {
            public IFrameSequence Fly, FlyTurn, Land, FlyDeath;
        }

	    /**
	     * Returns the sounds emitted by this flying-monster. Each subclass of this
	     * class will declare a FlyingMonsterSoundsGroup (or derivative) instance to
	     * specify its sounds, and that instance must be returned through an
	     * override of this method.
	     */
        public abstract FlyingMonsterSoundsGroup FlyingMonsterSounds { get; }

        /**
         * See parent class.  Specifies flying-monster-specific sounds.
         */
        public class FlyingMonsterSoundsGroup : MonsterSoundsGroup
        {
    	    public ISound Fly;
        }

        /**
         * Must be overridden to specify the between-fly-moves-duration-length for
         * each subclass of flying monsters. 
         */
        protected abstract int BetweenFlyMovesDurationLength {get;}
        
	    /**
	     * Keeps track of how long it's been since the last time this monster 
	     * fly-moved.
	     */
        protected readonly IDuration betweenFlyMovesDuration;
        
        [Constructor]
        protected FlyingMonster(IClock clock) : base(clock)
        {
    	    // have this monster start out flying
    	    AddState(FlyingMonsterState.Flying);

            betweenFlyMovesDuration =
                DunjaxFactory.CreateDuration(clock, BetweenFlyMovesDurationLength);
        }
        
        [OverriddenValue]
        protected override IDuration GetDurationSinceLastMove()
        {
            return Flying ? betweenFlyMovesDuration :
                base.GetDurationSinceLastMove();
        }
        
        [HookOverride]
        protected override void BeforeAnimationDoneAsMonster(IFrameSequence sequence)
        {
            var sequences = FlyingMonsterFrameSequences;
            if (sequence.IsSameOrMirror(sequences.FlyTurn)) {
                RemoveState(FlyingMonsterState.FlyTurning);

                // this monster's new facing is that of the sequence just finished
                FacingLeft = sequence.Mirror == null;

                // set monster back to the normal flying state, now that its 
                // new facing has been determined, above
                AddState(FlyingMonsterState.Flying);
            }
            else if (sequence.IsSameOrMirror(sequences.Land)) {
                RemoveState(FlyingMonsterState.Landing);
                AddState(BeingState.Still);
                currentMovementMethod = groundMovementMethod;
            }
        }
    	
        [HookOverride]
        protected override void ChangeMovementMethod()
        {
            // if this monster isn't flying, change it to try flying
            if (!Flying) currentMovementMethod = flyMovementMethod;

            // note that we only change to the ground-method when
            // this monster is done landing
        }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds
        {
            get {return FlyingMonsterSounds;}
        }

	    /**
         * Returns whether this flying-monster is currently in a flying state. 
	     */
        public bool Flying
        {
            get {
                return InState(FlyingMonsterState.Flying)
                    || InState(FlyingMonsterState.FlyTurning)
                    || InState(FlyingMonsterState.Landing);
            }
        }
        
        [OverriddenValue] 
        protected override IFrameSequence 
            GetFrameSequenceForStateAsMonsterSubtype(BeingState state)
        {
            var sequences = FlyingMonsterFrameSequences;
            IFrameSequence sequence = null;
            if (state == FlyingMonsterState.Flying) sequence = sequences.Fly;
            else if (state == FlyingMonsterState.Landing) sequence = sequences.Land;
            else if (state == BeingState.Dying && Flying) sequence = sequences.FlyDeath;

            // for fly-turning, the sequence is the one facing the opposite of 
            // the monster's current facing
            else if (state == FlyingMonsterState.FlyTurning)
                return sequences.FlyTurn.ThisOrMirror(!FacingLeft);

            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }

        [HookOverride]
        protected override void BeforeRemoveStatesForStateAdditionAsMonster(
            BeingState state)
        {
            if (state == BeingState.Still) {
                RemoveState(FlyingMonsterState.Landing);
            }

            else if (state == BeingState.Moving) {
                RemoveState(FlyingMonsterState.Landing);
            }

            else if (state == BeingState.Dying) {
                RemoveState(FlyingMonsterState.Flying);
                RemoveState(FlyingMonsterState.FlyTurning);
                RemoveState(FlyingMonsterState.Landing);
            }

            else if (state == FlyingMonsterState.Flying) {
                RemoveState(BeingState.Moving);
                RemoveState(BeingState.Turning);
                RemoveState(FlyingMonsterState.FlyTurning);
                RemoveState(FlyingMonsterState.Landing);
            }

            else if (state == FlyingMonsterState.FlyTurning) {
                RemoveState(BeingState.Moving);
                RemoveState(BeingState.Turning);
                RemoveState(FlyingMonsterState.Flying);
                RemoveState(FlyingMonsterState.Landing);
            }

            else if (state == FlyingMonsterState.Landing) {
                RemoveState(FlyingMonsterState.Flying);
                RemoveState(FlyingMonsterState.FlyTurning);
            }
        }
    }
}