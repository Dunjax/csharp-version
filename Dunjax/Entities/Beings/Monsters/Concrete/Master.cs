using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement;
using Dunjax.Entities.Shots;
using Dunjax.Geometry;
using OtherSounds = Dunjax.Sound.Sounds;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * The monster at the end of the game.  It is stationary, but can fire 
     * fireballs at the ground, and slimeballs at the ceiling.  It's only
     * vulnerable spot is its head.  The master starts out sleeping, and is
     * awoken only only when it is hit by the player, or the player gets 
     * close enough for the master to bite him. 
     */
    class Master : Monster, IMonsterMovementMethod, IMaster
    {
	    /**
	     * Whether this master has woken yet.
	     */
        protected bool woken;
    	
        [Constructor]
        public Master(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.Master;
            currentMovementMethod = this;
            FacingLeft = true;
            AddState(MonsterState.Sleeping);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 40;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 30;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 400;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 100;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class MasterFrameSequences : MonsterFrameSequencesGroup 
        {
    	    /**
             * Sequences which are specific to the master.
             */
    	    public IFrameSequence Fire, Slime, PostFire, PostSlime;

            [Singleton]
            public static MasterFrameSequences Singleton = 
                new MasterFrameSequences();

	        [Constructor]
            private MasterFrameSequences()
	        {
                Sleep = CreateFrameSequence("sleep", false, makeMirror:false);
                Wake = CreateFrameSequence("wake", false, makeMirror:false);
                Still = CreateFrameSequence("still", false, makeMirror:false);
                Fire = CreateFrameSequence("fire", false, makeMirror:false);
                Slime = CreateFrameSequence("slime", false, makeMirror:false);

                // the post-fire seqeuence is just the front portion of the fire 
                // sequence, in reverse
                PostFire = CreateFrameSequence("postFire", false, makeMirror:false,
                    images:new [] { Fire[2].Image, Fire[1].Image, Fire[0].Image, Still[0].Image });
		        const int restDuration = 3000;
		        PostFire[3].Duration = restDuration;

                // the post-slime seqeuence is just the front portion of the slime 
                // sequence, in reverse
                PostSlime = CreateFrameSequence("postSlime", false, makeMirror:false,
                    images:new [] { Slime[1].Image, Slime[0].Image, Still[0].Image });
		        PostSlime[2].Duration = restDuration;

                Death = CreateFrameSequence("death", false, makeMirror:false); 
                Death.SetDurationOfAllFrames(100);
                Death[6].Duration = 3000;

                Hit = CreateFrameSequence(
                    "hit", false, makeMirror:false, images:new[] { Death[0].Image });

                Attack = CreateFrameSequence(
                    "attack", false, makeMirror:false, images:new[] { Death[0].Image });
                Attack[0].Duration = 500;

                Carcass = CreateFrameSequence(
                    "carcass", false, makeMirror:false, images:new[] { Death[6].Image });
            }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "master";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return MasterFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();

        [Implementation]
        public bool MoveMonster() 
        {
            // the master does not move
            return true;
        }
        
        [HookOverride]
        protected override void BeforeAnimationDoneAsMonster(IFrameSequence sequence)
	    {
            var sequences = MasterFrameSequences.Singleton;
            if (sequence.IsSameOrMirror(sequences.Fire)) LaunchFireballShot();
		    else if (sequence.IsSameOrMirror(sequences.Slime)) LaunchSlimeShot();
		    else if (sequence.IsSameOrMirror(sequences.Wake)) FinishedWaking();
		    else if (sequence.IsSameOrMirror(sequences.PostFire)) {
                RemoveState(MasterState.PostFiring);
                AddState(!Map.Player.Dead ? MasterState.Sliming : MasterState.Still);
		    }
		    else if (sequence.IsSameOrMirror(sequences.PostSlime)) {
                RemoveState(MasterState.PostSliming);
                AddState(MasterState.Firing);
		    }
		    else if (sequence.IsSameOrMirror(sequences.Hit)) FinishedBeingHit();
            else if (sequence.IsSameOrMirror(sequences.Death)) Killed(this, null);
	    }

        /**
         * Has this master launch a fireball shot at some location along the ground
         * below and in front of it.
         */
        protected void LaunchFireballShot()
        {
            // launch a fireball shot at the ground
            var dx = (short) (-(8 + Random.NextInt(5)));
            var direction = GeometryFactory.CreatePoint(dx, (short)(20 + dx));
            var shot = ShotFactory.CreateShot(ShotType.FireballShot, direction, clock);
            const short alignmentNudge = 16;
            shot.CenterOn(LeftCenter.Add(-1, alignmentNudge));
            Map.AddEntity(shot);
            OtherSounds.FireballShot.Play();
            
            // this master is now in the post-firing state
            RemoveState(MasterState.Firing);
            AddState(MasterState.PostFiring);
        }
        
        /**
         * Has this master launch a slime-shot at some location along the ceiling
         * above and in front of it
         */
        protected void LaunchSlimeShot()
        {
            // launch a slime shot at the ceiling
            var dx = (short)(-(8 + Random.NextInt(5)));
            var direction = GeometryFactory.CreatePoint(dx, (short)(-(20 + dx)));
            var shot = ShotFactory.CreateShot(ShotType.SlimeShot, direction, clock);
            const short alignmentNudge = 16;
            shot.CenterOn(LeftCenter.Add(-1, (short) (-Height / 4 - alignmentNudge)));
            Map.AddEntity(shot);
            OtherSounds.SlimeballShot.Play();
            
            // this master is now in the post-sliming state
            RemoveState(MasterState.Sliming);
            AddState(MasterState.PostSliming);
        }
        
        /**
         * Informs this master that it has finished waking from being asleep.
         */
        protected void FinishedWaking()
        {
            // this master has now been woken
            woken = true;
            
            // this master is now in the firing state
            RemoveState(MonsterState.Waking);
            AddState(MasterState.Firing);
        }
        
        /**
         * Informs this master that it has finished going through its hit state.
         */
        protected void FinishedBeingHit()
        {
            // if this master has not yet woken
            if (!woken) {
                // this master is now in the waking state
                RemoveState(MonsterState.Sleeping);
                AddState(MonsterState.Waking);
                OtherSounds.MasterWake.Play();
            }
            
            else AddState(Random.NextBoolean() ? 
                MasterState.PostFiring : MasterState.PostSliming);
        }
        
        [OverriddenValue]
        protected override bool IsStrikableAtContainedLocation(IPoint location)
	    {
		    // if the given location falls within (an estimation of) the portion 
		    // of this master's bounds that it actually occupies, it is a 
		    // strikable location
		    return location.Y < TopY + Height / 4 
		        || location.X > LeftX + Width / 4; 
	    }

        [OverriddenValue]
        protected override bool IsVulnerableAtStrikableLocation(IPoint location)
	    {
		    // if the given location is at this master's head, it is a 
		    // vulnerable location
		    return location.X > LeftX + Width / 4
                && location.Y >= TopY + Height / 4 
                && location.Y <= Center.Y + 4;
	    }

        [OverriddenValue]
        protected override bool ShouldAlignWithGroundOrCeiling
        {
            get { return false; }
        }

        [OverriddenValue]
        protected override IFrameSequence 
            GetFrameSequenceForStateAsMonsterSubtype(BeingState state)
        {
            var sequences = MasterFrameSequences.Singleton;
            IFrameSequence sequence = null;
            if (state == MasterState.PostFiring) sequence = sequences.PostFire;
            else if (state == MasterState.PostSliming) sequence = sequences.PostSlime;
            else if (state == MasterState.Firing) sequence = sequences.Fire;
            else if (state == MasterState.Sliming) sequence = sequences.Slime;

            // we have to handle these states here, rather than in the base method
            // like we normally would, as the base method would choose a mirror,
            // and there are none for the master
            else if (state == BeingState.Still) sequence = sequences.Still;
            else if (state == MonsterState.Sleeping) sequence = sequences.Sleep;
            else if (state == MonsterState.Waking) sequence = sequences.Wake;
            else if (state == BeingState.Dying) sequence = sequences.Death;
            else if (state == BeingState.Dead) sequence = sequences.Carcass;
            else if (state == MonsterState.Attacking) sequence = sequences.Attack;
            else if (state == BeingState.Hit) sequence = sequences.Hit;

            // note that there is no reason to worry about selecting a mirror of
            // a sequence with the master, as it is always facing left
            return sequence;
        }

        [Implementation]
        public event EventHandler Killed;

        [HookOverride]
        protected override void BeforeRemoveStatesForStateAddition(
            BeingState state)
        {
            if (state == BeingState.Dying) {
                RemoveState(MasterState.Attacking);
                RemoveState(MasterState.Firing);
                RemoveState(MasterState.PostFiring);
                RemoveState(MasterState.Sliming);
                RemoveState(MasterState.PostSliming);
            }
        }
    }
}