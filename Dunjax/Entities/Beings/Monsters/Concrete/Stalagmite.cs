using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement;
using Dunjax.Map;
using OtherSounds = Dunjax.Sound.Sounds;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A monster that, once woken, falls from the ceiling to the ground.
     */
    class Stalagmite : Monster, IMonsterMovementMethod
    {
        [Constructor]
        public Stalagmite(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.Stalagmite;
            currentMovementMethod = this;
            AddState(MonsterState.Sleeping);
        }
    	
        [OverriddenValue]
        protected override int StartingHealth {get {return 100;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 1;}}

        [OverriddenValue]
        protected override int BetweenAttacksDurationLength
        {
            get {return BetweenMovesDurationLength;}
        }

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 3;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class StalagmiteFrameSequences : MonsterFrameSequencesGroup 
        {
    	    /**
             * Sequences that are specific to stalagmites.
             */
            public IFrameSequence Fall, HitGround;

            [Singleton]
            public static StalagmiteFrameSequences Singleton = 
                new StalagmiteFrameSequences();

	        [Constructor]
            private StalagmiteFrameSequences()
	        {
		        Sleep = CreateFrameSequence("sleep", false);
		        Wake = CreateFrameSequence("wake", false);
		        Fall = CreateFrameSequence("fall", false);
		        HitGround = CreateFrameSequence("hitGround", false);
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "stalagmite";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return StalagmiteFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();

        [HookOverride]
        protected override void ActBeforeActingAsMonster()
	    {
		    if (InState(MonsterState.Sleeping) && ShouldWake()) Wake();
	    }
        
        /**
         * Returns whether this stalagmite is in a suitable condition where it should
         * waken from its (presumed) slumber.
         */
        protected bool ShouldWake()
        {
            // return whether this stalagmite is sufficiently close to the player to 
            // wake up (including being above the player), and can see the player
            var playerCenter = Map.Player.Center;
            return playerCenter.Y > Center.Y
                && Math.Abs(Center.X - playerCenter.X) / (float)(playerCenter.Y - Center.Y) < 0.5
                && Map.IsLocationVisibleFrom(playerCenter, Center, false, false);
        }

        /**
         * Has this stalagmite awaken from its (presumed) slumber.
         */
        protected void Wake()
        {
            RemoveState(MonsterState.Sleeping);
            AddState(MonsterState.Waking);
            OtherSounds.StalagmiteWake.Play();
        }

        [OverriddenValue]
        protected override bool CanMoveInCurrentState()
        {
            return InState(StalagmiteState.Falling);
        }

        [Implementation]
        public bool MoveMonster()
	    {
            // if it's passible just below this stalagmite
            if (Map.IsPassible(BottomLeft.Add(6, 1), ImpassableEntitiesSet.NoneSet) &&
                Map.IsPassible(BottomRight.Add(-6, 1), ImpassableEntitiesSet.NoneSet)) {
                // move this stalagmite down one increment
                AddToLocation(0, 1);
            }

            // otherwise
            else {
                // this stalagmite is now hitting the ground
                RemoveState(StalagmiteState.Falling);
                AddState(StalagmiteState.HittingGround);
            }

            // if stalagmite is passing through the player,
            // the player takes a hit
            var player = Map.Player;
            if (player.Bounds.Contains(BottomLeft)
                || player.Bounds.Contains(BottomRight)
                || player.Bounds.Contains(BottomCenter))
                player.Struck(player.Center, AttackDamage);

            return true;
	    }
        
        [HookOverride]
        protected override void BeforeAnimationDoneAsMonster(IFrameSequence sequence)
	    {
            var sequences = StalagmiteFrameSequences.Singleton;
            if (sequence.IsSameOrMirror(sequences.Wake)) {
                RemoveState(MonsterState.Waking);
                AddState(StalagmiteState.Falling);
            }
		    else if (sequence.IsSameOrMirror(sequences.HitGround)) {
		        Map.RemoveEntity(this);
		    }
	    }

        [OverridableValue]
        public override bool IsCloseEnoughToAttack()
        {
            // a stalagmite attacks through its movement, rather than by an 
            // explicit attack
            return false;
        }

        [OverridableValue]
        protected override bool IsGroundDwelling() {return false;}

        [OverridableValue]
        protected override bool IsCeilingDwelling() {return true;}

        [OverridableValue]
        protected override IFrameSequence 
            GetFrameSequenceForStateAsMonsterSubtype(BeingState state)
        {
            IFrameSequence sequence = null;
            var sequences = StalagmiteFrameSequences.Singleton;
            if (state == StalagmiteState.HittingGround) sequence = sequences.HitGround;
            else if (state == StalagmiteState.Falling) sequence = sequences.Fall;

            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }
    }
}