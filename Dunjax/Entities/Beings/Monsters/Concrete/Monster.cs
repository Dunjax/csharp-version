using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Concrete;
using Dunjax.Entities.Beings.Monsters.Movement;
using Dunjax.Entities.Beings.Player;
using Dunjax.Sound;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    abstract class Monster : Being, IMonsterMovementMethodView
    {
        /**
         * This monster's monster-type.  Should be filled in by the implementing
         * subclass.
         */
        public MonsterTypeEnum MonsterType {get; protected set;}
         
        /**
         * The damage done per normal attack by this monster. 
         */
	    protected abstract int AttackDamage { get; }

        /**
         * The movement method currently being employed by this monster.
         */
        protected IMonsterMovementMethod currentMovementMethod;

        [Implementation]
        public new bool FacingLeft
        {
            get { return base.FacingLeft; }
            set { base.FacingLeft = value; }
        }

        /**
         * Whether this monster is currently attached to the ceiling.
         */
        public bool OnCeiling { get; set; }

        /**
	     * The frame-sequences used to depict this monster in its various
	     * states. Each subclass of this class will declare a MonsterFrameSequencesGroup
	     * (or derivative) instance to specify its frame-sequences, and that
	     * instance must be returned through an override of this method.
	     */
        [OverridableValue]
        public abstract MonsterFrameSequencesGroup MonsterFrameSequences { get; }
        
        /**
         * See parent class.  Specifies monster-specific frame-sequences.
         */
        public abstract class MonsterFrameSequencesGroup : BeingFrameSequencesGroup
        {
            /**
             * The different frame-sequences a monster may display.  Subtypes are free
             * to populate whichever of these fields they want.
             */
    	    public IFrameSequence Attack, Sleep, Wake, CeilingAttack;

            [OverriddenValue]
            protected override string ImagesPath { get {return "entities/monsters/";} }
        }

	    /**
	     * The sounds emitted by this monster. Each subclass of this class
	     * will declare a MonsterSoundsGroup (or derivative) instance to specify its
	     * sounds, and that instance must be returned through an override of this
	     * method.
	     */
        [OverridableValue]
        protected abstract MonsterSoundsGroup MonsterSounds { get; }

        /**
         * See parent class.  Specifies monster-specific sounds.
         */
        public class MonsterSoundsGroup : BeingSoundsGroup
        {
    	    public ISound Attack = Sounds.MonsterAttack;

            [Constructor]
    	    public MonsterSoundsGroup()
            {
                Death = Sounds.MonsterDeath;
            }
        }

        /**
         * Must be overridden to specify the between-moves-duration-length for
         * each subclass of monsters. 
         */
        protected abstract int BetweenMovesDurationLength { get; }
        
	    /**
	     * Keeps track of how long it's been since the last time this monster moved
         * on the ground or ceiling.
	     */
        protected readonly IDuration betweenMovesOnGroundOrCeilingDuration;
        
        /**
         * Must be overridden to specify the between-attacks-duration-length for 
         * each subclass of monsters. 
         */
        protected abstract int BetweenAttacksDurationLength { get; }

	    /**
	     * Keeps track of how long it's been since the last time this monster 
	     * attacked.
	     */
        protected readonly IDuration betweenAttacksDuration;

        /**
         * Whether this monster is not currently relevant enough to the game's action
         * to make it worth having it act.
         */
        protected bool passive;
        
        /**
         * If this monster is currently passive, keeps track of how long it's been 
         * since the last time this monster checked whether it should remain so.
         */
        protected readonly IDuration passiveDuration;

        [Constructor]
        protected Monster(IClock clock) : base(clock)
        {
            betweenMovesOnGroundOrCeilingDuration =
                DunjaxFactory.CreateDuration(clock, BetweenMovesDurationLength);
            betweenAttacksDuration =
                DunjaxFactory.CreateDuration(clock, BetweenAttacksDurationLength);
            passiveDuration = DunjaxFactory.CreateDuration(clock, 0, false);
        }
        
        [OverriddenValue]
        protected override float IntervalUntilNextAction
        {
            get {
                return Math.Min(
                    base.IntervalUntilNextAction,
                    passive ? 
                        passiveDuration.LengthLeft : 
                        BetweenMovesDurationLength);
            }
        }

        /**
         * Has this monster start an attack on the player.  
         */
        protected void StartAttack()
        {
            // turn this monster towards the player
            FacingLeft = GetSideEntityIsOn(Map.Player) == Side.Left;

            AddState(MonsterState.Attacking);

            PlayIfExists(MonsterSounds.Attack);
        }

        /**
         * Returns whether this monster is able to initiate an attack 
         * on the player.
         */
        protected bool CanStartAttack()
        {
            // if this monster is already attacking, it cannot attack again
            if (InState(MonsterState.Attacking)) return false;
            
            // if this monster is sleeping or waking, it cannot attack 
            if (InState(MonsterState.Sleeping) || InState(MonsterState.Waking)) return false;

            // if it hasn't been long enough since the last attack, this monster 
            // cannot attack now
            if (!betweenAttacksDuration.DoneOnly) return false;
            
            // if the player is dead, this monster may not attack
            if (Map.Player.Dead) return false;

            return IsCloseEnoughToAttack();
        }
        
        [HookOverride]
        protected override void BeforeAnimationDoneAsBeing(IFrameSequence sequence)
        {
            BeforeAnimationDoneAsMonster(sequence);

            // if this monster was attacking, finish the attack
            if (sequence.IsSameOrMirror(MonsterFrameSequences.Attack) 
                || sequence.IsSameOrMirror(MonsterFrameSequences.CeilingAttack))
                FinishAttack();
        }

        [Hook]
        protected virtual void BeforeAnimationDoneAsMonster(IFrameSequence sequence) { }
        
        /**
         * Has this monster finish up its current attack and go back to what it
         * was doing before attack was begun.
         */
        protected void FinishAttack()
        {
            // if this monster is still close enough to strike the player,
            // the player is struck
            if (IsCloseEnoughToAttack()) PlayerStruck();

            // this monster is no longer attacking
            RemoveState(MonsterState.Attacking);
            
            // this monster must wait a bit from this point before attacking again 
            betweenAttacksDuration.Start();
        }

        /**
         * Informs this monster that it has struck the player.
         */
        protected void PlayerStruck()
        {
            IPlayer player = Map.Player;
            if (AttackDamage > 0) player.Struck(player.Center, AttackDamage);

            if (!player.Dead) PlayerHit(player);
        }
        
        /**
         * Returns whether this monster is physically close enough to the player
         * to attack him.
         */
        [OverridableValue]
        public virtual bool IsCloseEnoughToAttack()
        {
            // if the player is too far from the side of this monster
            IPlayer player = Map.Player;
            Side sideBeingIsOn = GetSideEntityIsOn(player);
            const int maxDistance = 4;
    	    if ((sideBeingIsOn == Side.Left && LeftX - player.RightX > maxDistance)
                || sideBeingIsOn == Side.Right && player.LeftX - RightX > maxDistance) {
                // it's not close enough to attack
                return false; 
    	    }

            // if this monster is too far above or below the player
            // (allowing for more leeway if the monster is directly above or below
            // him), it's not close enough
    	    int centerX = Center.X;
            bool directlyAboveOrBelow = 
                centerX >= player.LeftX && centerX <= player.RightX;
            int margin = directlyAboveOrBelow ? 4 : 0;
    	    if (TopY > player.BottomY + margin || BottomY < player.TopY - margin) return false; 
        	
    	    return true;
        }

        /**
         * Returns whether this monster is currently allowed to move.
         */
        protected bool CanMove()
        {
            // if this monster cannot move while in its current state,  
            // it cannot move
            if (!CanMoveInCurrentState()) return false;
            
            // if it hasn't been long enough since this monster's last move, 
            // it cannot move
            if (!GetDurationSinceLastMove().Done) return false;

            return true;
        }
        
        /**
         * Returns whether this monster can normally move while in its current state.
         */
        [OverridableValue]
        protected virtual bool CanMoveInCurrentState()
        {
            return !InState(MonsterState.Attacking) 
                && !InState(MonsterState.Sleeping)
                && !InState(MonsterState.Waking)
                && !InState(BeingState.Hit);
        }
        
        /**
         * Returns the duration currently being used by this monster to space
         * out repeat movements.
         */
        [OverridableValue]
        protected virtual IDuration GetDurationSinceLastMove()
        {
            return betweenMovesOnGroundOrCeilingDuration;
        }
        
        /**
         * Has this monster perform whatever kind of movement it is capable of.
         */
        protected void Move()
        {
            if (!currentMovementMethod.MoveMonster()) {
                ChangeMovementMethod();
            }
        }

        /**
         * Has this monster check to see if it should switch movement methods (if
         * it has more than one to choose from) due to the current method not
         * producing any movement.
         */
        [Hook]
        protected virtual void ChangeMovementMethod() {}

        [HookOverride]
        protected override void ActAfterAnimationChecked_AsBeingSubtype()
        {
            if (passive) {
                ActWhilePassive();
                
                // if this monster is still passive, there's nothing more to do here
                if (passive) return;
            }
            
            // else, if this monster is too far from the player to make it worth having it act 
            else if (CanPassivate() && !IsCloseEnoughToPlayerToAct()) {
                // make this monster passive for a length of time,
                // and don't let it act during this call
                Passivate();
                return;
            }
            
            ActBeforeActingAsMonster();
            
            if (!Dead) ActWhenAlive();
        }
        
        /**
         * Has this monster act as it should when it is passive.
         */
        protected void ActWhilePassive()
        {
            // if its duration to be passive isn't yet up, don't let it act during this call
            if (!passiveDuration.DoneOnly) return;
            
            // if this monster should no longer be passive, make it not so
            if (IsCloseEnoughToPlayerToAct()) passive = false;
            
            // otherwise, have this monster be passive for another length of time
            else Passivate();
        }
        
        [OverridableValue]
        protected virtual bool CanPassivate() {return true;}

        /**
         * Returns whether this monster is close enough on the map to the player
         * to make it worth taking the time to have it be active.  Also returns true
         * when the monster can see the player.
         */
        [Optimized] 
        protected bool IsCloseEnoughToPlayerToAct()
        {
            // return whether this entity is within a certain distance of the player,
            // or is within a certain longer distance and can see the player
            var playerCenter = Map.Player.Center;
            return Center.IsDistanceAtMost(playerCenter, 13 * UnitsPerTenFeet)
                || (Center.IsDistanceAtMost(playerCenter, 20 * UnitsPerTenFeet)
                    && Map.IsLocationVisibleFrom(Center, playerCenter, false, false));
        }
        
        [Hook]
        protected virtual void ActBeforeActingAsMonster() {}
        
        /**
         * Makes this monster passive for a variable length of game time.
         */
        protected void Passivate()
        {  
            int passiveDurationLength =
                MsInASecond + Random.NextInt(MsInASecond);
            passiveDuration.Length = passiveDurationLength;
            passiveDuration.Start();
            passive = true;
        }
        
        /**
         * Performs the actions conducted by this monster while it's alive.
         */
        protected void ActWhenAlive()
        {
            if (CanStartAttack()) StartAttack();
            
            if (CanMove()) Move();
        }
        
        /**
         * Informs this monster that it has just scored a hit on the given player. 
         */
        [Hook]
        protected virtual void PlayerHit(IPlayer player) {}

        [HookOverride]
        protected override IFrameSequence 
            GetFrameSequenceForStateAsBeingSubtype(BeingState state)
        {
            IFrameSequence sequence = 
                GetFrameSequenceForStateAsMonsterSubtype(state);
            if (sequence != null || nullSequenceSpecified) return sequence;
            
            MonsterFrameSequencesGroup sequences = MonsterFrameSequences;
            if (state == BeingState.Still && OnCeiling) sequence = sequences.CeilingStill;
            else if (state == BeingState.Moving && OnCeiling) sequence = sequences.CeilingMove;
            else if (state == MonsterState.Attacking && OnCeiling) sequence = sequences.CeilingAttack;
            else if (state == BeingState.Dying && OnCeiling) sequence = sequences.CeilingDeath;
            else if (state == MonsterState.Attacking) sequence = sequences.Attack;
            else if (state == MonsterState.Sleeping) sequence = sequences.Sleep;
            else if (state == MonsterState.Waking) sequence = sequences.Wake;

            // for ceiling-turning, the sequence is the one facing the opposite of 
            // the monster's current facing
            else if (state == BeingState.Turning && OnCeiling)
                return sequences.CeilingTurn.ThisOrMirror(!FacingLeft);
            
            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }

        /**
         * Should be overridden to return results for MonsterState subtype instances.
         */
        [OverridableValue]
        protected virtual IFrameSequence 
            GetFrameSequenceForStateAsMonsterSubtype(BeingState state) 
        {
            return null;
        }

        [Implementation]
        public IClock Clock { get { return clock; } }

        [Implementation]
        public new IRandom Random { get { return Being.Random; } }

        [HookOverride]
        protected override void BeforeRemoveStatesForStateAddition(
            BeingState state)
        {
            BeforeRemoveStatesForStateAdditionAsMonster(state);

            if (state == BeingState.Still) {
                RemoveState(MonsterState.Attacking);
            }

            else if (state == BeingState.Moving) {
                RemoveState(MonsterState.Attacking);
                RemoveState(MonsterState.Waking);
            }

            else if (state == BeingState.Dying) {
                RemoveState(MonsterState.Attacking);
            }

            else if (state == MonsterState.Attacking) {
                RemoveState(MonsterState.Waking);
            }

            else if (state == MonsterState.Sleeping) {
                RemoveState(MonsterState.Still);
            }

            else if (state == MonsterState.Waking) {
                RemoveState(MonsterState.Sleeping);
            }
        }

        [Hook]
        protected virtual void BeforeRemoveStatesForStateAdditionAsMonster(
            BeingState state) { }

        [Implementation]
        public bool HasTurnSequence
        {
            get {
                var sequences = MonsterFrameSequences;
                return OnCeiling ?
                    sequences.CeilingTurn != null :
                    sequences.Turn != null;
            }
        }
    }
}