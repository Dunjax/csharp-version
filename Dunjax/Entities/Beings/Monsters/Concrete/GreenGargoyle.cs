using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A basic flying monster.
     */
    class GreenGargoyle : FlyingMonster
    {
        [Constructor]
        public GreenGargoyle(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.GreenGargoyle;
            groundMovementMethod = new MoveAlongGroundOrCeiling(this);
            currentMovementMethod = flyMovementMethod = new FlyMonsterMovementMethod(this);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 1;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 2;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 10;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 6;}}

        [OverriddenValue]
	    protected override int BetweenFlyMovesDurationLength {get {return 8;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class GreenGargoyleFrameSequences : FlyingMonsterFrameSequencesGroup 
        {
            [Singleton]
            public static GreenGargoyleFrameSequences Singleton = 
                new GreenGargoyleFrameSequences();

	        [Constructor]
            private GreenGargoyleFrameSequences()
	        {
		        Still = CreateFrameSequence("still");
		        Move = CreateFrameSequence("move");
		        Attack = CreateFrameSequence("attack", repeatedFrames:new [] {-1, -1, -1, 0});
		        Turn = CreateFrameSequence("turn");
		        Death = CreateFrameSequence("death");
		        Carcass = CreateFrameSequence("carcass");
                Fly = CreateFrameSequence("fly", true, repeatedFrames: new [] { -1, -1, 0, -1 });
                FlyTurn = CreateFrameSequence("flyTurn", false, repeatedFrames: new [] { -1, -1, 0, -1 });
		        Land = CreateFrameSequence("land");
                // the fly-death sequence is just the front part of the normal death sequence
		        FlyDeath = CreateFrameSequence("flyDeath", false,
                    images:new [] {Death[0].Image, Death[1].Image, Death[2].Image});
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "greenGargoyle";} }
	    }
    	
        [OverriddenValue]
        protected override FlyingMonsterFrameSequencesGroup FlyingMonsterFrameSequences 
            { get { return GreenGargoyleFrameSequences.Singleton; } }
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return FlyingMonsterFrameSequences; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }
    	

        [OverriddenValue]
        public override FlyingMonsterSoundsGroup FlyingMonsterSounds { get { return Sounds; } }
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly FlyingMonsterSoundsGroup Sounds = 
            new FlyingMonsterSoundsGroup {Fly = Sound.Sounds.GreenGargoyleFly};
    }
}