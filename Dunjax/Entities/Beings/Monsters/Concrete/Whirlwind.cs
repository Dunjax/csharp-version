using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;
using Dunjax.Entities.Beings.Player;
using Dunjax.Images;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A monster that sends the player flying when it hits him.
     */
    class Whirlwind : Monster
    {
        [Constructor]
        public Whirlwind(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.Whirlwind;
            currentMovementMethod = new MoveAlongGroundOrCeiling(this);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 10;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 0;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 600;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 4;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class WhirlwindFrameSequences : MonsterFrameSequencesGroup 
        {
            [Singleton]
            public static WhirlwindFrameSequences Singleton = 
                new WhirlwindFrameSequences();

	        [Constructor]
            private WhirlwindFrameSequences()
	        {
                Still = Move = CreateFrameSequence("move", true);
                Attack = CreateFrameSequence("attack", images:new [] { Still[0].Image });

                // the death sequence is the same as the normal sequence, except with
                // blanks thrown in to make the whirlwind look like it's blinking out
                var deathImages = new IImage[Move.NumFrames * 2];
                for (int i = 1; i < deathImages.Length; i += 2) {
                    deathImages[i] = Move[i / 2].Image;
                }
		        Death = CreateFrameSequence("death", images:deathImages);
                Death.SetDurationOfAllFrames(140);
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "whirlwind";} }
	    }

        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences
            { get { return WhirlwindFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = 
            new MonsterSoundsGroup {Death = null};

        [HookOverride]
	    protected override void PlayerHit(IPlayer player)
	    {
		    // the player is whirlwinded
            player.Whirlwinded(FacingLeft ? Side.Left : Side.Right);
	    }
    }
}