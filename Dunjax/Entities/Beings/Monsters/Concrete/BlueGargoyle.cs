using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A large monster with no special abilities.
     */
    class BlueGargoyle : Monster
    {
        [Constructor]
        public BlueGargoyle(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.BlueGargoyle;
            currentMovementMethod = new MoveAlongGroundOrCeiling(this);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 8;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 9;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 8;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 6;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class BlueGargoyleFrameSequences : MonsterFrameSequencesGroup 
        {
            [Singleton]
            public static BlueGargoyleFrameSequences Singleton = 
                new BlueGargoyleFrameSequences();

	        [Constructor]
            private BlueGargoyleFrameSequences()
	        {
		        Still = CreateFrameSequence("still");
		        Move = CreateFrameSequence("move");
		        Attack = CreateFrameSequence("attack", repeatedFrames:new [] {-1, -1, -1, 1, 0});
		        Turn = CreateFrameSequence("turn");
                Hit = CreateFrameSequence("hit", priority:FramePriority.BeingHit);
                Death = CreateFrameSequence("death", priority: FramePriority.BeingDeath);
		        Carcass = CreateFrameSequence("carcass");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "blueGargoyle";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences
            { get { return BlueGargoyleFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();
    }
}