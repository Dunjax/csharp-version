using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;
using OtherSounds = Dunjax.Sound.Sounds;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A monster that can move along either the floor or the ceiling, 
     * falling up and down intermittently between the two.
     */
    class Slime : Monster, IFallDownAndUpMovementMethodView
    {
        [Constructor]
        public Slime(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.Slime;
            currentMovementMethod = new FallDownAndUpMovementMethod(this);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 8;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 4;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 400;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 11;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class SlimeFrameSequences : MonsterFrameSequencesGroup 
        {
    	    /**
             * Sequences that are specific to stalagmites.
             */
            public IFrameSequence Fall, FallUp;

            [Singleton]
            public static SlimeFrameSequences Singleton = new SlimeFrameSequences();

	        [Constructor]
            private SlimeFrameSequences()
	        {
		        Still = CreateFrameSequence("still");
		        Move = CreateFrameSequence("move", repeatedFrames:new [] {-1, -1, -1, -1, -1, 1});
		        Fall = CreateFrameSequence("fall");
		        FallUp = CreateFrameSequence("fallUp");
                CeilingStill = CreateFrameSequence("ceilingStill");
		        Death = CreateFrameSequence("death");
		        CeilingDeath = CreateFrameSequence("ceilingDeath");

                // the attack sequence is just the front portion of the move sequence
		        Attack = CreateFrameSequence("attack", 
                    images:new [] {Move[0].Image, Move[1].Image, Move[2].Image});

                CeilingMove = CreateFrameSequence("ceilingMove",
                    repeatedFrames: new[] { -1, -1, -1, -1, -1, 1 });

                // the ceiling-attack sequence is just the front portion of the 
                // ceiling-move sequence
		        CeilingAttack = CreateFrameSequence("ceilingAttack",
                    images:new[] { CeilingMove[0].Image, CeilingMove[1].Image, CeilingMove[2].Image });
            }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "slime";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return SlimeFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();

        [OverriddenValue]
        protected override IFrameSequence 
            GetFrameSequenceForStateAsMonsterSubtype(BeingState state)
        {
            IFrameSequence sequence = null;
            var sequences = SlimeFrameSequences.Singleton;
            if (state == SlimeState.FallingUp) sequence = sequences.FallUp;
            else if (state == SlimeState.Falling) sequence = sequences.Fall;

            return (sequence != null) ? sequence.ThisOrMirror(FacingLeft) : null;
        }

	    /**
	     * Whether this slime's moved() method has been called yet.
	     */
	    protected bool movedCalled;
    	
	    [HookOverride] 
	    protected override void Moved()
        {
            // if this call is to set this slime's initial position, and 
            // this slime is to start out on the ceiling
            if (!movedCalled && 
                !Map.IsPassible(TopCenter.Add(0, -1))) {
                // set this slime as being on the ceiling
                OnCeiling = true;
            }
            
            movedCalled = true;
        }

        [OverriddenValue]
        protected override bool IsGroundDwelling() {return false;}

        [Implementation]
        public MonsterState FallingState { get { return SlimeState.Falling; } }

        [Implementation]
        public MonsterState FallingUpState { get { return SlimeState.FallingUp; } }

        [Implementation]
        public void FallStarted()
        {
            OtherSounds.SlimeJump.Play();
        }

        [OverriddenValue]
        protected override bool CanPassivate()
        {
            // this slime should not passivate while it is falling
            return !InState(SlimeState.Falling) && !InState(SlimeState.FallingUp);
        }
    }
}