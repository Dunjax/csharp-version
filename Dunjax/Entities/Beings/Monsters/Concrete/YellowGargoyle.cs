using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A monster with no special abilities.
     */
    class YellowGargoyle : Monster
    {
        [Constructor]
        public YellowGargoyle(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.YellowGargoyle;
            currentMovementMethod = new MoveAlongGroundOrCeiling(this);
        }
        
        [OverriddenValue]
        protected override int StartingHealth {get {return 2;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 3;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 10;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 7;}}

	    /**
	     * Specifies the frame sequences for this kind of monster.
	     */
	    protected class YellowGargoyleFrameSequences : MonsterFrameSequencesGroup 
        {
            [Singleton]
            public static YellowGargoyleFrameSequences Singleton = 
                new YellowGargoyleFrameSequences();

	        [Constructor]
            private YellowGargoyleFrameSequences()
	        {
		        Still = CreateFrameSequence("still");
		        Move = CreateFrameSequence("move");
		        Attack = CreateFrameSequence(
                    "attack", false, FramePriority.Normal, true, new [] {-1, -1, -1, 2}, null);
		        Turn = CreateFrameSequence("turn");
                Death = CreateFrameSequence("death", false, FramePriority.Normal, true, null, null);
		        Carcass = CreateFrameSequence("carcass");
		        Spatter = CreateFrameSequence("spatter");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "yellowGargoyle";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return YellowGargoyleFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();
    }
}