using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters.Movement.Concrete;
using Dunjax.Entities.Shots;
using Dunjax.Geometry;
using Dunjax.Map;
using OtherSounds = Dunjax.Sound.Sounds;

namespace Dunjax.Entities.Beings.Monsters.Concrete
{
    /**
     * A monster which can shoot webs at the player.
     */
    class Spider : Monster
    {
        /**
         * How long (in ms) this spider should sleep between checks to wake.
         */
        protected const int SleepDuration = 500;

        [OverriddenValue]
        protected override int StartingHealth {get {return 20;}}

        [OverriddenValue]
	    protected override int AttackDamage {get {return 8;}}

        [OverriddenValue]
	    protected override int BetweenAttacksDurationLength {get {return 50;}}

        [OverriddenValue]
	    protected override int BetweenMovesDurationLength {get {return 6;}}

	    /**
	     * Keeps track of how long it's been since the last time this spider 
	     * fired a web-shot.
	     */
        protected readonly IDuration betweenFiresDuration;
        protected const int BetweenFiresDurationLength = 1500;

        [Constructor]
        public Spider(IClock clock) : base(clock)
        {
            MonsterType = MonsterTypeEnum.Spider;
            currentMovementMethod = new MoveAlongGroundOrCeiling(this);
            AddState(MonsterState.Sleeping);
            betweenFiresDuration =
                DunjaxFactory.CreateDuration(clock, BetweenFiresDurationLength);
        }

        /**
         * Specifies the frame sequences for this kind of monster.
         */
	    protected class SpiderFrameSequences : MonsterFrameSequencesGroup 
        {
            [Singleton]
            public static SpiderFrameSequences Singleton = 
                new SpiderFrameSequences();

	        [Constructor]
            private SpiderFrameSequences()
	        {
		        Still = CreateFrameSequence("still");
		        Move = CreateFrameSequence("move");
		        Turn = CreateFrameSequence("turn");
		        Death = CreateFrameSequence("death");
		        Carcass = CreateFrameSequence("carcass");
		        Wake = CreateFrameSequence("wake");

                // the attack sequence is the same as the back half of the move sequence 
                Attack = CreateFrameSequence("attack",
                    images:new [] {Move[3].Image, Move[4].Image, Move[5].Image});
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "spider";} }
	    }
    	
        [OverriddenValue]
        public override MonsterFrameSequencesGroup MonsterFrameSequences 
            { get { return SpiderFrameSequences.Singleton; } }
        protected override BeingFrameSequencesGroup BeingFrameSequences 
            { get { return MonsterFrameSequences; } }

        [OverriddenValue]
        protected override MonsterSoundsGroup MonsterSounds {get {return Sounds;}}
        protected override BeingSoundsGroup BeingSounds { get { return Sounds; } }
        private static readonly MonsterSoundsGroup Sounds = new MonsterSoundsGroup();

        [OverriddenValue]
        protected override float IntervalUntilNextAction
        {
            get {
                return InState(MonsterState.Sleeping) ? 
                    SleepDuration : base.IntervalUntilNextAction;
            }
        }

        [HookOverride]
        protected override void ActBeforeActingAsMonster()
	    {
		    if (InState(MonsterState.Sleeping) && ShouldWake()) Wake();
    		
	        if (CanFire()) Fire();
	    }

        /**
         * Returns whether this spider is in a suitable condition where it should
         * waken from its (presumed) slumber.
         */
        protected bool ShouldWake()
        {
            // if this spider is sufficiently close to the player to 
            // wake up, and it can see the player
            var playerCenter = Map.Player.Center;
            return Math.Abs(Center.X - playerCenter.X) < 3 * UnitsPerTenFeet
                && Math.Abs(Center.Y - playerCenter.Y) < 3 * UnitsPerTenFeet
                && Map.IsLocationVisibleFrom(playerCenter, Center, false, false);
        }
        
        /**
         * Has this spider awaken from its (presumed) slumber.
         */
        protected void Wake() 
        {
            RemoveState(MonsterState.Sleeping);
            AddState(MonsterState.Waking);
            DetermineImageForThisTurn();
            AlignWithSurroundings();
            OtherSounds.SpiderWake.Play();
            betweenFiresDuration.Start();
        }
        
        /**
         * Returns whether this spider is currently in a state where it can 
         * fire a web at the player.
         */
        protected bool CanFire()
        {
            if (Dead || InState(MonsterState.Sleeping) || InState(MonsterState.Waking)
               || !betweenFiresDuration.DoneOnly ||  Map.Player.Dead)
               return false;
            
            // return whether this spider can see the player
            return Map.IsLocationVisibleFrom(
                Map.Player.Center, GetFrontLocation(), false, false);
        }
        
        /**
         * Returns the location of this spider's web-emitting front on its map.
         */
        protected IPoint GetFrontLocation()
        {
            return GeometryFactory.CreatePoint(
                (short) (FacingLeft ? LeftX + 4 : RightX - 4), Center.Y);
        }
        
        [HookOverride]
        protected override void BeforeAnimationDoneAsMonster(IFrameSequence sequence)
	    {
		    // if this spider is done coming out of its tunnel, it's now in its
            // normal state
		    var sequences = MonsterFrameSequences;
            if (sequence.IsSameOrMirror(sequences.Wake)) RemoveState(MonsterState.Waking);
	    }

        [OverriddenValue]
	    protected override bool IsStrikableAtContainedLocation(IPoint location)
	    {
		    // if this spider is sleeping or waking, it is still in its tunnel, and
		    // therefore cannot be hit
		    return !InState(MonsterState.Sleeping) && !InState(MonsterState.Waking);
	    }
    	
	    /**
	     * Has this spider fire a web-shot at the player. 
	     */
	    protected void Fire()
	    {
		    // create a web-shot at this spider's front, aimed at the player
	        var front = GetFrontLocation();
		    var shot = ShotFactory.CreateShot(
                ShotType.WebShot, front.GetDifference(Map.Player.Center), clock);
		    shot.CenterOn(front);
		    Map.AddEntity(shot);
		    OtherSounds.WebFire.Play();
            
            // randomize this spider's next fire-time so that all spiders 
            // don't fire at once
            betweenFiresDuration.Length =  
                BetweenFiresDurationLength / 2 + 
                Random.NextInt(BetweenFiresDurationLength);
            
            betweenFiresDuration.Start();
	    }

        [OverriddenValue]
        protected override bool ShouldAlignWithGroundOrCeiling
        {
            get { return !InState(MonsterState.Sleeping); }
        }

        [HookOverride]
        protected override void AlignSpecially()
        {
            // align this spider with its background passageway 
            AddToLocation(
                (short)-(LeftX % UnitsPerTenFeet), 
                (short)-(TopY % UnitsPerTenFeet));
        }
    }
}