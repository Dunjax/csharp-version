using Dunjax.Entities.Beings.Monsters.Concrete;

namespace Dunjax.Entities.Beings.Monsters
{
    class MonsterFactory
    {
        public static IMonster CreateMonster(MonsterTypeEnum type, IClock clock)
        {
            IMonster monster = null;

            switch (type)
            {
                case MonsterTypeEnum.GreenGargoyle:
                    monster = new GreenGargoyle(clock);
                    break;
                case MonsterTypeEnum.YellowGargoyle:
                    monster = new YellowGargoyle(clock);
                    break;
                case MonsterTypeEnum.BlueGargoyle:
                    monster = new BlueGargoyle(clock);
                    break;
                case MonsterTypeEnum.Whirlwind:
                    monster = new Whirlwind(clock);
                    break;
                case MonsterTypeEnum.Stalagmite:
                    monster = new Stalagmite(clock);
                    break;
                case MonsterTypeEnum.Master:
                    monster = new Master(clock);
                    break;
                case MonsterTypeEnum.Spider:
                    monster = new Spider(clock);
                    break;
                case MonsterTypeEnum.Slime:
                    monster = new Slime(clock);
                    break;
            }

            return monster;
        }
    }
}