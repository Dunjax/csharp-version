﻿using Dunjax.Entities.Beings.Monsters.Concrete;

namespace Dunjax.Entities.Beings.Monsters.Movement
{
    public interface IMonsterMovementMethod
    {
        /**
         * Returns whether the monster was moved.
         */
        bool MoveMonster();
    }

    public interface IMonsterMovementMethodView : IMonster
    {
        /**
         * Adds the given state to those this monster is currently in.
         */
        void AddState(BeingState state);

        /**
         * Removes the given state from those this monster is currently in.
         */
        void RemoveState(BeingState state);

            /**
         * Whether this monster is currently facing left (vs. right).
         */
        new bool FacingLeft { get; set; }

        /**
         * Whether this monster is currently attached to the ceiling.
         */
        bool OnCeiling { get; set; }

        /**
         * Whether this monster has a turning sequence for its current state
         * of being either on the ground or the ceiling.
         */
        bool HasTurnSequence { get; }

        /**
         * Returns whether this monster is physically close enough to the player
         * to attack him.
         */
        bool IsCloseEnoughToAttack();

        /**
         * The clock this monster is using to measure time.
         */
        IClock Clock { get; }

        /**
         * The random number generator used by this monster, to also be used 
         * by the movement method.
         */
        IRandom Random { get; }
    }

    interface IFlyMonsterMovementMethodView : IMonsterMovementMethodView
    {
        /**
         * Returns whether this flying-monster is currently in a flying state. 
         */
        bool Flying { get; }

        /**
         * Specifies flying-monster-specific sounds.
         */
        FlyingMonster.FlyingMonsterSoundsGroup FlyingMonsterSounds { get; }
    }

    interface IFallDownAndUpMovementMethodView : IMonsterMovementMethodView
    {
        /**
         * Returns the monster's particular state which indicates it is falling.
         */
        MonsterState FallingState { get; }

        /**
         * Returns the monster's particular state which indicates it is falling up.
         */
        MonsterState FallingUpState { get; }

        /**
         * Informs the monster that this method started it on a fall.
         */
        void FallStarted();
    }
}
