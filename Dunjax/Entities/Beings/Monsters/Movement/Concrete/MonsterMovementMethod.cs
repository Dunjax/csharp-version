﻿using Dunjax.Attributes;

namespace Dunjax.Entities.Beings.Monsters.Movement.Concrete
{
    /**
     * Is meant to provide base functionality of use to the various 
     * monster movement methods.
     */
    public abstract class MonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving.
         */
        protected IMonsterMovementMethodView monster;

        [Constructor]
        protected MonsterMovementMethod(IMonsterMovementMethodView monster)
        {
            this.monster = monster;
        }

        /**
         * Returns whether the player is to one side of this monster (versus
         * being coincident, x-wise).
         */
        protected bool IsPlayerToSide()
        {
            return monster.GetSideEntityIsOn(monster.Map.Player) != Side.NeitherSide;
        }

        /**
         * Returns whether the player is to the left of this monster.
         */
        protected bool IsPlayerOnLeft()
        {
            return monster.GetSideEntityIsOn(monster.Map.Player) == Side.Left;
        }
    }
}
