﻿using Dunjax.Attributes;
using Dunjax.Geometry;
using Dunjax.Map;

namespace Dunjax.Entities.Beings.Monsters.Movement.Concrete
{
    /**
     * Tries to move this monster towards the player along the ground 
     * (or the ceiling, if this is a ceiling-dwelling monster).
     */
    class MoveAlongGroundOrCeiling : MonsterMovementMethod, IMonsterMovementMethod
    {
        /**
         * Whether the monster is currently moving in a wandering way 
         * (i.e. moving away from the player for a random period, to give 
         * its movement some variance).
         */
        private bool isWandering;

        /**
         * The odds of any single check to stop wandering succeeding.  This is 
         * randomly set when the monster starts wandering.
         */
        private int oddsToStopWandering;

        [Constructor]
        public MoveAlongGroundOrCeiling(IMonsterMovementMethodView monster)
            : base(monster)
        {
        }

        [Implementation]
        public bool MoveMonster()
        {
            CheckForWanderingEnd();

            CheckForWanderingStart();

            if (ShouldTurn()) monster.AddState(BeingState.Turning);

            else if (ShouldChangeFacingWithoutTurn()) 
                monster.FacingLeft = !monster.FacingLeft;

            if (monster.InState(BeingState.Turning)) return true;

            if (ShouldMoveToSide()) {
                if (CanMoveToSide()) {
                    MoveToSide();
                    return true;
                }
                
                else MoveBlocked();
            }

            return false;
        }

        /**
         * Returns whether the monster should currently move one increment
         * to the side it's facing.
         */
        protected bool ShouldMoveToSide()
        {
            return !monster.InState(BeingState.Turning)
                && (IsPlayerToSide() || isWandering);
        }
        
        /**
         * Returns whether the monster can currently move one increment
         * to the side it's facing.
         */
        protected bool CanMoveToSide()
        {
            // return whether the monster is not turning,
            // it's passible to the side of this monster's facing, 
            // and there's solid ground/ceiling to that side. 
            var map = monster.Map;
            var side = monster.FacingLeft ? Side.Left : Side.Right;
            return monster.IsPassibleToSide(side, ImpassableEntitiesSet.BeingsSet)
                && !map.IsFallThroughable(
                    monster.Center.Set(
                        (short)(side == Side.Left ? monster.LeftX - 1 : monster.RightX + 1),
                        (short)(monster.OnCeiling ? monster.TopY - 1 : monster.BottomY + 1)));
        }

        /**
         * Moves this monster one increment to the side of its current facing.
         */
        protected void MoveToSide()
        {
            // move this monster one increment to the side the monster is facing
            monster.AddToLocation((short)(monster.FacingLeft ? -1 : 1), 0);
            
            // add the moving state even if the monster is already in it, as doing 
            // so keeps the moving animation going
            monster.AddState(BeingState.Moving);
        }

        /**
         * Is called when the monster cannot move the next increment in the direction
         * it's facing.
         */
        private void MoveBlocked()
        {
            // if the monster is wandering, that wandering is over
            if (isWandering) isWandering = false;

            // otherwise, check to start wandering
            else if (monster.Random.NextInt(50) == 0) StartWandering();
        }
        
        /**
         * Returns whether this monster should turn to face the player.
         */
        protected bool ShouldTurn()
        {
            // return whether the monster is not wandering,
            // is not already turning,
            // has a turning frame-sequence,
            // and is facing the wrong direction
            return !isWandering
                && !monster.InState(BeingState.Turning)
                && monster.HasTurnSequence
                && IsPlayerToSide() 
                && monster.FacingLeft != IsPlayerOnLeft();
        }

        /**
         * Returns whether this monster should change facing (without a turn)
         * to face the player.
         */
        protected bool ShouldChangeFacingWithoutTurn()
        {
            // return whether the monster the monster is not wandering,
            // doesn't have a turning frame-sequence,
            // and is facing the wrong direction
            return !isWandering
                && !monster.HasTurnSequence
                && IsPlayerToSide()
                && monster.FacingLeft != IsPlayerOnLeft();
        }

        /**
         * If the monster is wandering, checks to see if it should now stop.
         */
        private void CheckForWanderingEnd()
        {
            if (!isWandering) return;

            // if a random chance says yes, stop the wandering
            if (monster.Random.NextInt(oddsToStopWandering) == 0) isWandering = false;
        }

        /**
         * If the monster is not wandering, checks to see if it should now start.
         */
        private void CheckForWanderingStart()
        {
            if (isWandering) return;

            // if the monster is to neither side of the player,
            // but is not close enough to attack,
            // and a random chance says yes,
            // start the wandering
            if (!IsPlayerToSide() 
                && !monster.IsCloseEnoughToAttack()
                && monster.Random.NextInt(100) == 0) StartWandering();
        }

        /**
         * Has the monster enter into its wandering movement mode.
         */
        private void StartWandering()
        {
            // enter into wandering mode
            isWandering = true;

            // randomly determine how long the wandering is likely to continue
            oddsToStopWandering = 50 + monster.Random.NextInt(300);
            
            // if the monster can turn, have it turn
            if (monster.HasTurnSequence) monster.AddState(BeingState.Turning);

            // otherwise, change its facing
            else monster.FacingLeft = !monster.FacingLeft;
        }
    }
}
