﻿using Dunjax.Attributes;

namespace Dunjax.Entities.Beings.Monsters.Movement.Concrete
{
    /**
     * A movement method which causes the affected monster to move along
     * the ground or ceiling normally, then fall down or up to the ground below or
     * ceiling above, then continue the pattern.
     */
    class FallDownAndUpMovementMethod : IMonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving.
         */
        protected IFallDownAndUpMovementMethodView monster;

        /**
         * The movement method employed to move this monster along the ground or ceiling.
         */
        protected IMonsterMovementMethod groundAndCeilingMovementMethod;

        /**
         * Keeps track of how much time is left for this monster to move along
         * the ground (or the ceiling) until it should fall down (or up).
         */
        protected readonly IDuration betweenFallsDuration;

        [Constructor]
        public FallDownAndUpMovementMethod(
            IFallDownAndUpMovementMethodView monster)
        {
            this.monster = monster;
            groundAndCeilingMovementMethod = new MoveAlongGroundOrCeiling(monster);
            betweenFallsDuration = DunjaxFactory.CreateDuration(monster.Clock, 700, false);

            // have the monster start with a fall whose direction is chosen randomly
            monster.RemoveState(BeingState.Still);
            monster.AddState(monster.Random.NextBoolean() ?
                monster.FallingState : monster.FallingUpState);
        }

        [Implementation]
        public bool MoveMonster()
        {
            // if this monster is at a point where it could start falling, move appropriately
            if (CouldStartFall()) MoveWhenCouldStartFall();

            // else, if this monster is falling, have it fall some more
            else if (monster.InState(monster.FallingState)) Fall(false);

            // else, if this monster is falling up, have it fall up some more 
            else if (monster.InState(monster.FallingUpState)) Fall(true);

            return true;
        }

        /**
         * Returns whether this monster is in a state where it could start a fall.
         */
        protected bool CouldStartFall()
        {
            return monster.InState(BeingState.Still) 
                || monster.InState(BeingState.Moving);
        }

        /**
         * Has this monster under the presumption that it's in a state/position to
         * start a fall.
         */
        protected void MoveWhenCouldStartFall()
        {
            // if it's time for this monster to fall/fall-up, start the fall
            if (betweenFallsDuration.Done) StartFall();

            // otherwise, try to move along the ground or ceiling normally 
            else groundAndCeilingMovementMethod.MoveMonster();
        }

        /**
         * Starts this monster on a fall.
         */
        protected void StartFall()
        {
            monster.RemoveState(BeingState.Still);
            monster.RemoveState(BeingState.Moving);
            monster.AddState(monster.OnCeiling ?
                monster.FallingState : monster.FallingUpState);
            monster.FallStarted();
        }

        /**
         * Has this monster try to fall one increment downward or upward. 
         * 
         * @return	Whether the fall-movement occurred.
         */
        protected bool Fall(bool upward)
        {
            bool fell = false;

            if (CanFallOneIncrement(upward)) {
                // move this monster down/up one increment
                monster.AddToLocation(0, (short)(upward ? -1 : 1));
                fell = true;
            }

            else EndFall(upward);

            return fell;
        }

        /**
         * Returns whether the map one increment in the given direciton is 
         * fall-througable for this monster.
         */
        protected bool CanFallOneIncrement(bool upward)
        {
            var test = 
                upward ? monster.TopCenter.Add(0, -1) : monster.BottomCenter.Add(0, 1);
            var width = monster.Width;
            return monster.Map.IsFallThroughable(test.Add((short)(-width / 4), 0)) &&
                monster.Map.IsFallThroughable(test.Add((short)(width / 4), 0));
        }

        /**
         * Ends the fall this monster is presumed to be currently in.
         */
        protected void EndFall(bool fallWasUpward)
        {
            // this monster is no longer falling
            monster.RemoveState(fallWasUpward ? monster.FallingUpState : monster.FallingState);
            betweenFallsDuration.Start();

            // this monster is now considered to be on the ground/ceiling;
            // this has to come before the addition of the still state below,
            // otherwise a wrong sequence will be assigned to the monster
            monster.OnCeiling = fallWasUpward;

            // this monster is now still
            monster.AddState(BeingState.Still);
        }
    }
}
