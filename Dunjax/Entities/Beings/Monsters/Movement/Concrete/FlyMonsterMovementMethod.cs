﻿using Dunjax.Attributes;
using Dunjax.Map;

namespace Dunjax.Entities.Beings.Monsters.Movement.Concrete
{
    class FlyMonsterMovementMethod : MonsterMovementMethod, IMonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving.
         */
        protected new IFlyMonsterMovementMethodView monster;

        /**
	     * Whether this monster is currently allowed to fly diagonally.  We 
	     * don't always let it fly diagonally, as then its path looks too simple
	     * and unnatural.
	     */
        protected bool mayFlyDiagonally;

        /**
         * Keeps track of how long this monster's current spell of diagonal flight
         * has lasted.
         */
        protected readonly IDuration diagonalFlightDuration;

        /**
         * Keeps track of how long it's been since the last time this monster's
         * flight sound was played.
         */
        protected readonly IDuration durationBetweenFlightSounds;

        /**
         * The vertical direction in which the monster is currently pointed to fly.
         */
        protected VerticalSide verticalFlightDirection;

        /**
         * Sometimes, this flying monster needs to fly up or down to get around
         * an obstacle, when the player is in the opposite vertical direction.
         * In such a case we don't we this monster to revert to flying towards
         * the player vertically until a way around the obstacle is found 
         * and taken.  When this variable is true, flight should continue 
         * in the current vertical direction until the obstacle is no longer 
         * in the way, or the monster runs into something.
         */
        protected bool flyingVerticallyToPassObstacle;

        /**
         * Sometimes, this flying monster needs to fly left or right to get around
         * an obstacle, when the player is in the opposite horizontal direction.
         * In such a case we don't we this monster to revert to flying horizontally
         * towards the player until a way around the obstacle is found
         * and taken.  When this variable is true, flight should continue 
         * in the current horizontal direction  until the obstacle is no longer 
         * in the way, or the monster runs into something.
         */
        protected bool flyingHorizontallyToPassObstacle;

        /**
         * While this monster is taking off to fly, how many fly-moves must still
         * elapse before the takeoff is considered complete. 
         */
        protected int takeoffMovesLeft;

        [Constructor]
        public FlyMonsterMovementMethod(IFlyMonsterMovementMethodView monster)
            : base(monster)
        {
            this.monster = monster;

            diagonalFlightDuration = DunjaxFactory.CreateDuration(monster.Clock, 1000);
            durationBetweenFlightSounds = DunjaxFactory.CreateDuration(monster.Clock, 270);
        }

        [Implementation]
        public bool MoveMonster()
        {
            if (!monster.Flying) {
                StartFlight();
                return true;
            }

            if (ShouldLand()) {
                Land();
                return true;
            }

            return FlyMove();
        }

        /**
         * Returns whether this monster's state and position is such that it can 
         * (and should) immediately land on a surface.
         */
        protected bool ShouldLand()
        {
            // return whether this monster's feet are at ground level and it's not 
            // in the process of taking off, and it's not flying in such 
            // a way as to pass an obstacle (in which case, it shouldn't land
            // because it's trying to find its way around something)
            return !flyingHorizontallyToPassObstacle
                && !flyingVerticallyToPassObstacle
                && takeoffMovesLeft == 0
                && !monster.Map.IsPassible(monster.BottomCenter.Add(0, 1));
        }

        /**
         * Puts the monster in the landing state.
         */
        protected void Land()
        {
            monster.RemoveState(FlyingMonsterState.Flying);
            monster.RemoveState(FlyingMonsterState.FlyTurning);
            monster.AddState(FlyingMonsterState.Landing);
        }
        
        /**
         * Starts this monster taking off to fly.
         */
        protected void StartFlight()
        {
            // change this monster back to flying
            monster.AddState(FlyingMonsterState.Flying);
            flyingHorizontallyToPassObstacle = false;
            flyingVerticallyToPassObstacle = false;
            verticalFlightDirection = VerticalSide.Top;

            // remember that this monster is just taking off
            takeoffMovesLeft = 6;
        }

        /**
         * Tries to make this monster fly one increment towards the player,
         * for some periods diagonally, and for others not. Will also attempt to fly
         * this monster vertically or horizontally to pass an obstacle if one is 
         * in the way.
         * 
         * @return  Whether a fly-movement took place.
         */
        protected bool FlyMove()
        {
            // if this monster should fly-turn
            bool movedHorizontally = false, flyMoved = false;
            if (ShouldFlyTurn()) {
                // do so
                monster.AddState(FlyingMonsterState.FlyTurning);
                flyMoved = true;
            }

            // if this monster should try to fly-move horizontally
            else if (ShouldFlyMoveHorizontally()) {
                // if it can do so
                if (CanFlyMoveHorizontally()) {
                    // do so
                    FlyMoveHorizontally();
                    flyMoved = movedHorizontally = true;
                }

                // otherwise, it is blocked
                else HorizontalFlyMoveBlocked();
            }

            // check to see if it's time to allow/disallow diagonal flight 
            // for the next period of time
            if (diagonalFlightDuration.Done) mayFlyDiagonally = !mayFlyDiagonally;

            CheckShouldFlyVerticallyToPassObstacle(movedHorizontally);

            CheckToDetermineVerticalFlightDirection();

            // if this monster should try to fly-move vertically
            if (ShouldFlyMoveVertically(movedHorizontally)) {
                // if it can do so
                if (CanFlyMoveVertically()) {
                    // do so
                    FlyMoveVertically();
                    flyMoved = true;
                }

                // otherwise, it is blocked
                else VerticalFlyMoveBlocked(movedHorizontally);
            }

            // if any flight-move occurred above, and it's an okay time to play
            // this monster's flight sound, play it
            if (flyMoved && ShouldPlayFlightSound()) 
                monster.FlyingMonsterSounds.Fly.Play();

            // if any flight-move occurred above, and this monster is taking off, 
            // one more movement during the takeoff has just occurred
            if (flyMoved && takeoffMovesLeft > 0) takeoffMovesLeft--;

            return flyMoved;
        }

        /**
         * Returns whether the monster should begin a fly-turn.
         */
        protected bool ShouldFlyTurn()
        {
            // return whether it is not already fly-turning,
            // if it is not currently flying horizontally to pass an obstacle, 
            // and is facing away from the player
            return !monster.InState(FlyingMonsterState.FlyTurning)
                && !flyingHorizontallyToPassObstacle 
                && IsPlayerToSide()
                && monster.FacingLeft != IsPlayerOnLeft();
        }
        
        /**
         * Returns whether the monster should fly-move horizontally one increment
         * in the direction of its current facing. 
         */
        protected bool ShouldFlyMoveHorizontally()
        {
            // return whether the monster is not fly-turning,
            // is either to one side of the player or 
            // is flying horizontally to try to get past an obstacle,
            // and isn't flying horizontally to try to get past an obstacle
            // while it could move vertically (as the fact that horizontal
            // moves occur first means that monster would otherwise miss
            // certain vertical openings which may allow it past the obstacle)
            return !monster.InState(FlyingMonsterState.FlyTurning) 
                && (IsPlayerToSide() || flyingHorizontallyToPassObstacle)
                && !(flyingHorizontallyToPassObstacle && IsPlayerToVerticalSide() && CanFlyMoveVertically());
        }

        /**
         * Checks whether the monster's vertical flight direction should be 
         * changed, and if it does, changes it to be towards the player.
         */
        protected void CheckToDetermineVerticalFlightDirection()
        {
            if (!flyingVerticallyToPassObstacle 
                && IsPlayerToVerticalSide()) 
                verticalFlightDirection = 
                    monster.GetVerticalSideEntityIsOn(monster.Map.Player);
        }

        /**
         * Returns whether the monster should fly-move vertically one increment
         * in the direction of its current vertical facing. 
         */
        protected bool ShouldFlyMoveVertically(bool movedHorizontally)
        {
            return
                !monster.InState(FlyingMonsterState.FlyTurning)
                && (
                    flyingVerticallyToPassObstacle
                    || (
                        (mayFlyDiagonally || !movedHorizontally)
                        && (
                            IsPlayerToVerticalSide()
                            || (!movedHorizontally && !monster.IsCloseEnoughToAttack()))));
        }

        /**
         * Assuming that this monster has just performed a flight-move, 
         * this returns whether it's flight sound should be played to 
         * accompany that movement. 
         */
        protected bool ShouldPlayFlightSound()
        {
            // return whether it's been long enough since the last time this 
            // monster's flight sound was played, and this monster can see the 
            // player from its location
            return durationBetweenFlightSounds.Done
                && monster.Map.IsLocationVisibleFrom(
                    monster.Map.Player.Center, monster.Center, false, false);
        }

        /**
         * Returns whether the monster is currently capable of fly-moving
         * horizontally one increment in the direction of its current facing. 
         */
        protected bool CanFlyMoveHorizontally()
        {
            // if this monster is fly-turning, then no
            if (monster.InState(FlyingMonsterState.FlyTurning)) return false;

            // check passability to the side of the monster's facing
            return monster.IsPassibleToSide(
                monster.FacingLeft ? Side.Left : Side.Right, 
                ImpassableEntitiesSet.BeingsSet);
        }
        
        /**
         * Flies this monster one increment to the side of its current facing.
         */
        protected bool FlyMoveHorizontally()
        {
            // move this monster one increment to the side the monster is facing 
            monster.AddToLocation((short)(monster.FacingLeft ? -1 : 1), 0);

            // add the flying state even if the monster is already in it, as doing 
            // so keeps the flying animation going
            monster.AddState(FlyingMonsterState.Flying);

            return true;
        }

        /**
         * Informs this monster that it's most recent fly-move to the given side
         * was blocked by something impassable.
         */
        protected void HorizontalFlyMoveBlocked()
        {
            // if this monster has been flying horizontally to pass an 
            // intervening obstacle, that movement has proven to be blocked, 
            // so do so no longer
            if (flyingHorizontallyToPassObstacle)
                flyingHorizontallyToPassObstacle = false;

            // otherwise, if this monster isn't close enough to
            // attack the player, can't move vertically, a random 
            // chance says yes (since always doing this will limit the situations 
            // in which this monster can find a way to reach the player), 
            // and it's passable to opposite side of this monster
            else if (!CanFlyMoveVertically()
                && !monster.IsCloseEnoughToAttack()
                && monster.Random.NextInt(2) == 0
                && monster.IsPassibleToSide(
                    monster.FacingLeft ? Side.Right : Side.Left,
                    ImpassableEntitiesSet.BeingsSet)) {
                // have the monster begin to try to fly horizontally in the opposite
                // direction from that tried above; such action helps
                // this monster find a way out of situations when it is
                // enclosed in a "C" or reverse-"C" terrain feature by
                // moving towards the opening
                monster.AddState(FlyingMonsterState.FlyTurning);
                flyingHorizontallyToPassObstacle = true;
            }
        }

        /**
         * Returns whether the monster is currently capable of fly-moving
         * vertically one increment in the direction of its current vertical facing. 
         */
        protected bool CanFlyMoveVertically()
        {
            return monster.IsPassibleToVerticalSide(
                verticalFlightDirection, ImpassableEntitiesSet.BeingsSet);
        }

        /**
         * Detm's whether this monster is in a state to start
         * flying vertically in order to maneuver around an obstacle.
         * 
         * @param movedHorizontally     Whether this monster has already flown
         *                              horizontally this game turn.
         */
        protected void CheckShouldFlyVerticallyToPassObstacle(
            bool movedHorizontally)
        {
            // if the monster isn't already flying to avoid an obstacle, 
            // is level vertically with the player, 
            // it hasn't yet moved horizontally this turn, 
            // isn't fly-turning,
            // and isn't close enough to the player to attack
            if (!flyingVerticallyToPassObstacle
                && !flyingHorizontallyToPassObstacle
                && !movedHorizontally
                && !IsPlayerToVerticalSide()
                && !monster.InState(FlyingMonsterState.FlyTurning)
                && !monster.IsCloseEnoughToAttack()) {
                // have this monster choose a vertical direction randomly
                // to which to try to fly around the obstruction
                flyingVerticallyToPassObstacle = true;
                verticalFlightDirection = 
                    monster.Random.NextBoolean() ?
                        VerticalSide.Top : VerticalSide.Bottom;
            }
        }

        /**
         * Flies this monster one increment towards the monster's current
         * vertical facing. 
         */
        protected bool FlyMoveVertically()
        {
            monster.AddToLocation(0, 
                (short)(verticalFlightDirection == VerticalSide.Top ? -1 : 1));

            if (!monster.InState(FlyingMonsterState.FlyTurning)) 
                monster.AddState(FlyingMonsterState.Flying);

            return true;
        }

        /**
         * Informs this monster that it's most recent fly-move to the given vertical 
         * side was blocked by something impassable.
         * 
         * @param movedHorizontally    Whether this monster has already flown
         *                                          horizontally this game turn.
         */
        protected void VerticalFlyMoveBlocked(bool movedHorizontally)
        {
            if (flyingVerticallyToPassObstacle)
                flyingVerticallyToPassObstacle = false;

            else if (!movedHorizontally && !monster.IsCloseEnoughToAttack())
                FlyToPassObstacleToVerticalFlight();
        }

        /**
         * Has this monster fly either horizontally or vertically to pass 
         * an obstacle that's in the given vertical direction.
         */
        protected void FlyToPassObstacleToVerticalFlight()
        {
            // if this monster is at the player's x-position, 
            // and a random chance says yes
            if (!IsPlayerToSide() || monster.Random.NextInt(3) > 0) {
                // have it fly in a random, passable direction horizontally 
                // to try to get around the obstacle
                flyingHorizontallyToPassObstacle = true;
                if (monster.IsPassibleToSide(
                    monster.FacingLeft ? Side.Left : Side.Right, 
                    ImpassableEntitiesSet.BeingsSet) 
                    && monster.Random.NextInt(3) > 0) {}
                else if (monster.IsPassibleToSide(
                    monster.FacingLeft ? Side.Right : Side.Left,
                    ImpassableEntitiesSet.BeingsSet)) 
                    monster.AddState(FlyingMonsterState.FlyTurning);
                else flyingHorizontallyToPassObstacle = false;
            }

            // otherwise, if a random chance says yes
            else if (monster.Random.NextInt(2) == 0) {
                // this monster should now begin to try to fly vertically 
                // in the opposite direction of the obstacle attempted above
                // in order to find a way around the intervening obstacle
                flyingVerticallyToPassObstacle = true;
                verticalFlightDirection =
                    (verticalFlightDirection == VerticalSide.Top 
                        ? VerticalSide.Bottom : VerticalSide.Top);
            }
        }

        /**
         * Returns whether the player is to one vertical side of this monster (versus
         * being coincident, y-wise).
         */
        [Shorthand]
        protected bool IsPlayerToVerticalSide()
        {
            return monster.GetVerticalSideEntityIsOn(monster.Map.Player)
                != VerticalSide.NeitherVerticalSide;
        }

        /**
         * Returns whether the player is above this monster.
         */
        [Shorthand]
        protected bool IsPlayerAbove()
        {
            return monster.GetVerticalSideEntityIsOn(monster.Map.Player) 
                == VerticalSide.Top;
        }
    }
}
