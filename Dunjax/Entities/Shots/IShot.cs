using Dunjax.Geometry;

namespace Dunjax.Entities.Shots
{
    /**
     * A shot of a weapon or monster attack.
     */
    interface IShot : IMapEntity
    {
        /**
         * This shot's shot-type, which is one of those listed below.
         */
        ShotType ShotType { get; }
        
        /**
         * This shot's current unit vector direction. 
         */
        IFloatPoint Direction { get; }
        
       /**
         * The amount of damage this shot causes when it impacts something.
         */
        int Damage { get; }
    }

    /**
     * The different kinds of shots that are in the game. 
     */
    enum ShotType {FireballHit, FireballShot, FireSpot, GrapeshotShot,
        HalberdShot, PulseShot, SlimeballHit, SlimeShot, SlimeSpot, WebShot};
}