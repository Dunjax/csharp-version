using Dunjax.Attributes;
using Dunjax.Entities.Shots.Concrete;
using Dunjax.Geometry;

namespace Dunjax.Entities.Shots
{
    class ShotFactory
    {
        [Shorthand]
        public static IShot CreateShot(ShotType type, IClock clock)
        {
            return CreateShot(type, null, clock);
        }

        /**
         * Creates and returns a shot of a type corresponding to that given.
         */
        public static IShot CreateShot(ShotType type, IPoint startingDirection, IClock clock)
        {
            IShot shot = null;

            switch (type) {
                case ShotType.FireballHit:
                    shot = new FireballHit(clock);
                    break;
                case ShotType.FireballShot:
                    shot = new FireballShot(startingDirection, clock);
                    break;
                case ShotType.FireSpot:
                    shot = new FireSpot(clock);
                    break;
                case ShotType.GrapeshotShot:
                    shot = new GrapeshotShot(startingDirection, clock);
                    break;
                case ShotType.HalberdShot:
                    shot = new HalberdShot(startingDirection, clock);
                    break;
                case ShotType.PulseShot:
                    shot = new PulseShot(startingDirection, clock);
                    break;
                case ShotType.SlimeballHit:
                    shot = new SlimeballHit(clock);
                    break;
                case ShotType.SlimeShot:
                    shot = new SlimeballShot(startingDirection, clock);
                    break;
                case ShotType.SlimeSpot:
                    shot = new SlimeSpot(clock);
                    break;
                case ShotType.WebShot:
                    shot = new WebShot(startingDirection, clock);
                    break;
            }

            return shot;
        }
    }
}