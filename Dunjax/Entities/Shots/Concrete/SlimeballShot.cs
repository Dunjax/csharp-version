using Dunjax.Attributes;
using Dunjax.Entities.Beings;
using Dunjax.Geometry;
using Dunjax.Sound;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * Is fired by the master.  Hits the ceiling, and then causes a slimel-hit
     * at that location. 
     */
    class SlimeballShot : Shot
    {
        [Constructor]
	    public SlimeballShot(IPoint startingDirection, IClock clock) :
            base(ShotType.SlimeShot, startingDirection, clock)
	    {
	    }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 4;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class SlimeballShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static SlimeballShotFrameSequences Singleton = 
                new SlimeballShotFrameSequences();

	        [Constructor]
            private SlimeballShotFrameSequences()
	        {
                UpAngle = CreateFrameSequence("upAngle");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "slimeShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return SlimeballShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 50; } }

        [OverriddenValue]
	    protected override ISound ImpactSound { get {return Sounds.SlimeballImpact;} }

        /** 
        * Overridden to check if this slimeball-shot hit the player.
        */
        protected override void PreRemovedFromPlayDueToImpact(IBeing beingHit) 
        {
		    // if a being wasn't hit
		    if (beingHit == null) {
			    // start a slime-hit at the impact location
                IShot shot = new SlimeballHit(clock);
                shot.CenterOn(Center);
			    Map.AddEntity(shot);
		    }
	    }
    }
}