using Dunjax.Attributes;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * When a fireball hits the ground, it is replaced by a shot of this kind,
     * which is invisible.  It creates fire-spots to either side of itself over 
     * a certain distance, and over a certain amount of time.
     */
    class FireballHit : Shot
    {
	    /**
	     * How many pairs of fire-spots this fireball-hit must set before
	     * going away.
	     */
	    protected static int NumFireSpotPairsToSet = 5;
    	
	    /**
	     * How many pairs of fire-spots this fireball-hit has set so far.
	     */
	    protected int numFireSpotPairsSet;
    	
        [Constructor]
        public FireballHit(IClock clock) : base(ShotType.FireballHit, clock)
        {
        }

        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return null; } }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 100;} }

        [OverriddenValue]
        public override int Damage { get { return 0; } }

        [OverriddenValue]
        protected override bool Moves { get {return false;} }

        /** 
        * Overridden to specify this fireball-hit's action.
        */
        protected override void ActBeforeMove()
	    {
            // if it hasn't been long enough since this shot's last act
		    // (equating actions with moves for this purpose), don't act this time
            if (!betweenMovesDuration.Done) return;
            
		    // start fire-spots at an equal and increasing distance on the 
            // left and right sides of this fireball-hit
		    IShot spot = new FireSpot(clock);
            var y = (short)(Map.GetYJustAboveGround(Center) - spot.Height + 1);
            spot.MoveTo((short)(Center.X - (numFireSpotPairsSet + 1) * spot.Width), y);
		    Map.AddEntity(spot);
            spot = new FireSpot(clock);
            Map.AddEntity(spot);
            spot.MoveTo((short)(Center.X + numFireSpotPairsSet * spot.Width), y);

		    // if this fireball-hit has spawned all of its firespot pairs, remove it
		    if (++numFireSpotPairsSet == NumFireSpotPairsToSet) RemoveFromPlay();
	    }
    }
}