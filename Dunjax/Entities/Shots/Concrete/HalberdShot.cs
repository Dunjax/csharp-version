using Dunjax.Attributes;
using Dunjax.Geometry;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * The shot issued by the player's melee weapon.  It has a very short range.
     */
    class HalberdShot : Shot
    {
        [Constructor]
        public HalberdShot(IPoint startingDirection, IClock clock) :
            base(ShotType.HalberdShot, startingDirection, clock)
	    {
	    }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 2;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class HalberdShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static HalberdShotFrameSequences Singleton = 
                new HalberdShotFrameSequences();

	        [Constructor]
            private HalberdShotFrameSequences()
	        {
                Level = CreateFrameSequence("level");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "halberdShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return HalberdShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 3; } }

        [OverriddenValue]
        protected override TargetBeingTypeEnum TargetBeingType
        {
            get { return TargetBeingTypeEnum.Monster; }
        }

        [OverriddenValue]
        protected override int Range { get { return UnitsPerTenFeet; } }
    }
}