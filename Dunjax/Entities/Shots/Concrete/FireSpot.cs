using Dunjax.Animations;
using Dunjax.Attributes;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * Is a stationary spot of fire, usually started by a fireball-hit.
     */
    class FireSpot : Shot
    {
        [Constructor]
        public FireSpot(IClock clock) : base(ShotType.FireballShot, StationaryDirection, clock)
	    {
            existenceDuration.Length = 1000;
		    betweenMovesDuration.Start();
	    }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 70;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class FireSpotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static FireSpotFrameSequences Singleton = 
                new FireSpotFrameSequences();

	        [Constructor]
            private FireSpotFrameSequences()
	        {
                Stationary = CreateFrameSequence("", true, FramePriority.Normal, false, null, null);
                Stationary.SetDurationOfAllFrames(60);
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "fireSpot";} }
	    }

        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return FireSpotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 10; } }

        [OverriddenValue]
        protected override bool CanFall() {return false;}
    }
}