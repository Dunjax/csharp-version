using Dunjax.Attributes;
using Dunjax.Entities.Beings;
using Dunjax.Entities.Beings.Player;
using Dunjax.Geometry;
using Dunjax.Sound;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * Is the shot fired by a spider.
     */
    class WebShot : Shot
    {
        [Constructor]
        public WebShot(IPoint startingDirection, IClock clock) :
            base(ShotType.WebShot, startingDirection, clock)
	    {
	    }
        
        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 3; } }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class WebShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static WebShotFrameSequences Singleton = 
                new WebShotFrameSequences();

	        [Constructor]
            private WebShotFrameSequences()
	        {
                Level = DownAngle = Down = CreateFrameSequence("level");
                Up = UpAngle = CreateFrameSequence("upAngle");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "webShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return WebShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 0; } }

        [OverriddenValue]
	    protected override ISound ImpactSound { get {return Sounds.WebImpact;} }

        /** 
        * Overridden to check if this web-shot hit the player.
        */
        protected override void PreRemovedFromPlayDueToImpact(IBeing beingHit) 
        {
            // if this web-shot impacted the player
            if (beingHit is IPlayer) {
                // the player is webbed
                ((IPlayer)beingHit).Webbed();
            }
	    }
    }
}