using Dunjax.Attributes;
using Dunjax.Geometry;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * One of the three shots fired at a time by the player's grapeshot gun.
     */
    class GrapeshotShot : Shot
    {
        [Constructor]
        public GrapeshotShot(IPoint startingDirection, IClock clock) :
            base(ShotType.GrapeshotShot, startingDirection, clock)
	    {
	    }

        [Constructor]
        protected GrapeshotShot(IClock clock) : this(null, clock)
        {
        }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 2;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class GrapeshotShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static GrapeshotShotFrameSequences Singleton = 
                new GrapeshotShotFrameSequences();

	        [Constructor]
            private GrapeshotShotFrameSequences()
	        {
                Level = CreateFrameSequence("level");
                Up = CreateFrameSequence("up");
                Down = CreateFrameSequence("Down");
                UpAngle = CreateFrameSequence("upAngle");
                DownAngle = CreateFrameSequence("downAngle");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "grapeShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return GrapeshotShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 1; } }

        [OverriddenValue]
        protected override TargetBeingTypeEnum TargetBeingType
        {
            get { return TargetBeingTypeEnum.Monster; }
        }
    }
}