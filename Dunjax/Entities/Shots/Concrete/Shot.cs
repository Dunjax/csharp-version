using System;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Beings;
using Dunjax.Entities.Concrete;
using Dunjax.Geometry;
using Dunjax.Sound;

namespace Dunjax.Entities.Shots.Concrete
{
    abstract class Shot : MapEntity, IShot
    {
        /**
         * This shot's shot-type, to allow identification of that type by foreign objects
         * without them resorting to looking at this shot's implementation class. 
         */
        public ShotType ShotType {get; protected set;}
        
	    /**
	     * A more precise storage of this shot location than that provided by
	     * MapEntity, as shots can travel in any direction.  This class is 
	     * responsible for keeping the two location values in sync.
	     */
        protected IFloatPoint floatLocation = GeometryFactory.CreateFloatPoint();

	    /**
	     * The unitized direction in which this shot is currently travelling.
	     */
        public IFloatPoint Direction {get; protected set;}

        /**
         * How many more moves this shot must make before its next check to see
         * whether it has hit a being.  We don't check every time since it is
         * expensive to do so.
         */
        protected int movesUntilCheckForBeings;
        
        /**
         * What movesUntilCheckForBeings gets set back to after each check to 
         * see if a being is hit.
         */
        protected static int MovesBetweenChecksForBeings = 8;
        
        /**
         * The direction of a shot that isn't moving.
         */
	    protected static readonly IPoint StationaryDirection = 
            GeometryFactory.CreatePoint();
        
	    /**
	     * A grouping of the frame-sequences used to depict the various directions
	     * a shot may travel.  Subtypes should subclass this to specify their sequences.
	     */
        protected abstract class ShotFrameSequencesClass : FrameSequences
        {
            /**
             * The sequences for the directions a shot may travel.  Subtypes are free
             * to populate whichever of these fields they want.
             */
    	    public IFrameSequence Level, Up, Down, UpAngle, DownAngle, Stationary;

            [OverriddenValue]
            protected override string ImagesPath { get {return "entities/shots/";} }
        }
        
	    /**
         * The grouping of frame sequences this shot will use to depict its travel in
         * various directions.
         */
        [OverridableValue]
        protected abstract ShotFrameSequencesClass ShotFrameSequences { get; }
        
	    /**
	     * Keeps track of how long it's been since the last time this shot moved.
	     */
        protected readonly IDuration betweenMovesDuration;
    	
        /**
         * How many moves this shot has left to make before it is finished
         */
        protected int movesUntilDone;

        /**
         * Keeps track of how long time-wise this shot should exist.
         */
        protected readonly IDuration existenceDuration;

        /**
         * Times this shot's falling motion.
         */
        protected readonly IFallDuration betweenFallsDuration;

        [Property]
        public abstract int Damage { get; }

        [Constructor]
        protected Shot(ShotType shotType, IClock clock) : this(shotType, null, clock) {}
        
        [Constructor]
        protected Shot(ShotType shotType, IPoint startingDirection, IClock clock) : base(clock)
        {
            ShotType = shotType;

            if (startingDirection != null) {
                Direction = GeometryFactory.CreatePoint().GetUnitVector(startingDirection);
                AddAnimation(GetFrameSequenceForDirection(Direction));
            }

            movesUntilDone = Range;

            existenceDuration = 
                DunjaxFactory.CreateDuration(clock, int.MaxValue);
            existenceDuration.Start();

            betweenMovesDuration = DunjaxFactory.CreateDuration(clock);
            betweenMovesDuration.Length = BetweenMovesDurationLength;

            betweenFallsDuration = DunjaxFactory.CreateFallDuration(clock);
            betweenFallsDuration.SlowdownFactor = 25;
            betweenFallsDuration.FallStarted();
        }

        [Destructor]
        ~Shot()
        {
            GeometryFactory.ReclaimFloatPoint(floatLocation);
        }

        [OverriddenValue]
        protected override float IntervalUntilNextAction 
        {
            get {return BetweenMovesDurationLength;}
        }
        
	    /**
	     * How much time should pass between this shot's movements.
	     */
        [OverridableValue]
	    protected abstract float BetweenMovesDurationLength { get; }
    	
	    /**
	     * Returns the sound this shot makes when it hits something impassible.
	     */
        [OverridableValue]
	    protected virtual ISound ImpactSound { get {return Sounds.ShotImpact;} }
    	
        /** 
        * Overridden to specify a shot's usual action.
        */
        [HookOverride]
        protected override void ActAfterAnimationChecked()
	    {
            // if this shot has run its course
            if (existenceDuration.Done) {
                // remove it
                RemoveFromPlay();
                return;
            }

            ActBeforeMove();

            if (Moves && CanFall()) Fall();

            // if it's been long enough since this shot's last move, have it move
            if (Moves && betweenMovesDuration.Done) Move();
	    }
        
        [Hook]
        protected virtual void ActBeforeMove() {}
        
        /**
         * Returns whether this shot moves, vs. being meant to stay in one place.
         */
        [OverridableValue]
        protected virtual bool Moves { get {return true;} }

	    /**
	     * Returns this shot's frame-sequence which corresponds to the given 
	     * direction.
	     */
	    protected IFrameSequence GetFrameSequenceForDirection(IFloatPoint direction)
	    {
		    // detm the slope of the given direction, which we'll use to categorize
	        // that direction, below
            float slope = (direction.X != 0) ? direction.Y / direction.X : 
			    1000 * (direction.Y > 0 ? 1 : -1);
            
            // if the direction is stationary
            var sequences = ShotFrameSequences;
            bool useMirror = direction.X < 0;
            if (direction.X == 0 && direction.Y == 0) {
                // return the stationary sequence
                return sequences.Stationary;
            }
            
            // else, if the direction is leftwards or rightwards
            if (slope >= -0.5 && slope <= 0.5) {
                // return a level sequence
                return useMirror ? sequences.Level.Mirror : sequences.Level;
		    }
            
            // else, if the direction is upwards or downwards
		    if (slope >= 2.0 || slope <= -2.0) {
		        // return an up or Down sequence 
		        return direction.Y < 0 ? sequences.Up : sequences.Down;
		    }
            
            // else, if the direction towards one of the four diagonals
		    if ((slope > 0.5 && slope < 2.0) 
		        || (slope < -0.5 && slope > -2.0)) {
                // return the appropriate diagonal sequence
		        return direction.Y < 0 ?
                    (useMirror ? sequences.UpAngle.Mirror : sequences.UpAngle) :
                    (useMirror ? sequences.DownAngle.Mirror : sequences.DownAngle);
		    }
            
            // this should never be reached
            throw new Exception("Direction corresponds to no condition");
	    }

	    /**
	     * The kind of being (player vs. monster) that a kind of shot is 
	     * intended to hit.
	     */
	    protected enum TargetBeingTypeEnum {Player, Monster};
    	
	    /**
	     * Returns whether this kind of shot is meant to hit the player, or
	     * the monsters.
	     */
	    [OverridableValue]
	    protected virtual TargetBeingTypeEnum TargetBeingType
	    {
	        get { return TargetBeingTypeEnum.Player; }
	    }
    	
	    /**
         * Returns the target being (if any) occupying this shot's center with a 
         * strikable location.
         */
	    protected IBeing GetTargetBeingStruck()
	    {
	        // if there's no target being at this shot's center, no being was struck
	        IBeing being = (TargetBeingType == TargetBeingTypeEnum.Player) ?
                (IBeing)Map.GetPlayerAt(Center) : Map.GetMonsterAt(Center);
            if (being == null) return null;
            
            // return whether the target being at this shot's center is strikable
            // at the location of this shot's center
            return being.IsStrikableAt(Center) ? being : null;
	    }

        /**
	     * Moves this shot one more increment along its current direction.
	     */
	    protected void Move()
	    {
            // move this shot one increment in its current direction
		    floatLocation.Add(Direction.X, Direction.Y);
            Move(
                (short) Math.Round(floatLocation.X), 
                (short) Math.Round(floatLocation.Y));
            
		    bool targetBeingStruck = false;
		    if (IsTimeToCheckForTargetBeingStruck()) 
		        targetBeingStruck = CheckForTargetBeingStruck();
    		
		    if (!targetBeingStruck) {
                if (!Map.IsPassible(Center)) Impacted(null);

                // else, if this shot has done all of its moves, remove it
		        else if (--movesUntilDone <= 0) RemoveFromPlay();
		    }
        }
        
	    /** 
	     * Returns whether it's time for this shot to check whether it has 
	     * struck a target being.  If it is, the time is reset for the next
	     * interval.
	     */
	    protected bool IsTimeToCheckForTargetBeingStruck()
	    {
            // if this shot has moved enough times since the last check
	        if (--movesUntilCheckForBeings <= 0) {
	            // a new check is warranted 
                movesUntilCheckForBeings = MovesBetweenChecksForBeings;
                return true;
            }
            
            return false;
	    }
    	
	    /**
	     * Checks (and returns) whether this shot has struck a target being.
	     */
	    protected bool CheckForTargetBeingStruck()
	    {
	        IBeing being = GetTargetBeingStruck();
	        if (being != null) {
	            TargetBeingStruck(being);
	            return true;
	        }

	        return false;
	    }
    	
        /**
	     * Informs this shot that it has struck the given target being.
	     */
	    protected void TargetBeingStruck(IBeing being)
	    {
            if (Damage > 0) being.Struck(Center, Damage);                
            
            Impacted(being);
	    }
            
	    /**
	     * Informs this shot that it has made impact with something.
	     * 
	     * @param beingHit	If the impact was with a being, the being impacted.
	     */
	    protected void Impacted(IBeing beingHit)
	    {
	        PlayIfExists(ImpactSound);
    		
	        PreRemovedFromPlayDueToImpact(beingHit);
    	    
	        RemoveFromPlay();
	    }
    	
	    [Hook]
	    protected virtual void PreRemovedFromPlayDueToImpact(IBeing beingHit) {}

        /** 
        * Overridden to sync this shot's more-precise location with its integer-based location.
        */
        protected override void Moved()
        {
            // if this shot's more precise location is not in sync with its new integer-based
            // location (likely because it hasn't been set for the first time), sync it
            if (Math.Abs(floatLocation.X - LeftX) >= 1
                || (Math.Abs(floatLocation.Y - TopY)) >= 1)
                floatLocation.Set(LeftX, TopY);
        }

        [OverridableValue]
        protected virtual int Range { get { return 25 * UnitsPerTenFeet; } }

        /**
         * Returns whether this shot can fall this turn.
         */
        [OverridableValue]
        protected virtual bool CanFall()
        {
            return betweenFallsDuration.Done;
        }

        /**
         * Has this being shot fall during this turn.
         */
        private void Fall()
        {
            // move this shot downward one increment
            AddToLocation(0, 1);
        }
    }
}