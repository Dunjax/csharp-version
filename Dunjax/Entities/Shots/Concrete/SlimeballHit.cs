using Dunjax.Attributes;
using Dunjax.Entities.Beings.Monsters;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * When a slimeball hits the ceiling, it is replaced by a shot of this kind,
     * which is invisible.  It creates slime-spots to either side of itself over 
     * a certain distance, and over a certain amount of time.  Within those
     * slime-spots it also creates a slime monster.
     */
    class SlimeballHit : Shot
    {
	    /**
	     * How many pairs of slime-spots this slimeball-hit must set before
	     * going away.
	     */
        protected const int NumSlimeSpotPairsToSet = 5;
    	
	    /**
	     * How many pairs of slime-spots this slimeball-hit has set so far.
	     */
	    protected int numSlimeSpotPairsSet;
    	
        [Constructor]
        public SlimeballHit(IClock clock) : base(ShotType.SlimeballHit, clock)
        {
        }

        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return null; } }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 140;} }

        [OverriddenValue]
        public override int Damage { get { return 0; } }

        [OverriddenValue]
        protected override bool Moves { get { return false;} }

        /** 
        * Overridden to specify this slimeball-hit's action.
        */
        protected override void ActBeforeMove() 
	    {
            // if it hasn't been long enough since this shot's last act
		    // (equating actions with moves for this purpose), don't act this time
            if (!betweenMovesDuration.Done) return;

		    // start slime-spots at an equal and increasing distance on the 
            // left and right sides of this slimeball-hit
            IShot spot = new SlimeSpot(clock);
		    var y = Map.GetYJustBelowCeiling(Center);
            spot.MoveTo((short)(Center.X - (numSlimeSpotPairsSet + 1) * spot.Width), y);
		    Map.AddEntity(spot);
            spot = new SlimeSpot(clock);
            spot.MoveTo((short)(Center.X + numSlimeSpotPairsSet * spot.Width), y);
		    Map.AddEntity(spot);

		    // if this slimeball-hit has spawned all of its slime-spot pairs,
		    // replace it with a newly created slime monster
		    if (++numSlimeSpotPairsSet == NumSlimeSpotPairsToSet) 
		        CreateSlime(spot.Width);
	    }
        
        /**
         * Replaces this slimeball-hit on the map with a newly created slime
         * monster, which is placed within this slimeball-hit's generated 
         * slime-spots. 
         */
        protected void CreateSlime(int spotWidth)
        {
            // create a new slime monster somewhere within this slimeball-hit's
            // slime-spots
            var monster = MonsterFactory.CreateMonster(MonsterTypeEnum.Slime, clock);
            Map.AddEntity(monster);
            monster.MoveTo(
                (short)(LeftX + Random.NextInt(NumSlimeSpotPairsToSet * spotWidth) 
                    * (Random.NextBoolean() ? -1 : 1)),
                TopY);

            RemoveFromPlay();
        }
    }
}