using Dunjax.Attributes;
using Dunjax.Geometry;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * The shot fired by the player's normal gun.
     */
    class PulseShot : Shot
    {
        [Constructor]
        public PulseShot(IPoint startingDirection, IClock clock) : 
            base(ShotType.PulseShot, startingDirection, clock)
        {
	    }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 1.7f;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class PulseShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static PulseShotFrameSequences Singleton = 
                new PulseShotFrameSequences();

	        [Constructor]
            private PulseShotFrameSequences()
	        {
                Level = CreateFrameSequence("level");
                Up = CreateFrameSequence("up");
                Down = CreateFrameSequence("Down");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "pulseShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return PulseShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 1; } }

        [OverriddenValue]
        protected override TargetBeingTypeEnum TargetBeingType
        {
            get { return TargetBeingTypeEnum.Monster; }
        }
    }
}