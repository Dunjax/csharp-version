using Dunjax.Animations;
using Dunjax.Attributes;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * Is a stationary spot of slime, usually started by a slimeball-hit.
     */
    class SlimeSpot : Shot
    {
        [Constructor]
        public SlimeSpot(IClock clock) : base(ShotType.SlimeSpot, StationaryDirection, clock)
	    {
		    existenceDuration.Length = 1000;
            betweenMovesDuration.Start();
	    }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class SlimeSpotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static SlimeSpotFrameSequences Singleton = 
                new SlimeSpotFrameSequences();

	        [Constructor]
            private SlimeSpotFrameSequences()
	        {
                Stationary = CreateFrameSequence("", true, FramePriority.Normal, false, null, null);
                Stationary.SetDurationOfAllFrames(60);
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "slimeSpot";} }
	    }

        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return SlimeSpotFrameSequences.Singleton; } }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 70;} }

        [OverriddenValue]
        public override int Damage { get { return 1; } }

        [OverriddenValue]
        protected override bool CanFall() { return false; }
    }
}