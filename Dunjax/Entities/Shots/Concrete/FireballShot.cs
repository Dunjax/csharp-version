using Dunjax.Attributes;
using Dunjax.Entities.Beings;
using Dunjax.Geometry;
using Dunjax.Sound;

namespace Dunjax.Entities.Shots.Concrete
{
    /**
     * Is fired by the master.  Hits the ground, and then causes a fireball-hit
     * at that location. 
     */
    class FireballShot : Shot
    {
        [Constructor]
        public FireballShot(IPoint startingDirection, IClock clock) :
            base(ShotType.FireballShot, startingDirection, clock)
	    {
	    }

        [OverriddenValue]
	    protected override float BetweenMovesDurationLength { get { return 3;} }

	    /**
         * Specifes the frame sequences used for this kind of shot.
         */
        protected class FireballShotFrameSequences : ShotFrameSequencesClass
	    {
            [Singleton]
            public static FireballShotFrameSequences Singleton = 
                new FireballShotFrameSequences();

	        [Constructor]
            private FireballShotFrameSequences()
	        {
                DownAngle = CreateFrameSequence("downAngle");
	        }

            [OverriddenValue]
            protected override string ImagesPrefix { get {return "fireballShot";} }
	    }
        
        [OverriddenValue]
        protected override ShotFrameSequencesClass ShotFrameSequences 
            { get { return FireballShotFrameSequences.Singleton; } }

        [OverriddenValue]
        public override int Damage { get { return 50; } }

        [OverriddenValue]
	    protected override ISound ImpactSound { get {return Sounds.FireballImpact;} }

        /** 
        * Overridden to check if this fireball-shot hit the player.
        */
        protected override void PreRemovedFromPlayDueToImpact(IBeing beingHit) 
	    {
		    // if a being wasn't hit
		    if (beingHit == null) {
			    // start a fireball-hit at the impact location
                IShot shot = new FireballHit(clock);
			    shot.CenterOn(Center);
			    Map.AddEntity(shot);
		    }
        }
    }
}