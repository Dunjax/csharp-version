namespace Dunjax.Entities
{
    /**
     * A simple entity which is stationary and animates.
     */
    public interface IAnimationEntity : IMapEntity
    {
        /**
         * What kind of animation entity this is.
         */
        AnimationEntityType AnimationEntityType { get; }
    }

    /**
     * The different kinds of animation entities.
     */
    public enum AnimationEntityType { Item, Spatter, FinalRoomTrigger };
}
