﻿using System;
using System.Collections.Generic;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Entities.Concrete;

namespace Dunjax.Entities
{
    class EntitiesFactory
    {
        [ObjectPool]
        private static readonly IObjectPool<IEntityAction> EntityActionPool =
            DunjaxFactory.CreateObjectPool<IEntityAction, EntityAction>();

        [ObjectPool]
        private static readonly IObjectPool<List<IEntityAction>> EntityActionListPool =
            DunjaxFactory.CreateObjectPool<List<IEntityAction>, List<IEntityAction>>();

        [Factory]
        public static IAnimationEntity CreateAnimationEntity(
            AnimationEntityType type, IFrameSequence sequence, IClock clock)
        {
            return new AnimationEntity(type, sequence, clock);
        }

        [Factory]
        public static IAnimationEntity CreateSpatter(
            IFrameSequence sequence, IClock clock)
        {
            return new Spatter(sequence, clock);
        }

        [Factory]
        public static IEntityActionSchedule CreateEntityActionSchedule(IClock clock)
        {
            return new EntityActionSchedule(clock);
        }

        [Factory]
        public static IEntityAction CreateEntityAction(
            IMapEntity entity, float timeFromCurrent, IClock clock)
        {
            // the action always has to be at least slightly in the future, otherwise
            // we get bugs where it won't get processed because the actions for the 
            // current time have already been gathered for processing 
            timeFromCurrent = Math.Max(1, timeFromCurrent);

            var action = EntityActionPool.Retrieve();
            action.Entity = entity;
            action.Time = clock.Time + timeFromCurrent;
            return action;
        }

        [Reclaimer]
        public static void ReclaimEntityAction(IEntityAction point)
        {
            EntityActionPool.Reclaim(point);
        }

        [Factory]
        public static List<IEntityAction> CreateEntityActionList()
        {
            return EntityActionListPool.Retrieve();
        }

        [Reclaimer]
        public static void ReclaimEntityActionList(List<IEntityAction> list)
        {
            // reclaim each action in the list
            for (int i = 0; i < list.Count; i++) 
                EntityActionPool.Reclaim(list[i]);

            // reclaim the list
            list.Clear();
            EntityActionListPool.Reclaim(list);
        }

        /**
         * Causes map-entities created henceforth to be given IDs starting at the
         * lowest unreserved value.
         */
        public static void ResetEntitiesIdCounter()
        {
            MapEntity.ResetEntitiesIdCounter();
        }
    }
}
