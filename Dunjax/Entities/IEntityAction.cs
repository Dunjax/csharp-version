﻿namespace Dunjax.Entities
{
    /**
     * Specifies a time at which a map-entity next hopes to act.
     */
    public interface IEntityAction
    {
        /**
         * The entity which desires to act.
         */
        IMapEntity Entity {get; set;}

        /**
         * The desired time to act.
         */
        float Time { get; set; }
    }
}