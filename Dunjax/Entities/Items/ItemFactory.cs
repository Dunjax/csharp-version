using Dunjax.Entities.Items.Concrete;

namespace Dunjax.Entities.Items
{
    class ItemFactory
    {
        public static IItem CreateItem(ItemType type, IClock clock)
        {
            return new Item(type, clock);
        }
    }
}