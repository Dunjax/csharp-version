namespace Dunjax.Entities.Items
{
    public interface IItem : IAnimationEntity
    {
        /**
         * This item's item-type.
         */
	    ItemType ItemType {get;}
    	
	    /**
	     * Informs this item that it has been consumed.
	     */
	    void Consumed();
    }

    /**
     * The different kinds of items that are in the game. 
     */
    public enum ItemType
    {
        PulseAmmo, PulseAmmoBig, GrapeshotAmmo, GrapeshotGun,
        ShieldPowerUp, ShieldPowerPack, Key
    };
}