using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.ExtensionMethods;
using Dunjax.Entities.Concrete;
using Dunjax.Sound;

namespace Dunjax.Entities.Items.Concrete
{
    class Item : AnimationEntity, IItem
    {
        [Property]
        public ItemType ItemType { get; protected set; }
        
        /**
         * The frame sequences for the different item types, in the same order
         * as in the types' enum declarations.
         */
        private static readonly int NumItemTypes = GetValues<ItemType>().Count();
        private static readonly IFrameSequence[] ItemsFrameSequences =
            new IFrameSequence[NumItemTypes];
        static Item() 
        {
            // for each item type
            var sequences = ItemsFrameSequences;
            var types = GetValues<ItemType>();
            foreach (ItemType type in types) {
                // create the sequence for this type
                sequences[(int)type] = 
                    AnimationFactory.CreateFrameSequence(
                        "entities/items/", "", 
                        type.ToString().MakeFirstLetterLowercase(), 
                        true, FramePriority.Normal, false);
            }
        }
        
        [Constructor]
        public Item(ItemType itemType, IClock clock) : 
            base(AnimationEntityType.Item, ItemsFrameSequences[(int)itemType], clock)
        {
            ItemType = itemType;
        }

        [Implementation]
        public void Consumed()
        {
            RemoveFromPlay();

            Sounds.ItemTaken.Play();
        }

        /** 
        * Overridden to specify that items other than keys should be aligned with the 
         * ground or ceiling.
        */
        protected override bool ShouldAlignWithGroundOrCeiling
        {
            get { return ItemType != ItemType.Key; }
        }

        /**
         * Used as a replacement for Enum.GetValues(), which isn't available on the Xbox.
         */
        public static IEnumerable<T> GetValues<T>()
        {
            return typeof(T).GetFields(BindingFlags.Static | BindingFlags.Public)
                .Select(x => (T)x.GetValue(null));
        } 
    }
}