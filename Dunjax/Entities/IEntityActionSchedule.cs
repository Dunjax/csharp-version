using System.Collections.Generic;

namespace Dunjax.Entities
{
    /**
     * Keeps track of the game times at which entities desire to act. 
     */
    public interface IEntityActionSchedule
    {
        /**
         * Returns the next game time at which an entity desires to act.
         */
        long GetNextActionTime();

        /**
         * Removes and returns this schedule's list of actions scheduled for the 1
         * ms chunk of game time starting at the time given, or null if there are no
         * such actions.
         */
        List<IEntityAction> RemoveActionsScheduledForTime(long time);

        /**
         * Adds the given action to this schedule.
         */
        void AddAction(IEntityAction action);

        /**
         * Returns whether this schedule contains an action matching that given.
         */
        bool ContainsAction(IEntityAction action);

        /**
         * Moves all actions within this schedule to the given time.
         */
        void MoveAllActionsToTime(long time);
    }
}