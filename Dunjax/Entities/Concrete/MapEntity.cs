using System.Collections.Generic;
using System.Linq;
using Dunjax.Animations;
using Dunjax.Attributes;
using Dunjax.Concrete;
using Dunjax.Geometry;
using Dunjax.Sound;
using Dunjax.Images;
using Dunjax.Map;

namespace Dunjax.Entities.Concrete 
{
    abstract class MapEntity : IMapEntity
    {
        /**
         * Keeps track of how many map-entities have been created so far, so that
         * unique IDs may be assigned to any new ones which are created. 
         */
        protected static int NumMapEntities;

        /**
         * Identifies this map-entity uniquely over the current running of 
         * this program.
         */
        public int Id { get { return id_;} }
        private readonly int id_ = ++NumMapEntities;
        
        /**
         * The map in which this entity resides.
         */
        [Property]
        public IMap Map
        {
            get { return _map; }
            set {
                _map = value;

                // schedule a starting action for this entity within the given map 
                ScheduleActionTime(0, "start");
            }
        }
        private IMap _map;
        
	    /**
         * The current bounds occupied by this entity within its map.
         * Changes to this object's location value should only be made through 
         * the move() method, as it reports such changes to interested listeners.
         */
        [Property]
        public IRectangle Bounds
        {
            get { return boundsCopy_.Set(bounds); }
        }
        private readonly IRectangle bounds = GeometryFactory.CreateRectangle();
        private readonly IRectangle boundsCopy_ = GeometryFactory.CreateRectangle();

        [Property] public short LeftX {get { return Bounds.LeftX; }}
        [Property] public short RightX {get { return Bounds.RightX; }}
        [Property] public short TopY {get { return Bounds.TopY; }}
        [Property] public short BottomY {get { return Bounds.BottomY; }}
        [Property] public IPoint Center {get { return Bounds.Center; }}
        [Property] public IPoint TopLeft {get { return Bounds.TopLeft; }}
        [Property] public IPoint TopRight {get { return Bounds.TopRight; }}
        [Property] public IPoint BottomLeft {get { return Bounds.BottomLeft; }}
        [Property] public IPoint BottomRight {get { return Bounds.BottomRight; }}
        [Property] public IPoint TopCenter {get { return Bounds.TopCenter; }}
        [Property] public IPoint BottomCenter {get { return Bounds.BottomCenter; }}
        [Property] public IPoint LeftCenter {get { return Bounds.LeftCenter; }}
        [Property] public IPoint RightCenter {get { return Bounds.RightCenter; }}
        [Property] public short Width {get { return Bounds.Width; }}
        [Property] public short Height {get { return Bounds.Height; }}
        [Property] public ISize Size {get { return Bounds.Size; }}
        protected bool Contains(IPoint location) { return Bounds.Contains(location); }

        /**
         * Assigns a default size to this entity.
         */
        protected void SetToDefaultSize()
        {
            bounds.Width = bounds.Height = UnitsPerTenFeet;
        }

        /**
         * The animations currently being used to depict this entity.  They all 
         * progress simultaneously, and the one with the highest frame-sequence
         * priority is the one which is currently being displayed.
         * 
         * Call AddAnimation() rather than directly adding an animation to here.
         */
        private readonly List<IAnimation> animations = new List<IAnimation>();

        /**
         * The animation currently being displayed for this entity, out of those
         * in the above animations list.
         */
        protected IAnimation currentAnimation;

        /**
         * Whether this entity needs to take no further action during the
         * game.  Examples where this would be true would include entities which
         * have been removed from the map, as well as those which remain but which
         * also stay inactive, such as a non-animating carcass. 
         */
        public bool PermanentlyInactive
        {
            get {return permanentlyInactive_;}
            protected set {permanentlyInactive_ = value;}
        }
        protected bool permanentlyInactive_;
        
        /**
         * Is set to true during a call to act().  This allows tests to determine
         * whether act() was called, since it often has no easily verifiable effects.
         */
        protected bool acted;
        
        [StaticImport]
        protected const int UnitsPerTenFeet = MapConstants.UnitsPerTenFeet;

        /**
         * The number of milliseconds in a second.
         */
        protected const int MsInASecond = 1000;

        /**
         * Supplies this map-entity with the current time, as required.
         */
        protected readonly IClock clock;

        [Property]
        public IImage Image { get; private set; }

        /**
         * The random number generator used by all map-entities, as there is no
         * advantage to having more than just one shared instance.
         */
        protected static readonly IRandom Random = new Random();

        [Constructor]
        protected MapEntity(IClock clock)
        {
            this.clock = clock;
        }

        [Implementation]
        public void Act()
        {
            acted = false;

            CheckForAnimationAdvanceOrFinish();
            
            // if the above caused this entity to be removed from the map, it should
            // take no further action
            if (Map == null) return;
            
            ActAfterAnimationChecked();
            
            acted = true;
            
            ScheduleActionTime(IntervalUntilNextAction, "next");

            DetermineImageForThisTurn();
            
            AfterActed();
        }

        [Hook]
        protected virtual void ActAfterAnimationChecked() {}
        
        [Hook]
        protected virtual void AfterActed() {}
        
        /**
         * Sets this entity's image to be that of the current image
         * of its current animation.
         */
        protected void DetermineImageForThisTurn()
        {
            // if this entity doesn't have a current animation, leave the
            // current image as is
            if (currentAnimation == null) return;

            // use the current image of the current animation as the 
            // current image for this entity
            Image = currentAnimation.CurrentImage;

            // if this entity has a current image, and its size is different
            // than that of this entity's bounds
            if (Image != null && bounds.Size != Image.Size) {
                // adjust this entity's bounds-location for the size difference
                bounds.LeftX -= (short)((Image.Size.Width - bounds.Size.Width) / 2);
                bounds.TopY -= (short)((Image.Size.Height - bounds.Size.Height) / 2);
                
                // use the image size as this entity's new size
                bounds.Size = Image.Size;
            }
        }
        
        /**
         * Checks whether this entity should advance in its current animation 
         * sequence and/or whether that sequence has completed.
         */
        protected void CheckForAnimationAdvanceOrFinish()
        {
            // for each animation this entity is currently progressing through
            for (int i = 0; i < animations.Count; i++) {
                var animation = animations[i];

                // check to see if this entity's current animation should advance to
                // the next frame
                bool wasDone = animation.Done;
                animation.CheckForAdvance();

                // if the above advance (if any) caused this entity's 
                // current animation to finish
                if (!wasDone && animation.Done) {
                    // do whatever post-finish processing that needs to be done
                    AnimationDone(animation.FrameSequence);

                    // if the animation is marked as needing to be removed when done
                    if (animation.RemoveWhenDone) {
                        // remove it from this entity
                        animations.Remove(animation);
                        i--;

                        // update what this entity's current animation is
                        currentAnimation = GetHighestPriorityCurrentAnimation();
                    }
                }
            }
        }
        
        /**
         * Informs this entity that its current animation sequence has completed.
         */
        [Hook]
        protected virtual void AnimationDone(IFrameSequence sequence) {}

        [Property]
	    public virtual bool Passible { get {return true;} }

        [Implementation]
        public void MoveTo(short x, short y) { Move(x, y); }

        [Implementation]
        public void MoveTo(IPoint to)
        {
            Move(to.X, to.Y);
        }

        [Implementation]
        public void CenterOn(IPoint on)
        {
            Move(
                (short)(on.X - Width / 2), 
                (short)(on.Y - Height / 2));
        }

        /**
         * Moves this entity to the given coordinates on the map.  Also performs pre-
         * and post-movement notification to the map.  Thus, all methods which 
         * change this entity's location should delegate the actual movement to this
         * method.
         */
        protected void Move(short x, short y)
        {
            if (Map != null) Map.EntityAboutToMove(this);
            
            bounds.LeftX = x;
            bounds.TopY = y;
            
            if (Map != null) Map.EntityMoved(this);
            Moved();
        }
        
        [Hook]
        protected virtual void Moved() {}

        [Implementation]
        public void AddToLocation(short dx, short dy)
        {
            Move((short)(LeftX + dx), (short)(TopY + dy));
        }

        [Implementation]
	    public Side GetSideEntityIsOn(IMapEntity entity)
	    {
		    int entityX = entity.Center.X;
            int thisX = Center.X;
		    if (entityX < thisX) return Side.Left;
		    if (entityX > thisX) return Side.Right;
		    return Side.NeitherSide;
	    }
    	
        [Implementation]
	    public VerticalSide GetVerticalSideEntityIsOn(IMapEntity entity)
	    {
		    int entityY = entity.Center.Y;
            int thisY = Center.Y;
		    if (entityY < thisY) return VerticalSide.Top;
		    if (entityY > thisY) return VerticalSide.Bottom;
		    return VerticalSide.NeitherVerticalSide;
	    }
    	
        /**
	     * The different frame-sequences used to depict the various activities of
	     * entities of a particular class. Subtypes should override this class and
         * specify within that override those sequences.
	     */
        public abstract class FrameSequences
        {
            /**
             * Subtypes should override this to provide the string that is at the front
             * of the filename of each of the images in its frame sequences.
             */
            [OverridableValue]
            protected abstract string ImagesPrefix { get; }

            /**
             * Subtypes should override this to provide the path (relative to the 
             * images folder) to the folder containing its frame sequences.
             */
            [OverridableValue]
            protected abstract string ImagesPath { get; }

            [Shorthand]
            protected IFrameSequence CreateFrameSequence(
                string name, bool repeating = false, 
                FramePriority priority = FramePriority.Normal, 
                bool makeMirror = true, int[] repeatedFrames = null, 
                IImage[] images = null)
            {
                return AnimationFactory.CreateFrameSequence(
                    ImagesPath, ImagesPrefix, name, repeating, priority, 
                    makeMirror, repeatedFrames, images);   
            }
        }

        /**
         * Returns whether this entity is currently on the ground (vs. in the air
         * or on a ladder).
         */
        protected bool IsOnGround()
        {
            return !Map.IsPassible(BottomCenter.Add(0, 1));
        }

        /**
         * Returns whether this entity spends most (or all) of its time on the 
         * ceiling.
         */
        [OverridableValue]
        protected virtual bool IsCeilingDwelling() { return false; }

        /**
         * Returns whether this entity spends most (or all) of its time on the 
         * ground.
         */
        [OverridableValue]
        protected virtual bool IsGroundDwelling() { return true; }

        [Implementation]
        public void AlignWithSurroundings()
        {
            if (ShouldAlignWithGroundOrCeiling) {
                MoveUpUntilAboveImpassible();
            
                MoveDownUntilBelowImpassible();

                if (IsGroundDwelling()) MoveDownUntilOnGround();
            
                if (IsCeilingDwelling()) MoveUpUntilContactingCeiling();
            }

            AlignSpecially();
        }
        
        [OverridableValue]
        protected virtual bool ShouldAlignWithGroundOrCeiling { get { return true; } }

        [Hook]
        protected virtual void AlignSpecially() {}

        /**
         * Moves this entity upwards to be above what impassible thing (if any) 
         * it is currently over.
         */
        protected void MoveUpUntilAboveImpassible()
        {
            // while this entity's lower-y value is impassible,
            // move this entity up one increment
            var test = Center;
            int limit = TopY - 4 * UnitsPerTenFeet;
            while (!Map.IsPassible(test.SetY(BottomY)) && TopY > limit) 
                bounds.AddToLocation(0, -1);
        }
        
        /**
         * Moves this entity downwards to be below what impassible thing (if any) 
         * it is currently over.
         */
        protected void MoveDownUntilBelowImpassible()
        {
            // while this entity's y value is impassible,
            // move this entity down one increment
            var test = Center;
            int limit = TopY + 4 * UnitsPerTenFeet;
            while (!Map.IsPassible(test.SetY(TopY)) && TopY < limit) 
                bounds.AddToLocation(0, 1);
        }
        
        /**
         * Moves this entity downwards to be resting on the non-fall-throughable
         * thing it is currently above.
         */
        protected void MoveDownUntilOnGround()
        {
            // while just below this entity is fall-throughable,
            // move this entity down one increment
            var test = Center;
            int limit = TopY + 4 * UnitsPerTenFeet;
            while (Map.IsFallThroughable(test.SetY((short) (BottomY + 1))) 
                && TopY < limit) 
                bounds.AddToLocation(0, 1);
        }
        
        /**
         * Moves this entity upwards to be contacting the impassible thing 
         * it is currently under.
         */
        protected void MoveUpUntilContactingCeiling()
        {
            // while just above this entity is passible,
            // move this entity up one increment
            var test = Center;
            int limit = TopY - 4 * UnitsPerTenFeet;
            while (Map.IsPassible(test.SetY((short) (TopY - 1))) && TopY > limit)
                bounds.AddToLocation(0, -1);
        }
        
        /**
         * Returns whether this entity's map is passible one increment to this
         * entity's given side. 
         * 
         * @param set   Which entities are to be considered impassible 
         *              for this call.
         */
        [Optimized]
        public bool IsPassibleToSide(Side side, ImpassableEntitiesSet set)
        {
            var test = 
                Center.SetX(
                    (short) (side == Side.Left ? LeftX - 1 : RightX + 1));
            if (!Map.IsPassible(test, set)) return false;
            test.Y = TopY;
            if (!Map.IsPassible(test, set)) return false;
            test.Y = BottomY;
            if (!Map.IsPassible(test, set)) return false;
            return true;
        }
        
        /**
         * Returns whether this entity's map is passible one increment to this
         * entity's given vertical-side. 
         * 
         * @param set   Which entities are to be considered impassible 
         *              for this call.
         */
        [Optimized]
        public bool IsPassibleToVerticalSide(
            VerticalSide side, ImpassableEntitiesSet set)
        {
            var test = 
                Center.SetY(
                    (short) (side == VerticalSide.Top ? TopY - 1 : BottomY + 1));
            if (!Map.IsPassible(test, set)) return false;
            test.X = LeftX;
            if (!Map.IsPassible(test, set)) return false;
            test.X = RightX;
            if (!Map.IsPassible(test, set)) return false;
            return true;
        }
        
        /**
         * Makes this entity permanently inactive and removes it from its map, 
         * two actions which must commonly be performed together.
         */
        protected void RemoveFromPlay()
        {
            permanentlyInactive_ = true;
            Map.RemoveEntity(this);
        }
        
        /**
         * A helper method which schedules an action for this entity with the given 
         * parameters.  
         */
        protected void ScheduleActionTime(
            float deltaFromCurrentTime, string description)
        {
            if (Map == null) return;
            
            var action = 
                EntitiesFactory.CreateEntityAction(this, deltaFromCurrentTime, clock);
            Map.EntityActionSchedule.AddAction(action);
        }
        
        /**
         * Returns the interval of time until this monster should next be given a chance
         * to act.  To simplify things, an override of this will usually return the minimum
         * interval between successive executions of its most rapid action.  Entities
         * are free to manually schedule their own special actions when this 
         * simplified scheme is not precise enough for them.    
         */
        [OverridableValue]
        protected abstract float IntervalUntilNextAction {get;}

        [Shorthand]
        protected static void PlayIfExists(ISound sound)
        {
            if (sound != null) sound.Play();
        }

        [Shorthand]
        protected void AddAnimation(IFrameSequence sequence)
        {
            AddAnimation(sequence, false);
        }

        /**
         * Creates a new animation of the given frame-sequence, and 
         * adds it to those which are currently being used to 
         * depict this entity's actions.
         */
        protected void AddAnimation(
            IFrameSequence sequence, bool removeWhenDone)
        {
            // create the animation to run the given sequence
            var animation = AnimationFactory.CreateAnimation(clock);
            if (removeWhenDone) animation.RemoveWhenDone = true;
            if (sequence != null) animation.SetFrameSequence(sequence);
            animations.Add(animation);

            // if this entity doesn't yet have a size, use the size
            // of the first image of the given sequence
            if (Width == 0) bounds.Size = sequence[0].Image.Size;

            // update what this entity's current animation is
            currentAnimation = GetHighestPriorityCurrentAnimation();
        }

        /**
         * Removes the animation that is running the given frame-sequence 
         * from this entity's collection of current animations.
         */
        protected void RemoveAnimation(IFrameSequence sequence)
        {
            // for each animation assigned to this entity 
            // (as there may be more than one to remove, 
            // since we don't check for duplicates when adding them)
            for (int i = 0; i < animations.Count; i++) {
                // if this animation's sequence matches the one given
                var animation = animations[i];
                if (animation.FrameSequence.IsSameOrMirror(sequence)) {
                    // remove it
                    animations.RemoveAt(i--);
                    AnimationFactory.ReclaimAnimation(animation);
                }
            }

            // update what this entity's current animation is
            currentAnimation = GetHighestPriorityCurrentAnimation();
        }

        /**
         * Returns the highest priority animation this entity is 
         * currently progressing through.  Ties are won by the
         * animation which was added later.
         */
        protected IAnimation GetHighestPriorityCurrentAnimation()
        {
            // for each animation this entity is currently progressing through
            IAnimation highestPriorityAnimation = null;
            for (int i = 0; i < animations.Count; i++) {
                // if this animation's priority is the highest so far
                var animation = animations[i];
                if (highestPriorityAnimation == null
                    || animation.FrameSequence.Priority >=
                        highestPriorityAnimation.FrameSequence.Priority)
                    // remember this animation for below
                    highestPriorityAnimation = animation;
            }

            // return the highest priority animation found above
            return highestPriorityAnimation;
        }

        /**
         * Restarts this entity's current animation of the given 
         * frame-sequence, if it has finished playing.
         */
        protected void RestartCurrentAnimationIfDone(IFrameSequence sequence)
        {
            var match = FindAnimationOfSequence(sequence, animations);
            if (match != null && match.Done) match.Restart();
        }

        /**
         * Returns the first animation found in the given list whose sequence
         * matches that given, or its mirror.
         */
        protected static IAnimation FindAnimationOfSequence(
            IFrameSequence sequence, List<IAnimation> animations)
        {
            // for each animation in the given list
            for (var i = 0; i < animations.Count; i++) {
                // if this animation has the same sequence (or the mirror of)
                // the one given, return it
                var animation = animations[i];
                if (animation.FrameSequence.IsSameOrMirror(sequence))
                    return animation;
            }

            return null;
        }

        /**
         * Causes map-entities created henceforth to be given IDs starting at the
         * lowest unreserved value.
         */
        public static void ResetEntitiesIdCounter()
        {
            // it's presumed that the player will always be the first entity
            NumMapEntities = 1;
        }
    }
}