using System;
using System.Collections.Generic;
using Dunjax.Attributes;

namespace Dunjax.Entities.Concrete 
{
    class EntityActionSchedule : IEntityActionSchedule
    {
        /**
         * Holds the entity actions which are scheduled to occur during each 1 millisecond
         * chunk of game time, mapped by the whole number floor of that millisecond
         * interval.  For example, actions scheduled from 24 to 24.999... ms are mapped
         * with a key of 24.  
         */
        protected Dictionary<long, List<IEntityAction>> actionsByTime = 
            new Dictionary<long, List<IEntityAction>>();
        
        /**
         * The latest game time for which entity actions have been removed
         * from this schedule.
         */
        protected long lastTimeActionsRemoved = -1;
        
        /**
         * The maximum amount of time (in ms) from the current game time that 
         * getNextActionTime() should search for actions. 
         */
        protected const long NextActionTimeUpperBound = 3000;

        /**
         * Supplies this schedule with the current time, as required.
         */
        protected readonly IClock clock;

        [Constructor]
        public EntityActionSchedule(IClock clock)
        {
            this.clock = clock;
        }

        [Implementation]
        public long GetNextActionTime()
        {
            // for each millisecond of game time from the last time for which actions
            // were removed from this schedule, to some upper bound above the 
            // current game time (to avoid an infinite loop)
            var currentTimeFloored = (long)Math.Floor(clock.Time);
            long maxTimeToSearchTo = currentTimeFloored + NextActionTimeUpperBound;
            for (long i = lastTimeActionsRemoved + 1; i <= maxTimeToSearchTo; i++) {
                // if there is at least one action scheduled for this time, 
                // return this time
                if (actionsByTime[i] != null) return i;
            }

            // no actions were found, so return our upper bound
            return maxTimeToSearchTo;
        }
        
        [Implementation]
        public List<IEntityAction> RemoveActionsScheduledForTime(long time)
        {
            // if this schedule contains no list of actions for the given
            // time, return null
            if (!actionsByTime.ContainsKey(time)) return null;

            // remove and return the list of actions for the given time
            var result = actionsByTime[time];
            actionsByTime.Remove(time);
            lastTimeActionsRemoved = time;
            return result;
        }

        [Implementation]
        public void AddAction(IEntityAction action)
        {
            // if there is already a list of actions at the given action's 
            // time, use it
            var key = (long)action.Time;
            List<IEntityAction> actionTimes;
            if (actionsByTime.ContainsKey(key)) actionTimes = actionsByTime[key];

            // otherwise
            else {
                // create one
                actionTimes = EntitiesFactory.CreateEntityActionList();
                actionsByTime.Add(key, actionTimes);
            }
            
            // add the given action to the list found (or created) above
            actionTimes.Add(action);
        }

        [Implementation]
        public bool ContainsAction(IEntityAction action)
        {
            // if there is no list of actions at the given action's time, 
            // this schedule does not contain a matching action
            var key = (long)Math.Floor(action.Time);
            var actionTimes = actionsByTime[key];
            if (actionTimes == null) return false;
            
            // for each action in the list retrieved above
            for (int i = 0; i < actionTimes.Count; i++) {
                // if this action matches that given, we've found a match
                if (actionTimes[i].Equals(action)) return true;
            }
            
            // no match was found
            return false;
        }

        [Implementation]
        public void MoveAllActionsToTime(long time)
        {
            // for each time for which there are actions stored
            // in this schedule
            var keys = new long[actionsByTime.Keys.Count];
            actionsByTime.Keys.CopyTo(keys, 0);
            foreach (var key in keys) {
                // remove the actions stored at this time
                var actions = actionsByTime[key];
                actionsByTime.Remove(key);

                // re-schedule the actions for the given time
                foreach (var action in actions) action.Time = time;
                actionsByTime.Add(time, actions);
            }
        }
    }
}
