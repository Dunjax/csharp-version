﻿using Dunjax.Animations;
using Dunjax.Attributes;

namespace Dunjax.Entities.Concrete
{
    /**
     * Displays a being's spatter sequence animation.  
     * Spatters can fall, and once done falling, they no longer
     * need to be checked for actions.
     */
    class Spatter : AnimationEntity
    {
        [Constructor]
        public Spatter(IFrameSequence sequence, IClock clock)
            : base(AnimationEntityType.Spatter, sequence, clock)
        {
            betweenFallsDuration = DunjaxFactory.CreateFallDuration(clock);
            betweenFallsDuration.FallStarted();
        }
        
        /**
         * Times this spatter's fall.
         */
        private readonly IFallDuration betweenFallsDuration;

        [OverriddenValue]
        protected override float IntervalUntilNextAction
        {
            get { return betweenFallsDuration.LengthLeft; }
        }

        [HookOverride]
        protected override void ActAfterAnimationChecked()
        {
            // if this spatter can fall, have it fall
            bool fallBlocked;
            if (CanFall(out fallBlocked)) Fall();

            // otherwise, if the fall was blocked by an impediment,
            // and this spatter's animation is done, 
            // it never needs to act, again
            else if (fallBlocked && currentAnimation.Done) 
                PermanentlyInactive = true;
        }

        /**
         * Returns whether this spatter can fall this turn.
         * 
         * fallBlocked: when the spatter can't fall, if it's because its fall
         * was blocked by an impediment
         */
        private bool CanFall(out bool fallBlocked)
        {
            // if the time since this being's last fall hasn't been long enough,
            // it cannot fall at this moment
            fallBlocked = false;
            if (!betweenFallsDuration.Done) return false;

            // if the area just beneath this spatter's bottom-center
            // is not passible (note we don't use fall-throughable-ness 
            // as we want spatters to fall through terrain like ladders)
            var test = BottomCenter.Add(0, 1);
            if (!Map.IsPassible(test.Add(-4, 0))
                || !Map.IsPassible(test.Add(4, 0))) {
                // no fall is allowed
                fallBlocked = true;
                return false;
            }

            return true;
        }

        /**
         * Has this spatter fall.
         */
        private void Fall()
        {
            // move this spatter downward one increment
            AddToLocation(0, 1);
        }
    }
}
