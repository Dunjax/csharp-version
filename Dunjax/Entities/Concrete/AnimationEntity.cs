using Dunjax.Animations;
using Dunjax.Attributes;

namespace Dunjax.Entities.Concrete 
{
    class AnimationEntity : MapEntity, IAnimationEntity
    {
        [Property]
        public AnimationEntityType AnimationEntityType { get { return type; } }
        protected AnimationEntityType type;

        [Constructor]
        public AnimationEntity(AnimationEntityType type, IFrameSequence sequence, IClock clock) : base(clock)
        {
            this.type = type;

            // if a frame sequence was given, add an animation for it to this entity
            if (sequence != null) AddAnimation(sequence);
            
            // otherwise
            else {
                // give this entity a default size, so that it occupies
                // a testable area on the map
                SetToDefaultSize();
            }
        }
        
        /**
         * The interval value used when this entity has no frame sequence to display.
         */
        public const int DefaultIntervalUntilNextAction = 1000;
        
        [OverriddenValue]
        protected override float IntervalUntilNextAction 
        {
            get {
                return currentAnimation != null ?
                    currentAnimation.CurrentFrameDuration : DefaultIntervalUntilNextAction;
            }
        } 
    }
}