namespace Dunjax.Entities.Concrete
{
    /**
     * Specifies a time at which a map-entity is planning to act, plus associated
     * information.
     */
    class EntityAction : IEntityAction
    {
        /**
         * The entity which desires to act.
         */
        public IMapEntity Entity {get; set;}

        /**
         * The desired time to act.
         */
        public float Time { get; set; }
    }
}