using Dunjax.Map;
using Dunjax.Geometry;
using Dunjax.Images;

namespace Dunjax.Entities 
{
    /**
     * An entity in the game.  An entity lives within a map.
     * 
     * It is presumed that map-entities do not change size over their lifetime.
     */
    public interface IMapEntity
    {
	    /**
	     * This entity's ID, which is unique over the current running of 
	     * this program.
	     */
        int Id { get; }
    	
        /**
	     * The map in which this entity resides.
	     */
        IMap Map { get; set; }

        /**
         * Returns this entity's bounds within its map.
         * 
         * Only certain callers should modify this data, such as the object
         * responsible for placing this entity on its map.  In all other cases,
         * the entity itself is responsibile for maintaining and computing its
         * locational data.
         */
        IRectangle Bounds { get; }

        short LeftX { get; }
        short RightX { get; }
        short TopY { get; }
        short BottomY { get; }
        IPoint Center { get; }
        IPoint TopLeft { get; }
        IPoint TopRight { get; }
        IPoint BottomLeft { get; }
        IPoint BottomRight { get; }
        IPoint TopCenter { get; }
        IPoint BottomCenter { get; }
        IPoint LeftCenter { get; }
        IPoint RightCenter { get; }
        short Width { get; }
        short Height { get; }
        ISize Size { get; }

        /**
         * Moves this entity so that its top-left corner is at the given location.
         */
        void MoveTo(short x, short y);

        /**
         * Moves this entity so that its top-left corner is at the given location.
         */
        void MoveTo(IPoint to);

        /**
         * Moves this entity so that it's centered on the given location.
         */
        void CenterOn(IPoint on);

        /**
         * Moves this entity the given amounts within its map.
         */
        void AddToLocation(short dx, short dy);

        /**
	     * Returns whether this entity is normally passible.
	     */
        bool Passible { get; }

	    /**
         * Is called when this entity gets to perform its actions for the current 
         * game turn.
         */
        void Act();
        
        /**
         * Returns the current image that visually represents this entity.
         */
        IImage Image { get; }
        
        /**
         * Returns on which side the given entity is of this entity.
         */
        Side GetSideEntityIsOn(IMapEntity entity);

        /**
         * Returns on which vertical side the given entity is of this entity.
         */
        VerticalSide GetVerticalSideEntityIsOn(IMapEntity entity);

        /**
         * Returns whether this entity's map is passible one increment to this
         * entity's given side. 
         * 
         * @param set   Which entities are to be considered impassible 
         *              for this call.
         */
        bool IsPassibleToSide(Side side, ImpassableEntitiesSet set);

        /**
         * Returns whether this entity's map is passible one increment to this
         * entity's given vertical-side. 
         * 
         * @param set   Which entities are to be considered impassible 
         *              for this call.
         */
        bool IsPassibleToVerticalSide(VerticalSide side, ImpassableEntitiesSet set);
        
        /**
         * Whether this entity needs to take no further action during the
         * game.  Examples where this would be true would include entities which
         * have been removed from the map, as well as those which remain but which
         * also stay inactive, such as a non-animating carcass. 
         */
        bool PermanentlyInactive { get; }

        /**
         * Moves this entity up or down on its map so that its top and bottom 
         * parts aren't stuck in something impassible. Also moves this entity 
         * down if necessary to keep it from floating above the ground, but 
         * only if it dwells on the ground. Such actions are necessary since it's 
         * hard to specify the exact, correct positions of entities using the map editor.
         * 
         * May also perform other, specialty alignment, as necessary.
         */
        void AlignWithSurroundings();
    }

    /**
     * The horizontal sides of an entity.
     */
    public enum Side { Left, Right, NeitherSide };

    /**
     * The vertical sides of an entity.
     */
    public enum VerticalSide { Top, Bottom, NeitherVerticalSide };
}