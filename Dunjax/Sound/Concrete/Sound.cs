﻿using Dunjax.Attributes;

namespace Dunjax.Sound.Concrete
{
    public class Sound : ISound
    {
        /**
         * The name (minus extension) of this sound's file on disk.
         */
        public string fileName;

        /**
         * Whether this sound should play continously in a looped fashion.
         */
        public bool loop;

        /**
         * Whether only one instance of this sound should play at a time.
         */
        public bool onlyOneClipAtATime;

        /**
         * The system-side sound object which realizes the functionality of 
         * this game-side sound object.
         */
        private readonly ISystemSound systemSound;

        /**
         * The system-side sound object creator will plug itself in here 
         * to create such objects to pair with instances of this class.
         */
        public static ISystemSoundCreator SystemSoundCreator;

        [Constructor]
        public Sound(
            string fileName, bool loop, bool onlyOneClipAtATime)
        {
            this.fileName = fileName;
            this.loop = loop;
            this.onlyOneClipAtATime = onlyOneClipAtATime;

            // create the system-side sound object
            systemSound = SystemSoundCreator.CreateSystemSound(this);
        }

        [Implementation]
        public void Play() { systemSound.Play(); }

        [Implementation]
        public void Stop() { systemSound.Stop(); }

        [Implementation]
        public bool Playing { get { return systemSound.Playing; } }
    }

    public interface ISystemSound
    {
        void Play();
        
        void Stop();
        
        bool Playing { get; }
    }

    public interface ISystemSoundCreator
    {
        ISystemSound CreateSystemSound(Sound sound);
    }
}
