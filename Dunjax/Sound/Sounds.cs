using Dunjax.Attributes;

namespace Dunjax.Sound
{
    /**
     * Holds the flyweight sounds.
     */
    public class Sounds
    {
        /**
         * Whether sounds should play, at all.  This may be set to false for 
         * running tests, or when the user wishes to play the game silently.
         */
        public static bool PlaySounds = true;

        /**
         * The flyweight sounds.
         */
        public static readonly ISound
            PlayerMove = CreateSound("playerMove", false, true),
            PlayerJump = CreateSound("playerJump"),
            PlayerThrust = CreateSound("playerThrust", true, true),
            PlayerLand = CreateSound("playerLand"),
            PlayerSlide = CreateSound("playerSlide", true, true),
            PlayerSwitchGuns = CreateSound("playerSwitchGuns"),
            PlayerClimb = CreateSound("playerClimb", false, true),
            PlayerDeath = CreateSound("playerDeath"),
            PlayerHit = CreateSound("playerHit"),
            PlayerWhirlwinded = CreateSound("playerWhirlwinded", false, true),
            PlayerWebbed = CreateSound("playerWebbed", false, true),
            MonsterAttack = CreateSound("monsterAttack"),
            MonsterDeath = CreateSound("monsterDeath"),
            GreenGargoyleFly = CreateSound("greenGargoyleFly"),
            StalagmiteWake = CreateSound("stalagmiteWake"),
            SpiderWake = CreateSound("spiderWake"),
            MasterWake = CreateSound("masterWake"),
            SlimeJump = CreateSound("slimeJump"),
            PulseFire = CreateSound("pulseFire"),
            GrapeFire = CreateSound("grapeFire"),
            OutOfAmmo = CreateSound("outOfAmmo", false, true),
            HalberdFire = CreateSound("halberdFire"),
            WebFire = CreateSound("webFire"),
            WebImpact = CreateSound("webImpact"),
            ShotImpact = CreateSound("shotImpact"),
            FireballShot = CreateSound("fireballShot"),
            FireballImpact = CreateSound("fireballImpact"),
            SlimeballShot = CreateSound("slimeballShot"),
            SlimeballImpact = CreateSound("slimeballImpact"),
            LevelEndReached = CreateSound("levelEndReached", false, true),
            ItemTaken = CreateSound("itemTaken"),
            Door = CreateSound("door");

        [Shorthand]
        private static ISound CreateSound(string fileName)
        {
            return SoundFactory.CreateSound(fileName);
        }

        [Shorthand]
        private static ISound CreateSound(
            string fileName, bool loop, bool onlyOneClipAtATime)
        {
            return SoundFactory.CreateSound(
                fileName, loop, onlyOneClipAtATime);
        }
    }
}