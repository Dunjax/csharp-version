using Sound_ = Dunjax.Sound.Concrete.Sound;

namespace Dunjax.Sound
{
    class SoundFactory
    {
        public static ISound CreateSound(
            string fileName, bool loop = false, bool onlyOneClipAtATime = false)
        {
            return new Sound_(fileName, loop, onlyOneClipAtATime);
        }
    }
}