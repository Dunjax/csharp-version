namespace Dunjax.Sound
{
    /**
     * A sound effect.
     */
    public interface ISound
    {
        /**
         * Plays a clip of this sound.
         */
        void Play();

        /**
         * Has all currently-playing clips of this sound stop playing.
         */
        void Stop();

        /**
         * Returns whether this sound is currently playing.
         */
        bool Playing {get;}
    }
}