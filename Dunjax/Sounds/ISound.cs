namespace Dunjax.Sounds
{
    /**
     * A sound effect.
     */
    interface ISound
    {
        /**
         * Plays a clip of this sound.
         */
        void play();

        /**
         * Returns whether any clips of this sound are currently playing.
         */
        bool isPlaying();

        /**
         * Has all currently-playing clips of this sound stop playing.
         */
        void stop();
        
        /**
         * Returns the object through which this sound's whitebox functionality
         * may be invoked by tests.
         */
        ISoundTestView testView();
    }

    /**
     * Provides whitebox functionality meant only to be used by tests.
     */
    interface ISoundTestView
    {
        /**
         * Returns how many times this sound has been played.
         */
        int getPlayCount();

        /**
         * Returns whether this sound has been played since the last time
         * setNotPlayed() was called.
         */
        bool hasPlayed();

        /**
         * Zeroes this sound's play count.
         */
        void setNotPlayed();

        /**
         * Returns whether this sound has been stopped since the last time 
         * setNotBeenStopped() was called.
         */
        bool hasBeenStopped();

        /**
         * Marks this sound as not having been stopped yet.
         */
        void setNotBeenStopped();
    }
}