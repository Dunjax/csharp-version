namespace Dunjax.Sounds
{
    /**
     * Holds the flyweight sounds.
     */
    class Sounds
    {
        /**
         * The flyweight sounds.
         */
        public static ISound
            playerMove = createSound("playerMove", false, true),
            playerJump = createSound("playerJump"),
            playerThrust = createSound("playerThrust", true, true),
            playerLand = createSound("playerLand"),
            playerSlide = createSound("playerSlide", true, true),
            playerSwitchGuns = createSound("playerSwitchGuns"),
            playerClimb = createSound("playerClimb", false, true),
            playerDeath = createSound("playerDeath"),
            playerHit = createSound("playerHit"),
            playerWhirlwinded = createSound("playerWhirlwinded", false, true),
            playerWebbed = createSound("playerWebbed", false, true),
            monsterAttack = createSound("monsterAttack"),
            monsterDeath = createSound("monsterDeath"),
            greenGargoyleFly = createSound("greenGargoyleFly"),
            stalagmiteWake = createSound("stalagmiteWake"),
            spiderWake = createSound("spiderWake"),
            masterWake = createSound("masterWake"),
            slimeJump = createSound("slimeJump"),
            pulseFire = createSound("pulseFire"),
            grapeFire = createSound("grapeFire"),
            outOfAmmo = createSound("outOfAmmo", false, true),
            halberdFire = createSound("halberdFire"),
            webFire = createSound("webFire"),
            webImpact = createSound("webImpact"),
            shotImpact = createSound("shotImpact"),
            fireballShot = createSound("fireballShot"),
            fireballImpact = createSound("fireballImpact"),
            slimeballShot = createSound("slimeballShot"),
            slimeballImpact = createSound("slimeballImpact"),
            levelEndReached = createSound("levelEndReached", false, true),
            itemTaken = createSound("itemTaken"),
            door = createSound("door");

        /** Shorthand */
        private ISound createSound(string fileName)
        {
            return SoundFactory.createSound(fileName);
        }

        /** Shorthand */
        private ISound createSound(
            string fileName, bool loop, bool onlyOneClipAtATime)
        {
            return SoundFactory.createSound(
                fileName, loop, onlyOneClipAtATime);
        }
    }
}