namespace Dunjax.Sounds
{
    class SoundFactory
    {
        public static ISound createSound(string fileName)
        {
            return new Sound(fileName, false, false);
        }

        public static ISound createSound(
            string fileName, bool loop, bool onlyOneClipAtATime)
        {
            return new Sound(fileName, loop, onlyOneClipAtATime);
        }
    }
}