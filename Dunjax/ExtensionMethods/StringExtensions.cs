﻿namespace Dunjax.ExtensionMethods
{
    public static class StringExtensions
    {
        /**
         * Returns the given string, with the first character converted
         * to lowercase.
         */
        public static string MakeFirstLetterLowercase(this string target)
        {
            if (target.Length == 0) return target;
            return target.Substring(0, 1).ToLower() + target.Substring(1);
        }

        /**
         * Returns the given string, with the first character converted
         * to uppercase.
         */
        public static string MakeFirstLetterUppercase(this string target)
        {
            if (target.Length == 0) return target;
            return target.Substring(0, 1).ToUpper() + target.Substring(1);
        }
    }
}
