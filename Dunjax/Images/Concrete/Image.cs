using Dunjax.Geometry;
using Dunjax.Attributes;

namespace Dunjax.Images.Concrete
{
    /**
     * An image formulated as a wrapper for an ISystemImage.
     */
    public class Image : IImage
    {
        /**
         * The system-image wrapped by this instance.
         */
        public ISystemImage image { get; private set; }

        /**
         * The path to this image's file on disk.
         */
        protected string path;

        /**
         * Whether this image should be drawn in an xwise-flipped 
         * manner, as a mirror of another image.
         */
        public bool DrawFlipped { get; private set; }

        [Constructor]
        public Image(string path)
        {
            this.path = path;

            // create the system-image for which this object is a wrapper
            image = systemImageCreator.CreateSystemImage(path);
        }
        
        [Implementation]
        public ISize Size { get { return image.Size; } }

        [Implementation]
        public IImage FlipXWise()
        {
            return new Image(path) {image = image, DrawFlipped = true};
        }

        /**
         * This class's delegate for creating the system-images 
         * it will wrap.  Is set directly by code in a separate, 
         * system-dependent project.
         */
        public static ISystemImageCreator systemImageCreator;
    }

    /**
     * A system-image which the above Image class will wrap.
     */
    public interface ISystemImage
    {
        ISize Size { get; }
    }

    /**
     * Creates the system-images which the above Image 
     * class will wrap.
     */
    public interface ISystemImageCreator
    {
        /**
         * Creates and returns a system-image whose image file
         * is located at the given path.
         */
        ISystemImage CreateSystemImage(string imageFilePath);

        /**
         * Returns whether a (presumably, image) file exists 
         * at the given path from wherever images are being
         * loaded by the system-dependent code.
         */
        bool FileExists(string path);
    }
}