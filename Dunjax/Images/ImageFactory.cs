using System.IO;
using Dunjax.Images.Concrete;

namespace Dunjax.Images
{
    public static class ImageFactory
    {
        /**
	     * Creates an image from the gif file of the given name (without the ".gif") 
         * in the images folder on disk. 
         * 
         * Returns null if the image can't be found on disk.
	     */
        public static Image CreateImage(string fileName)
        {
            var path = "images/" + fileName;
            return Image.systemImageCreator.FileExists(path) ? new Image(path) : null;
        }
    }
}
