using Dunjax.Geometry;

namespace Dunjax.Images
{
    /**
     * An image displayed by the program.
     */
    public interface IImage
    {
        /**
         * This image's size, in pixels.
         */
        ISize Size { get; }

        /**
         * Returns a copy of this image, flipped in the horizontal direction.
         */
        IImage FlipXWise();
    }
}