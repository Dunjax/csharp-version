using System;
using Dunjax.Entities.Beings;
using Dunjax.Entities.Beings.Monsters;
using Dunjax.Geometry;
using Dunjax.Entities;
using Dunjax.Entities.Items;
using Dunjax.Entities.Beings.Player;

namespace Dunjax.Map
{
    /**
     * A map in which the game play takes place.  A map specifies the layout of 
     * the terrain it contains.  Maps also contain map-entities of various sorts.
     */
    public interface IMap
    {
        /**
         * Is a shorthand for IsPassible(location, ImpassableEntitiesSet.NoneSet).
         */
        bool IsPassible(IPoint location);

        /**
         * Returns whether the given location on this map is passable, taking
         * into account the given specification of which classes of entities
         * should be considered impassable.  
         */
        bool IsPassible(IPoint location, ImpassableEntitiesSet set);

        /**
         * Returns whether the given location on this map is fall-throughable.
         */
        bool IsFallThroughable(IPoint location);

        /**
         * Returns whether the given location is visible from the given
         * from-location, taking into account the given specification of whether
         * intervening beings block sight.
         * 
         * @param forMapView    Whether the call to this method is being made by the 
         *                      map-view thread, where higher accuracy is necessary.
         */
        bool IsLocationVisibleFrom(IPoint location,
            IPoint from, bool beingsBlockSight, bool forMapView);

        /**
         * Adds the given map-entity to those contained by this map.
         */
        void AddEntity(IMapEntity entity);

        /**
         * Removes the given map-entity from those contained by this map.
         */
        void RemoveEntity(IMapEntity entity);

        /**
         * Returns the player (if any) which resides in this map.
         */
        IPlayer Player {get;}

        /**
         * Returns the player (if any) that's at the given location on this map.
         */
        IPlayer GetPlayerAt(IPoint location);

        /**
         * Returns the being that's at the given location (or null, if there is 
         * none) within this map.  A dead being will not be returned.
         */
        IBeing GetBeingAt(IPoint location);

        /**
         * Returns the monster that's at the given location (or null, if there is 
         * none) within this map.  A dead monster will not be returned.
         */
        IMonster GetMonsterAt(IPoint location);

        /**
         * Returns the first animation entity found within the given bounds 
         * (or null, if there is none) within this map.
         */
        IAnimationEntity GetAnimationEntityInBounds(IRectangle bounds);

        /**
         * Returns the first item found within the given bounds (or null, if there
         * is none) within this map.
         */
        IItem GetItemInBounds(IRectangle bounds);

        /**
         * Returns the center location of the presumably only instance of the
         * given terrain feature within this map.  Is used for purposes such as
         * finding the level entrance, to know where to start the player. 
         */
        IPoint GetLocationOfTerrainFeature(TerrainFeature feature);

        /**
         * Returns the terrain feature found at the given location.
         */
        TerrainFeature GetTerrainFeatureAt(IPoint location);

        /**
         * Sets the given terrain feature into the given location on this map.
         * This replaces the previous feature that was there.
         * 
         * Currently, the only supported terrain feature is a closed door. 
         */
        void SetTerrainFeatureAt(IPoint location, TerrainFeature feature);

        /**
         * Tells this map to perform its action for the current game turn.  
         * The map is expected to give its inherent features a chance to act, as 
         * well.
         */
        void Act();

        /**
         * Informs this map that the given entity is about to change location, so 
         * this map may update its tracking of the entity.
         */
        void EntityAboutToMove(IMapEntity entity);

        /**
         * Informs this map that the given entity has changed location, so 
         * this map may update its tracking of the entity.
         */
        void EntityMoved(IMapEntity entity);

        /**
         * Returns the y-value of the first location found at or above the one
         * given where that location is passible, and the location just below it 
         * is impassible.
         */
        short GetYJustAboveGround(IPoint location);

	    /**
	     * Returns the y-value of the first location found at or below the one
	     * given where that location is passible, and the location just above it 
	     * is impassible.
	     */
        short GetYJustBelowCeiling(IPoint location);

        /**
         * Returns the object this map is using to coordinate the actions of its
         * contained entities, which must access this object to schedule 
         * their actions.
         */
        IEntityActionSchedule EntityActionSchedule {get;}

        /**
         * Is fired when a change occurs to this map's terrain (e.g. opening
         * a door) which could immediately affect what the player can see.
         */
        event EventHandler LineOfSightChangeOccurred;
    }

    /**
     * Values that are passed to the IsPassible(...) methods to specify
     * which classes of entities should be considered impassable.
     */
    public enum ImpassableEntitiesSet { NoneSet, BeingsSet, PlayerOnlySet };

    /**
     * Abstract terrain features a map may contain.
     */
    public enum TerrainFeature
    {
        None, LockedDoor, ClosedDoor, OpenDoor, StartCave, ExitCave, Ladder,
        SlideLeft, SlideRight, Stalagmite, SlimePool, Spikes
    };

    public class MapConstants
    {
        /**
         * How many units of x- or y- change in a map would equate to ten feet 
         * of distance in the world being modeled.
         */
        public const int UnitsPerTenFeet = 32;
    }
}