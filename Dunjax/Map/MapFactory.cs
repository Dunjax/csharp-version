using Dunjax.Geometry;
using Dunjax.Map.Concrete;
using Map_ = Dunjax.Map.Concrete.Map;

namespace Dunjax.Map
{
    public class MapFactory
    {
        public static IMap CreateMap(string name, ISize size, IClock clock)
        {
            return new Map_(name, size, clock);
        }

        public static IMapLoader CreateMapLoader(IClock clock)
        {
            return new MapLoader(clock);
        }

        public static IMapView CreateMapView()
        {
            return new MapView();
        }
    }
}