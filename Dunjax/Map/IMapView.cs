using Dunjax.Geometry;
using Dunjax.Gui;

namespace Dunjax.Map
{
    /**
     * A view onto the contents of a map.
     */
    public interface IMapView
    {
        /**
         * The map this view should draw.
         */
        IMap Map {get; set;}

        /**
         * Has this view draw the player's current surroundings within the map 
         * into the given drawing area.
         */
        void Draw(IDrawingArea drawingArea);

        /**
         * This view's physical dimensions, in pixels.
         */
        ISize Size { get; set; }

        /**
         * Whether this view should label entities with their ID numbers.
         */
        bool LabelEntities { get; set; }
    }
}