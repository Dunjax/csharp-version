using Dunjax.Geometry;
namespace Dunjax.Map.Tiles
{
    /**
     * A square building-block of a map. 
     */
    interface ITile
    {
        /**
         * A descriptive name for this tile-type.
         */
        string name {get;}
        
        /**
	     * The index (or ID) of this tile-type amongst all others. These values
	     * correspond with those of the WGT map data.
	     */
	    int index {get;}
        
        /**
         * The terrain feature (if any) represented by this tile-type.
         */
        TerrainFeature terrainFeature {get;}

        /**
	     * Whether this tile-type blocks line-of-sight.
	     */
	    bool sightBlocking {get;}

	    /**
	     * What slide-type this tile-type is.
	     */
	    SlideType slideType {get;}

	    /**
	     * The image which visually represents this tile-type.
	     */
	    IImage image {get;}

	    /**
         * How long should be spent on ths tile-type before moving on to 
         * the next one in its animation sequence (if it is in one). 
	     */
        int animationDuration {get;}
        
        /**
	     * Returns whether the point at the given map-location is fall-throughable.
	     */
	    bool isFallThroughable(IPoint location);

        /**
	     * Returns whether the point at the given map-location is passible.
	     */
	    bool isPassible(IPoint location);
    }

    /**
     * The kinds of slides a tile may be.
     */
    enum SlideType {notASlide, left, right};
    	
    /**
     * Specifies a subregion of a tile's square shape.
     */
    interface ITileRegion
    {
        /**
         * Returns whether this region contains the given coordinates
         * within a tile.
         */
        bool contains(IPoint location);
    }

    /**
     * The different tile regions which may be specified.
     */
    class TileRegions
    {
        public static ITileRegion all, none, up, down, upLeft, upRight, downLeft, downRight;
    }

    /**
     * All the tiles used in the game.
     */
    class Tiles
    {
        public static ITile[]
            darkness, openBrick, wallRock, 
            stalagmiteCave, stalagmiteBrick, 
            openCave, bloodyCave, openCaveBorder, 
            smallFire, torchBrick, 
            fungi1, fungi2,
            pillarTop, pillarBase, brokenPillarTop, brokenPillarBottom,
            pillarCrack1, pillarCrack2,
            rockBrick, rockCave, trashBrick, trashCave,
            spiderCaveUp, spiderCave, spiderCaveDown,
            chainBrick, chainCave,
            slimePoolBrick, slimePoolCave,
            ledge1, ledge2;

        public static ITile
            secretDoorRock, secretDoorTerraceWall,
            ladderBrick, ladderCave,
            slideTerraceBrickLeft, slideTerraceBrickRight,
            closedDoorBrick, lockedDoorBrick, openDoorBrick,
            slideTerraceBrickUpLeft, slideTerraceBrickUpRight,
            terraceRockUp,
            startCaveDownLeft, startCaveUpLeft, startCaveUpRight, startCaveDownRight,
            pillarShaft, pillarBase1,
            wallTerraceDownRight, wallTerraceDown, wallTerraceDownLeft, wallTerrace,
            slideTerraceRockLeft, slideTerraceRockRight, slideTerraceRockUpLeft, slideTerraceRockUpRight,
            slideRockCaveLeft, slideRockCaveRight, slideRockCaveUpLeft, slideRockCaveUpRight,
            slideRockBrickLeft, slideRockBrickRight, slideRockBrickUpLeft, slideRockBrickUpRight,
            exitCaveUpLeft, exitCaveUpRight, exitCaveDownLeft, exitCaveDownRight,
            projectingLedge, projectingLedgeShadow,
            keyLedgeBrick, keyLedgeCave,
            closedDoorCave, lockedDoorCave, openDoorCave,
            pillarTopUpLeft1, pillarTopUpLeft2, pillarTopUpRight1, pillarTopUpRight2,
            pillarLeft, pillarRight,
            pillarCrack3,
            spikesBrick, spikesCave;
    }
}