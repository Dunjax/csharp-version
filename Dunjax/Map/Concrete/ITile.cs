using Dunjax.Geometry;
using Dunjax.Images;

namespace Dunjax.Map.Concrete
{
    /**
     * A square building-block of a map. 
     */
    interface ITile
    {
        /**
         * A descriptive name for this tile-type.
         */
        string Name {get;}
        
        /**
	     * The index (or ID) of this tile-type amongst all others. These values
	     * correspond with those of the WGT map data.
	     */
	    int Index {get;}
        
        /**
         * The terrain feature (if any) represented by this tile-type 
         * at the given map location.
         */
        TerrainFeature GetTerrainFeatureAt(IPoint location);

        /**
	     * Whether this tile-type blocks line-of-sight.
	     */
	    bool SightBlocking {get;}

	    /**
	     * What slide-type this tile-type is.
	     */
	    SlideType SlideType {get;}

	    /**
	     * The image which visually represents this tile-type.
	     */
	    IImage Image {get;}

	    /**
         * How long should be spent on ths tile-type before moving on to 
         * the next one in its animation sequence (if it is in one). 
	     */
        int AnimationDuration {get;}

        /**
         * The tile that comes after this one in this tile's animation sequence
         * (if there is one).
         */
        ITile NextTileInAnimation { get; }
        
        /**
	     * Returns whether the point at the given map-location is fall-throughable.
	     */
	    bool IsFallThroughable(IPoint location);

        /**
	     * Returns whether the point at the given map-location is passible.
	     */
	    bool IsPassible(IPoint location);
    }

    /**
     * The kinds of slides a tile may be.
     */
    enum SlideType {NotASlide, Left, Right};
    	
    /**
     * Specifies a subregion of a tile's square shape.
     */
    interface ITileRegion
    {
        /**
         * Returns whether this region contains the given coordinates
         * within a tile.
         */
        bool Contains(IPoint location);
    }

    /**
     * All the tiles used in the game.
     */
    partial class Tiles
    {
        // sequences of tiles
        public static Tile[]
            Darkness, OpenBrick, WallRock, 
            StalagmiteCave, StalagmiteBrick, 
            OpenCave, BloodyCave, OpenCaveBorder, 
            SmallFire, TorchBrick, 
            Fungi1, Fungi2,
            PillarTop, PillarBase, BrokenPillarTop, BrokenPillarBottom,
            PillarCrack1, PillarCrack2,
            WallTerraceDown,
            RockBrick, RockCave, TrashBrick, TrashCave,
            SpiderCave,
            ChainBrick, ChainCave,
            SlimePoolBrick, SlimePoolCave,
            Ledge1, Ledge2;

        // single tiles
        public static Tile
            SecretDoorRock, SecretDoorTerraceWall,
            LadderBrick, LadderCave,
            SlideTerraceBrickLeft, SlideTerraceBrickRight,
            ClosedDoorBrick, LockedDoorBrick, OpenDoorBrick,
            SlideTerraceBrickUpLeft, SlideTerraceBrickUpRight,
            TerraceRockUp,
            StartCaveDownLeft, StartCaveUpLeft, StartCaveUpRight, StartCaveDownRight,
            PillarShaft, PillarBase1,
            WallTerrace,
            SlideTerraceRockLeft, SlideTerraceRockRight, SlideTerraceRockUpLeft, SlideTerraceRockUpRight,
            SlideRockCaveLeft, SlideRockCaveRight, SlideRockCaveUpLeft, SlideRockCaveUpRight,
            SlideRockBrickLeft, SlideRockBrickRight, SlideRockBrickUpLeft, SlideRockBrickUpRight,
            ExitCaveUpLeft, ExitCaveUpRight, ExitCaveDownLeft, ExitCaveDownRight,
            ProjectingLedge, ProjectingLedgeShadow,
            KeyLedgeBrick, KeyLedgeCave,
            ClosedDoorCave, LockedDoorCave, OpenDoorCave,
            PillarTopUpLeft1, PillarTopUpLeft2, PillarTopUpRight1, PillarTopUpRight2,
            PillarLeft, PillarRight,
            PillarCrack3,
            SpikesBrick, SpikesCave;
    }
}