using System;
using System.IO;
using Dunjax.Attributes;
using Dunjax.Entities;
using Dunjax.Entities.Beings.Monsters;
using Dunjax.Entities.Items;
using Dunjax.Geometry;

namespace Dunjax.Map.Concrete
{
    class MapLoader : IMapLoader
    {
        /**
         * The map this loader is currently loading.
         */
        protected ITiledMap map;
        
        /**
         * The stream from which the map data is read.
         */
        protected BinaryReader reader;
        
        /**
         * The size (in tiles) of the map currently being loaded.
         */
        protected ISize mapTileSize;

        /**
         * The clock to supply to the loaded map for it to use.
         */
        protected readonly IClock clock;

        [StaticImport]
        protected static readonly ISize TileSize = Tile.TileSize;

        /**
         * When a value is specifed here, only the entity with that ID will be loaded,
         * to make debugging easier.
         */
        public static int EntityIdToLoad;

        [Constructor]
        public MapLoader(IClock clock)
        {
            this.clock = clock;
        }

        /**
         * Returns the next short read from this loader's input stream.
         */
        protected short ReadShort()
        {
            return reader.ReadInt16();
        }

        /**
         * Returns the next byte read from this loader's input stream.
         */
        protected byte ReadByte()
        {
            return reader.ReadByte();
        }

        /**
         * Creates and stores a data-input-stream on the file of the given name.
         */
        protected void CreateStream(string fileName)
        {
            reader = new BinaryReader(File.OpenRead(fileName));
        }
        
        [Implementation]
        public IMap Load(string fileName)
        {
            // open a stream on the map file of the given name
            CreateStream(fileName);
            
            // create the map object that will house the loaded data
            map = (Map)MapFactory.CreateMap(fileName, LoadMapSize(), clock);
            
            LoadTiles();
            
            map.CreateTileAnimations();
            
            // ignore the tile-types in the file
            for (int i = 0; i < 256; i++) ReadShort();

            LoadEntities();
            
            return map;
        }
        
        /**
         * Returns the map size read from the current input stream.
         */
        protected ISize LoadMapSize()
        {
            // read in the map's dimensions
            ReadShort();
            mapTileSize = GeometryFactory.CreateSize(ReadShort(), ReadShort());
            return GeometryFactory.CreateSize(
                (short) (mapTileSize.Width * TileSize.Width),
                (short) (mapTileSize.Height * TileSize.Height));
        }
        
        /**
         * Loads in the contents of the map's tiles-grid. 
         */
        protected void LoadTiles()
        {
            // for each spot in this map's tiles-grid
            for (short y = 0; y < mapTileSize.Height; y++) {
                for (short x = 0; x < mapTileSize.Width; x++) {
                    // read in the tile data-value for this spot
                    int datum = ReadShort();
                    
                    // map the tile data-value for this spot to the corresponding 
                    // tile-type, and set that type into this spot in the tiles-grid
                    if (datum < 0) datum += 256;
                    map.SetTile(x, y, Tile.GetTile(datum));
                    
                    CheckForTileSubstitution(x, y);
                }
            }
        }
        
        /**
         * Checks whether the tile-type at the given tile-location should be 
         * replaced by another, for any of various reasons.  If it should,
         * the replacement is performed.  
         */
        protected void CheckForTileSubstitution(short tileX, short tileY)
        {
            // if the tile at the given location is for a non-wavy slime-pool 
            // (w/brick background)
            var tile = map.GetTile(tileX, tileY);
            if (tile == Tiles.SlimePoolBrick[0]) {
                // change the tile to one with a wave, alternating
                // wave direction with the like tile (if one exists) to the left
                ITile tileToLeft = map.GetTile((short) (tileX - 1), tileY);
                map.SetTile(tileX, tileY,
                    (tileToLeft == Tiles.SlimePoolBrick[1]) ?
                        Tiles.SlimePoolBrick[2] : Tiles.SlimePoolBrick[1]); 
            }
            
            // else, if the tile at the given location is for a non-wavy slime-pool 
            // (w/cave background)
            else if (tile == Tiles.SlimePoolCave[0]) {
                // change the tile to one with a wave, alternating
                // wave direction with the like tile (if one exists) to the left
                var tileToLeft = map.GetTile((short) (tileX - 1), tileY);
                map.SetTile(tileX, tileY,
                    (tileToLeft == Tiles.SlimePoolCave[1]) ? 
                    Tiles.SlimePoolCave[2] : Tiles.SlimePoolCave[1]); 
            }
        }
        
        /**
         * The map-file data values that represent the different kinds of entities.
         * 
         * Note that some entity IDs whose meanings are unknown are mapped to 
         * pulse-ammo items, which are the least valuable items to find.
         */
        protected static class EntityIds
        {
            public const short
                GreenGargoyleLeftId = 5,
                GreenGargoyleRightId = 6,
                YellowGargoyleLeftId = 23,
                YellowGargoyleRightId = 24,
                StalagmiteId = 30,
                MasterId = 400,
                WhirlwindLeftId = 11,
                WhirlwindRightId = 12,
                SpiderLeftId = 27,
                SpiderRightId = 28,
                SlimeLeftId = 31,
                SlimeRightId = 32,
                BlueGargoyleLeftId = 25,
                BlueGargoyleRightId = 26,
                PulseAmmoBigId = 37,
                GrapeshotGunId = 39,
                PulseAmmoId = 38,
                PulseAmmoId2 = 151,
                PulseAmmoId3 = 33,
                PulseAmmoId4 = 34,
                PulseAmmoId5 = 35,
                PulseAmmoId6 = 36,
                GrapeshotAmmoId = 40,
                GrapeshotAmmoId2 = 45,
                ShieldPowerUpId = 42,
                ShieldPowerUpId2 = 43,
                ShieldPowerPackId = 41,
                ShieldPowerPackId2 = 44,
                KeyId = 47,
                FinalRoomTriggerId = 20;
        }

        /**
         * Loads the data for this map's monsters and items from the map-file
         * data-input-stream.
         */
        protected void LoadEntities() 
        {
            // start the entities' IDs at one
            EntitiesFactory.ResetEntitiesIdCounter();

            // load every entity in the file
            int numEntities = ReadShort();
            for (int k = 0; k < numEntities; k++) LoadEntity();
        }
        
        /**
         * Loads the data for an entity from the map-file data-input-stream.
         */
        protected void LoadEntity()
        {
            // read the data for this entity from the file; note that we 
            // double the location ordinates, since they are based on a map
            // with 16x16 tiles, rather than the 32x32 ones used for this 
            // version of the game
            ReadByte();
            short x = (short) (ReadShort() * 2), 
                y = (short) (ReadShort() * 2);
            int id = ReadShort();

            // create the entity to be of the type specified by the ID value 
            var entity = CreateEntityFromId(id);
            
            // for some reason, there are some entity values in the first 
            // map whose IDs don't match any of the known entity-type IDs;
            // we skip those entities;
            // also, skip this entity if we are to load only one entity 
            // of a given ID, and this entity isn't it
            if (entity == null
                || (EntityIdToLoad > 0 && entity.Id != EntityIdToLoad)) 
                return;

            // add this entity to this map
            map.AddEntity(entity);

            // set this entity's location to that given by the file
            entity.MoveTo(x, y);

            AlignEntity(entity);
        }
        
        /**
         * Aligns the given entity with its surroundings, according to its type.
         */
        protected void AlignEntity(IMapEntity entity)
        {
            if (IsKey(entity)) AlignKeyWithLedgeTop(entity);
            else entity.AlignWithSurroundings();
        }
        
        /**
         * Returns whether the given entity is a key item.
         */
        protected static bool IsKey(IMapEntity entity)
        {
            return entity is IItem && ((IItem)entity).ItemType == ItemType.Key;
        }
        
        /**
         * Creates and returns a map-entity of the type specified by the given 
         * ID value. 
         */
        protected IMapEntity CreateEntityFromId(int id)
        {
            IMapEntity entity = null;
            switch (id) {
                case EntityIds.GreenGargoyleLeftId:
                case EntityIds.GreenGargoyleRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.GreenGargoyle, clock);
                    break;
                case EntityIds.YellowGargoyleLeftId:
                case EntityIds.YellowGargoyleRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.YellowGargoyle, clock);
                    break;
                case EntityIds.BlueGargoyleLeftId:
                case EntityIds.BlueGargoyleRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.BlueGargoyle, clock);
                    break;
                case EntityIds.WhirlwindLeftId:
                case EntityIds.WhirlwindRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.Whirlwind, clock);
                    break;
                case EntityIds.StalagmiteId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.Stalagmite, clock);
                    break;
                case EntityIds.MasterId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.Master, clock);
                    MasterCreated(entity, null);
                    break;
                case EntityIds.SpiderLeftId:
                case EntityIds.SpiderRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.Spider, clock);
                    break;
                case EntityIds.SlimeLeftId:
                case EntityIds.SlimeRightId:
                    entity = MonsterFactory.CreateMonster(
                        MonsterTypeEnum.Slime, clock);
                    break;
                case EntityIds.PulseAmmoBigId:
                    entity = ItemFactory.CreateItem(ItemType.PulseAmmoBig, clock);
                    break;
                case EntityIds.GrapeshotGunId:
                    entity = ItemFactory.CreateItem(ItemType.GrapeshotGun, clock);
                    break;
                case EntityIds.PulseAmmoId:
                case EntityIds.PulseAmmoId2:
                case EntityIds.PulseAmmoId3:
                case EntityIds.PulseAmmoId4:
                case EntityIds.PulseAmmoId5:
                case EntityIds.PulseAmmoId6:
                    entity = ItemFactory.CreateItem(ItemType.PulseAmmo, clock);
                    break;
                case EntityIds.GrapeshotAmmoId:
                case EntityIds.GrapeshotAmmoId2:
                    entity = ItemFactory.CreateItem(ItemType.GrapeshotAmmo, clock);
                    break;
                case EntityIds.ShieldPowerUpId:
                case EntityIds.ShieldPowerUpId2:
                    entity = ItemFactory.CreateItem(ItemType.ShieldPowerUp, clock);
                    break;
                case EntityIds.ShieldPowerPackId:
                case EntityIds.ShieldPowerPackId2:
                    entity = ItemFactory.CreateItem(ItemType.ShieldPowerPack, clock);
                    break;
                case EntityIds.KeyId:
                    entity = ItemFactory.CreateItem(ItemType.Key, clock);
                    break;
                case EntityIds.FinalRoomTriggerId:
                    entity = EntitiesFactory.CreateAnimationEntity(
                        AnimationEntityType.FinalRoomTrigger, null, clock);
                    break;
            }
            
            return entity;
        }
        
        /**
         * Moves the given key to a bit above the center of the tile it's
         * on, so that it appears on top of the ledge that is presumed to be 
         * in that tile.
         */
        protected void AlignKeyWithLedgeTop(IMapEntity key)
        {
            key.CenterOn(
                map.GetTileCenterLocation(key.BottomCenter.Add(0, 8)).Add(0, -8));
        }

        [Implementation]
        public event EventHandler MasterCreated;
    }
}
