using System.Collections.Generic;
using Dunjax.Attributes;
using Dunjax.Geometry;
using Dunjax.Entities;

namespace Dunjax.Map.Concrete
{
    class AreaBins<T> : IAreaBins<T> where T : IMapEntity
    {
        /**
         * The grid of bins covering corresponding areas on a map. 
         */
        protected List<T>[,] bins;
        
        /**
         * The size (in terms of map pixels covered) of every bin.
         */
        protected const int BinPixelWidth = 200, BinPixelHeight = BinPixelWidth;
        
        [Implementation]
        public void AddToBins(T entity)
        {
            AddToOrRemoveFromBins(entity, true);
        }

        [Implementation]
        public void RemoveFromBins(T entity)
        {
            AddToOrRemoveFromBins(entity, false);
        }
        
        /**
         * Adds or removes the given entity to/from those bins representing the 
         * areas it intersects.
         * 
         * Optimized.
         */
        protected void AddToOrRemoveFromBins(T entity, bool add) 
        {
            // add/remove the entity from the bin covering its upper-left location
            int binX = entity.Bounds.LeftX / BinPixelWidth;
            int binY = entity.Bounds.TopY / BinPixelHeight;
            AddToOrRemoveFromBin(entity, add, binX, binY);

            // if the entity spans more than one bin, add/remove it from those
            // other bins
            int binRightX = entity.Bounds.RightX / BinPixelWidth;
            if (binRightX != binX) AddToOrRemoveFromBin(entity, add, binRightX, binY);
            int binBottomY = entity.Bounds.BottomY / BinPixelHeight;
            if (binBottomY != binY) AddToOrRemoveFromBin(entity, add, binX, binBottomY);
            if (binRightX != binX && binBottomY != binY) 
                AddToOrRemoveFromBin(entity, add, binRightX, binBottomY);
        }
        
        /**
         * Adds or removes the given entity to/from the bin at the given 
         * coordinates in the bin grid.  If an add is specified, this method presumes
         * that the entity isn't already in the targeted bin.
         */
        protected void AddToOrRemoveFromBin(
            T entity, bool add, int binX, int binY)
        {
            List<T> bin = bins[binX, binY];
            if (add) bin.Add(entity);
            else bin.Remove(entity);
        }

        [Implementation]
        public List<T> GetEntitiesFromBin(IPoint location)
        {
            return bins[location.X / BinPixelWidth, location.Y / BinPixelHeight];
        }

        [Implementation]
        public void OnMapSizeSet(ISize size)
        {
            // create the bins grid
            int numBinsWide = size.Width / BinPixelWidth + 1;
            int numBinsHigh = size.Height / BinPixelHeight + 1;
            bins = new List<T>[numBinsWide, numBinsHigh];
            
            // create each of the bins in the bins grid
            for (int i = 0; i < numBinsWide; i++) {
                for (int j = 0; j < numBinsHigh; j++) {
                    bins[i, j] = new List<T>();
                }
            }
        }
    }
}