﻿using Dunjax.Geometry;

namespace Dunjax.Map.Concrete
{
    /**
     * A map whose terrain is composed of tiles.
     */
    interface ITiledMap : IMap
    {
        ITile GetTile(IPoint location);
        ITile GetTile(short tileX, short tileY);
        void SetTile(IPoint location, ITile tile);
        void SetTile(short tileX, short tileY, ITile tileType);

        /**
         * Returns the center location of the tile at the given location.
         */
        IPoint GetTileCenterLocation(IPoint location);

        /**
         * Records which tiles in this map will animate, and creates the necessary
         * data structures for the animations to occur while the game is being
         * played. 
         */
        void CreateTileAnimations();

        /**
         * Returns whether any part of the tile at the given location is 
         * visible from the given from-location, taking into account the 
         * given specification of whether intervening beings block sight.
         */
        bool IsTileAtLocationVisibleFrom(IPoint location, IPoint from, bool beingsBlockSight);
    }
}
