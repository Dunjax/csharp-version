using Dunjax.Attributes;
using Dunjax.Geometry;
using Dunjax.Images;

namespace Dunjax.Map.Concrete
{
    class Tile : ITile
    {
        /**
         * A descriptive name for this tile.
         */
        public string Name { get; private set; }
        
        /**
	     * The index (or ID) of this tile amongst all others. These values
	     * correspond with those of the WGT map data.
	     */
        public int Index { get; private set; }
        
        /**
         * The terrain feature (if any) represented by this tile.
         */
        private readonly TerrainFeature terrainFeature;

        /**
         * The region of this tile to which the above terrain-feature applies.
         */
        private readonly Region terrainFeatureRegion;

	    /**
	     * Specifies which parts of this tile are passible.
	     */
        internal Region passibleRegion = Region.All;

	    /**
	     * Specifies which parts of this tile may be fallen through by the
	     * player. If no region is specified here, this value should be assumed to
	     * equal that for passibleRegion. Ladders are an example of where the values
	     * would differ between the two.
	     */
        internal Region fallThroughableRegion;

	    /**
	     * Whether this tile blocks line-of-sight.
	     */
        public bool SightBlocking { get; set; }

	    /**
	     * What slide-type this tile is.
	     */
	    public SlideType SlideType { get; set; }

	    /**
	     * The image which visually represents this tile.
	     */
        public IImage Image { get; private set; }

	    /**
         * How long should be spent on ths tile before moving on to 
         * the next one in its animation sequence (if it is in one). 
	     */
        public int AnimationDuration { get; set; }

        /**
         * The tile that comes after this one in this tile's animation sequence
         * (if there is one).
         */
        public ITile NextTileInAnimation { get; set; }

        /**
	     * The total number of tile-types there are in the game.
	     */
	    private const int NumTiles = 240;

	    /**
	     * An array of all the tile-types.
	     */
	    private static readonly Tile[] Tiles = new Tile[NumTiles];

        /**
         * The size of a tile, in pixels.
         */
        public static readonly ISize TileSize = GeometryFactory.CreateSize(32, 32);
        
        [Constructor]
        public Tile(int index, string name,
            TerrainFeature terrainFeature = TerrainFeature.None,
            Region terrainFeatureRegion = null)
        {
            Name = name;
            Index = index;
            this.terrainFeature = terrainFeature;
            this.terrainFeatureRegion = terrainFeatureRegion ?? Region.All;
            Tiles[index] = this;
            Image = ImageFactory.CreateImage("tiles/" + name);
            AnimationDuration = 100;
            SlideType = SlideType.NotASlide;
        }

        /**
         * Returns the tile of the given index.
         */
        public static Tile GetTile(int index) {return Tiles[index];}

        /**
	     * Returns whether the point at the given map-location is fall-throughable.
	     */
	    public bool IsFallThroughable(IPoint location)
	    {
		    return fallThroughableRegion != null 
			    ? fallThroughableRegion.Contains(location) : IsPassible(location);
	    }

        /**
	     * Returns whether the point at the given map-location is passible.
	     */
	    public bool IsPassible(IPoint location)
	    {
		    if (passibleRegion == Region.All) return true;
		    if (passibleRegion == Region.None) return false;
            return passibleRegion.Contains(location);
	    }

        /** 
        * Overridden to represent this tile by its name.
        */
        public override string ToString()
        {
            return "Tile: " + Name;
        }

        [Implementation]
        public TerrainFeature GetTerrainFeatureAt(IPoint location)
        {
            return terrainFeatureRegion.Contains(location)
                ? terrainFeature : TerrainFeature.None;
        }
    }

    /**
     * Specifies a subregion of a tile's square shape.
     */
    internal class Region
    {
        /** 
         * The function this region uses to determine whether it contains the given location.
         */
        private delegate bool ContainsDelegate(IPoint location);
        private readonly ContainsDelegate containsDelegate;

        [StaticImport]
        protected static readonly ISize TileSize = Tile.TileSize;

        [Constructor]
        Region(ContainsDelegate containsDelegate)
        {
            this.containsDelegate = containsDelegate;
        }

        /**
         * Returns whether this region contains the given location.  
         * The location is presumed to be a zero-based offset into an abstract tile's bounds.
         */
        public bool Contains(IPoint location)
        {
            return containsDelegate(location);
        }

        /**
         * The flyweight tile regions.
         */
        public static readonly Region
            All = new Region(location => true),
            None = new Region(location => false),
            Up = new Region(
                location => (location.Y % TileSize.Height < TileSize.Height / 2)),
            Down = new Region(
                location => (location.Y % TileSize.Height >= TileSize.Height / 2)),
            UpLeftInclusive = new Region(
                location => (location.Y % TileSize.Height <= -(location.X % TileSize.Width) + (TileSize.Width - 1))),
            UpRightInclusive = new Region(
                location => (location.Y % TileSize.Height <= location.X % TileSize.Width)),
            DownLeftInclusive = new Region(
                location => (location.Y % TileSize.Height >= location.X % TileSize.Width)),
            DownRightInclusive = new Region(
                location => (location.Y % TileSize.Height >= -(location.X % TileSize.Width) + (TileSize.Width - 1))),
            UpLeftExclusive = new Region(
                location => (location.Y % TileSize.Height < -(location.X % TileSize.Width) + (TileSize.Width - 1))),
            UpRightExclusive = new Region(
                location => (location.Y % TileSize.Height < location.X % TileSize.Width)),
            DownLeftExclusive = new Region(
                location => (location.Y % TileSize.Height > location.X % TileSize.Width)),
            DownRightExclusive = new Region(
                location => (location.Y % TileSize.Height > -(location.X % TileSize.Width) + (TileSize.Width - 1)));
    }

    partial class Tiles
    {
        /**
         * The flyweight tile-types.
         */
        static Tiles() {
            Darkness = new [] {
                new Tile(0, "darkness"),
                new Tile(5, "darkness"),
                new Tile(51, "darkness"),
            };
            OpenBrick = new [] {
                new Tile(1, "openBrick"),
                new Tile(52, "openBrick2"),
                new Tile(53, "openBrick3"),
                new Tile(54, "openBrick4"),
                new Tile(55, "openBrick5"),
                new Tile(122, "openBrick6"),
                new Tile(123, "openBrick7"),
                new Tile(124, "openBrick8"),
                new Tile(125, "openBrick9"),
                new Tile(126, "openBrick10"),
                new Tile(127, "openBrick11"),
                new Tile(128, "openBrick12"),
                new Tile(129, "openBrick13"),
                new Tile(130, "openBrick14"),
                new Tile(131, "openBrick15"),
                new Tile(132, "openBrick16"),
                new Tile(133, "openBrick17"),
                new Tile(134, "openBrick18"),
                new Tile(135, "openBrick19"),
                new Tile(136, "openBrick20"),
                new Tile(137, "openBrick21"),
            };
            WallRock = new [] {
                new Tile(2, "wallRock1"),
                new Tile(60, "wallRock2"),
            };
            SecretDoorRock = new Tile(6, "secretDoorRock");
            LadderBrick = new Tile(8, "ladderBrick", TerrainFeature.Ladder);
            SlideTerraceBrickLeft = new Tile(12, "slideTerraceBrickLeft", TerrainFeature.SlideLeft, Region.DownRightInclusive);
            SlideTerraceBrickRight = new Tile(13, "slideTerraceBrickRight", TerrainFeature.SlideRight, Region.DownLeftInclusive);
            ClosedDoorBrick = new Tile(29, "closedDoorBrick", TerrainFeature.ClosedDoor);
            LockedDoorBrick = new Tile(30, "lockedDoorBrick", TerrainFeature.LockedDoor);
            KeyLedgeBrick = new Tile(31, "keyLedgeBrick");
            SlideTerraceBrickUpLeft = new Tile(39, "slideTerraceBrickUpLeft", TerrainFeature.SlideRight, Region.UpRightInclusive);
            SlideTerraceBrickUpRight = new Tile(40, "slideTerraceBrickUpRight", TerrainFeature.SlideLeft, Region.UpLeftInclusive);
            TerraceRockUp = new Tile(41, "terraceRockUp");
            StalagmiteCave = new [] {
                new Tile(42, "stalagmiteCave1", TerrainFeature.Stalagmite),
                new Tile(83, "stalagmiteCave2", TerrainFeature.Stalagmite),
                new Tile(84, "stalagmiteCave3", TerrainFeature.Stalagmite),
                new Tile(85, "stalagmiteCave4", TerrainFeature.Stalagmite),
                new Tile(149, "stalagmiteCave8", TerrainFeature.Stalagmite),
                new Tile(150, "stalagmiteCave9", TerrainFeature.Stalagmite),
                new Tile(151, "stalagmiteCave10", TerrainFeature.Stalagmite),
                new Tile(152, "stalagmiteCave11", TerrainFeature.Stalagmite),
            };
            StartCaveDownLeft = new Tile(43, "startCaveDownLeft", TerrainFeature.StartCave);
            PillarTop = new [] {
                new Tile(45, "pillarTop1"),
                new Tile(90, "pillarTop2"),
                new Tile(93, "pillarTop3"),
                new Tile(94, "pillarTop4"),
            };
            PillarShaft = new Tile(46, "pillarShaft");
            PillarBase1 = new Tile(47, "pillarBase1");
            ExitCaveDownLeft = new Tile(49, "exitCaveDownLeft", TerrainFeature.ExitCave);
            SecretDoorTerraceWall = new Tile(50, "secretDoorTerraceWall");
            OpenCave = new [] {
                new Tile(56, "openCave"),
                new Tile(57, "openCave2"),
                new Tile(58, "openCave3"),
                new Tile(59, "openCave4"),
                new Tile(153, "openCave5"),
                new Tile(154, "openCave6"),
                new Tile(155, "openCave7"),
                new Tile(156, "openCave8"),
                new Tile(157, "openCave9"),
                new Tile(158, "openCave10"),
                new Tile(159, "openCave11"),
                new Tile(160, "openCave12"),
                new Tile(161, "openCave13"),
            };
            WallTerraceDown = new [] {
                new Tile(61, "wallTerraceDownRight"),
                new Tile(62, "wallTerraceDown"),
                new Tile(63, "wallTerraceDownLeft"),
            };
            WallTerrace = new Tile(64, "wallTerrace");
            LadderCave = new Tile(65, "ladderCave", TerrainFeature.Ladder);
            SlideTerraceRockLeft = new Tile(66, "slideTerraceRockLeft", TerrainFeature.SlideLeft, Region.DownRightInclusive);
            SlideTerraceRockRight = new Tile(67, "slideTerraceRockRight", TerrainFeature.SlideRight, Region.DownLeftInclusive);
            SlideTerraceRockUpLeft = new Tile(68, "slideTerraceRockUpLeft", TerrainFeature.SlideRight, Region.UpRightInclusive);
            SlideTerraceRockUpRight = new Tile(69, "slideTerraceRockUpRight", TerrainFeature.SlideLeft, Region.UpLeftInclusive);
            SlideRockCaveLeft = new Tile(70, "slideRockCaveLeft", TerrainFeature.SlideLeft, Region.DownRightInclusive);
            SlideRockCaveRight = new Tile(71, "slideRockCaveRight", TerrainFeature.SlideRight, Region.DownLeftInclusive);
            SlideRockCaveUpLeft = new Tile(72, "slideRockCaveUpLeft", TerrainFeature.SlideRight, Region.UpRightInclusive);
            SlideRockCaveUpRight = new Tile(73, "slideRockCaveUpRight", TerrainFeature.SlideLeft, Region.UpLeftInclusive);
            SlideRockBrickLeft = new Tile(74, "slideRockBrickLeft", TerrainFeature.SlideLeft, Region.DownRightInclusive);
            SlideRockBrickRight = new Tile(75, "slideRockBrickRight", TerrainFeature.SlideRight, Region.DownLeftInclusive);
            SlideRockBrickUpLeft = new Tile(76, "slideRockBrickUpLeft", TerrainFeature.SlideRight, Region.UpRightInclusive);
            SlideRockBrickUpRight = new Tile(77, "slideRockBrickUpRight", TerrainFeature.SlideLeft, Region.UpLeftInclusive);
            OpenDoorBrick = new Tile(78, "openDoorBrick", TerrainFeature.OpenDoor);
            StalagmiteBrick = new [] {
                new Tile(79, "stalagmiteBrick1", TerrainFeature.Stalagmite),
                new Tile(80, "stalagmiteBrick2", TerrainFeature.Stalagmite),
                new Tile(81, "stalagmiteBrick3", TerrainFeature.Stalagmite),
                new Tile(82, "stalagmiteBrick4", TerrainFeature.Stalagmite),
                new Tile(138, "stalagmiteBrick5", TerrainFeature.Stalagmite),
                new Tile(139, "stalagmiteBrick6", TerrainFeature.Stalagmite),
                new Tile(140, "stalagmiteBrick7", TerrainFeature.Stalagmite),
                new Tile(141, "stalagmiteCave5", TerrainFeature.Stalagmite),
                new Tile(142, "stalagmiteCave6", TerrainFeature.Stalagmite),
                new Tile(143, "stalagmiteCave7", TerrainFeature.Stalagmite),
            };
            StartCaveUpLeft = new Tile(86, "startCaveUpLeft", TerrainFeature.StartCave);
            StartCaveUpRight = new Tile(87, "startCaveUpRight", TerrainFeature.StartCave);
            StartCaveDownRight = new Tile(88, "startCaveDownRight", TerrainFeature.StartCave);
            PillarTopUpRight1 = new Tile(89, "pillarTopUpRight1");
            PillarTopUpLeft1 = new Tile(91, "pillarTopUpLeft1");
            PillarTopUpRight2 = new Tile(92, "pillarTopUpRight2");
            PillarTopUpLeft2 = new Tile(95, "pillarTopUpLeft2");
            PillarLeft = new Tile(96, "pillarLeft");
            PillarRight = new Tile(97, "pillarRight");
            PillarBase = new [] {
                new Tile(98, "pillarBaseLeft"),
                new Tile(99, "pillarBase2"),
                new Tile(100, "pillarBase3"),
                new Tile(101, "pillarBaseRight"),
            };
            BrokenPillarTop = new [] {
                new Tile(102, "brokenPillarTopLeft"),
                new Tile(103, "brokenPillarTop"),
                new Tile(104, "brokenPillarTopRight"),
            };
            BrokenPillarBottom = new [] {
                new Tile(105, "brokenPillarBottomLeft"),
                new Tile(106, "brokenPillarBottom"),
                new Tile(107, "brokenPillarBottomRight"),
            };
            ExitCaveUpLeft = new Tile(108, "exitCaveUpLeft", TerrainFeature.ExitCave);
            ExitCaveUpRight = new Tile(109, "exitCaveUpRight", TerrainFeature.ExitCave);
            ExitCaveDownRight = new Tile(110, "exitCaveDownRight", TerrainFeature.ExitCave);
            TorchBrick = new [] {
                new Tile(111, "torchBrick1"),
                new Tile(112, "torchBrick2"),
                new Tile(113, "torchBrick3"),
                new Tile(114, "torchBrick4"),
            };
            LoopTileAnimation(TorchBrick);
            BloodyCave = new [] {
                new Tile(115, "bloodyCave1"),
                new Tile(116, "bloodyCave2"),
                new Tile(117, "bloodyCave3"),
                new Tile(118, "bloodyCave4"),
            };
            ProjectingLedgeShadow = new Tile(120, "projectingLedgeShadow");
            ProjectingLedge = new Tile(121, "projectingLedge");
            KeyLedgeCave = new Tile(144, "keyLedgeCave");
            OpenCaveBorder = new [] {
                new Tile(145, "openCaveBorderDownLeft"),
                new Tile(147, "openCaveBorderDown1"),
                new Tile(148, "openCaveBorderDown1"),
                new Tile(146, "openCaveBorderDownRight"),
            };
            SmallFire = new [] {
                new Tile(162, "smallFire1"),
                new Tile(163, "smallFire2"),
                new Tile(164, "smallFire3"),
                new Tile(165, "smallFire4"),
            };
            LoopTileAnimation(SmallFire);
            Fungi1 = new[] {
                new Tile(166, "fungi1a"),
                new Tile(167, "fungi1b"),
                new Tile(168, "fungi1c"),
                new Tile(169, "fungi1d"),
                new Tile(170, "fungi1e"),
                new Tile(171, "fungi1f"),
            };
            LoopTileAnimation(Fungi1);
            Fungi2 = new[] {
                new Tile(172, "fungi2a"),
                new Tile(173, "fungi2b"),
                new Tile(174, "fungi2c"),
                new Tile(175, "fungi2d"),
                new Tile(176, "fungi2e"),
                new Tile(177, "fungi2f"),
            };
            LoopTileAnimation(Fungi2);
            ClosedDoorCave = new Tile(178, "closedDoorCave", TerrainFeature.ClosedDoor);
            LockedDoorCave = new Tile(179, "lockedDoorCave", TerrainFeature.LockedDoor);
            OpenDoorCave = new Tile(180, "openDoorCave", TerrainFeature.OpenDoor);
            PillarCrack1 = new [] {
                new Tile(181, "pillarCrackLeft1"),
                new Tile(182, "pillarCrack1"),
                new Tile(183, "pillarCrackRight1"),
            };
            PillarCrack2 = new [] {
                new Tile(184, "pillarCrack2"),
                new Tile(185, "pillarCrackRight2"),
                new Tile(186, "pillarCrackLeft2"),
            };
            PillarCrack3 = new Tile(187, "pillarCrack3");
            RockBrick = new [] {
                new Tile(188, "rockBrick1"),
                new Tile(189, "rockBrick2"),
            };
            TrashBrick = new [] {
                new Tile(190, "trashBrick1"),
                new Tile(191, "trashBrick2"),
                new Tile(192, "trashBrick3"),
                new Tile(193, "trashBrick4"),
            };
            LoopTileAnimation(TrashBrick);
            RockCave = new[] {
                new Tile(194, "rockCave1"),
                new Tile(195, "rockCave2"),
            };
            TrashCave = new [] {
                new Tile(196, "trashCave1"),
                new Tile(197, "trashCave2"),
                new Tile(198, "trashCave3"),
                new Tile(199, "trashCave4"),
            };
            LoopTileAnimation(TrashCave);
            SpiderCave = new[] {
                new Tile(200, "spiderCaveUpLeft"),
                new Tile(201, "spiderCaveUp"),
                new Tile(202, "spiderCaveUpRight"),
                new Tile(203, "spiderCaveLeft"),
                new Tile(204, "spiderCaveMiddle"),
                new Tile(205, "spiderCaveRight"),
                new Tile(206, "spiderCaveDownLeft"),
                new Tile(207, "spiderCaveDown"),
                new Tile(208, "spiderCaveDownRight"),
            };
            SpikesBrick = new Tile(209, "spikesBrick", TerrainFeature.Spikes);
            SpikesCave = new Tile(210, "spikesCave", TerrainFeature.Spikes);
            ChainBrick = new [] {
                new Tile(211, "chainBrick1"),
                new Tile(212, "chainBrick2"),
            };
            ChainCave = new [] {
                new Tile(213, "chainCave1"),
                new Tile(214, "chainCave2"),
            };
            SlimePoolBrick = new [] {
                new Tile(215, "slimePoolBrick1", TerrainFeature.SlimePool),
                new Tile(216, "slimePoolBrick2", TerrainFeature.SlimePool),
                new Tile(217, "slimePoolBrick3", TerrainFeature.SlimePool),
            };
            LoopTileAnimation(SlimePoolBrick);
            SlimePoolCave = new[] { 
                new Tile(218, "slimePoolCave1", TerrainFeature.SlimePool),
                new Tile(219, "slimePoolCave2", TerrainFeature.SlimePool),
                new Tile(220, "slimePoolCave3", TerrainFeature.SlimePool),
            };
            LoopTileAnimation(SlimePoolCave);
            Ledge1 = new[] { 
                new Tile(230, "ledge1Left"),
                new Tile(231, "ledge1a"),
                new Tile(232, "ledge1b"),
                new Tile(233, "ledge1c"),
                new Tile(234, "ledge1Right"),
            };
            Ledge2 = new [] { 
                new Tile(235, "ledge2Left"),
                new Tile(236, "ledge2a"),
                new Tile(237, "ledge2b"),
                new Tile(238, "ledge2c"),
                new Tile(239, "ledge2Right"),
            };

            // specify passible-region values for those tile-types that don't use the 
            // default
            foreach (var tile in WallRock) tile.passibleRegion = Region.None;
            SlideTerraceBrickLeft.passibleRegion = Region.UpLeftExclusive;
            SlideTerraceBrickRight.passibleRegion = Region.UpRightExclusive;
            ClosedDoorBrick.passibleRegion = Region.None;
            LockedDoorBrick.passibleRegion = Region.None;
            SlideTerraceBrickUpLeft.passibleRegion = Region.DownLeftExclusive;
            SlideTerraceBrickUpRight.passibleRegion = Region.DownRightExclusive;
            foreach (var tile in WallTerraceDown) tile.passibleRegion = Region.None;
            WallTerrace.passibleRegion = Region.None;
            SlideTerraceRockLeft.passibleRegion = Region.UpLeftExclusive;
            SlideTerraceRockRight.passibleRegion = Region.UpRightExclusive;
            SlideTerraceRockUpLeft.passibleRegion = Region.DownLeftExclusive;
            SlideTerraceRockUpRight.passibleRegion = Region.DownRightExclusive;
            SlideRockCaveLeft.passibleRegion = Region.UpLeftExclusive;
            SlideRockCaveRight.passibleRegion = Region.UpRightExclusive;
            SlideRockCaveUpLeft.passibleRegion = Region.DownLeftExclusive;
            SlideRockCaveUpRight.passibleRegion = Region.DownRightExclusive;
            SlideRockBrickLeft.passibleRegion = Region.UpLeftExclusive;
            SlideRockBrickRight.passibleRegion = Region.UpRightExclusive;
            SlideRockBrickUpLeft.passibleRegion = Region.DownLeftExclusive;
            SlideRockBrickUpRight.passibleRegion = Region.DownRightExclusive;
            ProjectingLedge.passibleRegion = Region.Down;
            ClosedDoorCave.passibleRegion = Region.None;
            LockedDoorCave.passibleRegion = Region.None;
            foreach (var tile in RockBrick) tile.passibleRegion = Region.Up;
            foreach (var tile in TrashBrick) tile.passibleRegion = Region.Up;
            foreach (var tile in RockCave) tile.passibleRegion = Region.Up;
            foreach (var tile in TrashCave) tile.passibleRegion = Region.Up;
            foreach (var tile in Ledge1) tile.passibleRegion = Region.Up;

            // specify fall-throughable-region values for those tile-types that don't 
            // use the default
            LadderBrick.fallThroughableRegion = Region.None;
            LadderCave.fallThroughableRegion = Region.None;

            // specify sight-blocking values for those tile-types that don't use the default
            Darkness[0].SightBlocking = true;
            foreach (var tile in WallRock) tile.SightBlocking = true;
            SecretDoorRock.SightBlocking = true;
            ClosedDoorBrick.SightBlocking = true;
            LockedDoorBrick.SightBlocking = true;
            TerraceRockUp.SightBlocking = true;
            SecretDoorTerraceWall.SightBlocking = true;
            foreach (var tile in WallTerraceDown) tile.SightBlocking = true;
            WallTerrace.SightBlocking = true;
            ClosedDoorCave.SightBlocking = true;
            LockedDoorCave.SightBlocking = true;

            // specify animation-duration values for those tile-types that don't use the default
            const int slimePoolDuration = 660;
            foreach (var tile in SlimePoolBrick) tile.AnimationDuration = slimePoolDuration;
            foreach (var tile in SlimePoolCave) tile.AnimationDuration = slimePoolDuration;
        }

        /**
         * Points each tile in the given array to the next one as the next 
         * in its animation sequence, except for the last, which is pointed
         * back at the first.
         */
        private static void LoopTileAnimation(Tile[] tiles)
        {
            for (int i = 0; i < tiles.Length; i++) 
                tiles[i].NextTileInAnimation =
                    tiles[i < tiles.Length - 1 ? i + 1 : 0];
        }
    }
}