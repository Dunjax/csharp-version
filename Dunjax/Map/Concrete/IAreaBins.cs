using Dunjax.Entities;
using Dunjax.Geometry;
using System.Collections.Generic;

namespace Dunjax.Map.Concrete
{
    /**
     * Represents a grid of bins, where each bin covers a rectangular portion of a
     * map.  Entities are placed in the bins to greatly improve the speed of 
     * finding those at a particular map location, since only one bin's entities
     * need to be searched, rather than all of them, leading to an n^2 speedup, 
     * where n is the length in bins of each side of the grid.
     */
    interface IAreaBins<T> where T : IMapEntity
    {
        /**
         * Adds the given entity to those bins representing the areas it intersects.
         */
        void AddToBins(T entity);

        /**
         * Removes the given entity from those bins representing the areas it 
         * intersects.
         */
        void RemoveFromBins(T entity);

        /**
         * Returns the entities found in the bin which cover the given map-location.
         * Note that the actual bin itself is what is returned (for speed), so 
         * its contents should not be directly modified.
         */
        List<T> GetEntitiesFromBin(IPoint location);
        
        /**
         * Informs this area-bins object that the size of its associated map has
         * been set to that given.
         */
        void OnMapSizeSet(ISize size);
    }
}