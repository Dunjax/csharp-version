using Dunjax.Entities.Beings;
using Dunjax.Entities.Beings.Monsters;
using Dunjax.Geometry;
using Dunjax.Entities;
using Dunjax.Entities.Beings.Player;
using Dunjax.Entities.Shots;
using Dunjax.Entities.Items;
using System.Collections.Generic;
using Dunjax.Attributes;
using System;

namespace Dunjax.Map.Concrete
{
    /**
     * This map's terrain is implemented in terms of tiles, a fact which should
     * not be exposed to clients. 
     */
    class Map : ITiledMap
    {
        /**
         * The name of this map (if any), so we may identify it while debugging.
         */
        protected string name;

        /**
         * The dimensions of this map, in pixels.
         */
        private ISize mapSize_;
        public ISize MapSize { 
            get { return mapSize_; } 
            set { mapSize_ = value;  OnMapSizeSet(); } 
        }
        
        /**
         * The rectangular array of tiles that makes up this map.
         */
        protected ITile[,] tilesGrid;

        /**
         * The map-entities that are contained within this map.
         */
        private readonly List<IMapEntity> mapEntities = new List<IMapEntity>();

        /**
         * The beings that are contained within this map.
         */
        protected readonly List<IBeing> beings = new List<IBeing>();

        /**
         * The player (if any) which currently resides in this map.
         */
        public IPlayer Player { get; set; }
       
        /**
         * The monsters that are contained within this map.
         */
        internal readonly List<IMonster> monsters = new List<IMonster>();

        /**
         * The shots that are contained within this map.
         */
        internal readonly List<IShot> shots = new List<IShot>();

        /**
         * The items that are contained within this map.
         */
        internal readonly List<IItem> items = new List<IItem>();

        /**
         * The animated tiles within this map.  
         */
        protected readonly List<AnimatedTileData> animatedTileDatas = new List<AnimatedTileData>();
        
        /**
         * Keeps track of how long it's been since the last time this map
         * checked its tiles for animation changes.  The duration amount should 
         * be a common factor of all the durations for the individual tiles
         * (to prevent those durations from being rendered inaccurate by this
         * one, as they aren't checked for completion until this one completes). 
         */
        protected readonly IDuration animateTilesDuration;
        
        /**
         * The animation-entities that are contained within this map.
         */
        internal readonly List<IAnimationEntity> animationEntities = 
            new List<IAnimationEntity>();

        /**
         * Supplies this map-entity with the current time, as required.
         */
        protected readonly IClock clock;

        /**
         * Groups the beings in this map by which rectangular portions they occupy,
         * for faster retrieval when locating those at a particular location.
         */
        protected IAreaBins<IBeing> beingAreaBins = new AreaBins<IBeing>();

        /**
         * The entity-action-schedule used for this map.
         */
        public IEntityActionSchedule EntityActionSchedule {
            get { return entityActionSchedule; }
        }
        protected readonly IEntityActionSchedule entityActionSchedule;
        
        [StaticImport]
        protected static readonly ISize TileSize = Tile.TileSize;

        [Constructor]
        public Map() {}
        
        [Constructor]
        public Map(string name, ISize size, IClock clock)
        {
            this.name = name;
            MapSize = size;
            this.clock = clock;

            animateTilesDuration = 
                DunjaxFactory.CreateDuration(clock, 40);
            entityActionSchedule = 
                EntitiesFactory.CreateEntityActionSchedule(clock);
        }

        /**
         * Informs this map that it's map-size has changed.
         */
        protected void OnMapSizeSet()
        {
            CreateTilesGrid();
            
            InitializeTilesGrid(Tiles.OpenBrick[0]);
            
            beingAreaBins.OnMapSizeSet(MapSize);
        }
        
        /**
         * Creates a tiles-grid of this map's size in which it may store its terrain. 
         */
        protected void CreateTilesGrid()
        {
            // create a new tiles grid which is large enough to cover this map's size
            var width = (int)Math.Ceiling(MapSize.Width / (float)TileSize.Width);
            var height = (int)Math.Ceiling(MapSize.Height / (float)TileSize.Height);
            tilesGrid = new ITile[width, height];
        }
        
        /**
         * Sets every location in this map's tiles grid to the having the given
         * tile-type.
         */
        protected void InitializeTilesGrid(ITile tileType)
        {
            // put a default tile at each spot in the grid, so that map-employing 
            // tests don't fail due to there being no tiles
            int width = tilesGrid.GetLength(0), height = tilesGrid.GetLength(1);
            for (var i = 0; i < width; i++) {
                for (var j = 0; j < height; j++) tilesGrid[i, j] = tileType;
            }
        }
        
        /**
         * Returns the tile-type at the given location within this map.
         */
        [Optimized]
        [Implementation]
        public ITile GetTile(IPoint location)
        {
            if (!ContainsLocation(location)) return Tiles.Darkness[0];
            
            // it was observed that changing the divisions to shifts did not
            // produce any speedup
            return tilesGrid[location.X / TileSize.Width, location.Y / TileSize.Height];
        }

        /**
         * Returns the tile-type at the given tile-location within this map.
         */
        [Implementation]
        public ITile GetTile(short tileX, short tileY)
        {
            return tilesGrid[tileX, tileY];
        }

        /**
         * Sets the given tile-type into the given location within this map.
         */
        [Implementation]
        public void SetTile(IPoint location, ITile tile)
        {
            tilesGrid[location.X / TileSize.Width, location.Y / TileSize.Height] = tile;
        }

        /**
         * Sets the given tile-type into the given tile-location within this map.
         */
        [Implementation]
        public void SetTile(short tileX, short tileY, ITile tile)
        {
            tilesGrid[tileX, tileY] = tile;
        }

        /**
         * A helper method which returns whether the tile at the given location in this 
         * map is passible.
         */
        protected bool IsTilePassible(IPoint location) 
        {
            return GetTile(location).IsPassible(location);        
        }
        
        [Implementation] 
        public bool IsPassible(IPoint location)
        {
            return IsPassible(location, ImpassableEntitiesSet.NoneSet);
        }

        [Implementation]
        public bool IsPassible(IPoint location, ImpassableEntitiesSet set)
        {
            // if the tile at the given location isn't passible, the location 
    	    // isn't passible
            if (!IsTilePassible(location)) return false;

            // return whether there are entities of the impassible set given
            // (if one was given) at the given location
            IBeing being;
            var impassible =
                (set == ImpassableEntitiesSet.BeingsSet 
                    && (being = GetBeingAt(location)) != null && !being.Passible)
                || (set == ImpassableEntitiesSet.PlayerOnlySet 
                    && Player != null && Player.Bounds.Contains(location));
            return !impassible;
        }
        
        /**
         * Returns whether this map contains the given tile location.
         */
        protected bool ContainsLocation(IPoint location)
        {
    	    return location.X >= 0 && location.Y >= 0	
    	        && location.X < MapSize.Width && location.Y < MapSize.Height;
        }

        [Implementation]
	    public IPlayer GetPlayerAt(IPoint location)
	    {
            if (Player == null) return null;
            return Player.Bounds.Contains(location) ? Player : null;
	    }

        [Implementation]
        public IBeing GetBeingAt(IPoint location)
        {
            return GetMapEntityAt(location, beingAreaBins.GetEntitiesFromBin(location));
        }

        [Implementation]
        public IMonster GetMonsterAt(IPoint location)
        {
            // first, see if GetBeingAt() can supply a correct answer, since it's 
            // much faster, due to its use of the area-bins
    	    var being = GetBeingAt(location);
    	    if (being == null || being is IMonster) return (IMonster)being;
        	
            return GetMapEntityAt(location, monsters);
        }

        /**
         * Returns the entity (out of those found in the given list)
         * which occupies the given location within this map.  If no such entity 
         * is found, null is returned.  Note that a dead being will not be returned.
         */
        private static TE GetMapEntityAt<TE>(
            IPoint location, IList<TE> entities) where TE : class, IMapEntity
        {
    	    // for each entity in the given list
            for (int i = 0; i < entities.Count; i++) {
    	        var entity = entities[i];
        	    
    		    // if this entity contains the given location, and isn't a dead being
                if (entity.Bounds.Contains(location)
                    && (!(entity is IBeing) || !((IBeing)entity).Dead)) {
                    // we've found our result
                    return entity;
    		    }
    	    }
        	
    	    return null;
        }

        [Implementation]
        public IAnimationEntity GetAnimationEntityInBounds(IRectangle bounds)
        {
            return GetMapEntityIn(bounds, animationEntities);
        }

        [Implementation]
        public IItem GetItemInBounds(IRectangle bounds)
        {
            return GetMapEntityIn(bounds, items);
        }

        /**
         * Returns a list of the entities (out of those found in the given list)
         * which intersect the given rectangle.  If no such entities are found,
         * null is returned.
         */
        private static TE GetMapEntityIn<TE>(IRectangle rect, List<TE> entities)
            where TE : class, IMapEntity
        {
    	    // for each map-entity in the given list
            for (int i = 0; i < entities.Count; i++) {
    		    // if this map-entity's bounds intersect the given rectangle,
                // return it as our result
                var entity = entities[i];
                if (entity.Bounds.Intersects(rect)) return entity;
    	    }
        	
    	    return null;
        }

        [Implementation]
        public bool IsFallThroughable(IPoint location)
        {
    	    return GetTile(location).IsFallThroughable(location);
        }

        /**
         * Is reused by IsTileAtLocationVisibleFrom(). This is permissible
         * because only the map-view thread currently calls the method.
         */
        private readonly IPoint isTileAtLocationVisibleFrom_to = 
            GeometryFactory.CreatePoint();

        [Optimized]
        [Implementation]
        public bool IsTileAtLocationVisibleFrom(IPoint location,
            IPoint from, bool beingsBlockSight)
        {
            // if the given location itself is visible from the given
            // from-location, then the tile at that location is visible 
            // (note that we can't optimize this check away, as the checks
            // below don't cover the case where the two given locations 
            // lie on the same row or column of tiles)
            if (IsLocationVisibleFrom(location, from, beingsBlockSight, true))
                return true;
            
            // check whether sight is not blocked to just outside each of 
            // the closest three corners of the tile at the given location
            int leftTileX = location.X - location.X % TileSize.Width;
            int topTileY = location.Y - location.Y % TileSize.Height;
            int rightTileX = leftTileX + TileSize.Width - 1;
            int bottomTileY = topTileY + TileSize.Height - 1;
            IPoint to = isTileAtLocationVisibleFrom_to;
            bool checkUpLeft = !(location.X < from.X && location.Y < from.Y);
            if (checkUpLeft) {
                to.X = (short) (leftTileX - 1);
                to.Y = (short) (topTileY - 1);
                if (IsLocationVisibleFrom(to, from, beingsBlockSight, true)) return true;
            }
            bool checkUpRight = !(location.X > from.X && location.Y < from.Y);
            if (checkUpRight) {
                to.X = (short) (rightTileX + 1);
                to.Y = (short) (topTileY - 1);
                if (IsLocationVisibleFrom(to, from, beingsBlockSight, true)) return true;
            }
            bool checkDownLeft = !(location.X > from.X && location.Y > from.Y);
            if (checkDownLeft) {
                to.X = (short) (leftTileX - 1);
                to.Y = (short) (bottomTileY + 1);
                if (IsLocationVisibleFrom(to, from, beingsBlockSight, true)) return true;
            }
            bool checkDownRight = !(location.X < from.X && location.Y > from.Y);
            if (checkDownRight) {
                to.X = (short) (rightTileX + 1);
                to.Y = (short) (bottomTileY + 1);
                if (IsLocationVisibleFrom(to, from, beingsBlockSight, true)) return true;
            }
            return false;
        }
            
        /**
         * Is reused by IsLocationVisibleFrom(), for speed.  There are two different
         * fields, one for each of the two threads that call the method.
         */
        private readonly IFloatPoint 
            isLocationVisibleFrom_direction = GeometryFactory.CreateFloatPoint(),
            isLocationVisibleFrom_direction_forMapView = GeometryFactory.CreateFloatPoint();
        
        private readonly IFloatPoint 
            isLocationVisibleFrom_exact = GeometryFactory.CreateFloatPoint(),
            isLocationVisibleFrom_exact_forMapView = GeometryFactory.CreateFloatPoint();

        private readonly IPoint 
            isLocationVisibleFrom_current = GeometryFactory.CreatePoint(),
            isLocationVisibleFrom_current_forMapView = GeometryFactory.CreatePoint();

        [Implementation] 
        [Optimized]
        public bool IsLocationVisibleFrom(IPoint location,
		    IPoint from, bool beingsBlockSight, bool forMapView)
	    {
            if (!ContainsLocation(location)) return false;
            
		    // detm the direction of the line between the locations
    	    int pixelsPerStep = TileSize.Width / 2;
            float dx = location.X - from.X, dy = location.Y - from.Y;
            var distance = (float)Math.Sqrt(dx * dx + dy * dy);
            if (distance < 1) return true;
            var direction = forMapView ? 
                isLocationVisibleFrom_direction_forMapView : isLocationVisibleFrom_direction;
            direction.X = dx / distance;
            direction.Y = dy / distance;
		    direction.X *= pixelsPerStep;
            direction.Y *= pixelsPerStep;

		    // detm how many steps it will take to get to the end of the line, 
		    // but skip the last, fractional step (which takes us to the 
		    // to-location, for which we don't care if it blocks LOS); the case 
		    // where there is no such step is handled below
		    var numSteps = (int)(distance / pixelsPerStep);

		    // for each step along the line
            var exact = forMapView ? 
                isLocationVisibleFrom_exact_forMapView : isLocationVisibleFrom_exact;
		    exact.X = from.X;
            exact.Y = from.Y;
            var current = forMapView ? 
                isLocationVisibleFrom_current_forMapView : isLocationVisibleFrom_current;
		    for (var i = 0; i < numSteps; i++) {
			    // detm the location of the next step on the line being tested
                exact.X += direction.X;
                exact.Y += direction.Y;
			    current.X = (short) exact.X;
                current.Y = (short) exact.Y;

			    // this handles the case mentioned above, where there is no last,
			    // fractional step, by explicitly testing if this is the last step 
			    // and whether the given location has been reached
			    if (i == numSteps - 1 
                    && Math.Abs(current.X - location.X) <= 1
                    && Math.Abs(current.Y - location.Y) <= 1)
				    return true;

                // if the tile at this step blocks sight, then the given 
                // location is not visible
                var tileType = tilesGrid[current.X / TileSize.Width, current.Y / TileSize.Height];
                if (tileType.SightBlocking) return false;

                // if beings block sight for this call 
                if (beingsBlockSight) {
                    // if there is a being at this step, then the given 
                    // location is not visible
                    if (GetBeingAt(current) != null) return false;
                }
		    }

		    return true;
	    }

        [Implementation]
        public void AddEntity(IMapEntity entity)
        {
            entity.Map = this;
            
            // add the entity to this map's appropriate list(s) of such entities.
    	    mapEntities.Add(entity);
    	    if (entity is IBeing) {
                beings.Add((IBeing)entity);
    		    beingAreaBins.AddToBins((IBeing)entity);
                if (entity is IMonster) monsters.Add((IMonster)entity);
                if (entity is IPlayer) Player = (IPlayer)entity;
    	    }
            else if (entity is IShot) shots.Add((IShot)entity);
    	    else if (entity is IAnimationEntity) {
                animationEntities.Add((IAnimationEntity)entity);
                if (entity is IItem) items.Add((IItem)entity);
            }
        }

        [Implementation]
        public void RemoveEntity(IMapEntity entity)
        {
            entity.Map = null;
            
            // remove the entity from the lists of this map that were
            // containing it
    	    mapEntities.Remove(entity);
            if (entity is IBeing) {
                beings.Remove((IBeing)entity);
                beingAreaBins.RemoveFromBins((IBeing)entity);
                if (entity is IMonster) monsters.Remove((IMonster)entity);
                if (entity is IPlayer) Player = null;
            }
            else if (entity is IShot) shots.Remove((IShot)entity);
            else if (entity is IAnimationEntity) {
                animationEntities.Remove((IAnimationEntity)entity);
                if (entity is IItem) items.Remove((IItem)entity);
            }
        }

        [Implementation]
        public IPoint GetLocationOfTerrainFeature(TerrainFeature feature)
        {
    	    // map the given terrain-feature to its corresponding tile-type
    	    ITile tile;
            if (feature == TerrainFeature.StartCave) tile = Tiles.StartCaveDownRight;
            else if (feature == TerrainFeature.ExitCave) tile = Tiles.ExitCaveDownLeft;
    	    else return null;
        		
    	    // for each entry in this map's tiles-grid
            for (int i = 0; i < tilesGrid.GetLength(0); i++) {
                for (int j = 0; j < tilesGrid.GetLength(1); j++) {
                    // if the tile-type at this entry is the same as the one given
                    if (tilesGrid[i, j] == tile) {
                        // return the location of this spot
                        return AsMapLocation(i, j);
                    }
                }
            }

            return null;
        }
        
        /**
         * Returns the location of the center of the tile at the given 
         * tile-coordinates within this map.
         */
        protected static IPoint AsMapLocation(int tileX, int tileY)
        {
    	    return GeometryFactory.CreatePoint(
    		    (short) (tileX * TileSize.Width + TileSize.Width / 2), 
    		    (short) (tileY * TileSize.Height + TileSize.Height / 2));
        }

        [Implementation]
	    public TerrainFeature GetTerrainFeatureAt(IPoint location)
	    {
		    // return the terrain-feature of the tile-type that's at the 
            // given location
		    return GetTile(location).GetTerrainFeatureAt(location);
	    }

        [Implementation]
	    public void SetTerrainFeatureAt(IPoint location, TerrainFeature feature)
	    {
		    // if an open door is what's desired, replace the closed door at the
            // given location with the corresponding open door
            if (feature == TerrainFeature.OpenDoor) {
		        SetTile(location, 
		            GetOpenDoorTileTypeToReplaceClosedDoor(GetTile(location)));
                LineOfSightChangeOccurred(this, null);
            }
    		
		    else throw new Exception("Unsupported terrain feature");
	    }

        /**
         * Returns the kind of open door tile-type that corresponds to the given
         * closed door tile-type.
         */
        protected static ITile GetOpenDoorTileTypeToReplaceClosedDoor(ITile closedDoor)
        {
            ITile result = null;
            if (closedDoor == Tiles.ClosedDoorBrick || closedDoor == Tiles.LockedDoorBrick)
                result = Tiles.OpenDoorBrick;
            else if (closedDoor == Tiles.ClosedDoorCave || closedDoor == Tiles.LockedDoorCave)
                result = Tiles.OpenDoorCave;
            return result;
        }
        
        [Implementation]
        public void Act()
        {
            // if it's time to animation this map's tiles, do so
            if (animateTilesDuration.Done) AnimateTiles();
        }
        
        /**
         * Stores data about a tile within this map that changes from one tile-type
         * to another within a sequence of such types.
         */
        protected struct AnimatedTileData
        {
            [NestedTypeParent]
            public Map Parent;

            /**
             * The tile's location with this map.
             */
            public IPoint Location;
            
            /**
             * Keeps track of how long it's been since the last time this tile
             * advanced to the next one in its animation sequence.
             */
            public IDuration DurationUntilNext;

            /**
             * Checks to see whether the tile animation being managed by this 
             * object should advance to the next tile in the animation series.
             */
            public void CheckForAdvance() 
            {
                if (DurationUntilNext.Done) Advance();            
            }
            
            /**
             * Advances the tile animation being managed by this object to the next
             * tile in its animation series.
             */
            public void Advance()
            {
                var next = Parent.GetTile(Location).NextTileInAnimation;
                Parent.SetTile(Location, next);
                DurationUntilNext.Length = next.AnimationDuration;
            }
        }
        
        [Implementation]
        public void CreateTileAnimations()
        {
            // for each tile in this map
            for (int x = 0; x < tilesGrid.GetLength(0); x++) {
                for (int y = 0; y < tilesGrid.GetLength(1); y++) {
                    ITile tile = tilesGrid[x, y];

                    // if this tile's type is part of an animation series, 
                    // create a data structure to manage the animation
                    if (tile.NextTileInAnimation != null) 
                        CreateAnimationDataStorageForTile(x, y, tile);
                }
            }
        }
        
        /**
         * Creates a data structure to manage the animation of a tile (where the
         * given tile-type is part of the animation series to use) at the given
         * tile-location.
         */
        protected void CreateAnimationDataStorageForTile(
            int tileX, int tileY, ITile tileType)
        {
            // create a data structure to keep track of this tile's
            // progression through its animation
            var data = new AnimatedTileData {
                Parent = this,
                Location = AsMapLocation(tileX, tileY),
                DurationUntilNext = 
                    DunjaxFactory.CreateDuration(clock, tileType.AnimationDuration)
            };
            animatedTileDatas.Add(data);
        }
        
        /**
         * Checks each animating tile in this map to see if it should advance to 
         * the next step in its animation.
         */
        protected void AnimateTiles()
        {
            // for each animating tile within this map
            for (int i = 0; i < animatedTileDatas.Count; i++) {
                // check whether this tile's animation should advance
                animatedTileDatas[i].CheckForAdvance();
            }
        }
        
        [Implementation]
        public IPoint GetTileCenterLocation(IPoint location)
        {
            int x = location.X, y = location.Y;
            return GeometryFactory.CreatePoint(
                (short) (x - x % TileSize.Width + TileSize.Width / 2),
                (short) (y - y % TileSize.Height + TileSize.Height / 2));
        }

        [Implementation]
        public void EntityAboutToMove(IMapEntity entity)
        {
            if (entity is IBeing) beingAreaBins.RemoveFromBins((IBeing)entity);
        }

        [Implementation]
        public void EntityMoved(IMapEntity entity)
        {
            if (entity is IBeing) beingAreaBins.AddToBins((IBeing)entity);
        }
        
        /**
         * Returns the y-value of the first location found at or above the one
         * given where that location is passible, and the location just below it 
         * is impassible.
         */
        public short GetYJustAboveGround(IPoint location)
        {
            // keep doing this, starting at the given location
            var test = location;
            short y = location.Y;
            int limit = 4 * TileSize.Height;
            while (y > location.Y - limit)
            {
                // if the current test location is passible, and the location below
                // it by the given height is not passible, then we have our result
                if (IsPassible(test.SetY(y))
                    && !IsPassible(test.SetY((short) (y + 1)))) return y;

                // try the next location upwards next time
                y--;
            }

            throw new Exception("Limit reached.");
        }

        /**
         * Returns the y-value of the first location found at or below the one
         * given where that location is passible, and the location just above it 
         * is impassible.
         */
        public short GetYJustBelowCeiling(IPoint location)
        {
            // keep doing this, starting at the given location
            var test = location;
            short y = location.Y;
            int limit = 4 * TileSize.Height;
            while (y < location.Y + limit)
            {
                // if the current test location is passible, and the location below
                // it by the given height is not passible, then we have our result
                if (IsPassible(test.SetY(y))
                    && !IsPassible(test.SetY((short) (y - 1)))) return y;

                // try the next location downwards next time
                y++;
            }

            throw new Exception("Limit reached.");
        }

        [Implementation]
        public event EventHandler LineOfSightChangeOccurred;
    }
}