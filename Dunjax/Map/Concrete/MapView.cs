using System;
using Dunjax.Attributes;
using Dunjax.Entities;
using Dunjax.Geometry;
using Dunjax.Gui;

namespace Dunjax.Map.Concrete
{
    class MapView : IMapView
    {
        /**
         * The map this view is to draw.
         */
        public IMap Map
        {
            get { return map_; }
            set {
                map_ = value;
                map_.LineOfSightChangeOccurred += MapLineOfSightChangeOccurred;
            }
        }
        private IMap map_;
        
        /**
         * The area on the map this view is currently displaying.
         */
        protected IRectangle boundsOnMap = GeometryFactory.CreateRectangle();
        
        /**
         * The size of this view, in pixels.
         */
        public ISize Size
        {
            get {return boundsOnMap.Size;}
            set
            {
                boundsOnMap.Size = value;
                OnSizeSet(value);
            }
        }
        
        /**
         * The tile dimensions of the view image, with partial tiles rounded up 
         * to full tiles.
         */
        protected ISize imageTileSize = GeometryFactory.CreateSize();
        
        /**
         * Keeps track of which tiles can't be seen from the player's location.
         */
        protected bool[,] tileBlocked;

        /**
         * Whether instances of this class should block out tiles which are
         * blocked from the player's sight.
         */
        public static bool UseLOSBlocking = true;
        
        /**
         * The location within the map of the top-left-most tiles that will be 
         * drawn into the view image.  
         */
        protected IPoint topLeftTileLocation = GeometryFactory.CreatePoint();
        
        /**
         * The location within the view image of the top-left-most tiles that will
         * be drawn into the view image.
         */
        protected IPoint topLeftTileImageLocation = GeometryFactory.CreatePoint();
        
        /**
         * The drawing area into which the drawing methods of this view
         * are supposed to draw.
         */
        protected IDrawingArea DrawingArea;
        
        /**
         * Is used by draw() to check if the view's center has changed tile
         * location across calls.
         */
        private readonly IPoint oldTopLeftTileLocation = GeometryFactory.CreatePoint(-1, -1);

        [StaticImport]
        protected static readonly ISize TileSize = Tile.TileSize;

        [Implementation]
        public bool LabelEntities { get; set; }

        [Implementation]
        public void Draw(IDrawingArea intoDrawingArea)
        {
            // if this view hasn't yet been sized, it should not be drawn 
            if (boundsOnMap.Width == 0) return;

            DrawingArea = intoDrawingArea;
            
            // we can't draw this map until it has a player to center this view on
            if (Map.Player == null) return;
            
            // center this view on the player
            boundsOnMap.Center = Map.Player.Bounds.Center;

            FindLeftmostTileX();
            FindTopmostTileY();
            
            // if the center of this view has changed tile locations since the 
            // last time this was called
            if (!topLeftTileLocation.Equals(oldTopLeftTileLocation)) {
                // redetermine the visibility of the tiles in this view
                oldTopLeftTileLocation.Set(topLeftTileLocation);
                DetermineTilesVisibility();
            }
            
            DrawVisibleTiles();
            
            // we don't use iterators below, as there could be concurrent 
            // modification due to the game-model thread adding or removing
            // an entity from a list; we also get the list size with each 
            // iteration, for the same reason
            
            var map = (Map)Map;
            var monsters = map.monsters;
            for (int i = 0; i < monsters.Count; i++) DrawEntity(monsters[i]);

            var entities = map.animationEntities;
            for (int i = 0; i < entities.Count; i++) DrawEntity(entities[i]);

            DrawEntity(Map.Player);

            var shots = map.shots;
            for (int i = 0; i < shots.Count; i++) DrawEntity(shots[i]);
            
            DrawSightBlockedTiles();
        }
        
        /**
         * Determines (and stores) the leftmost x-position of the tiles that will be drawn,
         * in both this map and in the view-image.
         */
        protected void FindLeftmostTileX()
        {
            // find the first tile boundary to the left of the view-image's center, 
            // then move to the left one tile until the left edge of the view-image 
            // has been passed
            int imageCenterX = boundsOnMap.Center.X;
            int leftDistanceToTileBoundary = imageCenterX % TileSize.Width;
            topLeftTileLocation.X = (short) (imageCenterX - leftDistanceToTileBoundary);
            topLeftTileImageLocation.X =  
                (short) (boundsOnMap.Width / 2 - leftDistanceToTileBoundary);
            while (topLeftTileImageLocation.X > 0) {
                topLeftTileLocation.Add((short) (-TileSize.Width), 0);
                topLeftTileImageLocation.Add((short) (-TileSize.Width), 0);
            }
        }
        
        /**
         * Determines (and stores) the topmost y-position of the tiles that will be
         * drawn, in both this map and in the view-image.
         */
        protected void FindTopmostTileY()
        {
            // find the first tile boundary above the image's center, then move
            // upward one tile until the top edge of the image has been passed
            int imageCenterY = boundsOnMap.Center.Y;
            topLeftTileLocation.Y = (short) (imageCenterY - imageCenterY % TileSize.Height);
            topLeftTileImageLocation.Y =
                (short) (boundsOnMap.Height / 2 - imageCenterY % TileSize.Height);
            while (topLeftTileImageLocation.Y > 0) {
                topLeftTileLocation.Add(0, (short) (-TileSize.Height));
                topLeftTileImageLocation.Add(0, (short) (-TileSize.Height));
            }
        }

        /**
         * Draws those tiles within the view-image that are visible from 
         * the view-image's center.
         */
        protected void DetermineTilesVisibility()
        {
            if (!UseLOSBlocking) return;

            // for each tile-spot in the map that's within the bounds of this view
            var tileLocation = GeometryFactory.CreatePoint();
            var tiledMap = (ITiledMap)Map;
            var sightBlockingFrom = tiledMap.GetTileCenterLocation(boundsOnMap.Center);
            tileLocation.X = (short) (topLeftTileLocation.X + TileSize.Width / 2);
            for (int i = 0; i < imageTileSize.Width; i++, tileLocation.X += TileSize.Width) {
                tileLocation.Y = (short) (topLeftTileLocation.Y + TileSize.Height / 2);
                for (int j = 0; j < imageTileSize.Height; j++, tileLocation.Y += TileSize.Height) {
                    // determine whether this tile-spot is blocked from sight
                    // of the center this view
                    tileBlocked[i, j] =
                        !tiledMap.IsTileAtLocationVisibleFrom(
                            tileLocation, sightBlockingFrom, false);
                }
            }
        }

        /**
         * Draws those tiles within the view-image that are visible from 
         * the view-image's center.
         */
        protected void DrawVisibleTiles()
        {
            // for each tile-spot in the map that's within the bounds of this view
            IPoint tileLocation = GeometryFactory.CreatePoint();
            tileLocation.X = (short) (topLeftTileLocation.X + TileSize.Width / 2);
            for (int i = 0, imageX = topLeftTileImageLocation.X;
                i < imageTileSize.Width; 
                i++, tileLocation.X += TileSize.Width, imageX += TileSize.Width) {
                tileLocation.Y = (short) (topLeftTileLocation.Y + TileSize.Height / 2);
                for (int j = 0, imageY = topLeftTileImageLocation.Y;
                    j < imageTileSize.Height; 
                    j++, tileLocation.Y += TileSize.Height, imageY += TileSize.Height) {
                    // if this tile-spot is currently visible
                    if (!tileBlocked[i, j]) {
                        // draw the image of this tile-spot's tile
                        DrawingArea.DrawImage(
                            (Map as ITiledMap).GetTile(tileLocation).Image, imageX, imageY);
                    }
                }
            }
        }
        
        /**
         * If deemed appropriate, draws the given entity at its location within 
         * the view. 
         * 
         * @returns  Whether the entity was actually drawn.
         */
        protected void DrawEntity(IMapEntity entity)
        {
            // if the entity lies completely outside this view, don't draw it
            if (!entity.Bounds.Intersects(boundsOnMap)) return;
            
            // if the tiles at this entity's four corners are all sight-blocked,
            // don't draw it
            if (IsCompletelySightBlocked(entity.Bounds)) return;
                
            // if the entity has an image
            var drawX = entity.Bounds.LeftX - boundsOnMap.LeftX;
            var drawY = entity.Bounds.TopY - boundsOnMap.TopY;
            var image = entity.Image;
            if (image != null) {
                // draw the entity's image
                DrawingArea.DrawImage(image, drawX, drawY);
            }
            
            // if this view is supposed to draw labels on entities
            if (LabelEntities) {
                // label the entity with its ID
                DrawingArea.DrawText("" + entity.Id, drawX, drawY, Color.Yellow);
            }
        }
        
        /**
         * Returns whether all of the given map-rectangle is currently sight-blocked
         * within this view.  It is presumed that the given rectangle lies completely
         * within the map.
         */
        protected bool IsCompletelySightBlocked(IRectangle rect)
        {
            // return whether the tiles at this entity's four corners are all sight-blocked
            return IsTileSightBlocked(rect.TopLeft) 
                && IsTileSightBlocked(rect.TopRight)
                && IsTileSightBlocked(rect.BottomLeft)
                && IsTileSightBlocked(rect.BottomRight);
        }
        
        /**
         * Returns whether the tile at the given location is currently considered
         * to be sight-blocked by this view.
         */
        protected bool IsTileSightBlocked(IPoint location)
        {
            // if the location is outside the view, the tile is not blocked
            if (!boundsOnMap.Contains(location)) return false;

            // return whether the tile at the given location is currently sight-blocked
            int tileX = (location.X - topLeftTileLocation.X) / TileSize.Width;
            int tileY = (location.Y - topLeftTileLocation.Y) / TileSize.Height;
            return tileBlocked[tileX, tileY];
        }
        
        /**
         * Blacks out those tiles within the view-image that are not visible from 
         * the player's position.
         */
        protected void DrawSightBlockedTiles()
        {
            // for each tile-spot within this map that's within the bounds of 
            // the view image
            for (int i = 0, imageX = topLeftTileImageLocation.X;
                i < imageTileSize.Width; 
                i++, imageX += TileSize.Width) {
                for (int j = 0, imageY = topLeftTileImageLocation.Y;
                    j < imageTileSize.Height; 
                    j++, imageY += TileSize.Height) {
                    // if this tile-spot was marked as sight-blocked, above
                    if (tileBlocked[i, j]) {
                        // fill the rectangle with black where this tile would be
                        // (this is quicker than drawing the darkness tile there -
                        // while both are accelerated, the fill doesn't require
                        // the reading from video memory)
                        DrawingArea.FillRect(
                            imageX, imageY, TileSize.Width, TileSize.Height, Color.Black);
                    }
                }
            }
        }

        /**
         * Adjusts this view's data storage to accomodate it's new, given size.
         */
        private void OnSizeSet(ISize size)
        {
            // determine the new tile-dimensions of this view.
            imageTileSize.Set( 
                (short) (Math.Ceiling(size.Width / (float)TileSize.Width) + 1),
                (short) (Math.Ceiling(size.Height / (float)TileSize.Height) + 1));

            // reallocate the sight-blocking data array to the new tile-size of this view
            tileBlocked = new bool[imageTileSize.Width, imageTileSize.Height];
        }

        [Handler]
        private void MapLineOfSightChangeOccurred(object sender, EventArgs args)
        {
            DetermineTilesVisibility();
        }
    }
}