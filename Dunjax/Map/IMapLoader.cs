using System;

namespace Dunjax.Map
{
    /**
     * Loads maps from data files on disk.
     */
    public interface IMapLoader
    {
        /**
         * Loads and returns a map constructed of data from the file of the 
         * given name.
         */
        IMap Load(string fileName);

        /**
         * Fires when this loader adds a master-monster to the map it's loading.
         */
        event EventHandler MasterCreated;
    }
}