﻿namespace Dunjax
{
    /**
     * Manages an expanding pool of objects which may be reused 
     * to prevent unnecessary creation and garbage collection.
     */
    public interface IObjectPool<T>
    {
        /**
         * Returns an instance from this pool.  If the pool is empty,
         * creates a returns a new instance.
         */
        T Retrieve();

        /**
         * Returns the given instance to this pool, to be reused later.
         */
        void Reclaim(T instance);
    }
}
