namespace Dunjax
{
    /**
     * Reports a current time relative to its inception.  
     * Has no way to update its current value.
     * To be used by objects which shouldn't have the ability to
     * cause this clock to update its time value.
     */
    public interface IClock
    {
        /**
         * This clock's current value for the length of time in ms since its inception. 
         */
        float Time { get; }
    }

    /**
     * Updates its current time value when asked to do so.
     */
    public interface IUpdateableClock : IClock
    {
        /**
         * Starts this clock running with a time value of zero.
         */
        void Start();

        /**
         * Has this clock calculate its current time.
         */
        void Update();

        /**
         * This clock's current value for the length of time in ms since its inception. 
         */
        new float Time { get; set; }

        /**
         * Holds this clock to its current time measurement value.
         */
        void Stop();

        /**
         * Resumes this clock's progression of its time measurement value
         * after a call to Stop().
         */
        void Resume();
    }
}