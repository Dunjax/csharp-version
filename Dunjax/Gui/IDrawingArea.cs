using Dunjax.Images;

namespace Dunjax.Gui
{
    /**
     * Abstracts drawing operations on a drawing area.
     */
    public interface IDrawingArea
    {
        void DrawImage(IImage image, int x, int y);

        void FillRect(int x, int y, int width, int height, Color color);

        void DrawText(string text, int x, int y, Color color);
    }

    /**
     * Platform-independent colors to use with the above 
     * IDrawingArea methods.
     */
    public enum Color { Black, White, Yellow };
}