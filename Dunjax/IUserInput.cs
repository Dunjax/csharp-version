namespace Dunjax
{
    /**
     * Abstracts user input from multiple input devices.
     */
    public interface IUserInput
    {
        /**
         * Returns whether the given command is currently being given by the user.
         */
        bool IsSignaled(Command command);
    }

    /**
     * The different commands the user may input.
     */
    public enum Command { Left, Right, Up, Down, Swing, SwitchGuns, Fire, Jump };
}