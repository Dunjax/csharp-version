﻿using Dunjax.Map.Concrete;

namespace Dunjax
{
    /**
     * Sets static values in other classes which allow for easier testing.
     */
    public class DebugSettings
    {
        static DebugSettings()
        {
            //MapView.UseLOSBlocking = false;
            //MapLoader.EntityIdToLoad = 132;
            //Game.Concrete.Game.startingLevelNumber = 2;
        }
    }
}
