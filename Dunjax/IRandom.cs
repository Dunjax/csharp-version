namespace Dunjax
{
    public interface IRandom
    {
        int NextInt(int maxValue);

        bool NextBoolean();
    }
}