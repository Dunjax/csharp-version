namespace Dunjax
{
    /**
     * Keeps track of whether a specified amount of time has elapsed.
     */
    public interface IDuration
    {
        /**
         * Whether this duration has completed.  Restarts this duration after being read.
         */
        bool Done {get;}

        /**
         * Whether this duration has completed.  Does not restart this duration after being read.
         */
        bool DoneOnly {get;}

        /**
         * If this duration represents a period, advances this duration's starting time
         * to the beginning of the next period.  Otherwise, uses the current time as 
         * the start time.
         */
        void Start();

        /**
         * Whether this duration is at its exact start.
         */
        bool AtStart {get;}

        /**
         * The length of this duration, in milliseconds.
         */
        float Length {get; set;}

        /**
         * The length of this duration that is left, in milliseconds.
         */
        float LengthLeft {get;}

        /**
         * Makes this duration done. 
         */
        void MakeDone();
    }
}