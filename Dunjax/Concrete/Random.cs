﻿using Random_ = System.Random;

namespace Dunjax.Concrete
{
    public class Random : IRandom
    {
        private readonly Random_ random = new Random_();

        public int NextInt(int maxValue)
        {
            return random.Next(maxValue);
        }

        public bool NextBoolean()
        {
            return random.Next() % 2 == 1;
        }
    }
}