﻿using System;
using Dunjax.Attributes;

namespace Dunjax.Concrete
{
    class FallDuration : Duration, IFallDuration
    {
        /**
         * The time when the current fall-being-managed started.
         */
        protected float currentFallStartTime;

        /**
	     * The initial time (in ms) that must elapse between downward moves 
         * due to a fall. The actual delay value will decrease from this one
	     * as the fall continues, picking up speed.
	     */
        protected const int StartingDelayBetweenFallMoves = 64;

        /**
	     * The minimum time (in ms) that must elapse between downward moves due to a
	     * fall, intended to simulate terminal velocity. The actual delay value 
         * will decrease to this one as the fall continues, picking up speed.
	     */
        protected const int MinDelayBetweenFallMoves = 2;

        [Implementation, Property]
        public float SlowdownFactor { set { slowdownFactor = value; }}
        private float slowdownFactor = 1;

        [Constructor]
        public FallDuration(IClock clock)
            : base(clock)
        {
        }

        [Implementation]
        public void FallStarted()
        {
            currentFallStartTime = clock.Time;
        }

        [HookOverride]
        protected override void AfterStart()
        {
            // calculate how long it should be before the next fall-move
            // takes place, according to how much time has elapsed
            // during the current fall
            const float accelerationInPixelsPerMilliSecond = .55f;
            float timeFalling = Math.Max(clock.Time - currentFallStartTime, 1);
            float fallSpeed = 
                accelerationInPixelsPerMilliSecond * timeFalling / slowdownFactor;
            float durationLength = (int)(1000 / fallSpeed);
            durationLength = Math.Max(durationLength, MinDelayBetweenFallMoves);
            durationLength = Math.Min(durationLength, StartingDelayBetweenFallMoves);
            Length = durationLength;
        }
    }
}
