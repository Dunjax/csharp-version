package dunjax.concrete;

import dunjax.game.GameFactory;
import dunjax.game.IGame;
import dunjax.gui.GuiFactory;
import dunjax.gui.IPlayFrame;

public class Dunjax
{
    /**
     * The program's starting point.
     */
    public static final void main(String[] args)
    {
        IPlayFrame playFrame = GuiFactory.createPlayFrame();
        IGame game = GameFactory.createGame();
        playFrame.setGame(game);
            
        game.startOnFirstLevel();
        game.run();

        System.exit(0);
    }
}
