﻿using System.Collections.Generic;
using System.Threading;
using Dunjax.Attributes;

namespace Dunjax.Concrete
{
    /**
     * Implements a thread-safe IObjectPool by creating a separate pool
     * for each calling thread.
     */
    public class ObjectPool<T, U> : IObjectPool<T>
        where U : T, new()
    {
        /**
         * The pools of instances available for reuse, with one pool
         * per client thread.
         */
        private readonly Dictionary<Thread, List<U>> poolsByThread 
            = new Dictionary<Thread, List<U>>();

        [Implementation]
        public T Retrieve()
        {
            // if the pool isn't empty
            var pool = GetPoolForCurrentThread();
            if (pool.Count > 0) {
                // remove and return the first instance
                // in the pool
                var instance = pool[0];
                pool.RemoveAt(0);
                return instance;
            }

            // otherwise, create and return a new instance
            return new U();
        }

        /**
         * Returns the individual object-pool associated with
         * the calling thread.
         */
        private List<U> GetPoolForCurrentThread()
        {
            // if we don't have a pool for the current thread
            List<U> pool;
            if (!poolsByThread.TryGetValue(Thread.CurrentThread, out pool)) {
                // create one
                pool = new List<U>();
                poolsByThread[Thread.CurrentThread] = pool;
            }

            return pool;
        }

        [Implementation]
        public void Reclaim(T instance)
        {
            var pool = GetPoolForCurrentThread();
            pool.Add((U)instance);
        }
    }
}
