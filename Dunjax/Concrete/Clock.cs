using System.Diagnostics;
using Dunjax.Attributes;

namespace Dunjax.Concrete
{
    public class Clock : IUpdateableClock
    {
        /**
         * Provides high-resolution timing services, when they are available.
         */
        private Stopwatch stopwatch;

        [Implementation]
        public void Start()
        {
            stopwatch = Stopwatch.StartNew();
            Time = 0;
        }

        [Implementation]
        public float Time { get; set; }

        [Implementation]
        public void Update()
        {
            Time = stopwatch.ElapsedMilliseconds;
        }

        [Implementation]
        public void Stop()
        {
            stopwatch.Stop();
        }

        [Implementation]
        public void Resume()
        {
            stopwatch.Start();
        }
    }
}