using Dunjax.Attributes;

namespace Dunjax.Concrete
{
    public class Duration : IDuration
    {
        /**
         * The length of this duration, in ms. 
         */
        public float Length {get; set;}
    	
        /**
         * The time (in ms) when this duration started.
         * 
         * We set this to a value far back in the past, such that if this duration's 
         * start() method is not called, IsDone() will return true.  This is necessary 
         * since, in such a case, this duration is likely being used as a period between 
         * executions of the same, repetitive action, and we normally want the first
         * such execution to happen right away. 
         */
        protected float startTime = float.MinValue;
    	
        /**
         * Whether this duration is being used to keep a repetitive event occurring
         * at a regular interval.  In such a case, the period thus represented should 
         * be measured from the last period's end, rather than the current game time
         * when this duration is to be restarted.  The latter approach would make 
         * the periods uneven.  
         */
        protected bool representsPeriod = true;
        
        /**
         * Supplies this duration with the current time, as required.
         */
        protected readonly IClock clock;

        [Constructor]
        public Duration(
            IClock clock, float length = 0, bool representsPeriod = true)
        {
            this.clock = clock;
            Length = length;
            this.representsPeriod = representsPeriod;
        } 
        
        [Implementation] 
        public bool Done {get {return IsDone(true);} }

        [Implementation]
        public bool DoneOnly { get { return IsDone(false); } }

	    private bool IsDone(bool restartIfDone)
	    {
            bool done = clock.Time - startTime >= Length;
            if (done && restartIfDone) Start();
            return done;
	    }

        [Implementation]
        public void Start()
        {
            // if this call is an advance of this duration to a next successive period, and
            // the periods haven't gotten too far off track from the current game time, 
            // perform the advancing
            if (representsPeriod && startTime + Length >= clock.Time - Length) 
                startTime += Length;
            
            // otherwise, use the current game time as the starting time
            else startTime = clock.Time;

            AfterStart();
        }

        [Hook]
        protected virtual void AfterStart() {}

        [Implementation]
        public float LengthLeft {
            get {return startTime + Length - clock.Time;}
        }

        [Implementation]
        public void MakeDone()
        {
            startTime = clock.Time - Length;
        }

        [Implementation]
        public bool AtStart {
            get {return startTime == clock.Time;}
        }
    }
}
