using Dunjax.Animations.Concrete;
using Dunjax.Attributes;
using Dunjax.ExtensionMethods;
using Dunjax.Images;

namespace Dunjax.Animations
{
    class AnimationFactory
    {
        [ObjectPool]
        private static readonly IObjectPool<IAnimation> AnimationPool =
            DunjaxFactory.CreateObjectPool<IAnimation, Animation>();

        [Factory]
        public static IAnimation CreateAnimation(IClock clock)
        {
            var animation = AnimationPool.Retrieve();
            ((Animation)animation).Clock = clock;
            return animation;
        }

        [Reclaimer]
        public static void ReclaimAnimation(IAnimation animation)
        {
            ((Animation)animation).Reset();
            AnimationPool.Reclaim(animation);
        }

        [Factory]
        public static IFrameSequence CreateFrameSequence(
            string path, string prefix, string name, bool repeating = false, 
            FramePriority priority = FramePriority.Normal,
            bool makeMirror = true, int[] repeatedFrames = null, 
            IImage[] images = null)
        {
            string fullName = null;
            if (name != null)
                fullName = path + prefix +
                    (prefix.Length > 0 ? name.MakeFirstLetterUppercase() : name);

            return new FrameSequence(
                fullName, repeating, priority, makeMirror, false, 
                repeatedFrames, images);
        }
    }
}