using Dunjax.Attributes;
using Dunjax.Images;

namespace Dunjax.Animations.Concrete
{
    class Frame : IFrame
    {
        [Implementation]
        public IImage Image
        {
            get {return image_;}
        }
        private readonly IImage image_;

        [Implementation]
        public int Duration { get; set; }

        private const int DefaultFrameDuration = 80;

        [Constructor]
        public Frame(IImage image)
        {
            image_ = image;
            Duration = DefaultFrameDuration;
        }
    }
}