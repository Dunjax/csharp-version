using Dunjax.Attributes;
using Dunjax.Images;

namespace Dunjax.Animations.Concrete
{
    class Animation : IAnimation
    {
        [Property, Implementation]
        public IFrameSequence FrameSequence { get; private set; }

        [Property, Implementation]
        public int CurrentFrameIndex { get; set; }

        [Property, Implementation]
        public bool Done { get; private set; }

        [Property, Implementation]
        public bool RemoveWhenDone { get; set; }
        
        /**
         * The system time (in ms) when the current frame of this animation
         * became current.
         */
        protected float frameStartTime;

        /**
         * Supplies this animation with the current time, as required.
         */
        public IClock Clock {
            set { clock = value; }
        }
        protected IClock clock;

        [Property, Implementation]
        public IImage CurrentImage
        {
            get {
                return FrameSequence != null ? FrameSequence[CurrentFrameIndex].Image : null;
            }
        }

        [Constructor]
        public Animation() {}

        [OverriddenValue]
        public override string ToString()
        {
            // provide a meaningful self-description for when debugging
            return "Animation: " + FrameSequence.ImagePrefix;
        }

        [Implementation]
        public bool SetFrameSequence(IFrameSequence sequence)
        {
    	    // if the given sequence is already currently in progress, do nothing
    	    // so it may finish
            if (!Done && FrameSequence == sequence) return false;

            // if the current sequence is still in progress, and has a higher
            // priority than that of the one given, ignore this request
            if (!Done && sequence != null && FrameSequence != null 
                && sequence.Priority < FrameSequence.Priority)
                return false;
            
            // position this animation at the start of the given sequence
            FrameSequence = sequence;
            Done = false;
            CurrentFrameIndex = 0;
            frameStartTime = clock.Time;
            
            return true;
        }

        [Implementation]
        public bool CheckForAdvance()
        {
            if (Done || FrameSequence == null) return false;

            // if the current frame's duration is complete
            bool advanced = false;
            if (IsTimeToAdvanceToNextFrame()) {
                // if there are more frames to play, advance to the next one
                if (HasMoreFramesToPlay()) {AdvanceToNextFrame(); advanced = true;}

                else advanced = PlayedLastFrame();
            }

            return advanced;
        }
        
        /**
         * Returns whether this animation's current frame has played for as
         * long as its intended duration.
         */
        protected bool IsTimeToAdvanceToNextFrame()
        {
            var frame = FrameSequence[CurrentFrameIndex];
            return clock.Time - frameStartTime >= frame.Duration;
        }
        
        /**
         * Returns whether there are frames that have yet to be played in this
         * animation's current frame sequence.
         */
        protected bool HasMoreFramesToPlay()
        {
            return CurrentFrameIndex < FrameSequence.NumFrames - 1;        
        }
        
        /**
         * Advances this animation to the next frame in its current frame-sequence.
         */
        protected void AdvanceToNextFrame()
        {
            CurrentFrameIndex++;
            frameStartTime = clock.Time;
        }
        
        /**
         * Informs this animation that it has finished playing the last frame in 
         * its current frame sequence.
         */
        protected bool PlayedLastFrame()
        {
            // if the sequence repeats, restart it
            bool advanced = false;
            if (FrameSequence.Repeating) {Restart(); advanced = true;}

            // otherwise, the sequence is done
            else Done = true;
            
            return advanced;
        }
        
        [Implementation]
        public void Restart()
        {
            CurrentFrameIndex = 0;
            frameStartTime = clock.Time;
            Done = false;
        }

        [Property] 
        public int CurrentFrameDuration
        {
            get { return FrameSequence[CurrentFrameIndex].Duration; }
        }

        /**
         * Resets this animation's state to be as if it were a newly-created
         * instance.
         */
        public void Reset()
        {
            FrameSequence = null;
            CurrentFrameIndex = 0;
            Done = false;
            RemoveWhenDone = false;
        }
    }
}