using System;
using System.Collections;
using System.Collections.Generic;
using Dunjax.Attributes;
using Dunjax.Images;

namespace Dunjax.Animations.Concrete
{
    class FrameSequence : IFrameSequence
    {
        /**
         * The filename which should start of the name of each of the
         * frames in this sequence.
         */
        [Property]
        public string ImagePrefix
        {
            get { return imagePrefix_; }
        }
        private readonly string imagePrefix_;

        /**
         * The sequence of frames this object encapsulates.
         */
        [Property]
        public IFrame this[int index] { get { return frames[index]; } }
        private readonly Frame[] frames;

        [Property]
        public int NumFrames { get {return frames.Length;} }

        /**
         * Whether this frame sequence should start over at the beginning once it 
         * has completed.
         */
        [Property]
        public bool Repeating { get { return repeating_; } }
        private readonly bool repeating_;

        /**
         * This sequence's priority for display.  Once set into an 
         * animation, this sequence may not be replaced by one with a lesser
         * priority until it finishes.   
         */
        [Property]
        public FramePriority Priority { get { return priority_; } }
        private readonly FramePriority priority_;

        /**
         * A sequence (if this sequence has one) that has the same frames as this
         * one, only with the images flipped x-wise.
         */
        [Property]
        public IFrameSequence Mirror { get { return mirror_; } }
        private readonly IFrameSequence mirror_; 
        
        /**
         * @param imagePrefix
         *            See field.
         * @param repeating
         *            See field.
         * @param priority
         *            See field.
         * @param makeMirror
         *            Whether a mirror of this sequence should also be created, and
         *            stored as this sequence's mirror.
         * @param isMirror
         *            Whether this sequence is a mirror of another, meaning that its
         *            images should be flipped x-wise.
         * @param repeatedFrames
         *            If provided, each non-negative entry n in the ith position in
         *            this array means to reuse the image of frame n for the ith
         *            frame in this sequence.
         * @param images
         *            If provided, is an array of images to use for this sequence's
         *            frames, rather than trying to load them from disk. This allows
         *            sharing of images across sequences.
         */
        [Constructor]
        public FrameSequence(string imagePrefix, bool repeating, 
            FramePriority priority, bool makeMirror, 
            bool isMirror, int[] repeatedFrames, IImage[] images)
        {
            imagePrefix_ = imagePrefix;
            repeating_ = repeating;
            priority_ = priority;

            // keep doing this to create a list of frames for this sequence
    	    var framesList = new List<Frame>();
    	    int frameIndex = 0;
    	    while (true) {
    	        // get the image for this next frame; if there is none, then
                // if we aren't out of given images to process, we are
    	        // done creating frames for this sequence
    	        var image = 
    	            GetFrameImage(frameIndex, repeatedFrames, images, framesList);
    	        if (image == null 
                    && (images == null || frameIndex >= images.Length - 1)) break;
        		
    		    // if this sequence is a mirror of another, and there is an 
                // image for this frame, then flip the image x-wise
    		    if (isMirror && image != null) image = image.FlipXWise();

    		    // create the next frame for this sequence using the image just loaded
                var frame = new Frame(image);
                framesList.Add(frame);

                frameIndex++;
            }

            // if no frames were added to this sequence above, it's an error
            if (frameIndex == 0) throw new Exception(
                "No frames found for sequence with prefix " + imagePrefix);
            
            // create this object's frames array from the list of frames 
    	    // formed above
    	    frames = framesList.ToArray();
            
            // if we are also to create a mirror of this sequence, do so,
    	    // and store it as this sequence's mirror
            if (makeMirror) mirror_ = 
                new FrameSequence(imagePrefix, repeating, priority,
                    false, true, repeatedFrames, images);
        }

        /**
         * Returns the image for the frame of the given index, culled from one
         * of three sources:
         * 
         *  1) the given set of images, when such a set is given
         *  2) an earlier frame (from the given list of frames), 
         *     when repeated-frames data is given, and the value of the given
         *     index in that data is non-negative
         *  3) from disk, whether the previous two conditions don't apply 
         *  
         *  Null is returned when the given frame-index is beyond the end
         *  of this sequence.
         */
        protected IImage GetFrameImage(int frameIndex, int[] repeatedFrames, 
            IImage[] images, List<Frame> fromFrames)
        {
            // if we were given a set of images to use, and there is an entry
            // in that set for the frame of the given index
            IImage image;
            if (images != null && frameIndex < images.Length) {
                // use the image from that set
                image = images[frameIndex];
            }
            
            // else, if repeated-frames data was given, and there is an entry
            // in that data for the frame of the given index
            else if (repeatedFrames != null 
                && frameIndex < repeatedFrames.Length 
                && repeatedFrames[frameIndex] >= 0) {
                // use the image from the repeated frame for this frame
                image = fromFrames[repeatedFrames[frameIndex]].Image;
            }

            // otherwise, retrieve the image of the current frame index number
            else image = ImageFactory.CreateImage(ImagePrefix + (frameIndex + 1));
            
            return image;
        }

        /** 
        * Overridden to represent this frame-sequence by its image-prefix.
        */
        public override string ToString()
        {
            return "frame sequence: " + ImagePrefix;
        }

        [Implementation]
        public IEnumerator GetEnumerator()
        {
            return frames.GetEnumerator();
        }

        [Implementation]
        public void SetDurationOfAllFrames(int duration)
        {
            foreach (var frame in frames) frame.Duration = duration;

            // if there is a mirror sequence, 
            // set the duration on its frames, as well
            if (Mirror != null) Mirror.SetDurationOfAllFrames(duration);
        }

        [Implementation]
        public IFrameSequence ThisOrMirror(bool mirror)
        {
            return mirror ? Mirror : this;
        }

        [Implementation]
        public bool IsSameOrMirror(IFrameSequence other)
        {
            return other != null 
                && (other == this
                    || (other.Mirror != null && other.Mirror == this)
                    || (Mirror != null && other == Mirror));
        }
    }
}