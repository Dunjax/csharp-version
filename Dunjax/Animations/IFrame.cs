using Dunjax.Images;

namespace Dunjax.Animations
{
    /**
     * One frame in an animation's frame-sequence.
     */
    interface IFrame
    {
        /**
         * Returns the image to display for this frame.  
         * Some frames display no image, so this can be null!
         */
        IImage Image { get; }

        /**
         * How long (in ms) this frame should be displayed before 
         * moving on to the next.
         */
        int Duration { get; set; }
    }
}