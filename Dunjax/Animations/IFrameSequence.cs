using System.Collections;

namespace Dunjax.Animations
{
    /**
     * A timed sequence of frames through which an animation may progress.
     */
    interface IFrameSequence : IEnumerable
    {
        /**
         * Returns the filename which should start of the name of each of the
         * frames in this sequence.
         */
        string ImagePrefix { get; }

        /**
         * Returns how many frames there are in this sequence.
         */
        int NumFrames { get; }

        /**
         * Returns the frame of the given index within this sequence.
         */
        IFrame this [int index] { get; }

        /**
         * Returns this sequence's priority for display.  Once set into an 
         * animation, this sequence may not be replaced by one with a lesser
         * priority until it finishes. 
         */
        FramePriority Priority { get; }

        /**
         * Returns whether this frame sequence should start over at its beginning 
         * once it has completed.
         */
        bool Repeating { get; }

        /**
         * Returns this sequence's x-wise mirror (if it has one).
         */
        IFrameSequence Mirror { get; }

        /**
         * Returns either this sequence or its mirror, depending on the given parameter.
         */
        IFrameSequence ThisOrMirror(bool mirror);

        /**
         * Returns whether the given sequence is the same as, or the mirror of, this one.
         */
        bool IsSameOrMirror(IFrameSequence other);

        /**
         * Sets the duration of all the frames in this sequence to the value given.
         */
        void SetDurationOfAllFrames(int duration);
    }

    /**
     * The different priorities, ordered by increasing priority.
     */
    enum FramePriority { Normal, BeingHit, BeingWebbed, BeingDeath };
}