using Dunjax.Images;

namespace Dunjax.Animations
{
    /**
     * Manages timed progressions through sequences of frames.
     */
    interface IAnimation
    {
        /**
         * Has this animation check to see if it should advance to the next frame 
         * in its sequence, and do so if it is indeed time.
         *
         * @return  Whether the animation advanced.
         */
        bool CheckForAdvance();

        /**
         * The current frame-sequence being displayed for this animation. 
         */
        IFrameSequence FrameSequence { get; }

        /**
         * Sets the sequence of frames through which this animation should progress.
         * If the given sequence is already in progress within this animation,
         * or the current sequence isn't done and has a higher priority that
         * the one given, no changes are made
         * 
         * @return  Whether the given sequence has become that displayed for
         *          this animation. 
         */
        bool SetFrameSequence(IFrameSequence sequence);

        /**
         * How long (in ms) this animation's current frame is supposed to 
         * be shown.
         */
        int CurrentFrameDuration { get; }

        /**
         * The index within this animation's frame-sequence of the image
         * it is currently displaying. 
         */
        int CurrentFrameIndex { get; }

        /**
         * The current image to be displayed for this animation.
         */
        IImage CurrentImage { get; }

        /**
         * Whether the progression through this animation's current frame 
         * sequence has been completed.
         */
        bool Done { get; }

        /**
         * Has this animation restart playing its current frame sequence from the
         * beginning.
         */
        void Restart();

        /**
         * Whether this animation should be removed from the entity it is 
         * depicting, once it has finished.
         */
        bool RemoveWhenDone { get; set; }
    }
}